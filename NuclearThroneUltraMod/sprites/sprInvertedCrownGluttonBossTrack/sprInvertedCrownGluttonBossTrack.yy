{
  "$GMSprite":"",
  "%Name":"sprInvertedCrownGluttonBossTrack",
  "bboxMode":1,
  "bbox_bottom":63,
  "bbox_left":0,
  "bbox_right":47,
  "bbox_top":0,
  "collisionKind":0,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"cc2a15f0-75b4-4492-9b75-f783e24434ee","name":"cc2a15f0-75b4-4492-9b75-f783e24434ee","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"eb2a639a-6394-45c2-8fdf-d3182b97544f","name":"eb2a639a-6394-45c2-8fdf-d3182b97544f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"3fc515cf-4554-4fa5-8af6-f2019b05d9ae","name":"3fc515cf-4554-4fa5-8af6-f2019b05d9ae","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"be01e4da-0931-402b-bbbc-12f6bc2b341e","name":"be01e4da-0931-402b-bbbc-12f6bc2b341e","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"4e9fcb40-6586-4279-902e-055b4de296ce","name":"4e9fcb40-6586-4279-902e-055b4de296ce","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"b6dabed5-ea19-4b9f-8555-6c6e692dd623","name":"b6dabed5-ea19-4b9f-8555-6c6e692dd623","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"5fd0dd25-ef63-4667-aa46-751902e82ee5","name":"5fd0dd25-ef63-4667-aa46-751902e82ee5","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"3ecdce8f-eb2d-4c4e-bdd7-c0ed28967545","name":"3ecdce8f-eb2d-4c4e-bdd7-c0ed28967545","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"4e0ad921-a3db-44cb-91e8-08f5d455f24d","name":"4e0ad921-a3db-44cb-91e8-08f5d455f24d","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"4e57e534-5949-4788-962f-9aae72de56d6","name":"4e57e534-5949-4788-962f-9aae72de56d6","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"3231c7d6-4e8f-4bf5-8f0d-84ffde178c23","name":"3231c7d6-4e8f-4bf5-8f0d-84ffde178c23","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"61e530f3-7dff-4d02-9c53-e00b3707241a","name":"61e530f3-7dff-4d02-9c53-e00b3707241a","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"904f6f8a-b2d3-4480-b12a-97512bb894c2","name":"904f6f8a-b2d3-4480-b12a-97512bb894c2","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"c94c7b38-7016-43be-9dbf-ae20e52a2d6c","name":"c94c7b38-7016-43be-9dbf-ae20e52a2d6c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"0fff9fb3-2b84-4d10-94e8-ee8fa61f47b9","name":"0fff9fb3-2b84-4d10-94e8-ee8fa61f47b9","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"125b0536-4e1d-42cf-b461-fcb03c62178e","name":"125b0536-4e1d-42cf-b461-fcb03c62178e","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"91af0eca-506e-461f-b355-932eecd26c63","name":"91af0eca-506e-461f-b355-932eecd26c63","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":64,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"c86ebf18-014e-4ee1-bb15-ad39b79c7392","blendMode":0,"displayName":"default","isLocked":false,"name":"c86ebf18-014e-4ee1-bb15-ad39b79c7392","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprInvertedCrownGluttonBossTrack",
  "nineSlice":null,
  "origin":9,
  "parent":{
    "name":"Crown",
    "path":"folders/Sprites/Enemies/Crown.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprInvertedCrownGluttonBossTrack",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":17.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprInvertedCrownGluttonBossTrack",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"cc2a15f0-75b4-4492-9b75-f783e24434ee","path":"sprites/sprInvertedCrownGluttonBossTrack/sprInvertedCrownGluttonBossTrack.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"5a465560-d497-4292-8a22-14f9f620f172","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"eb2a639a-6394-45c2-8fdf-d3182b97544f","path":"sprites/sprInvertedCrownGluttonBossTrack/sprInvertedCrownGluttonBossTrack.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"181c3721-9fd3-41db-b0e6-f6b53c5d4eda","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"3fc515cf-4554-4fa5-8af6-f2019b05d9ae","path":"sprites/sprInvertedCrownGluttonBossTrack/sprInvertedCrownGluttonBossTrack.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"fde6104f-7f3c-4a33-9c2b-23ee2e2d7a0f","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"be01e4da-0931-402b-bbbc-12f6bc2b341e","path":"sprites/sprInvertedCrownGluttonBossTrack/sprInvertedCrownGluttonBossTrack.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"736ebfa1-0ecd-4076-9703-ebe76a09abdb","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"4e9fcb40-6586-4279-902e-055b4de296ce","path":"sprites/sprInvertedCrownGluttonBossTrack/sprInvertedCrownGluttonBossTrack.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"75f45505-5657-40b5-9f28-2e41317cc5a2","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"b6dabed5-ea19-4b9f-8555-6c6e692dd623","path":"sprites/sprInvertedCrownGluttonBossTrack/sprInvertedCrownGluttonBossTrack.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"e24737aa-8ac8-4146-838f-8dd4718ea84b","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"5fd0dd25-ef63-4667-aa46-751902e82ee5","path":"sprites/sprInvertedCrownGluttonBossTrack/sprInvertedCrownGluttonBossTrack.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"41eeb063-528e-4d2a-a5d9-11495e4ae604","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"3ecdce8f-eb2d-4c4e-bdd7-c0ed28967545","path":"sprites/sprInvertedCrownGluttonBossTrack/sprInvertedCrownGluttonBossTrack.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"65356be4-6b06-4be3-b4c6-d9307929e4e6","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"4e0ad921-a3db-44cb-91e8-08f5d455f24d","path":"sprites/sprInvertedCrownGluttonBossTrack/sprInvertedCrownGluttonBossTrack.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"88b4c374-b875-41d0-8178-4d7a4fa44bfc","IsCreationKey":false,"Key":8.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"4e57e534-5949-4788-962f-9aae72de56d6","path":"sprites/sprInvertedCrownGluttonBossTrack/sprInvertedCrownGluttonBossTrack.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"2cace650-d25d-446d-900c-b0107965ea32","IsCreationKey":false,"Key":9.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"3231c7d6-4e8f-4bf5-8f0d-84ffde178c23","path":"sprites/sprInvertedCrownGluttonBossTrack/sprInvertedCrownGluttonBossTrack.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"d6cfe250-2d5c-46d7-b2ed-59896499d374","IsCreationKey":false,"Key":10.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"61e530f3-7dff-4d02-9c53-e00b3707241a","path":"sprites/sprInvertedCrownGluttonBossTrack/sprInvertedCrownGluttonBossTrack.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"3a38a663-d4ae-45ea-afd0-4469646824d0","IsCreationKey":false,"Key":11.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"904f6f8a-b2d3-4480-b12a-97512bb894c2","path":"sprites/sprInvertedCrownGluttonBossTrack/sprInvertedCrownGluttonBossTrack.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"27552a29-1dcf-458c-8218-04501a0341c6","IsCreationKey":false,"Key":12.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c94c7b38-7016-43be-9dbf-ae20e52a2d6c","path":"sprites/sprInvertedCrownGluttonBossTrack/sprInvertedCrownGluttonBossTrack.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"30251367-b71f-43e5-8551-a4322c793cec","IsCreationKey":false,"Key":13.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"0fff9fb3-2b84-4d10-94e8-ee8fa61f47b9","path":"sprites/sprInvertedCrownGluttonBossTrack/sprInvertedCrownGluttonBossTrack.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ffaf2686-65c8-45ee-8bc6-13c6d9cf562e","IsCreationKey":false,"Key":14.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"125b0536-4e1d-42cf-b461-fcb03c62178e","path":"sprites/sprInvertedCrownGluttonBossTrack/sprInvertedCrownGluttonBossTrack.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"462532e5-ea64-4fad-95e3-44ed882cd16f","IsCreationKey":false,"Key":15.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"91af0eca-506e-461f-b355-932eecd26c63","path":"sprites/sprInvertedCrownGluttonBossTrack/sprInvertedCrownGluttonBossTrack.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"8a0356a3-787d-47ed-830d-71d8961910aa","IsCreationKey":false,"Key":16.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":24,
    "yorigin":48,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"CrownVault",
    "path":"texturegroups/CrownVault",
  },
  "type":0,
  "VTile":false,
  "width":48,
}