{
  "$GMSprite":"",
  "%Name":"sprWall10Top",
  "bboxMode":1,
  "bbox_bottom":15,
  "bbox_left":0,
  "bbox_right":15,
  "bbox_top":0,
  "collisionKind":0,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"a6dfd0ca-9059-4560-ab77-284b6eebf738","name":"a6dfd0ca-9059-4560-ab77-284b6eebf738","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"cc72f49e-2f77-4f16-9f81-62cfd18e1efe","name":"cc72f49e-2f77-4f16-9f81-62cfd18e1efe","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"53b946dd-b97d-4dcd-a993-efe3a5899ebe","name":"53b946dd-b97d-4dcd-a993-efe3a5899ebe","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"bb2ed57c-c443-436c-aa73-91a58aa46fb2","name":"bb2ed57c-c443-436c-aa73-91a58aa46fb2","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"1b280f7d-e3a4-40e7-a633-8aec51413691","name":"1b280f7d-e3a4-40e7-a633-8aec51413691","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"4466072b-c2d1-4c35-9e14-e6fe14ff8d7d","name":"4466072b-c2d1-4c35-9e14-e6fe14ff8d7d","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"4f709604-ee36-42b6-a266-75ab0105942a","name":"4f709604-ee36-42b6-a266-75ab0105942a","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"55561c89-9d2a-4a1c-9dbe-2d69c2164d47","name":"55561c89-9d2a-4a1c-9dbe-2d69c2164d47","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":16,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"492f2abb-28ba-4f9c-b301-fd3271ac6195","blendMode":0,"displayName":"default","isLocked":false,"name":"492f2abb-28ba-4f9c-b301-fd3271ac6195","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprWall10Top",
  "nineSlice":null,
  "origin":0,
  "parent":{
    "name":"Savanna",
    "path":"folders/Sprites/Enviroment/Tiles/Savanna.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprWall10Top",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":8.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprWall10Top",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"a6dfd0ca-9059-4560-ab77-284b6eebf738","path":"sprites/sprWall10Top/sprWall10Top.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"1930f6eb-b128-4cce-9f96-d3a073d0fe20","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"cc72f49e-2f77-4f16-9f81-62cfd18e1efe","path":"sprites/sprWall10Top/sprWall10Top.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"a5009118-55cb-4d3f-a0b7-2383ec757ca4","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"53b946dd-b97d-4dcd-a993-efe3a5899ebe","path":"sprites/sprWall10Top/sprWall10Top.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"2fc5af36-1cbb-457a-91d6-815d35e59b79","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"bb2ed57c-c443-436c-aa73-91a58aa46fb2","path":"sprites/sprWall10Top/sprWall10Top.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"23f3cf5d-7d70-4be1-ae68-f26500b13547","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"1b280f7d-e3a4-40e7-a633-8aec51413691","path":"sprites/sprWall10Top/sprWall10Top.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"7d219d6a-1713-425c-9af7-07dffcb1c53d","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"4466072b-c2d1-4c35-9e14-e6fe14ff8d7d","path":"sprites/sprWall10Top/sprWall10Top.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"2c48a19c-b5ce-4844-b8f2-eb38081d1132","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"4f709604-ee36-42b6-a266-75ab0105942a","path":"sprites/sprWall10Top/sprWall10Top.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"262f9dba-5873-4062-96ec-2ff57cd68fb3","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"55561c89-9d2a-4a1c-9dbe-2d69c2164d47","path":"sprites/sprWall10Top/sprWall10Top.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"01c1b8ad-5cea-4750-858d-ef9a92febcec","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":0,
    "yorigin":0,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Savanna",
    "path":"texturegroups/Savanna",
  },
  "type":0,
  "VTile":false,
  "width":16,
}