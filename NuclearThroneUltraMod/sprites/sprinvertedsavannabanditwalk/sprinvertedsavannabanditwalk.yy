{
  "$GMSprite":"",
  "%Name":"sprInvertedSavannaBanditWalk",
  "bboxMode":1,
  "bbox_bottom":23,
  "bbox_left":0,
  "bbox_right":23,
  "bbox_top":0,
  "collisionKind":0,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"88a7005a-371a-4b85-a115-d94d21a95f88","name":"88a7005a-371a-4b85-a115-d94d21a95f88","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"bc02299e-7ff6-42af-a555-b1b7f838b732","name":"bc02299e-7ff6-42af-a555-b1b7f838b732","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"01326c69-040c-4dd8-b0cc-7e54ebe4c95f","name":"01326c69-040c-4dd8-b0cc-7e54ebe4c95f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"68114a7e-dd26-4fc2-8b1f-7a0c22c90421","name":"68114a7e-dd26-4fc2-8b1f-7a0c22c90421","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"42d2cf58-bfdb-48fd-ade8-ef27f4662a73","name":"42d2cf58-bfdb-48fd-ade8-ef27f4662a73","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"ea86e3f3-c08e-4f5a-ba1e-0921c472f1af","name":"ea86e3f3-c08e-4f5a-ba1e-0921c472f1af","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":24,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"f7b3f977-f48b-4cc7-8c7e-349db2815390","blendMode":0,"displayName":"default","isLocked":false,"name":"f7b3f977-f48b-4cc7-8c7e-349db2815390","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprInvertedSavannaBanditWalk",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"SavannaBandit",
    "path":"folders/Sprites/Enemies/Bandit/SavannaBandit.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprInvertedSavannaBanditWalk",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":6.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprInvertedSavannaBanditWalk",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"88a7005a-371a-4b85-a115-d94d21a95f88","path":"sprites/sprInvertedSavannaBanditWalk/sprInvertedSavannaBanditWalk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"d4edbfbe-6dee-49d1-a3c1-a366ce121b9f","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"bc02299e-7ff6-42af-a555-b1b7f838b732","path":"sprites/sprInvertedSavannaBanditWalk/sprInvertedSavannaBanditWalk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"d0949622-3998-42bc-9881-f25f2b500ec0","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"01326c69-040c-4dd8-b0cc-7e54ebe4c95f","path":"sprites/sprInvertedSavannaBanditWalk/sprInvertedSavannaBanditWalk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"3d98ec40-f697-4a14-a4ac-1b7ba8990029","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"68114a7e-dd26-4fc2-8b1f-7a0c22c90421","path":"sprites/sprInvertedSavannaBanditWalk/sprInvertedSavannaBanditWalk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"68b3d14a-0440-4842-8e20-bcb0afd7aa98","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"42d2cf58-bfdb-48fd-ade8-ef27f4662a73","path":"sprites/sprInvertedSavannaBanditWalk/sprInvertedSavannaBanditWalk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"2bf5f2fa-a43d-46c7-92bb-a06b42665f53","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"ea86e3f3-c08e-4f5a-ba1e-0921c472f1af","path":"sprites/sprInvertedSavannaBanditWalk/sprInvertedSavannaBanditWalk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"a7672da1-de85-4518-a2f4-0b30e4949b9f","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":12,
    "yorigin":12,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"AnywhereEnemy",
    "path":"texturegroups/AnywhereEnemy",
  },
  "type":0,
  "VTile":false,
  "width":24,
}