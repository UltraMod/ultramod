{
  "$GMSprite":"",
  "%Name":"sprMutant10CDead",
  "bboxMode":1,
  "bbox_bottom":17,
  "bbox_left":0,
  "bbox_right":21,
  "bbox_top":0,
  "collisionKind":0,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"479a9415-8714-4155-99cf-ab205689d85e","name":"479a9415-8714-4155-99cf-ab205689d85e","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"ebf3ae1b-d504-411a-8c7b-0b31bb016393","name":"ebf3ae1b-d504-411a-8c7b-0b31bb016393","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"a662c441-4d2d-41d0-83fb-cacdad8db923","name":"a662c441-4d2d-41d0-83fb-cacdad8db923","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"d9dab67c-329f-4b38-a25f-f800301f49ec","name":"d9dab67c-329f-4b38-a25f-f800301f49ec","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"2cfc45e9-994c-4c44-bb32-f8e5988bfed2","name":"2cfc45e9-994c-4c44-bb32-f8e5988bfed2","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"efa9f984-14c4-4887-b52f-5a64031f2642","name":"efa9f984-14c4-4887-b52f-5a64031f2642","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"239b1144-21ed-484a-892d-dcda553c3d71","name":"239b1144-21ed-484a-892d-dcda553c3d71","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":18,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"f5958397-bcd4-481e-a16b-0ccfe88a3219","blendMode":0,"displayName":"default","isLocked":false,"name":"f5958397-bcd4-481e-a16b-0ccfe88a3219","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprMutant10CDead",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"RebelC",
    "path":"folders/Sprites/Player/Rebel/RebelC.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprMutant10CDead",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":7.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprMutant10CDead",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"479a9415-8714-4155-99cf-ab205689d85e","path":"sprites/sprMutant10CDead/sprMutant10CDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"07c3afe5-8b45-4dff-9326-0e347323a98c","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"ebf3ae1b-d504-411a-8c7b-0b31bb016393","path":"sprites/sprMutant10CDead/sprMutant10CDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f359a414-a371-4562-988c-6e214b2b1fe3","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"a662c441-4d2d-41d0-83fb-cacdad8db923","path":"sprites/sprMutant10CDead/sprMutant10CDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"0d287c20-04bf-4ce2-9ca6-532b73eee4ea","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"d9dab67c-329f-4b38-a25f-f800301f49ec","path":"sprites/sprMutant10CDead/sprMutant10CDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"32340572-e74a-471d-86e9-0721fc13e0cc","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"2cfc45e9-994c-4c44-bb32-f8e5988bfed2","path":"sprites/sprMutant10CDead/sprMutant10CDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ee705f97-1e86-47f5-b29a-3a38994c1a95","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"efa9f984-14c4-4887-b52f-5a64031f2642","path":"sprites/sprMutant10CDead/sprMutant10CDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"763b28d9-00c6-4078-b752-f39a61973692","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"239b1144-21ed-484a-892d-dcda553c3d71","path":"sprites/sprMutant10CDead/sprMutant10CDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"30b34bfa-38f5-4f3e-b485-c1aa0833bc1e","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":{
      "x":0.0,
      "y":0.0,
    },
    "volume":1.0,
    "xorigin":11,
    "yorigin":9,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"PlayerCharacter",
    "path":"texturegroups/PlayerCharacter",
  },
  "type":0,
  "VTile":false,
  "width":22,
}