{
  "$GMSprite":"",
  "%Name":"sprDoubleFlameShotgun",
  "bboxMode":0,
  "bbox_bottom":7,
  "bbox_left":1,
  "bbox_right":19,
  "bbox_top":1,
  "collisionKind":0,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"907c80b8-3f54-43bb-b088-cbc3daa33f65","name":"907c80b8-3f54-43bb-b088-cbc3daa33f65","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"2830d70e-56fd-4851-ad31-348816ce9c92","name":"2830d70e-56fd-4851-ad31-348816ce9c92","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"0d2519f7-a2ed-4834-b143-415efd1a99d3","name":"0d2519f7-a2ed-4834-b143-415efd1a99d3","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"306545bc-3193-4834-91bc-af591c3d96c6","name":"306545bc-3193-4834-91bc-af591c3d96c6","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"d3e1e0ed-51f3-4d3d-920d-35ee24db72ba","name":"d3e1e0ed-51f3-4d3d-920d-35ee24db72ba","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"00a9f92b-6894-4b66-9927-cbf65cc85476","name":"00a9f92b-6894-4b66-9927-cbf65cc85476","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"c42a43e9-dfb0-410b-92d6-fa226a99d230","name":"c42a43e9-dfb0-410b-92d6-fa226a99d230","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":9,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"c6e03466-1dad-43e4-ad4d-3629481c387c","blendMode":0,"displayName":"default","isLocked":false,"name":"c6e03466-1dad-43e4-ad4d-3629481c387c","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprDoubleFlameShotgun",
  "nineSlice":null,
  "origin":9,
  "parent":{
    "name":"Remade",
    "path":"folders/Sprites/Weapons/Existing/Remade.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":7.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"907c80b8-3f54-43bb-b088-cbc3daa33f65","path":"sprites/sprDoubleFlameShotgun/sprDoubleFlameShotgun.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"11c68b21-1c46-4b6e-9c74-ea19759389b8","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"2830d70e-56fd-4851-ad31-348816ce9c92","path":"sprites/sprDoubleFlameShotgun/sprDoubleFlameShotgun.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"bb5ed8a9-8230-4fe9-bba7-5c31d22ee099","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"0d2519f7-a2ed-4834-b143-415efd1a99d3","path":"sprites/sprDoubleFlameShotgun/sprDoubleFlameShotgun.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"8d680e75-79f2-40b1-a251-c1495a342a3f","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"306545bc-3193-4834-91bc-af591c3d96c6","path":"sprites/sprDoubleFlameShotgun/sprDoubleFlameShotgun.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"1bc5ae1d-6dc4-4f61-8ddd-0579b5d43c13","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"d3e1e0ed-51f3-4d3d-920d-35ee24db72ba","path":"sprites/sprDoubleFlameShotgun/sprDoubleFlameShotgun.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"1fca5ddd-9dcb-4e30-b027-9254c3f1d900","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"00a9f92b-6894-4b66-9927-cbf65cc85476","path":"sprites/sprDoubleFlameShotgun/sprDoubleFlameShotgun.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"8b0a45be-e2a3-487e-a6f0-1214d8d0efe4","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c42a43e9-dfb0-410b-92d6-fa226a99d230","path":"sprites/sprDoubleFlameShotgun/sprDoubleFlameShotgun.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"1db4f0ad-a230-43de-9103-7622177f2132","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":3,
    "yorigin":2,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Default",
    "path":"texturegroups/Default",
  },
  "type":0,
  "VTile":false,
  "width":21,
}