{
  "$GMSprite":"",
  "%Name":"sprCharSelectCompleted",
  "bboxMode":1,
  "bbox_bottom":33,
  "bbox_left":0,
  "bbox_right":19,
  "bbox_top":0,
  "collisionKind":0,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"3fad964e-9c65-4df9-9328-bdf3081b2c1e","name":"3fad964e-9c65-4df9-9328-bdf3081b2c1e","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"91741722-1b7b-4585-a357-216c7cb4c60e","name":"91741722-1b7b-4585-a357-216c7cb4c60e","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"f82a0ae2-7f42-4451-8356-238c947645a7","name":"f82a0ae2-7f42-4451-8356-238c947645a7","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"1a2b0796-119b-4a29-9e51-6306fbfa158f","name":"1a2b0796-119b-4a29-9e51-6306fbfa158f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"addc67d8-c178-422b-b2c2-27199ab4dca1","name":"addc67d8-c178-422b-b2c2-27199ab4dca1","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"32b6cc1c-c748-4e59-9e51-03a2ca169e35","name":"32b6cc1c-c748-4e59-9e51-03a2ca169e35","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"19021b3e-32cb-43f6-948f-d715b77cda0e","name":"19021b3e-32cb-43f6-948f-d715b77cda0e","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"7a78a7d6-e02b-4127-84bf-98ae3bb419cc","name":"7a78a7d6-e02b-4127-84bf-98ae3bb419cc","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":34,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"5464e73f-70e3-465c-9ab3-5a4e33b22cd3","blendMode":0,"displayName":"default","isLocked":false,"name":"5464e73f-70e3-465c-9ab3-5a4e33b22cd3","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprCharSelectCompleted",
  "nineSlice":null,
  "origin":9,
  "parent":{
    "name":"Menu",
    "path":"folders/Sprites/Menu.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprCharSelectCompleted",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":8.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprCharSelectCompleted",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"3fad964e-9c65-4df9-9328-bdf3081b2c1e","path":"sprites/sprCharSelectCompleted/sprCharSelectCompleted.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"dafb1aa4-4530-44c4-857d-e729e5716294","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"91741722-1b7b-4585-a357-216c7cb4c60e","path":"sprites/sprCharSelectCompleted/sprCharSelectCompleted.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"1cb0178e-c979-4a1f-b3c4-1b7f6b6ffec6","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"f82a0ae2-7f42-4451-8356-238c947645a7","path":"sprites/sprCharSelectCompleted/sprCharSelectCompleted.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ecaf3d59-9128-42dd-899d-2e597062ce11","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"1a2b0796-119b-4a29-9e51-6306fbfa158f","path":"sprites/sprCharSelectCompleted/sprCharSelectCompleted.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"a4dd9423-cf43-4ca3-bc5a-245f0be48a5a","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"addc67d8-c178-422b-b2c2-27199ab4dca1","path":"sprites/sprCharSelectCompleted/sprCharSelectCompleted.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"082ae951-07f2-4c7a-a16f-1492fb0df423","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"32b6cc1c-c748-4e59-9e51-03a2ca169e35","path":"sprites/sprCharSelectCompleted/sprCharSelectCompleted.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"c2f0fd96-5770-45ec-b6dc-8692fd0eee45","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"19021b3e-32cb-43f6-948f-d715b77cda0e","path":"sprites/sprCharSelectCompleted/sprCharSelectCompleted.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"01bb40f8-cf7b-43ae-9e01-07ff3e2960a8","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"7a78a7d6-e02b-4127-84bf-98ae3bb419cc","path":"sprites/sprCharSelectCompleted/sprCharSelectCompleted.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"1d87a801-2e02-4cac-a907-1d7f263225cc","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":2,
    "yorigin":11,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"MainMenu",
    "path":"texturegroups/MainMenu",
  },
  "type":0,
  "VTile":false,
  "width":20,
}