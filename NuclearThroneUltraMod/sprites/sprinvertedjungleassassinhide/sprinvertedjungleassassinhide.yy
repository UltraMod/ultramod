{
  "$GMSprite":"",
  "%Name":"sprInvertedJungleAssassinHide",
  "bboxMode":0,
  "bbox_bottom":24,
  "bbox_left":5,
  "bbox_right":26,
  "bbox_top":7,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"3f437725-6198-4cff-b602-fe8143f5f449","name":"3f437725-6198-4cff-b602-fe8143f5f449","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"3403900f-cf5d-401a-8599-96f5dfadaf59","name":"3403900f-cf5d-401a-8599-96f5dfadaf59","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"989193e3-d82a-460c-b072-a7c81f581540","name":"989193e3-d82a-460c-b072-a7c81f581540","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"15920604-55ae-49ab-b56b-c59db3784633","name":"15920604-55ae-49ab-b56b-c59db3784633","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"1bdab43c-3577-4c5c-9c9d-4ae82b1325b3","name":"1bdab43c-3577-4c5c-9c9d-4ae82b1325b3","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"aed3fdd8-7c4b-4347-9486-fdec6ee4d7c3","name":"aed3fdd8-7c4b-4347-9486-fdec6ee4d7c3","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"b310e292-372f-4509-ac1a-4483fd7e6796","name":"b310e292-372f-4509-ac1a-4483fd7e6796","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"1da1313d-33f9-4bdc-8008-9ab5a538af1a","name":"1da1313d-33f9-4bdc-8008-9ab5a538af1a","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"2da8a8c2-6cbf-4896-a033-d2e39f03ecc9","name":"2da8a8c2-6cbf-4896-a033-d2e39f03ecc9","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"120879e6-1738-4da0-924c-2610a96a419a","name":"120879e6-1738-4da0-924c-2610a96a419a","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"4ab89dbc-ae1f-43ff-802d-e1a3b7036542","name":"4ab89dbc-ae1f-43ff-802d-e1a3b7036542","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"c178e8aa-3fbe-477a-994a-6f3e388818da","name":"c178e8aa-3fbe-477a-994a-6f3e388818da","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"28e13a7e-c185-4a91-97fe-9ca4b1566860","name":"28e13a7e-c185-4a91-97fe-9ca4b1566860","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"90c32013-9c52-46ca-8e12-93b5c933dc22","name":"90c32013-9c52-46ca-8e12-93b5c933dc22","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"fa64daa9-c4f7-43fb-b63e-52fe28525815","name":"fa64daa9-c4f7-43fb-b63e-52fe28525815","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"2f6486c9-c83c-48a6-b58c-b08a088133c5","name":"2f6486c9-c83c-48a6-b58c-b08a088133c5","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"ee61eb28-e48f-4241-8318-25db81255872","name":"ee61eb28-e48f-4241-8318-25db81255872","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"75fcbb28-8344-48e5-8fd9-f6dfd1c34a48","name":"75fcbb28-8344-48e5-8fd9-f6dfd1c34a48","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"292725c1-de0e-4b04-8bc9-61127f0af937","name":"292725c1-de0e-4b04-8bc9-61127f0af937","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"0a50bf9c-3268-4482-af68-53bc12cc1354","name":"0a50bf9c-3268-4482-af68-53bc12cc1354","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"21094236-acd5-4f49-87c6-8e76149591e9","name":"21094236-acd5-4f49-87c6-8e76149591e9","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"fcd09450-a991-4baf-8889-c59880050468","name":"fcd09450-a991-4baf-8889-c59880050468","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"f0f0ab7e-96c1-4113-9a3a-ed8901cbb3d9","name":"f0f0ab7e-96c1-4113-9a3a-ed8901cbb3d9","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"857a6ffb-17be-424b-babc-07ae15342d8a","name":"857a6ffb-17be-424b-babc-07ae15342d8a","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"969de047-4900-4ebd-a18b-b0459cf978da","name":"969de047-4900-4ebd-a18b-b0459cf978da","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"47e28c35-439e-4edc-a9ad-c991ad81501e","name":"47e28c35-439e-4edc-a9ad-c991ad81501e","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"ca2d2220-8d2e-4c75-853e-97a792a1b6bb","name":"ca2d2220-8d2e-4c75-853e-97a792a1b6bb","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"c3539c75-0ae4-4de2-9ce5-8f68580a9e2f","name":"c3539c75-0ae4-4de2-9ce5-8f68580a9e2f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"db52507c-0654-4ae3-b867-ea1c67d94d58","name":"db52507c-0654-4ae3-b867-ea1c67d94d58","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"f280a3f3-7225-4f0f-ad21-5fd95e960117","name":"f280a3f3-7225-4f0f-ad21-5fd95e960117","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"1087a041-7fea-4669-ba03-04c39356f249","name":"1087a041-7fea-4669-ba03-04c39356f249","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"0eda728f-d5d2-4292-8f46-f752bbb6e949","name":"0eda728f-d5d2-4292-8f46-f752bbb6e949","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"87291987-3865-4022-8791-09188165ff4f","name":"87291987-3865-4022-8791-09188165ff4f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"533a8cdd-1340-4e68-a433-4f8052caabbc","name":"533a8cdd-1340-4e68-a433-4f8052caabbc","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"cad8dcd8-c291-4106-802a-d5ecc2b2ddcd","name":"cad8dcd8-c291-4106-802a-d5ecc2b2ddcd","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"1a5af2e5-2696-4c64-b2e0-f53d772d84df","name":"1a5af2e5-2696-4c64-b2e0-f53d772d84df","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"5d78dbf2-b814-4352-8431-ebb8ac49e2dd","name":"5d78dbf2-b814-4352-8431-ebb8ac49e2dd","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"7bec9b02-d303-4a51-8e44-1a695cbef63d","name":"7bec9b02-d303-4a51-8e44-1a695cbef63d","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"02418f78-7abe-49ab-aef8-ee0997a9e69e","name":"02418f78-7abe-49ab-aef8-ee0997a9e69e","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"d2f7b692-5931-479e-9718-52928efbadd6","name":"d2f7b692-5931-479e-9718-52928efbadd6","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"ff557853-45a8-47e7-aea7-246c570059af","name":"ff557853-45a8-47e7-aea7-246c570059af","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":32,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"99191a18-e472-410f-a61e-7947eedae1a3","blendMode":0,"displayName":"default","isLocked":false,"name":"99191a18-e472-410f-a61e-7947eedae1a3","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprInvertedJungleAssassinHide",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"JungleEnemy",
    "path":"folders/Sprites/Enemies/JungleEnemy.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprInvertedJungleAssassinHide",
    "autoRecord":true,
    "backdropHeight":768,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1366,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":41.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprInvertedJungleAssassinHide",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"3f437725-6198-4cff-b602-fe8143f5f449","path":"sprites/sprInvertedJungleAssassinHide/sprInvertedJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f53bb54e-367c-47f6-8d8c-db60cd311523","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"3403900f-cf5d-401a-8599-96f5dfadaf59","path":"sprites/sprInvertedJungleAssassinHide/sprInvertedJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"42fbdb4e-5324-4755-9abc-c0864fc7af66","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"989193e3-d82a-460c-b072-a7c81f581540","path":"sprites/sprInvertedJungleAssassinHide/sprInvertedJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"081ba179-a86e-4155-89b3-d641da1b82f1","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"15920604-55ae-49ab-b56b-c59db3784633","path":"sprites/sprInvertedJungleAssassinHide/sprInvertedJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"e961c758-c49d-43f3-a77a-faf28c846bbe","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"1bdab43c-3577-4c5c-9c9d-4ae82b1325b3","path":"sprites/sprInvertedJungleAssassinHide/sprInvertedJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"5b87b2b4-9202-4a50-9c2d-f2ee84a56a3a","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"aed3fdd8-7c4b-4347-9486-fdec6ee4d7c3","path":"sprites/sprInvertedJungleAssassinHide/sprInvertedJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"7ddf4c5e-868a-4fbc-83b4-6ac59cde618a","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"b310e292-372f-4509-ac1a-4483fd7e6796","path":"sprites/sprInvertedJungleAssassinHide/sprInvertedJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"1918763a-a93b-43e1-9ce1-18153151beb3","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"1da1313d-33f9-4bdc-8008-9ab5a538af1a","path":"sprites/sprInvertedJungleAssassinHide/sprInvertedJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"50c8e498-7dd7-4579-a397-cf40307c1538","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"2da8a8c2-6cbf-4896-a033-d2e39f03ecc9","path":"sprites/sprInvertedJungleAssassinHide/sprInvertedJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"0a9b0dd3-28ba-42fd-93a9-2bfd86c2db2c","IsCreationKey":false,"Key":8.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"120879e6-1738-4da0-924c-2610a96a419a","path":"sprites/sprInvertedJungleAssassinHide/sprInvertedJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"50163e6a-9bf9-44b3-b131-bae61a9b270a","IsCreationKey":false,"Key":9.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"4ab89dbc-ae1f-43ff-802d-e1a3b7036542","path":"sprites/sprInvertedJungleAssassinHide/sprInvertedJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"7f7f1986-f337-46fd-9ed1-e7647e83bec1","IsCreationKey":false,"Key":10.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c178e8aa-3fbe-477a-994a-6f3e388818da","path":"sprites/sprInvertedJungleAssassinHide/sprInvertedJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"71d777be-068c-4c56-a674-7ebbd7f51b4b","IsCreationKey":false,"Key":11.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"28e13a7e-c185-4a91-97fe-9ca4b1566860","path":"sprites/sprInvertedJungleAssassinHide/sprInvertedJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"7e0ca4ff-3a30-40b2-9f67-1f51ee864af2","IsCreationKey":false,"Key":12.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"90c32013-9c52-46ca-8e12-93b5c933dc22","path":"sprites/sprInvertedJungleAssassinHide/sprInvertedJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"094c9885-2229-4756-bb2c-5a373df4877e","IsCreationKey":false,"Key":13.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"fa64daa9-c4f7-43fb-b63e-52fe28525815","path":"sprites/sprInvertedJungleAssassinHide/sprInvertedJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"213eba5f-9d15-453f-bd7a-3caccc4dcb65","IsCreationKey":false,"Key":14.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"2f6486c9-c83c-48a6-b58c-b08a088133c5","path":"sprites/sprInvertedJungleAssassinHide/sprInvertedJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"7fb7b081-4096-405f-804d-290f8e090dcd","IsCreationKey":false,"Key":15.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"ee61eb28-e48f-4241-8318-25db81255872","path":"sprites/sprInvertedJungleAssassinHide/sprInvertedJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ae826391-aad4-40d5-b203-86b7f13aced6","IsCreationKey":false,"Key":16.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"75fcbb28-8344-48e5-8fd9-f6dfd1c34a48","path":"sprites/sprInvertedJungleAssassinHide/sprInvertedJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"4bffe701-dd9a-4a59-b467-b9ce2f2118c9","IsCreationKey":false,"Key":17.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"292725c1-de0e-4b04-8bc9-61127f0af937","path":"sprites/sprInvertedJungleAssassinHide/sprInvertedJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"2fc3d668-8c86-4603-9506-a04e68d756ed","IsCreationKey":false,"Key":18.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"0a50bf9c-3268-4482-af68-53bc12cc1354","path":"sprites/sprInvertedJungleAssassinHide/sprInvertedJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"409f25ed-8efe-40b4-a4b7-a1b7c6b2cb47","IsCreationKey":false,"Key":19.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"21094236-acd5-4f49-87c6-8e76149591e9","path":"sprites/sprInvertedJungleAssassinHide/sprInvertedJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"b5836879-40f6-4c1c-b8f0-17def583a00c","IsCreationKey":false,"Key":20.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"fcd09450-a991-4baf-8889-c59880050468","path":"sprites/sprInvertedJungleAssassinHide/sprInvertedJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f9639b34-4f6b-41c0-8e40-8b7a975cabbe","IsCreationKey":false,"Key":21.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"f0f0ab7e-96c1-4113-9a3a-ed8901cbb3d9","path":"sprites/sprInvertedJungleAssassinHide/sprInvertedJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"dfb1f3cb-8895-4583-8caa-abf17e7253f5","IsCreationKey":false,"Key":22.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"857a6ffb-17be-424b-babc-07ae15342d8a","path":"sprites/sprInvertedJungleAssassinHide/sprInvertedJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"bf3bec08-2ad9-4878-9e9e-408a3fcb6043","IsCreationKey":false,"Key":23.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"969de047-4900-4ebd-a18b-b0459cf978da","path":"sprites/sprInvertedJungleAssassinHide/sprInvertedJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"fd6f9a14-8c43-454d-96a6-8c60e7bc5ad6","IsCreationKey":false,"Key":24.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"47e28c35-439e-4edc-a9ad-c991ad81501e","path":"sprites/sprInvertedJungleAssassinHide/sprInvertedJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"775852c4-31dc-4d1b-b8e4-9e65cf309258","IsCreationKey":false,"Key":25.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"ca2d2220-8d2e-4c75-853e-97a792a1b6bb","path":"sprites/sprInvertedJungleAssassinHide/sprInvertedJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"76fe7f55-849f-44fd-9485-ee984f827b70","IsCreationKey":false,"Key":26.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c3539c75-0ae4-4de2-9ce5-8f68580a9e2f","path":"sprites/sprInvertedJungleAssassinHide/sprInvertedJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"a1470089-b83c-4780-8bd0-a663d0a72951","IsCreationKey":false,"Key":27.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"db52507c-0654-4ae3-b867-ea1c67d94d58","path":"sprites/sprInvertedJungleAssassinHide/sprInvertedJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"911920c6-8671-47c3-b18f-6e88539b39be","IsCreationKey":false,"Key":28.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"f280a3f3-7225-4f0f-ad21-5fd95e960117","path":"sprites/sprInvertedJungleAssassinHide/sprInvertedJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f0df3416-6fb9-4523-8144-f04184e725c4","IsCreationKey":false,"Key":29.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"1087a041-7fea-4669-ba03-04c39356f249","path":"sprites/sprInvertedJungleAssassinHide/sprInvertedJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"defc0fb9-da71-42a7-baca-cf0f000f5439","IsCreationKey":false,"Key":30.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"0eda728f-d5d2-4292-8f46-f752bbb6e949","path":"sprites/sprInvertedJungleAssassinHide/sprInvertedJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ed0ac0fb-3349-4c30-8c12-8288230d8985","IsCreationKey":false,"Key":31.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"87291987-3865-4022-8791-09188165ff4f","path":"sprites/sprInvertedJungleAssassinHide/sprInvertedJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"4391da28-b078-4e59-93bf-d04e9479b008","IsCreationKey":false,"Key":32.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"533a8cdd-1340-4e68-a433-4f8052caabbc","path":"sprites/sprInvertedJungleAssassinHide/sprInvertedJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"cd9c9b22-4a0e-41b6-a640-2db6a9d87e47","IsCreationKey":false,"Key":33.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"cad8dcd8-c291-4106-802a-d5ecc2b2ddcd","path":"sprites/sprInvertedJungleAssassinHide/sprInvertedJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ec593028-b242-40e4-846e-3b177ffa53af","IsCreationKey":false,"Key":34.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"1a5af2e5-2696-4c64-b2e0-f53d772d84df","path":"sprites/sprInvertedJungleAssassinHide/sprInvertedJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"aaa14c77-93e0-44ff-a1c8-bd68402de3b2","IsCreationKey":false,"Key":35.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"5d78dbf2-b814-4352-8431-ebb8ac49e2dd","path":"sprites/sprInvertedJungleAssassinHide/sprInvertedJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f9794bf5-a646-46da-888d-80e734343163","IsCreationKey":false,"Key":36.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"7bec9b02-d303-4a51-8e44-1a695cbef63d","path":"sprites/sprInvertedJungleAssassinHide/sprInvertedJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"fa101bd7-ccb1-484a-a1c8-8e93241bd8e2","IsCreationKey":false,"Key":37.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"02418f78-7abe-49ab-aef8-ee0997a9e69e","path":"sprites/sprInvertedJungleAssassinHide/sprInvertedJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"5aa2dd0a-443c-4d08-8163-52348b4e88a8","IsCreationKey":false,"Key":38.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"d2f7b692-5931-479e-9718-52928efbadd6","path":"sprites/sprInvertedJungleAssassinHide/sprInvertedJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"df07bbf7-fee2-4ca7-808f-7cb00101434a","IsCreationKey":false,"Key":39.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"ff557853-45a8-47e7-aea7-246c570059af","path":"sprites/sprInvertedJungleAssassinHide/sprInvertedJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"2bdf89d2-c82b-49a5-a9c1-c457e3a56229","IsCreationKey":false,"Key":40.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":16,
    "yorigin":16,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Default",
    "path":"texturegroups/Default",
  },
  "type":0,
  "VTile":false,
  "width":32,
}