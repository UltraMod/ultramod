{
  "$GMSprite":"",
  "%Name":"sprGoldNecromancerWalk",
  "bboxMode":1,
  "bbox_bottom":31,
  "bbox_left":0,
  "bbox_right":31,
  "bbox_top":0,
  "collisionKind":0,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"d3e937b6-d56b-4ae9-a98b-36de4ed7937f","name":"d3e937b6-d56b-4ae9-a98b-36de4ed7937f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"f46df5be-fbd1-4a90-9190-e15f92b2235f","name":"f46df5be-fbd1-4a90-9190-e15f92b2235f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"34c46b97-2f88-426b-8e25-5dd21f513244","name":"34c46b97-2f88-426b-8e25-5dd21f513244","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"da92f5bd-6b89-4b96-a178-15a5a98d4054","name":"da92f5bd-6b89-4b96-a178-15a5a98d4054","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"683c8a2f-f666-4c4d-902a-64cf1fe0ec1f","name":"683c8a2f-f666-4c4d-902a-64cf1fe0ec1f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"a41f5a68-8adf-4b0d-bce4-a00fc8205d51","name":"a41f5a68-8adf-4b0d-bce4-a00fc8205d51","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":32,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"d66a313d-5391-4dab-8383-63e695e9dcaf","blendMode":0,"displayName":"default","isLocked":false,"name":"d66a313d-5391-4dab-8383-63e695e9dcaf","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprGoldNecromancerWalk",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"Labs",
    "path":"folders/Sprites/Enemies/Labs.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprGoldNecromancerWalk",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":6.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprGoldNecromancerWalk",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"d3e937b6-d56b-4ae9-a98b-36de4ed7937f","path":"sprites/sprGoldNecromancerWalk/sprGoldNecromancerWalk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"dc7f094d-72e2-4d15-855e-c99f5fd2d112","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"f46df5be-fbd1-4a90-9190-e15f92b2235f","path":"sprites/sprGoldNecromancerWalk/sprGoldNecromancerWalk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"53325c46-90f3-4c10-84cf-ea16a602af40","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"34c46b97-2f88-426b-8e25-5dd21f513244","path":"sprites/sprGoldNecromancerWalk/sprGoldNecromancerWalk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"91d4ee0a-8606-401a-a35f-667e911bb2e1","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"da92f5bd-6b89-4b96-a178-15a5a98d4054","path":"sprites/sprGoldNecromancerWalk/sprGoldNecromancerWalk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"39318588-4592-423f-9e15-5a1e18833188","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"683c8a2f-f666-4c4d-902a-64cf1fe0ec1f","path":"sprites/sprGoldNecromancerWalk/sprGoldNecromancerWalk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"4aeab3e4-dc75-4852-ad91-224e7c149e15","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"a41f5a68-8adf-4b0d-bce4-a00fc8205d51","path":"sprites/sprGoldNecromancerWalk/sprGoldNecromancerWalk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"cc7bcdf1-3834-4555-bce7-d019f7a06a6f","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":16,
    "yorigin":16,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Labs",
    "path":"texturegroups/Labs",
  },
  "type":0,
  "VTile":false,
  "width":32,
}