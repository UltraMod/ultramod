{
  "$GMSprite":"",
  "%Name":"sprAssassinBossDead",
  "bboxMode":0,
  "bbox_bottom":23,
  "bbox_left":1,
  "bbox_right":24,
  "bbox_top":4,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"691e749d-4334-44c7-ad11-2518bc4377c3","name":"691e749d-4334-44c7-ad11-2518bc4377c3","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"f830af5f-6a70-4455-96d4-73607aa59e0f","name":"f830af5f-6a70-4455-96d4-73607aa59e0f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"278b2500-bd13-4b57-8a9b-125b05d9d55c","name":"278b2500-bd13-4b57-8a9b-125b05d9d55c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"6129012a-092d-4543-84cd-b458f05521fb","name":"6129012a-092d-4543-84cd-b458f05521fb","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"fa885548-51ab-4079-b23e-4322133fc718","name":"fa885548-51ab-4079-b23e-4322133fc718","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"70cda76c-6335-428d-ae04-fccc5ff65731","name":"70cda76c-6335-428d-ae04-fccc5ff65731","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"e5399f5b-2ef2-4a82-8a8a-7da22840a169","name":"e5399f5b-2ef2-4a82-8a8a-7da22840a169","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"d7d72871-6bc6-4938-814b-52099261eaaa","name":"d7d72871-6bc6-4938-814b-52099261eaaa","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"76bc4dac-87e2-4921-8123-54b2f5b25a96","name":"76bc4dac-87e2-4921-8123-54b2f5b25a96","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"3e1491f5-fd73-4360-b210-e6960c3e5642","name":"3e1491f5-fd73-4360-b210-e6960c3e5642","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"9d4eebca-6b35-4781-8203-61592df7911d","name":"9d4eebca-6b35-4781-8203-61592df7911d","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"ae6d7f72-a217-4d10-b0d2-9aa59de10ce6","name":"ae6d7f72-a217-4d10-b0d2-9aa59de10ce6","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"249a6de1-497b-45eb-a2c0-d889cc51dabd","name":"249a6de1-497b-45eb-a2c0-d889cc51dabd","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"b9ad052a-f1f2-46f4-b487-6525cc83270c","name":"b9ad052a-f1f2-46f4-b487-6525cc83270c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":28,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"1e324a3f-9727-4af9-aaf6-868384adcfc3","blendMode":0,"displayName":"default","isLocked":false,"name":"1e324a3f-9727-4af9-aaf6-868384adcfc3","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprAssassinBossDead",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"AssassinBoss",
    "path":"folders/Sprites/Enemies/Boss/AssassinBoss.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprAssassinBossDead",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":14.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprAssassinBossDead",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"691e749d-4334-44c7-ad11-2518bc4377c3","path":"sprites/sprAssassinBossDead/sprAssassinBossDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"bfad473d-f6e7-4b9b-9b6e-d8a5adf121f3","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"f830af5f-6a70-4455-96d4-73607aa59e0f","path":"sprites/sprAssassinBossDead/sprAssassinBossDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"b324ae76-986e-4430-a589-b2cbe7a2cd75","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"278b2500-bd13-4b57-8a9b-125b05d9d55c","path":"sprites/sprAssassinBossDead/sprAssassinBossDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f75ac567-6646-4d1b-9a98-7c8bd20d1005","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"6129012a-092d-4543-84cd-b458f05521fb","path":"sprites/sprAssassinBossDead/sprAssassinBossDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"aa334e1b-5a1c-4f25-af80-6e8909d3a901","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"fa885548-51ab-4079-b23e-4322133fc718","path":"sprites/sprAssassinBossDead/sprAssassinBossDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ced3e4ad-13f6-47c1-b472-8bc0a336fe0b","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"70cda76c-6335-428d-ae04-fccc5ff65731","path":"sprites/sprAssassinBossDead/sprAssassinBossDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"610c0c8f-7cdc-4030-936d-ed1b3543ee42","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"e5399f5b-2ef2-4a82-8a8a-7da22840a169","path":"sprites/sprAssassinBossDead/sprAssassinBossDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"17b9208f-d112-4e20-8196-a78177941bb8","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"d7d72871-6bc6-4938-814b-52099261eaaa","path":"sprites/sprAssassinBossDead/sprAssassinBossDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"92bd3b34-a8bc-48d4-aff3-e80b47424b04","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"76bc4dac-87e2-4921-8123-54b2f5b25a96","path":"sprites/sprAssassinBossDead/sprAssassinBossDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"503c1278-359a-40fc-8cbc-aaba8b97f18f","IsCreationKey":false,"Key":8.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"3e1491f5-fd73-4360-b210-e6960c3e5642","path":"sprites/sprAssassinBossDead/sprAssassinBossDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"79c93234-4405-426b-a080-3668cb1d6fcf","IsCreationKey":false,"Key":9.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"9d4eebca-6b35-4781-8203-61592df7911d","path":"sprites/sprAssassinBossDead/sprAssassinBossDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"bc77b9b2-2cb9-46d2-bacb-f9f278b0e183","IsCreationKey":false,"Key":10.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"ae6d7f72-a217-4d10-b0d2-9aa59de10ce6","path":"sprites/sprAssassinBossDead/sprAssassinBossDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"4e718488-ddfb-474d-9f09-f3d0c999228f","IsCreationKey":false,"Key":11.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"249a6de1-497b-45eb-a2c0-d889cc51dabd","path":"sprites/sprAssassinBossDead/sprAssassinBossDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"0b11158b-f0bd-4eb4-88c4-5772812fcc37","IsCreationKey":false,"Key":12.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"b9ad052a-f1f2-46f4-b487-6525cc83270c","path":"sprites/sprAssassinBossDead/sprAssassinBossDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"bcae12fb-86a6-4396-ac2f-73aae1aa64d4","IsCreationKey":false,"Key":13.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":13,
    "yorigin":14,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Scrapyard",
    "path":"texturegroups/Scrapyard",
  },
  "type":0,
  "VTile":false,
  "width":26,
}