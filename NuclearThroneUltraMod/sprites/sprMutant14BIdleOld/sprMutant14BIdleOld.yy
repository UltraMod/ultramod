{
  "$GMSprite":"",
  "%Name":"sprMutant14BIdleOld",
  "bboxMode":1,
  "bbox_bottom":19,
  "bbox_left":0,
  "bbox_right":21,
  "bbox_top":0,
  "collisionKind":0,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"eccf4ba6-2f73-48c2-9265-4f5d2ef07d8c","name":"eccf4ba6-2f73-48c2-9265-4f5d2ef07d8c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"12c8535b-59a4-4b6f-884f-32bca267a8b3","name":"12c8535b-59a4-4b6f-884f-32bca267a8b3","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"0bb9d364-92fd-4abc-a3ed-31fec7caeb47","name":"0bb9d364-92fd-4abc-a3ed-31fec7caeb47","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"e6e129bc-594e-47a8-8d8c-8a1f5bbec123","name":"e6e129bc-594e-47a8-8d8c-8a1f5bbec123","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":20,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"7618749e-a85e-4d6a-beab-efe6d617d3ba","blendMode":0,"displayName":"default","isLocked":false,"name":"7618749e-a85e-4d6a-beab-efe6d617d3ba","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprMutant14BIdleOld",
  "nineSlice":null,
  "origin":9,
  "parent":{
    "name":"Panda",
    "path":"folders/Sprites/Player/Custom/Panda.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprMutant14BIdleOld",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":4.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprMutant14BIdleOld",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"eccf4ba6-2f73-48c2-9265-4f5d2ef07d8c","path":"sprites/sprMutant14BIdleOld/sprMutant14BIdleOld.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"08120a0a-a35f-4aca-a319-5bd1deabd1d1","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"12c8535b-59a4-4b6f-884f-32bca267a8b3","path":"sprites/sprMutant14BIdleOld/sprMutant14BIdleOld.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"355bfeef-b139-4736-9972-8cf14bfc2f60","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"0bb9d364-92fd-4abc-a3ed-31fec7caeb47","path":"sprites/sprMutant14BIdleOld/sprMutant14BIdleOld.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"aba64c55-b200-474e-a33a-7407c43722f6","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"e6e129bc-594e-47a8-8d8c-8a1f5bbec123","path":"sprites/sprMutant14BIdleOld/sprMutant14BIdleOld.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"d6956086-d506-4841-beed-177c2f31f22e","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":11,
    "yorigin":12,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"PlayerCharacter",
    "path":"texturegroups/PlayerCharacter",
  },
  "type":0,
  "VTile":false,
  "width":22,
}