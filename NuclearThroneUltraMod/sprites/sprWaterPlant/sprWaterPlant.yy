{
  "$GMSprite":"",
  "%Name":"sprWaterPlant",
  "bboxMode":0,
  "bbox_bottom":19,
  "bbox_left":7,
  "bbox_right":16,
  "bbox_top":3,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"8d298ae4-30d0-4c6c-ad52-49fe10f1c5c9","name":"8d298ae4-30d0-4c6c-ad52-49fe10f1c5c9","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"3a8ad3db-941b-4c4a-8763-0dd959d634ce","name":"3a8ad3db-941b-4c4a-8763-0dd959d634ce","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"2fab5b6f-c9a0-4390-a351-c2a28b199e7b","name":"2fab5b6f-c9a0-4390-a351-c2a28b199e7b","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"8643c9a7-dca1-4781-9141-4fbcdd6ca066","name":"8643c9a7-dca1-4781-9141-4fbcdd6ca066","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"49967b54-2316-4697-a6d9-3a2c9eed58ba","name":"49967b54-2316-4697-a6d9-3a2c9eed58ba","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"5f9897b5-d6d7-4b72-90d1-f44262892ba6","name":"5f9897b5-d6d7-4b72-90d1-f44262892ba6","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"d7488960-3a1f-473d-8042-39ceb69fb9a7","name":"d7488960-3a1f-473d-8042-39ceb69fb9a7","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"123f589b-71cb-4acb-adc1-68ef9eb5ceb6","name":"123f589b-71cb-4acb-adc1-68ef9eb5ceb6","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"ba46dae3-f109-45b4-b450-d1b10dc4a3be","name":"ba46dae3-f109-45b4-b450-d1b10dc4a3be","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"e7e9374c-e02f-4231-968b-789c64af9bd5","name":"e7e9374c-e02f-4231-968b-789c64af9bd5","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":24,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"e69cc14a-fbb4-4780-86f4-5db65ef93166","blendMode":0,"displayName":"default","isLocked":false,"name":"e69cc14a-fbb4-4780-86f4-5db65ef93166","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprWaterPlant",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"OasisProps",
    "path":"folders/Sprites/Enviroment/Props/OasisProps.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprWaterPlant",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":10.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprWaterPlant",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"8d298ae4-30d0-4c6c-ad52-49fe10f1c5c9","path":"sprites/sprWaterPlant/sprWaterPlant.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"c18243c6-a208-4499-bcd3-e68b3327fa77","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"3a8ad3db-941b-4c4a-8763-0dd959d634ce","path":"sprites/sprWaterPlant/sprWaterPlant.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"9b9ce6dd-945f-4957-8dd2-3270cc26b84e","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"2fab5b6f-c9a0-4390-a351-c2a28b199e7b","path":"sprites/sprWaterPlant/sprWaterPlant.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"b1c4bc27-062b-4693-af43-d9d6fb682fb7","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"8643c9a7-dca1-4781-9141-4fbcdd6ca066","path":"sprites/sprWaterPlant/sprWaterPlant.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"aaa71a3c-ee4f-4cd7-8a05-5d39e67711da","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"49967b54-2316-4697-a6d9-3a2c9eed58ba","path":"sprites/sprWaterPlant/sprWaterPlant.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"b546df53-9a5c-471f-a9d4-560f641803d7","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"5f9897b5-d6d7-4b72-90d1-f44262892ba6","path":"sprites/sprWaterPlant/sprWaterPlant.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"a3dc475b-13ae-4663-af8b-b08a8bf0cb39","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"d7488960-3a1f-473d-8042-39ceb69fb9a7","path":"sprites/sprWaterPlant/sprWaterPlant.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"3d5285a3-8c6d-4e49-8f78-9b941006d32b","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"123f589b-71cb-4acb-adc1-68ef9eb5ceb6","path":"sprites/sprWaterPlant/sprWaterPlant.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"e1efee6b-716c-487d-a4ea-6cdba3f08823","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"ba46dae3-f109-45b4-b450-d1b10dc4a3be","path":"sprites/sprWaterPlant/sprWaterPlant.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f8e0523f-d3f0-4d67-9ecb-cfa390319539","IsCreationKey":false,"Key":8.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"e7e9374c-e02f-4231-968b-789c64af9bd5","path":"sprites/sprWaterPlant/sprWaterPlant.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"81b76fa1-de46-4a89-9699-4bf9fc7d2297","IsCreationKey":false,"Key":9.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":{
      "x":0.0,
      "y":0.0,
    },
    "volume":1.0,
    "xorigin":12,
    "yorigin":12,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Oasis",
    "path":"texturegroups/Oasis",
  },
  "type":0,
  "VTile":false,
  "width":24,
}