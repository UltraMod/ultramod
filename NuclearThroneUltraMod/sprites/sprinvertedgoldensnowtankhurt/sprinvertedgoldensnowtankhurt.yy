{
  "$GMSprite":"",
  "%Name":"sprInvertedGoldenSnowTankHurt",
  "bboxMode":0,
  "bbox_bottom":39,
  "bbox_left":3,
  "bbox_right":43,
  "bbox_top":9,
  "collisionKind":0,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"115df8e6-4d86-4901-b27d-0169819145ae","name":"115df8e6-4d86-4901-b27d-0169819145ae","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"0bee7e37-8007-4782-9885-f163ffe8b2d6","name":"0bee7e37-8007-4782-9885-f163ffe8b2d6","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"4e7cf293-73d0-4399-8702-06c3d23eedd8","name":"4e7cf293-73d0-4399-8702-06c3d23eedd8","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":48,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"e206ebed-c52b-4f75-a414-95f6a416813c","blendMode":0,"displayName":"default","isLocked":false,"name":"e206ebed-c52b-4f75-a414-95f6a416813c","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprInvertedGoldenSnowTankHurt",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"FrozenCity",
    "path":"folders/Sprites/Enemies/FrozenCity.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprInvertedGoldenSnowTankHurt",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":3.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprInvertedGoldenSnowTankHurt",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"115df8e6-4d86-4901-b27d-0169819145ae","path":"sprites/sprInvertedGoldenSnowTankHurt/sprInvertedGoldenSnowTankHurt.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"95d1b753-1f79-4d5d-bf8f-5304091d34cb","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"0bee7e37-8007-4782-9885-f163ffe8b2d6","path":"sprites/sprInvertedGoldenSnowTankHurt/sprInvertedGoldenSnowTankHurt.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"b4d29a18-d23d-4192-a9a0-d5f36316152a","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"4e7cf293-73d0-4399-8702-06c3d23eedd8","path":"sprites/sprInvertedGoldenSnowTankHurt/sprInvertedGoldenSnowTankHurt.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f761463d-97aa-42ae-8401-3f6cf75c9c3e","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":24,
    "yorigin":24,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"FrozenCity",
    "path":"texturegroups/FrozenCity",
  },
  "type":0,
  "VTile":false,
  "width":48,
}