{
  "$GMSprite":"",
  "%Name":"sprInvertedFrogQueenDead",
  "bboxMode":0,
  "bbox_bottom":70,
  "bbox_left":2,
  "bbox_right":122,
  "bbox_top":6,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"8f2016a7-358d-4b22-aac3-8cd19dfff3af","name":"8f2016a7-358d-4b22-aac3-8cd19dfff3af","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"74c9d76a-5c16-4499-b28f-70f9869c5c77","name":"74c9d76a-5c16-4499-b28f-70f9869c5c77","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"a1c201ce-b3af-4f95-a94c-b42ad2c8a0b7","name":"a1c201ce-b3af-4f95-a94c-b42ad2c8a0b7","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"4f6f1283-d4ac-4d79-9a47-5136e14af1c7","name":"4f6f1283-d4ac-4d79-9a47-5136e14af1c7","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"d408861f-9dea-4064-a8da-80fcd414e407","name":"d408861f-9dea-4064-a8da-80fcd414e407","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"c100c252-9a49-458b-92e0-964c0231aca6","name":"c100c252-9a49-458b-92e0-964c0231aca6","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"70b14319-1327-4240-8489-d2cc41674bb4","name":"70b14319-1327-4240-8489-d2cc41674bb4","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"9a7757fd-af21-46d2-977b-91b61fabcd75","name":"9a7757fd-af21-46d2-977b-91b61fabcd75","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"b7b61e6c-bd08-4ba3-9f83-f91220b33335","name":"b7b61e6c-bd08-4ba3-9f83-f91220b33335","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"570342fb-500f-450f-980a-c8b20b232ae7","name":"570342fb-500f-450f-980a-c8b20b232ae7","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"01368210-7834-4cdf-97a1-14262805ca8d","name":"01368210-7834-4cdf-97a1-14262805ca8d","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":96,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"0e660af4-93c3-40b3-ae83-6b4c1570c115","blendMode":0,"displayName":"default","isLocked":false,"name":"0e660af4-93c3-40b3-ae83-6b4c1570c115","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprInvertedFrogQueenDead",
  "nineSlice":null,
  "origin":9,
  "parent":{
    "name":"BallMom",
    "path":"folders/Sprites/Enemies/Boss/BallMom.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprInvertedFrogQueenDead",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":11.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprInvertedFrogQueenDead",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"8f2016a7-358d-4b22-aac3-8cd19dfff3af","path":"sprites/sprInvertedFrogQueenDead/sprInvertedFrogQueenDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f74a6e6d-756d-4eac-8c95-8454cbb35141","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"74c9d76a-5c16-4499-b28f-70f9869c5c77","path":"sprites/sprInvertedFrogQueenDead/sprInvertedFrogQueenDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"94627778-fde3-4451-a512-7eb22018628c","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"a1c201ce-b3af-4f95-a94c-b42ad2c8a0b7","path":"sprites/sprInvertedFrogQueenDead/sprInvertedFrogQueenDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"b2efb5b0-34d0-465c-87fa-d5e17bc9628d","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"4f6f1283-d4ac-4d79-9a47-5136e14af1c7","path":"sprites/sprInvertedFrogQueenDead/sprInvertedFrogQueenDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ea2d0c56-0e25-45c8-8d34-cb3eb38a8a73","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"d408861f-9dea-4064-a8da-80fcd414e407","path":"sprites/sprInvertedFrogQueenDead/sprInvertedFrogQueenDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"36f791d4-ba6b-43ea-8866-2daa52b08e47","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c100c252-9a49-458b-92e0-964c0231aca6","path":"sprites/sprInvertedFrogQueenDead/sprInvertedFrogQueenDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"0d18beb1-52be-4466-a648-5f46b9343abd","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"70b14319-1327-4240-8489-d2cc41674bb4","path":"sprites/sprInvertedFrogQueenDead/sprInvertedFrogQueenDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"8856431b-9cd5-4aac-ae23-bd2de5ee56ac","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"9a7757fd-af21-46d2-977b-91b61fabcd75","path":"sprites/sprInvertedFrogQueenDead/sprInvertedFrogQueenDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"90f591f6-35d1-4cb3-a76c-16cc1decb2fe","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"b7b61e6c-bd08-4ba3-9f83-f91220b33335","path":"sprites/sprInvertedFrogQueenDead/sprInvertedFrogQueenDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"3f68e165-c77c-40d7-abab-51757353796b","IsCreationKey":false,"Key":8.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"570342fb-500f-450f-980a-c8b20b232ae7","path":"sprites/sprInvertedFrogQueenDead/sprInvertedFrogQueenDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"cd1c846d-5689-4319-b755-8cd4ae5254ca","IsCreationKey":false,"Key":9.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"01368210-7834-4cdf-97a1-14262805ca8d","path":"sprites/sprInvertedFrogQueenDead/sprInvertedFrogQueenDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"61e551a5-d275-44cc-b991-c04dbc98c7ca","IsCreationKey":false,"Key":10.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":72,
    "yorigin":53,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Sewers",
    "path":"texturegroups/Sewers",
  },
  "type":0,
  "VTile":false,
  "width":128,
}