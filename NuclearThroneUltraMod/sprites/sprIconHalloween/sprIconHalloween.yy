{
  "$GMSprite":"",
  "%Name":"sprIconHalloween",
  "bboxMode":0,
  "bbox_bottom":63,
  "bbox_left":0,
  "bbox_right":63,
  "bbox_top":0,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"eb4bce21-d43a-49b0-a7b8-e48c03539ee1","name":"eb4bce21-d43a-49b0-a7b8-e48c03539ee1","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"e83011b2-3cac-4d5b-94a6-bf05dbb4d4cf","name":"e83011b2-3cac-4d5b-94a6-bf05dbb4d4cf","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"c890fbb1-4b0a-4fbc-bcbf-5f994508e963","name":"c890fbb1-4b0a-4fbc-bcbf-5f994508e963","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":64,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"8bf0b101-a90a-4673-a29c-16f91af5eac9","blendMode":0,"displayName":"default","isLocked":false,"name":"8bf0b101-a90a-4673-a29c-16f91af5eac9","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprIconHalloween",
  "nineSlice":null,
  "origin":0,
  "parent":{
    "name":"Publish",
    "path":"folders/Sprites/Publish.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprIconHalloween",
    "autoRecord":true,
    "backdropHeight":768,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1366,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":3.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprIconHalloween",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"eb4bce21-d43a-49b0-a7b8-e48c03539ee1","path":"sprites/sprIconHalloween/sprIconHalloween.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"773e83a8-4654-4352-adb6-46c63bce0beb","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"e83011b2-3cac-4d5b-94a6-bf05dbb4d4cf","path":"sprites/sprIconHalloween/sprIconHalloween.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"4833cd67-3884-4e2e-866a-9d4aaf2eef69","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c890fbb1-4b0a-4fbc-bcbf-5f994508e963","path":"sprites/sprIconHalloween/sprIconHalloween.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"17242a3d-7cb2-4e04-9ab8-86c34c5512e3","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":0,
    "yorigin":0,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Default",
    "path":"texturegroups/Default",
  },
  "type":0,
  "VTile":false,
  "width":64,
}