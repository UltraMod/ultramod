{
  "$GMSprite":"",
  "%Name":"sprUltraSalamanderIdle",
  "bboxMode":0,
  "bbox_bottom":32,
  "bbox_left":7,
  "bbox_right":44,
  "bbox_top":10,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"88694d0b-1932-4c5a-afa0-710bf404c2d7","name":"88694d0b-1932-4c5a-afa0-710bf404c2d7","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"0e306d31-89d4-4e0a-80e0-665bbb0970c7","name":"0e306d31-89d4-4e0a-80e0-665bbb0970c7","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"a1921746-ddfa-412b-b769-8692fdd3e7aa","name":"a1921746-ddfa-412b-b769-8692fdd3e7aa","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"bf1f3654-f90a-4141-94bf-60fcc68b5bb5","name":"bf1f3654-f90a-4141-94bf-60fcc68b5bb5","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"067ec3c0-dba3-4bba-820c-eb9e713a3c9d","name":"067ec3c0-dba3-4bba-820c-eb9e713a3c9d","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"966aa942-96bd-49ad-8a43-d0d0cf3a1d5f","name":"966aa942-96bd-49ad-8a43-d0d0cf3a1d5f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"0963f04e-3681-4bda-a7c3-27df466a9635","name":"0963f04e-3681-4bda-a7c3-27df466a9635","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"ea8aa409-b231-4a89-9a15-da8f8e0b24b5","name":"ea8aa409-b231-4a89-9a15-da8f8e0b24b5","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":48,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"3d582c9f-ecd1-4f03-a60d-61418d7e5712","blendMode":0,"displayName":"default","isLocked":false,"name":"3d582c9f-ecd1-4f03-a60d-61418d7e5712","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprUltraSalamanderIdle",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"Salamander",
    "path":"folders/Sprites/Enemies/Salamander.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprUltraSalamanderIdle",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":8.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprUltraSalamanderIdle",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"88694d0b-1932-4c5a-afa0-710bf404c2d7","path":"sprites/sprUltraSalamanderIdle/sprUltraSalamanderIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"00f8fbcb-5963-4fc9-8706-ed0bae9129a4","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"0e306d31-89d4-4e0a-80e0-665bbb0970c7","path":"sprites/sprUltraSalamanderIdle/sprUltraSalamanderIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"27b5e7f5-da48-4cf0-b5e4-fb20f20aed02","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"a1921746-ddfa-412b-b769-8692fdd3e7aa","path":"sprites/sprUltraSalamanderIdle/sprUltraSalamanderIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"88650cb1-ba03-4c68-88f5-87fca2e9f5aa","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"bf1f3654-f90a-4141-94bf-60fcc68b5bb5","path":"sprites/sprUltraSalamanderIdle/sprUltraSalamanderIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"987ca271-ea7d-4ca9-b5b4-6e5fdc0e1ec2","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"067ec3c0-dba3-4bba-820c-eb9e713a3c9d","path":"sprites/sprUltraSalamanderIdle/sprUltraSalamanderIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"1ed7d2a5-3692-4a3a-a195-517221d226d9","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"966aa942-96bd-49ad-8a43-d0d0cf3a1d5f","path":"sprites/sprUltraSalamanderIdle/sprUltraSalamanderIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"9e30cc1c-1ebb-4019-998c-a666068af998","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"0963f04e-3681-4bda-a7c3-27df466a9635","path":"sprites/sprUltraSalamanderIdle/sprUltraSalamanderIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"7acd12ed-7ca0-40a9-b6ed-80004db263cf","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"ea8aa409-b231-4a89-9a15-da8f8e0b24b5","path":"sprites/sprUltraSalamanderIdle/sprUltraSalamanderIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"fd77c44e-fc00-4cdf-92d9-0eb657d5081b","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":24,
    "yorigin":24,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Scrapyard",
    "path":"texturegroups/Scrapyard",
  },
  "type":0,
  "VTile":false,
  "width":48,
}