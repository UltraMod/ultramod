{
  "$GMSprite":"",
  "%Name":"sprVoidFiendIntro",
  "bboxMode":0,
  "bbox_bottom":31,
  "bbox_left":0,
  "bbox_right":30,
  "bbox_top":0,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"59213ad3-2222-4f61-8e9a-e0493056f08c","name":"59213ad3-2222-4f61-8e9a-e0493056f08c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"be2d55d7-d144-47b6-9a60-f38ca323668e","name":"be2d55d7-d144-47b6-9a60-f38ca323668e","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"90f37df4-2c5d-41fd-b0d7-fe3316a22514","name":"90f37df4-2c5d-41fd-b0d7-fe3316a22514","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"85ec4c76-45a0-4c31-9549-f8d3cf8c13df","name":"85ec4c76-45a0-4c31-9549-f8d3cf8c13df","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"fc195c01-e943-4370-ae0a-08f2e8ba58a4","name":"fc195c01-e943-4370-ae0a-08f2e8ba58a4","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"ba1dce8c-33a2-4763-842c-6b5854101799","name":"ba1dce8c-33a2-4763-842c-6b5854101799","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"a0671f33-2a81-4d81-ba56-b031fd20322a","name":"a0671f33-2a81-4d81-ba56-b031fd20322a","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"060c8377-00b9-46a6-b6e1-f23a03359845","name":"060c8377-00b9-46a6-b6e1-f23a03359845","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"cb28cae3-6cd5-4de8-9dff-b8a838b4df7f","name":"cb28cae3-6cd5-4de8-9dff-b8a838b4df7f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":32,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"4539ea1a-a1c9-4ac3-b6ec-9b15b8fba59d","blendMode":0,"displayName":"default","isLocked":false,"name":"4539ea1a-a1c9-4ac3-b6ec-9b15b8fba59d","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
    {"$GMImageLayer":"","%Name":"b6dd8ae7-0d5a-40e0-9c89-20c37b4d3044","blendMode":0,"displayName":"d","isLocked":false,"name":"b6dd8ae7-0d5a-40e0-9c89-20c37b4d3044","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprVoidFiendIntro",
  "nineSlice":null,
  "origin":9,
  "parent":{
    "name":"Void",
    "path":"folders/Sprites/Void.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprVoidFiendIntro",
    "autoRecord":true,
    "backdropHeight":768,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1366,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":9.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprVoidFiendIntro",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"59213ad3-2222-4f61-8e9a-e0493056f08c","path":"sprites/sprVoidFiendIntro/sprVoidFiendIntro.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"3be72d9e-4466-400e-82c7-83a8c287b1b6","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"be2d55d7-d144-47b6-9a60-f38ca323668e","path":"sprites/sprVoidFiendIntro/sprVoidFiendIntro.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"dce186f1-fdde-495c-bd0d-f2917aecef8b","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"90f37df4-2c5d-41fd-b0d7-fe3316a22514","path":"sprites/sprVoidFiendIntro/sprVoidFiendIntro.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"e78fadb8-892f-46f4-a6b7-cce3da463b5a","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"85ec4c76-45a0-4c31-9549-f8d3cf8c13df","path":"sprites/sprVoidFiendIntro/sprVoidFiendIntro.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"b1db1f12-7cd8-4c6e-8499-c5393828210d","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"fc195c01-e943-4370-ae0a-08f2e8ba58a4","path":"sprites/sprVoidFiendIntro/sprVoidFiendIntro.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"3522f8fc-26dd-41ce-a690-0f5cdacf9b68","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"ba1dce8c-33a2-4763-842c-6b5854101799","path":"sprites/sprVoidFiendIntro/sprVoidFiendIntro.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f21343b9-68e7-4abd-b1a9-9d37915a16a1","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"a0671f33-2a81-4d81-ba56-b031fd20322a","path":"sprites/sprVoidFiendIntro/sprVoidFiendIntro.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"47c36071-d690-43c8-bfed-d0055f8896c5","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"060c8377-00b9-46a6-b6e1-f23a03359845","path":"sprites/sprVoidFiendIntro/sprVoidFiendIntro.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"e5a3d795-f0d1-4509-84ed-3d09f52cf147","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"cb28cae3-6cd5-4de8-9dff-b8a838b4df7f","path":"sprites/sprVoidFiendIntro/sprVoidFiendIntro.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ca0322fa-633a-4e56-9c1a-df8e9fd7877a","IsCreationKey":false,"Key":8.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":16,
    "yorigin":48,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Default",
    "path":"texturegroups/Default",
  },
  "type":0,
  "VTile":false,
  "width":32,
}