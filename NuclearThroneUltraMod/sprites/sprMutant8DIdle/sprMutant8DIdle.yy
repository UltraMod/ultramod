{
  "$GMSprite":"",
  "%Name":"sprMutant8DIdle",
  "bboxMode":1,
  "bbox_bottom":13,
  "bbox_left":0,
  "bbox_right":16,
  "bbox_top":0,
  "collisionKind":0,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"26db2fee-6176-44b2-b200-fad737f3d40d","name":"26db2fee-6176-44b2-b200-fad737f3d40d","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"da58075c-fe03-4099-a4d4-cf56d21f8056","name":"da58075c-fe03-4099-a4d4-cf56d21f8056","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"c50fc448-d33c-4a50-b6f3-a8377b237356","name":"c50fc448-d33c-4a50-b6f3-a8377b237356","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"8984d20d-e200-48c3-9489-5a332b77dff6","name":"8984d20d-e200-48c3-9489-5a332b77dff6","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"19c0c3af-aab4-4657-94e8-0208d63e1e55","name":"19c0c3af-aab4-4657-94e8-0208d63e1e55","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"ca082a91-eb1a-4095-8183-a2d56fdec572","name":"ca082a91-eb1a-4095-8183-a2d56fdec572","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"ea31fb7c-072e-42b5-90e1-f06304943af7","name":"ea31fb7c-072e-42b5-90e1-f06304943af7","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"7fc15bac-bf7a-4dd6-b8b7-b85061e9490d","name":"7fc15bac-bf7a-4dd6-b8b7-b85061e9490d","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"51bfa957-a48a-470f-975d-7f37a9e643c2","name":"51bfa957-a48a-470f-975d-7f37a9e643c2","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"59860761-12a5-45c5-8fbf-0d6010d15a97","name":"59860761-12a5-45c5-8fbf-0d6010d15a97","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"199fc391-8c69-410a-886f-476ba3cdf658","name":"199fc391-8c69-410a-886f-476ba3cdf658","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"4914f8a7-1d3b-4658-847b-975e673afa59","name":"4914f8a7-1d3b-4658-847b-975e673afa59","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"540c5423-5c43-4c56-89da-154b994f4499","name":"540c5423-5c43-4c56-89da-154b994f4499","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"1a977525-e4cf-4ae2-b4be-5708de485403","name":"1a977525-e4cf-4ae2-b4be-5708de485403","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"c33f2221-b2f5-4a32-9deb-5ac132b4f06b","name":"c33f2221-b2f5-4a32-9deb-5ac132b4f06b","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"4e2e9f02-4057-430b-a81c-662d8c91117e","name":"4e2e9f02-4057-430b-a81c-662d8c91117e","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"b595f9e6-2a4f-44e1-9670-90bb9fb72670","name":"b595f9e6-2a4f-44e1-9670-90bb9fb72670","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"77f9e5f7-3847-46cf-8588-472d31ac7ef3","name":"77f9e5f7-3847-46cf-8588-472d31ac7ef3","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"7bb350fc-0ffd-4582-a7e3-69afdaacbc15","name":"7bb350fc-0ffd-4582-a7e3-69afdaacbc15","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"98befd75-46a7-4e75-b7ba-1342c6681fdb","name":"98befd75-46a7-4e75-b7ba-1342c6681fdb","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"09d455a9-4e3b-46bf-ac79-1c844826ca6d","name":"09d455a9-4e3b-46bf-ac79-1c844826ca6d","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"7e21d156-2111-458f-af7c-f3e7e4504657","name":"7e21d156-2111-458f-af7c-f3e7e4504657","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"b2a0e4ca-10bc-4eaf-ab32-7a9cc9f1d9e5","name":"b2a0e4ca-10bc-4eaf-ab32-7a9cc9f1d9e5","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"5372375f-cb4e-498d-83b2-fef86f880aac","name":"5372375f-cb4e-498d-83b2-fef86f880aac","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"ad509bd9-a512-4514-acb2-57ec42071b4d","name":"ad509bd9-a512-4514-acb2-57ec42071b4d","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":14,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"4817f946-641e-44f9-83ed-da17a295c566","blendMode":0,"displayName":"default","isLocked":false,"name":"4817f946-641e-44f9-83ed-da17a295c566","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprMutant8DIdle",
  "nineSlice":null,
  "origin":9,
  "parent":{
    "name":"RobotD",
    "path":"folders/Sprites/Player/Robot/RobotD.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":25.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"26db2fee-6176-44b2-b200-fad737f3d40d","path":"sprites/sprMutant8DIdle/sprMutant8DIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"5ac7f186-4a0a-41c6-83da-0e9d20701168","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"da58075c-fe03-4099-a4d4-cf56d21f8056","path":"sprites/sprMutant8DIdle/sprMutant8DIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"aeb3bd6c-bbfd-484c-a995-6432ebf3ea51","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c50fc448-d33c-4a50-b6f3-a8377b237356","path":"sprites/sprMutant8DIdle/sprMutant8DIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"c31a8d09-86ce-4fa0-88f8-b1efc56d8c30","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"8984d20d-e200-48c3-9489-5a332b77dff6","path":"sprites/sprMutant8DIdle/sprMutant8DIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"9a4d83e1-6c8e-4bde-bc40-9daa377d1157","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"19c0c3af-aab4-4657-94e8-0208d63e1e55","path":"sprites/sprMutant8DIdle/sprMutant8DIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"0348eab8-f1df-411b-8b49-f078e380e825","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"ca082a91-eb1a-4095-8183-a2d56fdec572","path":"sprites/sprMutant8DIdle/sprMutant8DIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"a242971f-8519-427e-9446-b5a03f2835ca","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"ea31fb7c-072e-42b5-90e1-f06304943af7","path":"sprites/sprMutant8DIdle/sprMutant8DIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"80eed006-3940-4543-831d-58076e7925af","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"7fc15bac-bf7a-4dd6-b8b7-b85061e9490d","path":"sprites/sprMutant8DIdle/sprMutant8DIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"82856ffa-9420-4be0-96a7-973bcaf855f9","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"51bfa957-a48a-470f-975d-7f37a9e643c2","path":"sprites/sprMutant8DIdle/sprMutant8DIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"962f50da-36c9-4261-bb20-d16a2e29d5f7","IsCreationKey":false,"Key":8.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"59860761-12a5-45c5-8fbf-0d6010d15a97","path":"sprites/sprMutant8DIdle/sprMutant8DIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"3600f54b-7296-4635-8015-c2da045ebe3d","IsCreationKey":false,"Key":9.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"199fc391-8c69-410a-886f-476ba3cdf658","path":"sprites/sprMutant8DIdle/sprMutant8DIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"a3aa2f11-4edb-414f-98fb-b6357b834b35","IsCreationKey":false,"Key":10.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"4914f8a7-1d3b-4658-847b-975e673afa59","path":"sprites/sprMutant8DIdle/sprMutant8DIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"d3285095-2dff-4333-94cf-4be02fcd86bc","IsCreationKey":false,"Key":11.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"540c5423-5c43-4c56-89da-154b994f4499","path":"sprites/sprMutant8DIdle/sprMutant8DIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"9391677a-cbf3-4bcc-adc1-a3a004373ff7","IsCreationKey":false,"Key":12.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"1a977525-e4cf-4ae2-b4be-5708de485403","path":"sprites/sprMutant8DIdle/sprMutant8DIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"b282f9c0-22d8-4c40-8051-f5f2ae07b6b5","IsCreationKey":false,"Key":13.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c33f2221-b2f5-4a32-9deb-5ac132b4f06b","path":"sprites/sprMutant8DIdle/sprMutant8DIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"e55c15aa-5ae7-4664-9bf8-c65900bc52cc","IsCreationKey":false,"Key":14.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"4e2e9f02-4057-430b-a81c-662d8c91117e","path":"sprites/sprMutant8DIdle/sprMutant8DIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"86b89c57-63d6-439e-b984-83f821347f7f","IsCreationKey":false,"Key":15.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"b595f9e6-2a4f-44e1-9670-90bb9fb72670","path":"sprites/sprMutant8DIdle/sprMutant8DIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"409787b7-79c5-472f-a5a7-c6e8a3e10f32","IsCreationKey":false,"Key":16.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"77f9e5f7-3847-46cf-8588-472d31ac7ef3","path":"sprites/sprMutant8DIdle/sprMutant8DIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"990b814f-1003-44aa-a1f9-19ab19b2eb92","IsCreationKey":false,"Key":17.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"7bb350fc-0ffd-4582-a7e3-69afdaacbc15","path":"sprites/sprMutant8DIdle/sprMutant8DIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"0fd10200-0d81-4927-a80f-4435acbaf38e","IsCreationKey":false,"Key":18.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"98befd75-46a7-4e75-b7ba-1342c6681fdb","path":"sprites/sprMutant8DIdle/sprMutant8DIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"7a02b917-48bb-45fd-9cff-78d6c16a94da","IsCreationKey":false,"Key":19.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"09d455a9-4e3b-46bf-ac79-1c844826ca6d","path":"sprites/sprMutant8DIdle/sprMutant8DIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"529182cc-7941-40cc-a474-440dac4c28c1","IsCreationKey":false,"Key":20.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"7e21d156-2111-458f-af7c-f3e7e4504657","path":"sprites/sprMutant8DIdle/sprMutant8DIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"77e68412-0ea0-4d82-9d66-51f6dba56eca","IsCreationKey":false,"Key":21.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"b2a0e4ca-10bc-4eaf-ab32-7a9cc9f1d9e5","path":"sprites/sprMutant8DIdle/sprMutant8DIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"e9e35043-048f-437f-8e04-2f039f0ff9fe","IsCreationKey":false,"Key":22.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"5372375f-cb4e-498d-83b2-fef86f880aac","path":"sprites/sprMutant8DIdle/sprMutant8DIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"9b92ea4c-07d4-4b77-8bdf-43ea610705a9","IsCreationKey":false,"Key":23.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"ad509bd9-a512-4514-acb2-57ec42071b4d","path":"sprites/sprMutant8DIdle/sprMutant8DIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"8462f8b8-0103-418a-9ff7-52da4ddecb8e","IsCreationKey":false,"Key":24.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":8,
    "yorigin":5,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"PlayerCharacter",
    "path":"texturegroups/PlayerCharacter",
  },
  "type":0,
  "VTile":false,
  "width":17,
}