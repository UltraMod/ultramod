{
  "$GMSprite":"",
  "%Name":"sprBloodSewingNeedleCurveLine",
  "bboxMode":0,
  "bbox_bottom":20,
  "bbox_left":0,
  "bbox_right":33,
  "bbox_top":3,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"7c1408d0-bdb5-4e2f-b2e5-427286812829","name":"7c1408d0-bdb5-4e2f-b2e5-427286812829","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"87da5d99-45d8-4c8a-bee3-0e8bb91eb6e7","name":"87da5d99-45d8-4c8a-bee3-0e8bb91eb6e7","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"a13b638c-aa1b-4e10-9958-adff0e7f1ebc","name":"a13b638c-aa1b-4e10-9958-adff0e7f1ebc","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"76b40ed8-fc0d-4351-a62e-8879d258d4eb","name":"76b40ed8-fc0d-4351-a62e-8879d258d4eb","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"c3a030a8-ca12-49d4-a079-484f62b3e5ec","name":"c3a030a8-ca12-49d4-a079-484f62b3e5ec","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"2e3e9f1a-5eed-4977-b918-4e07aa51b264","name":"2e3e9f1a-5eed-4977-b918-4e07aa51b264","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"30530f31-9e49-4cd8-9905-a707d39bde51","name":"30530f31-9e49-4cd8-9905-a707d39bde51","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"b2ed37de-f420-48e8-9999-37942d1ea563","name":"b2ed37de-f420-48e8-9999-37942d1ea563","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"4076fec3-1af3-4041-a215-1d67dc26d159","name":"4076fec3-1af3-4041-a215-1d67dc26d159","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":24,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"eb45473b-090a-4c14-a7bc-a3224eb20f3b","blendMode":0,"displayName":"default","isLocked":false,"name":"eb45473b-090a-4c14-a7bc-a3224eb20f3b","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
    {"$GMImageLayer":"","%Name":"5ebb2d41-e4af-409a-b820-124a334cd99a","blendMode":0,"displayName":"Layer 1","isLocked":false,"name":"5ebb2d41-e4af-409a-b820-124a334cd99a","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":false,},
  ],
  "name":"sprBloodSewingNeedleCurveLine",
  "nineSlice":null,
  "origin":3,
  "parent":{
    "name":"Melee Atacks",
    "path":"folders/Sprites/Projectiles/Melee Atacks.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprBloodSewingNeedleCurveLine",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":9.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprBloodSewingNeedleCurveLine",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"7c1408d0-bdb5-4e2f-b2e5-427286812829","path":"sprites/sprBloodSewingNeedleCurveLine/sprBloodSewingNeedleCurveLine.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"5a5978b3-7f28-46dc-84f6-15d9f4d6757f","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"87da5d99-45d8-4c8a-bee3-0e8bb91eb6e7","path":"sprites/sprBloodSewingNeedleCurveLine/sprBloodSewingNeedleCurveLine.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"c97c7e75-af41-42f3-861c-d63bc6e5ed60","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"a13b638c-aa1b-4e10-9958-adff0e7f1ebc","path":"sprites/sprBloodSewingNeedleCurveLine/sprBloodSewingNeedleCurveLine.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"77572464-f8fd-4853-891e-b1a33382b4ab","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"76b40ed8-fc0d-4351-a62e-8879d258d4eb","path":"sprites/sprBloodSewingNeedleCurveLine/sprBloodSewingNeedleCurveLine.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"d31c3d08-8665-46dd-b209-41a8f2f8b2ba","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c3a030a8-ca12-49d4-a079-484f62b3e5ec","path":"sprites/sprBloodSewingNeedleCurveLine/sprBloodSewingNeedleCurveLine.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f19ee459-08d4-401b-af02-cb3d384c0666","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"2e3e9f1a-5eed-4977-b918-4e07aa51b264","path":"sprites/sprBloodSewingNeedleCurveLine/sprBloodSewingNeedleCurveLine.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"829ee68f-37e6-44f0-865c-3b9b0dea1dc3","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"30530f31-9e49-4cd8-9905-a707d39bde51","path":"sprites/sprBloodSewingNeedleCurveLine/sprBloodSewingNeedleCurveLine.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"2713d65d-8eaf-48d2-b52d-c1c94ffc84e7","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"b2ed37de-f420-48e8-9999-37942d1ea563","path":"sprites/sprBloodSewingNeedleCurveLine/sprBloodSewingNeedleCurveLine.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ab999a45-129a-4e97-9e34-497179f25864","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"4076fec3-1af3-4041-a215-1d67dc26d159","path":"sprites/sprBloodSewingNeedleCurveLine/sprBloodSewingNeedleCurveLine.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"d6a42ac9-d0aa-43c0-93ea-62fbdb1ca012","IsCreationKey":false,"Key":8.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":0,
    "yorigin":12,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Projectiles",
    "path":"texturegroups/Projectiles",
  },
  "type":0,
  "VTile":false,
  "width":34,
}