{
  "$GMSprite":"",
  "%Name":"sprInvertedGoldHyenaIdle",
  "bboxMode":1,
  "bbox_bottom":23,
  "bbox_left":0,
  "bbox_right":47,
  "bbox_top":0,
  "collisionKind":0,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"398a2f1c-6340-4fba-b395-ff07cf263502","name":"398a2f1c-6340-4fba-b395-ff07cf263502","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"15eca77b-c0e7-4949-abe4-07916402b7c4","name":"15eca77b-c0e7-4949-abe4-07916402b7c4","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"26df491a-df7f-4ef9-a48f-b7ea6c17ffea","name":"26df491a-df7f-4ef9-a48f-b7ea6c17ffea","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"817120f0-55a5-43ab-932a-63c47e99c48d","name":"817120f0-55a5-43ab-932a-63c47e99c48d","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"4455002f-fd1d-4534-a706-ffbe36d67324","name":"4455002f-fd1d-4534-a706-ffbe36d67324","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"d3017dee-b281-48f1-b8fb-fda57735d4de","name":"d3017dee-b281-48f1-b8fb-fda57735d4de","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":24,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"a6432adc-bc02-42e6-a69e-3c1699d775aa","blendMode":0,"displayName":"default","isLocked":false,"name":"a6432adc-bc02-42e6-a69e-3c1699d775aa","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
    {"$GMImageLayer":"","%Name":"b83d2a85-28fb-4fb9-9e2d-75273ffc19b5","blendMode":0,"displayName":"Layer 1","isLocked":false,"name":"b83d2a85-28fb-4fb9-9e2d-75273ffc19b5","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprInvertedGoldHyenaIdle",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"GoldHyena",
    "path":"folders/Sprites/Enemies/Savanna/GoldHyena.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprInvertedGoldHyenaIdle",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":6.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprInvertedGoldHyenaIdle",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"398a2f1c-6340-4fba-b395-ff07cf263502","path":"sprites/sprInvertedGoldHyenaIdle/sprInvertedGoldHyenaIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"6c1c3cc9-3140-4f9f-8900-7009f804c60f","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"15eca77b-c0e7-4949-abe4-07916402b7c4","path":"sprites/sprInvertedGoldHyenaIdle/sprInvertedGoldHyenaIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"c5f77cf8-77e8-465d-86e1-11f7dff0b99b","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"26df491a-df7f-4ef9-a48f-b7ea6c17ffea","path":"sprites/sprInvertedGoldHyenaIdle/sprInvertedGoldHyenaIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"56ad4c22-08bb-47d5-9f12-9eab94084cfa","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"817120f0-55a5-43ab-932a-63c47e99c48d","path":"sprites/sprInvertedGoldHyenaIdle/sprInvertedGoldHyenaIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"13d576a0-1ddf-4203-ad92-3f910c4d145a","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"4455002f-fd1d-4534-a706-ffbe36d67324","path":"sprites/sprInvertedGoldHyenaIdle/sprInvertedGoldHyenaIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ea103c83-9a79-4700-8de8-8d27b640b5d7","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"d3017dee-b281-48f1-b8fb-fda57735d4de","path":"sprites/sprInvertedGoldHyenaIdle/sprInvertedGoldHyenaIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"bdfce89c-f86f-44e5-addb-e65f1395381b","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":24,
    "yorigin":12,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Sewers",
    "path":"texturegroups/Sewers",
  },
  "type":0,
  "VTile":false,
  "width":48,
}