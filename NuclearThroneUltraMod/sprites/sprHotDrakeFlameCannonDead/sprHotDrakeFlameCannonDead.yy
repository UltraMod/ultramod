{
  "$GMSprite":"",
  "%Name":"sprHotDrakeFlameCannonDead",
  "bboxMode":0,
  "bbox_bottom":19,
  "bbox_left":0,
  "bbox_right":19,
  "bbox_top":0,
  "collisionKind":0,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"23f6195f-fa3b-4fa9-a7ef-d44a5aa554a1","name":"23f6195f-fa3b-4fa9-a7ef-d44a5aa554a1","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"27a8dc00-1363-4804-8185-9b43cd02ae5e","name":"27a8dc00-1363-4804-8185-9b43cd02ae5e","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"ed89aa91-23b3-41b4-873c-4c5e0730ce4a","name":"ed89aa91-23b3-41b4-873c-4c5e0730ce4a","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"1ca3cb9d-547a-427e-afb4-e5e1a9ff77fd","name":"1ca3cb9d-547a-427e-afb4-e5e1a9ff77fd","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"1c77551d-68c6-4e4b-ac2a-57ff4a67d534","name":"1c77551d-68c6-4e4b-ac2a-57ff4a67d534","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"90460c17-4e7c-42ad-a62b-34cca718f578","name":"90460c17-4e7c-42ad-a62b-34cca718f578","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"5860ca0c-cae9-46d6-80de-45070be46e8c","name":"5860ca0c-cae9-46d6-80de-45070be46e8c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":20,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"ceaa5fc2-fcb9-41ff-8366-63bd582b321c","blendMode":0,"displayName":"default","isLocked":false,"name":"ceaa5fc2-fcb9-41ff-8366-63bd582b321c","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprHotDrakeFlameCannonDead",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"HotDrake",
    "path":"folders/Sprites/Enemies/Boss/HotDrake.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":7.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"23f6195f-fa3b-4fa9-a7ef-d44a5aa554a1","path":"sprites/sprHotDrakeFlameCannonDead/sprHotDrakeFlameCannonDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"a7373c91-0e11-41c8-823a-e3a21161dfae","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"27a8dc00-1363-4804-8185-9b43cd02ae5e","path":"sprites/sprHotDrakeFlameCannonDead/sprHotDrakeFlameCannonDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"fe7e5ad9-9bd9-44b9-b367-25aa8dd07a23","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"ed89aa91-23b3-41b4-873c-4c5e0730ce4a","path":"sprites/sprHotDrakeFlameCannonDead/sprHotDrakeFlameCannonDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"1377be18-bf7b-45e6-9bff-ad220a5ba68b","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"1ca3cb9d-547a-427e-afb4-e5e1a9ff77fd","path":"sprites/sprHotDrakeFlameCannonDead/sprHotDrakeFlameCannonDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"8a4b1f17-e0c8-4b98-904e-8d5873f02dc5","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"1c77551d-68c6-4e4b-ac2a-57ff4a67d534","path":"sprites/sprHotDrakeFlameCannonDead/sprHotDrakeFlameCannonDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"6be471da-90f0-4449-9efb-2293df9bdb0e","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"90460c17-4e7c-42ad-a62b-34cca718f578","path":"sprites/sprHotDrakeFlameCannonDead/sprHotDrakeFlameCannonDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"95a6f430-a766-4a1d-bd8b-8b4aefae14b3","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"5860ca0c-cae9-46d6-80de-45070be46e8c","path":"sprites/sprHotDrakeFlameCannonDead/sprHotDrakeFlameCannonDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"82f55bce-d77f-4bd5-88de-6b1f8560797c","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":10,
    "yorigin":10,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Vulcano",
    "path":"texturegroups/Vulcano",
  },
  "type":0,
  "VTile":false,
  "width":20,
}