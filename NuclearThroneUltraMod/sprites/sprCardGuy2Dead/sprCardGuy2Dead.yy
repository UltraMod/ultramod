{
  "$GMSprite":"",
  "%Name":"sprCardGuy2Dead",
  "bboxMode":0,
  "bbox_bottom":31,
  "bbox_left":0,
  "bbox_right":23,
  "bbox_top":2,
  "collisionKind":0,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"6d2a1c65-72d9-4a68-ba22-0d34a1142f19","name":"6d2a1c65-72d9-4a68-ba22-0d34a1142f19","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"c95bbcbb-7289-4fae-b7e1-c1d5e9cbb07c","name":"c95bbcbb-7289-4fae-b7e1-c1d5e9cbb07c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"c5677c18-e06e-4423-9d57-9f648f5c5403","name":"c5677c18-e06e-4423-9d57-9f648f5c5403","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"2f45c9ff-8dcf-4994-b102-e89cc6d2f70e","name":"2f45c9ff-8dcf-4994-b102-e89cc6d2f70e","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"9877008c-1f6b-4442-bc59-e29a82bb4570","name":"9877008c-1f6b-4442-bc59-e29a82bb4570","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"b613d4e3-3e92-48ab-a2d0-f95fca226eaa","name":"b613d4e3-3e92-48ab-a2d0-f95fca226eaa","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"8f6f3201-d87a-4c3f-ab9f-a833e745c131","name":"8f6f3201-d87a-4c3f-ab9f-a833e745c131","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"32d18400-cd15-47a8-8ad8-b68497aba11e","name":"32d18400-cd15-47a8-8ad8-b68497aba11e","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"ad988754-8d76-426a-94dc-f3dbcc19ed3e","name":"ad988754-8d76-426a-94dc-f3dbcc19ed3e","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"afeede10-63b4-4c18-9850-b3ed412715e7","name":"afeede10-63b4-4c18-9850-b3ed412715e7","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"016857aa-8ca7-4fc7-91fb-6fd6ddb3ce88","name":"016857aa-8ca7-4fc7-91fb-6fd6ddb3ce88","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"2bc5d925-cba6-4d31-ab6f-2208443e2cf0","name":"2bc5d925-cba6-4d31-ab6f-2208443e2cf0","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":32,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"4319fc84-b93d-49ef-8f5a-848e72868288","blendMode":0,"displayName":"default","isLocked":false,"name":"4319fc84-b93d-49ef-8f5a-848e72868288","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprCardGuy2Dead",
  "nineSlice":null,
  "origin":9,
  "parent":{
    "name":"CardGuy",
    "path":"folders/Sprites/Enemies/CardGuy.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":12.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"6d2a1c65-72d9-4a68-ba22-0d34a1142f19","path":"sprites/sprCardGuy2Dead/sprCardGuy2Dead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ae74b7d1-0f8e-4d46-a6dd-3940a5f2a15e","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c95bbcbb-7289-4fae-b7e1-c1d5e9cbb07c","path":"sprites/sprCardGuy2Dead/sprCardGuy2Dead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"8e0f21f6-4b66-4134-8d8c-95dac19a3a0f","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c5677c18-e06e-4423-9d57-9f648f5c5403","path":"sprites/sprCardGuy2Dead/sprCardGuy2Dead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"5ec3270b-c959-40b3-a076-0eaff7325eb9","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"2f45c9ff-8dcf-4994-b102-e89cc6d2f70e","path":"sprites/sprCardGuy2Dead/sprCardGuy2Dead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"7e6d1f9c-1984-48bd-b918-724971aea5e4","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"9877008c-1f6b-4442-bc59-e29a82bb4570","path":"sprites/sprCardGuy2Dead/sprCardGuy2Dead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"e2c00817-41bb-4f3d-9bfa-5ab6e8544046","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"b613d4e3-3e92-48ab-a2d0-f95fca226eaa","path":"sprites/sprCardGuy2Dead/sprCardGuy2Dead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"9dbcaf2e-4e3c-49ae-ad3e-02deadc8873c","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"8f6f3201-d87a-4c3f-ab9f-a833e745c131","path":"sprites/sprCardGuy2Dead/sprCardGuy2Dead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f7c2d0b9-e974-4f33-a2a1-5a44f03044f6","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"32d18400-cd15-47a8-8ad8-b68497aba11e","path":"sprites/sprCardGuy2Dead/sprCardGuy2Dead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"c4351141-7ac3-4402-97f3-78d2662a179d","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"ad988754-8d76-426a-94dc-f3dbcc19ed3e","path":"sprites/sprCardGuy2Dead/sprCardGuy2Dead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"73029886-f77b-414c-8ac8-89cdc75dad14","IsCreationKey":false,"Key":8.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"afeede10-63b4-4c18-9850-b3ed412715e7","path":"sprites/sprCardGuy2Dead/sprCardGuy2Dead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ef2e1c82-ee1c-46ce-bc9c-edfd363b4ee6","IsCreationKey":false,"Key":9.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"016857aa-8ca7-4fc7-91fb-6fd6ddb3ce88","path":"sprites/sprCardGuy2Dead/sprCardGuy2Dead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"b96e965e-ea32-45ce-b391-9ecb8d7e3188","IsCreationKey":false,"Key":10.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"2bc5d925-cba6-4d31-ab6f-2208443e2cf0","path":"sprites/sprCardGuy2Dead/sprCardGuy2Dead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"d11cbfd9-ee06-4a74-a55b-c595f29eb331","IsCreationKey":false,"Key":11.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":{
      "x":0.0,
      "y":0.0,
    },
    "volume":1.0,
    "xorigin":12,
    "yorigin":12,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Wonderland",
    "path":"texturegroups/Wonderland",
  },
  "type":0,
  "VTile":false,
  "width":24,
}