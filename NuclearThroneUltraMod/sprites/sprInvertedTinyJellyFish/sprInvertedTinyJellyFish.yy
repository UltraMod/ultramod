{
  "$GMSprite":"",
  "%Name":"sprInvertedTinyJellyFish",
  "bboxMode":0,
  "bbox_bottom":15,
  "bbox_left":0,
  "bbox_right":10,
  "bbox_top":0,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"86ad5655-7238-4c93-b92d-0cf518952426","name":"86ad5655-7238-4c93-b92d-0cf518952426","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"77e29539-3fae-4997-9734-2ceb3817b737","name":"77e29539-3fae-4997-9734-2ceb3817b737","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"362889b1-73f2-4526-849a-ab4866ba346c","name":"362889b1-73f2-4526-849a-ab4866ba346c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"4ca53009-a2b6-43cc-89ad-8e9087516517","name":"4ca53009-a2b6-43cc-89ad-8e9087516517","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"07b4e656-72d5-4eb5-8269-f18d52d41efb","name":"07b4e656-72d5-4eb5-8269-f18d52d41efb","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":16,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"f9419a15-2ab2-446b-b032-6bff8a8a324d","blendMode":0,"displayName":"default","isLocked":false,"name":"f9419a15-2ab2-446b-b032-6bff8a8a324d","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprInvertedTinyJellyFish",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"Oasis",
    "path":"folders/Sprites/Enemies/Oasis.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprInvertedTinyJellyFish",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":5.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprInvertedTinyJellyFish",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"86ad5655-7238-4c93-b92d-0cf518952426","path":"sprites/sprInvertedTinyJellyFish/sprInvertedTinyJellyFish.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"b3c29e01-2eb2-4695-85e9-5cbab3302511","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"77e29539-3fae-4997-9734-2ceb3817b737","path":"sprites/sprInvertedTinyJellyFish/sprInvertedTinyJellyFish.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"d989af57-6ca7-41f2-9a41-dcb6e891a728","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"362889b1-73f2-4526-849a-ab4866ba346c","path":"sprites/sprInvertedTinyJellyFish/sprInvertedTinyJellyFish.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"6fd2c612-0ebe-4ce9-9823-7ddd61f5b62c","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"4ca53009-a2b6-43cc-89ad-8e9087516517","path":"sprites/sprInvertedTinyJellyFish/sprInvertedTinyJellyFish.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"75af98c6-4b96-4604-8a28-bdf7804fad0a","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"07b4e656-72d5-4eb5-8269-f18d52d41efb","path":"sprites/sprInvertedTinyJellyFish/sprInvertedTinyJellyFish.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"de5504bc-43c9-4482-89c5-9bb4725e83b6","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":6,
    "yorigin":8,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Oasis",
    "path":"texturegroups/Oasis",
  },
  "type":0,
  "VTile":false,
  "width":12,
}