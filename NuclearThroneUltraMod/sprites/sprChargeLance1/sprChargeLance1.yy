{
  "$GMSprite":"",
  "%Name":"sprChargeLance1",
  "bboxMode":1,
  "bbox_bottom":15,
  "bbox_left":0,
  "bbox_right":47,
  "bbox_top":0,
  "collisionKind":4,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"e42a5b2b-5f4f-4494-a258-8331d9f884c5","name":"e42a5b2b-5f4f-4494-a258-8331d9f884c5","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"b672741d-9645-48c2-b1d8-02324ec8e92e","name":"b672741d-9645-48c2-b1d8-02324ec8e92e","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"82a85649-0cf2-4823-bd41-7f48dfe994be","name":"82a85649-0cf2-4823-bd41-7f48dfe994be","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"041a8d1f-e8b8-4166-8047-74c1ea70e8a1","name":"041a8d1f-e8b8-4166-8047-74c1ea70e8a1","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"f268a44f-96e3-41f4-8ce3-92eebe3d6ac9","name":"f268a44f-96e3-41f4-8ce3-92eebe3d6ac9","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"37b96d24-b409-4864-9ce1-8a7418adc9c7","name":"37b96d24-b409-4864-9ce1-8a7418adc9c7","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"d1f83afc-f78a-4395-9793-bb2505f4b47b","name":"d1f83afc-f78a-4395-9793-bb2505f4b47b","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":16,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"4c8a2f2e-b6e0-4505-bb7f-aaaf0dfe1c32","blendMode":0,"displayName":"default","isLocked":false,"name":"4c8a2f2e-b6e0-4505-bb7f-aaaf0dfe1c32","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprChargeLance1",
  "nineSlice":null,
  "origin":9,
  "parent":{
    "name":"Custom",
    "path":"folders/Sprites/Weapons/Custom.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprChargeLance1",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":7.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprChargeLance1",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"e42a5b2b-5f4f-4494-a258-8331d9f884c5","path":"sprites/sprChargeLance1/sprChargeLance1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"87771723-6f4c-4d33-9080-077747f3ab66","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"b672741d-9645-48c2-b1d8-02324ec8e92e","path":"sprites/sprChargeLance1/sprChargeLance1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"20346c08-f9a8-4cfc-a779-8c08f2712261","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"82a85649-0cf2-4823-bd41-7f48dfe994be","path":"sprites/sprChargeLance1/sprChargeLance1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"cff8ffa2-f6c8-43e1-9a51-54893727ee60","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"041a8d1f-e8b8-4166-8047-74c1ea70e8a1","path":"sprites/sprChargeLance1/sprChargeLance1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"228b6d3e-f883-46dc-a424-6bb3e37420e6","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"f268a44f-96e3-41f4-8ce3-92eebe3d6ac9","path":"sprites/sprChargeLance1/sprChargeLance1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"af2c4b29-e274-4556-81ca-c0979304d0cf","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"37b96d24-b409-4864-9ce1-8a7418adc9c7","path":"sprites/sprChargeLance1/sprChargeLance1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f0e70ad3-9a77-49c7-827f-58f401f299e3","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"d1f83afc-f78a-4395-9793-bb2505f4b47b","path":"sprites/sprChargeLance1/sprChargeLance1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"fc04a454-d899-4f49-932d-24184aa2d14e","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":3,
    "yorigin":6,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Weapons",
    "path":"texturegroups/Weapons",
  },
  "type":0,
  "VTile":false,
  "width":48,
}