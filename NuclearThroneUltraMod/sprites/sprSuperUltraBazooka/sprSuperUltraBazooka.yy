{
  "$GMSprite":"",
  "%Name":"sprSuperUltraBazooka",
  "bboxMode":1,
  "bbox_bottom":23,
  "bbox_left":0,
  "bbox_right":47,
  "bbox_top":0,
  "collisionKind":4,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"18a8bd68-4b73-4c6a-be21-2c5478e14c1c","name":"18a8bd68-4b73-4c6a-be21-2c5478e14c1c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"7654babc-40d6-4de9-95b9-d39e129df0f6","name":"7654babc-40d6-4de9-95b9-d39e129df0f6","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"a62861fb-67ff-4165-a351-99b8cd0a4c12","name":"a62861fb-67ff-4165-a351-99b8cd0a4c12","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"fe9b0af3-6baa-41f9-bffc-0a2f590b429c","name":"fe9b0af3-6baa-41f9-bffc-0a2f590b429c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"f7e5e0bc-1a79-4928-ab9d-02ace33c3b03","name":"f7e5e0bc-1a79-4928-ab9d-02ace33c3b03","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"1ab2ad1d-dc00-4d26-b69b-a22f9de0c14a","name":"1ab2ad1d-dc00-4d26-b69b-a22f9de0c14a","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"019cbaaf-1eda-4308-9f82-7219e2497cd7","name":"019cbaaf-1eda-4308-9f82-7219e2497cd7","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":24,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"59187734-dedc-4373-a9e4-3f2546220588","blendMode":0,"displayName":"default","isLocked":false,"name":"59187734-dedc-4373-a9e4-3f2546220588","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprSuperUltraBazooka",
  "nineSlice":null,
  "origin":9,
  "parent":{
    "name":"UltraWeapons",
    "path":"folders/Sprites/Weapons/Custom/UltraWeapons.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprSuperUltraBazooka",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":7.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprSuperUltraBazooka",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"18a8bd68-4b73-4c6a-be21-2c5478e14c1c","path":"sprites/sprSuperUltraBazooka/sprSuperUltraBazooka.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"e4e431ba-1f49-4127-b4ae-e616f8aa7c27","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"7654babc-40d6-4de9-95b9-d39e129df0f6","path":"sprites/sprSuperUltraBazooka/sprSuperUltraBazooka.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"67299f07-e7ff-4bf2-8d3f-06a15b096b1e","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"a62861fb-67ff-4165-a351-99b8cd0a4c12","path":"sprites/sprSuperUltraBazooka/sprSuperUltraBazooka.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"a0ae36cc-de60-43e6-b595-ed7c125b5601","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"fe9b0af3-6baa-41f9-bffc-0a2f590b429c","path":"sprites/sprSuperUltraBazooka/sprSuperUltraBazooka.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"470df039-9dbf-4835-b2f0-9ffbb78d0154","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"f7e5e0bc-1a79-4928-ab9d-02ace33c3b03","path":"sprites/sprSuperUltraBazooka/sprSuperUltraBazooka.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"43e227b2-5aff-4cc7-aa1d-d6b8f9fa7f61","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"1ab2ad1d-dc00-4d26-b69b-a22f9de0c14a","path":"sprites/sprSuperUltraBazooka/sprSuperUltraBazooka.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"a021b7b3-3857-4982-a52f-39ad23c8133e","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"019cbaaf-1eda-4308-9f82-7219e2497cd7","path":"sprites/sprSuperUltraBazooka/sprSuperUltraBazooka.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"5a7d8420-c50f-475b-a874-47a4c14f5b70","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":15,
    "yorigin":8,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Weapons",
    "path":"texturegroups/Weapons",
  },
  "type":0,
  "VTile":false,
  "width":48,
}