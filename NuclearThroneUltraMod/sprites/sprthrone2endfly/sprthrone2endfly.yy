{
  "$GMSprite":"",
  "%Name":"sprThrone2EndFly",
  "bboxMode":0,
  "bbox_bottom":199,
  "bbox_left":97,
  "bbox_right":150,
  "bbox_top":64,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"f2dc5df3-49f9-466e-a8b5-a5b277b07a4d","name":"f2dc5df3-49f9-466e-a8b5-a5b277b07a4d","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"4960aa3e-db5c-47ab-90fc-b50de56f6527","name":"4960aa3e-db5c-47ab-90fc-b50de56f6527","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"513fe26a-87b3-499f-b52b-dafb5006883f","name":"513fe26a-87b3-499f-b52b-dafb5006883f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"a22840fc-5ed2-4b39-9a31-b6e7a2ade679","name":"a22840fc-5ed2-4b39-9a31-b6e7a2ade679","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"1a52f1d7-cbcc-49a3-9b21-95ab9b5cabb5","name":"1a52f1d7-cbcc-49a3-9b21-95ab9b5cabb5","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"b6ef03ff-1d15-4b69-bbc4-ecd4f86469b5","name":"b6ef03ff-1d15-4b69-bbc4-ecd4f86469b5","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"3f8e6cfd-127c-45b4-8b26-cb86d2948ffa","name":"3f8e6cfd-127c-45b4-8b26-cb86d2948ffa","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"dffb471f-07d6-4dee-bb43-31213ede07f5","name":"dffb471f-07d6-4dee-bb43-31213ede07f5","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"ea812ce3-b676-46b7-95fe-d26ac13c75b9","name":"ea812ce3-b676-46b7-95fe-d26ac13c75b9","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"dec0fee2-036d-4656-953c-6ba18cb06447","name":"dec0fee2-036d-4656-953c-6ba18cb06447","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":250,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"e0374256-6e4f-46d4-9e19-80e5fac46496","blendMode":0,"displayName":"default","isLocked":false,"name":"e0374256-6e4f-46d4-9e19-80e5fac46496","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprThrone2EndFly",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"TheThrone",
    "path":"folders/Sprites/Palace/Palace Enemy/TheThrone.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprThrone2EndFly",
    "autoRecord":true,
    "backdropHeight":768,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1366,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":10.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprThrone2EndFly",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"f2dc5df3-49f9-466e-a8b5-a5b277b07a4d","path":"sprites/sprThrone2EndFly/sprThrone2EndFly.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"175aee14-729d-4c03-bbae-43b90316d9f0","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"4960aa3e-db5c-47ab-90fc-b50de56f6527","path":"sprites/sprThrone2EndFly/sprThrone2EndFly.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"25cf8fe3-e802-4ff0-8708-19e058439238","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"513fe26a-87b3-499f-b52b-dafb5006883f","path":"sprites/sprThrone2EndFly/sprThrone2EndFly.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ca7a7e54-f028-4208-ad04-562dba7ba61a","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"a22840fc-5ed2-4b39-9a31-b6e7a2ade679","path":"sprites/sprThrone2EndFly/sprThrone2EndFly.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"7da8fc8f-d93e-40e5-b771-c4f31ead7eb6","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"1a52f1d7-cbcc-49a3-9b21-95ab9b5cabb5","path":"sprites/sprThrone2EndFly/sprThrone2EndFly.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"185873b0-6641-415b-9d9e-5a795b50c176","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"b6ef03ff-1d15-4b69-bbc4-ecd4f86469b5","path":"sprites/sprThrone2EndFly/sprThrone2EndFly.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"b583528f-3588-4716-8aa4-4a5379ea4cae","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"3f8e6cfd-127c-45b4-8b26-cb86d2948ffa","path":"sprites/sprThrone2EndFly/sprThrone2EndFly.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"c7e5e24d-4575-4933-b568-e12501076ce4","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"dffb471f-07d6-4dee-bb43-31213ede07f5","path":"sprites/sprThrone2EndFly/sprThrone2EndFly.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"9ba8a383-8ac7-4e09-ba7a-46b2166fc43e","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"ea812ce3-b676-46b7-95fe-d26ac13c75b9","path":"sprites/sprThrone2EndFly/sprThrone2EndFly.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f561340f-0dc8-47ce-9d95-cdbadd1f01f3","IsCreationKey":false,"Key":8.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"dec0fee2-036d-4656-953c-6ba18cb06447","path":"sprites/sprThrone2EndFly/sprThrone2EndFly.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"134fc455-596f-43c2-9507-5dcf46435c81","IsCreationKey":false,"Key":9.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":128,
    "yorigin":125,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Palace",
    "path":"texturegroups/Palace",
  },
  "type":0,
  "VTile":false,
  "width":256,
}