{
  "$GMSprite":"",
  "%Name":"sprInvertedTerminal",
  "bboxMode":1,
  "bbox_bottom":23,
  "bbox_left":0,
  "bbox_right":23,
  "bbox_top":0,
  "collisionKind":0,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"9d6d6510-4f41-408e-b799-265ddd8a27bd","name":"9d6d6510-4f41-408e-b799-265ddd8a27bd","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"889f7d4e-41b8-4f1e-b5d5-6b893fba68a3","name":"889f7d4e-41b8-4f1e-b5d5-6b893fba68a3","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"1d86231b-0a3a-44d4-922e-e9b8629a7f9b","name":"1d86231b-0a3a-44d4-922e-e9b8629a7f9b","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"86b89413-d7db-44c0-83ef-eb80e1f060a2","name":"86b89413-d7db-44c0-83ef-eb80e1f060a2","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"8c39bcdf-8bf7-4cc1-8624-6b11dbaf64e1","name":"8c39bcdf-8bf7-4cc1-8624-6b11dbaf64e1","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"5f7134b3-875d-4ba5-8492-afdd9d6a6f8e","name":"5f7134b3-875d-4ba5-8492-afdd9d6a6f8e","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"f8c464c0-3691-4379-8baa-4dc79b6d8d9e","name":"f8c464c0-3691-4379-8baa-4dc79b6d8d9e","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"fcdf2478-fe23-44ab-8898-c4c6569b2959","name":"fcdf2478-fe23-44ab-8898-c4c6569b2959","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"a6b91e6a-a699-40e7-a478-40b4c3fb89ab","name":"a6b91e6a-a699-40e7-a478-40b4c3fb89ab","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"9160253c-c9e4-430d-8541-22045076e1b1","name":"9160253c-c9e4-430d-8541-22045076e1b1","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"6055dc52-4f8b-4fd7-874a-d891b6df7a56","name":"6055dc52-4f8b-4fd7-874a-d891b6df7a56","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"f5964162-6ba2-45e8-80b7-1c346d8eeb60","name":"f5964162-6ba2-45e8-80b7-1c346d8eeb60","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"e825798d-69ab-4768-a9d7-9bb2e588dfca","name":"e825798d-69ab-4768-a9d7-9bb2e588dfca","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"a0bfb330-8804-496f-a521-3085617061ad","name":"a0bfb330-8804-496f-a521-3085617061ad","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"7799e141-3a35-413e-aa12-d541f3a37aaf","name":"7799e141-3a35-413e-aa12-d541f3a37aaf","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"6d36805e-91ff-490f-af2d-e61a4c04579b","name":"6d36805e-91ff-490f-af2d-e61a4c04579b","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"5b7d8301-0076-4901-93c6-9b2eaa0017b5","name":"5b7d8301-0076-4901-93c6-9b2eaa0017b5","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"ff957a3b-938f-4357-ac28-f9cde598bc03","name":"ff957a3b-938f-4357-ac28-f9cde598bc03","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"b4cb9463-4cf3-4760-bc4c-555f0b8784c6","name":"b4cb9463-4cf3-4760-bc4c-555f0b8784c6","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"29c0f23d-4996-43d0-82d4-ce6402d8ac6f","name":"29c0f23d-4996-43d0-82d4-ce6402d8ac6f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"0a3bc4bb-01bf-4dcc-9e08-f4d5b70d962e","name":"0a3bc4bb-01bf-4dcc-9e08-f4d5b70d962e","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"fcd8d791-db4f-4dd6-be67-23b67bc18e52","name":"fcd8d791-db4f-4dd6-be67-23b67bc18e52","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"05909406-4d56-4a05-8674-0f331c349b62","name":"05909406-4d56-4a05-8674-0f331c349b62","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"2bafc49b-34dd-4c2b-abe4-4be2f5934067","name":"2bafc49b-34dd-4c2b-abe4-4be2f5934067","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":24,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"8145cfb4-4152-451d-a735-b7636279b8cc","blendMode":0,"displayName":"default","isLocked":false,"name":"8145cfb4-4152-451d-a735-b7636279b8cc","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprInvertedTerminal",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"LabsProps",
    "path":"folders/Sprites/Enviroment/Props/LabsProps.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":24.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"9d6d6510-4f41-408e-b799-265ddd8a27bd","path":"sprites/sprInvertedTerminal/sprInvertedTerminal.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"de36b532-a6c8-49be-a441-73de223bbe60","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"889f7d4e-41b8-4f1e-b5d5-6b893fba68a3","path":"sprites/sprInvertedTerminal/sprInvertedTerminal.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"fb4b8065-2742-4a80-b4d4-48adad506c25","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"1d86231b-0a3a-44d4-922e-e9b8629a7f9b","path":"sprites/sprInvertedTerminal/sprInvertedTerminal.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ad826563-4a26-4728-ac55-864fc3da8c77","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"86b89413-d7db-44c0-83ef-eb80e1f060a2","path":"sprites/sprInvertedTerminal/sprInvertedTerminal.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"13eab4fc-e33d-4193-b6c2-3fa9e189f745","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"8c39bcdf-8bf7-4cc1-8624-6b11dbaf64e1","path":"sprites/sprInvertedTerminal/sprInvertedTerminal.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"6d9516e5-e823-4903-8b91-55df2ac9f58f","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"5f7134b3-875d-4ba5-8492-afdd9d6a6f8e","path":"sprites/sprInvertedTerminal/sprInvertedTerminal.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"a7f25ed1-e4f3-4ba6-96fd-755e69ad534e","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"f8c464c0-3691-4379-8baa-4dc79b6d8d9e","path":"sprites/sprInvertedTerminal/sprInvertedTerminal.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"9ce8980a-8637-49f4-aad3-fdb01dd26e71","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"fcdf2478-fe23-44ab-8898-c4c6569b2959","path":"sprites/sprInvertedTerminal/sprInvertedTerminal.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"12cd6629-6701-4238-b965-bda344da10f8","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"a6b91e6a-a699-40e7-a478-40b4c3fb89ab","path":"sprites/sprInvertedTerminal/sprInvertedTerminal.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"8cef24c6-286d-420a-bbb4-94007f0327bd","IsCreationKey":false,"Key":8.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"9160253c-c9e4-430d-8541-22045076e1b1","path":"sprites/sprInvertedTerminal/sprInvertedTerminal.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"2da0f484-2487-4ce3-835e-9a1e4d454995","IsCreationKey":false,"Key":9.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"6055dc52-4f8b-4fd7-874a-d891b6df7a56","path":"sprites/sprInvertedTerminal/sprInvertedTerminal.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"8dff437d-4aca-474f-9416-a300ad1d0abf","IsCreationKey":false,"Key":10.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"f5964162-6ba2-45e8-80b7-1c346d8eeb60","path":"sprites/sprInvertedTerminal/sprInvertedTerminal.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"44358023-f90a-49d5-bff2-f896685364dc","IsCreationKey":false,"Key":11.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"e825798d-69ab-4768-a9d7-9bb2e588dfca","path":"sprites/sprInvertedTerminal/sprInvertedTerminal.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"231dd432-1de7-43a8-baf9-dd8c6cc680d2","IsCreationKey":false,"Key":12.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"a0bfb330-8804-496f-a521-3085617061ad","path":"sprites/sprInvertedTerminal/sprInvertedTerminal.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"30aa925a-856d-42ac-98a8-92a7b6ece6bd","IsCreationKey":false,"Key":13.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"7799e141-3a35-413e-aa12-d541f3a37aaf","path":"sprites/sprInvertedTerminal/sprInvertedTerminal.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"de865564-44ae-4c77-8cb7-d0970a664ec3","IsCreationKey":false,"Key":14.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"6d36805e-91ff-490f-af2d-e61a4c04579b","path":"sprites/sprInvertedTerminal/sprInvertedTerminal.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"54ed6184-76b5-497a-ac76-44df30bfc03c","IsCreationKey":false,"Key":15.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"5b7d8301-0076-4901-93c6-9b2eaa0017b5","path":"sprites/sprInvertedTerminal/sprInvertedTerminal.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"4e2a3cbd-2480-4ece-bb7d-d6fa80248e72","IsCreationKey":false,"Key":16.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"ff957a3b-938f-4357-ac28-f9cde598bc03","path":"sprites/sprInvertedTerminal/sprInvertedTerminal.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f942b6df-9564-4959-be3f-c49b5f2d5427","IsCreationKey":false,"Key":17.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"b4cb9463-4cf3-4760-bc4c-555f0b8784c6","path":"sprites/sprInvertedTerminal/sprInvertedTerminal.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"c78fe3e4-1990-4200-82ed-84a953c470cf","IsCreationKey":false,"Key":18.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"29c0f23d-4996-43d0-82d4-ce6402d8ac6f","path":"sprites/sprInvertedTerminal/sprInvertedTerminal.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"317577c4-5cc9-4898-9ca8-2a1987440d34","IsCreationKey":false,"Key":19.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"0a3bc4bb-01bf-4dcc-9e08-f4d5b70d962e","path":"sprites/sprInvertedTerminal/sprInvertedTerminal.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"fd8b9797-531c-42c7-829f-62ba7fcccae6","IsCreationKey":false,"Key":20.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"fcd8d791-db4f-4dd6-be67-23b67bc18e52","path":"sprites/sprInvertedTerminal/sprInvertedTerminal.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"95286f4f-695a-4f13-9d6e-9ee7b9906cd8","IsCreationKey":false,"Key":21.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"05909406-4d56-4a05-8674-0f331c349b62","path":"sprites/sprInvertedTerminal/sprInvertedTerminal.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"5530ae32-d7cf-4beb-8015-bdcb360a64e1","IsCreationKey":false,"Key":22.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"2bafc49b-34dd-4c2b-abe4-4be2f5934067","path":"sprites/sprInvertedTerminal/sprInvertedTerminal.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"d2d29517-a085-4bc1-a2d5-797a3366d0b6","IsCreationKey":false,"Key":23.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":{
      "x":0.0,
      "y":0.0,
    },
    "volume":1.0,
    "xorigin":12,
    "yorigin":12,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Labs",
    "path":"texturegroups/Labs",
  },
  "type":0,
  "VTile":false,
  "width":24,
}