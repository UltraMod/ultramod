{
  "$GMSprite":"",
  "%Name":"sprInvertedChesireCatIdle",
  "bboxMode":0,
  "bbox_bottom":57,
  "bbox_left":6,
  "bbox_right":53,
  "bbox_top":5,
  "collisionKind":0,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"80ea75ca-67cd-458b-98da-46faaf88d369","name":"80ea75ca-67cd-458b-98da-46faaf88d369","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"7a07f7d2-cd97-4833-9111-35bf7f4b336a","name":"7a07f7d2-cd97-4833-9111-35bf7f4b336a","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"55e92e6c-c38c-4987-be49-f2a2e9bd88b2","name":"55e92e6c-c38c-4987-be49-f2a2e9bd88b2","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"9470266c-698b-43cd-97c9-3c057a790bf7","name":"9470266c-698b-43cd-97c9-3c057a790bf7","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"bc8f21a9-a133-47e6-af58-313b5cdba173","name":"bc8f21a9-a133-47e6-af58-313b5cdba173","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"3d628749-1e26-4cbf-bc1b-3afe5055918d","name":"3d628749-1e26-4cbf-bc1b-3afe5055918d","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":60,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"82732317-6be0-46f6-a85f-d229d244151f","blendMode":0,"displayName":"default","isLocked":false,"name":"82732317-6be0-46f6-a85f-d229d244151f","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprInvertedChesireCatIdle",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"FlyingCatHead",
    "path":"folders/Sprites/Enemies/Boss/FlyingCatHead.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":6.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"80ea75ca-67cd-458b-98da-46faaf88d369","path":"sprites/sprInvertedChesireCatIdle/sprInvertedChesireCatIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"163bc8a6-fae1-4e25-b966-f4aaf286f67f","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"7a07f7d2-cd97-4833-9111-35bf7f4b336a","path":"sprites/sprInvertedChesireCatIdle/sprInvertedChesireCatIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ed8be31c-2085-4f93-842c-8bf9898a1e9a","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"55e92e6c-c38c-4987-be49-f2a2e9bd88b2","path":"sprites/sprInvertedChesireCatIdle/sprInvertedChesireCatIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"4105b9fb-4938-458b-9489-fcbc5e958609","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"9470266c-698b-43cd-97c9-3c057a790bf7","path":"sprites/sprInvertedChesireCatIdle/sprInvertedChesireCatIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"c4b61c40-f34d-4cf7-9eef-cd3f74bf0f9b","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"bc8f21a9-a133-47e6-af58-313b5cdba173","path":"sprites/sprInvertedChesireCatIdle/sprInvertedChesireCatIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"6755188c-c149-4483-8a3d-ce304d306b62","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"3d628749-1e26-4cbf-bc1b-3afe5055918d","path":"sprites/sprInvertedChesireCatIdle/sprInvertedChesireCatIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"73faf14d-c32d-4514-a327-bb392f42fd10","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":30,
    "yorigin":30,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Wonderland",
    "path":"texturegroups/Wonderland",
  },
  "type":0,
  "VTile":false,
  "width":60,
}