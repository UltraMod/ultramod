{
  "$GMSprite":"",
  "%Name":"sprAllUltramodbosses",
  "bboxMode":0,
  "bbox_bottom":237,
  "bbox_left":0,
  "bbox_right":348,
  "bbox_top":0,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"c70a818d-a4d2-4199-aa22-ef14582a32fe","name":"c70a818d-a4d2-4199-aa22-ef14582a32fe","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"7d9b556f-c3e1-479a-956c-fbe8b9b9aba6","name":"7d9b556f-c3e1-479a-956c-fbe8b9b9aba6","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"06f2eb38-696a-4983-a25f-c0f64f733832","name":"06f2eb38-696a-4983-a25f-c0f64f733832","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"7ad3db2a-e7e7-4dc2-accc-493775090f86","name":"7ad3db2a-e7e7-4dc2-accc-493775090f86","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"8b452733-14c1-4de4-bea7-5ba123469af7","name":"8b452733-14c1-4de4-bea7-5ba123469af7","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"f029b249-2b90-465f-bbd2-16d5ab52ba7c","name":"f029b249-2b90-465f-bbd2-16d5ab52ba7c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"1ef925e8-bfcd-4807-9d5b-20515bc68a3e","name":"1ef925e8-bfcd-4807-9d5b-20515bc68a3e","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"dd3854bb-3df9-4c3a-b132-84a17b5076db","name":"dd3854bb-3df9-4c3a-b132-84a17b5076db","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"4b9f98a0-58f6-453d-8284-ebe24054318c","name":"4b9f98a0-58f6-453d-8284-ebe24054318c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"d282fe54-988f-4dae-9495-42c9cb7f595a","name":"d282fe54-988f-4dae-9495-42c9cb7f595a","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"0f2c76c9-b6ad-4a96-bc29-f46a59380f64","name":"0f2c76c9-b6ad-4a96-bc29-f46a59380f64","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"69d4c6c5-b0a2-4074-9869-fa75dfcc896e","name":"69d4c6c5-b0a2-4074-9869-fa75dfcc896e","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"84051300-8116-4bce-99e9-70e51382493a","name":"84051300-8116-4bce-99e9-70e51382493a","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"df98376d-9b87-412c-a38d-39b5cffff667","name":"df98376d-9b87-412c-a38d-39b5cffff667","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"58250915-4f60-4f41-bcc4-9247f45ab257","name":"58250915-4f60-4f41-bcc4-9247f45ab257","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"92d58e0f-176d-4d07-be09-f11235017850","name":"92d58e0f-176d-4d07-be09-f11235017850","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"28b33f80-8432-4608-a0d8-98a64f303034","name":"28b33f80-8432-4608-a0d8-98a64f303034","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"f03641f1-6559-4bda-9cfd-396f66afd0ba","name":"f03641f1-6559-4bda-9cfd-396f66afd0ba","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"56931027-5e0a-4d0f-944b-883e8bf7c495","name":"56931027-5e0a-4d0f-944b-883e8bf7c495","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"30341068-4e6e-46d6-8760-08b01be0c3ef","name":"30341068-4e6e-46d6-8760-08b01be0c3ef","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"d4418539-7abc-40c5-813d-05147fcddf7d","name":"d4418539-7abc-40c5-813d-05147fcddf7d","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"d81eb189-3ff7-4e71-bf09-b0cf27157733","name":"d81eb189-3ff7-4e71-bf09-b0cf27157733","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"8bf87e13-c130-4bfd-8a5c-a7d43df199ee","name":"8bf87e13-c130-4bfd-8a5c-a7d43df199ee","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"8b90179b-0b3f-45b2-b578-f049b103d859","name":"8b90179b-0b3f-45b2-b578-f049b103d859","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"a997ec94-e46e-4644-bc3b-a2bd8f574eae","name":"a997ec94-e46e-4644-bc3b-a2bd8f574eae","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":240,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"e9fc15fc-f13b-49e3-8aef-63372687f6f8","blendMode":0,"displayName":"default","isLocked":false,"name":"e9fc15fc-f13b-49e3-8aef-63372687f6f8","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprAllUltramodbosses",
  "nineSlice":null,
  "origin":9,
  "parent":{
    "name":"Boss",
    "path":"folders/Sprites/Enemies/Boss.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprAllUltramodbosses",
    "autoRecord":true,
    "backdropHeight":768,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1366,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":25.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprAllUltramodbosses",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c70a818d-a4d2-4199-aa22-ef14582a32fe","path":"sprites/sprAllUltramodbosses/sprAllUltramodbosses.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"5106a5c4-7815-4a4a-8df1-94cbfa7bc1d2","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"7d9b556f-c3e1-479a-956c-fbe8b9b9aba6","path":"sprites/sprAllUltramodbosses/sprAllUltramodbosses.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"b35fb593-9cc6-4399-80e1-83958385169b","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"06f2eb38-696a-4983-a25f-c0f64f733832","path":"sprites/sprAllUltramodbosses/sprAllUltramodbosses.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"92e808d1-afee-450e-86a8-07ec31d60318","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"7ad3db2a-e7e7-4dc2-accc-493775090f86","path":"sprites/sprAllUltramodbosses/sprAllUltramodbosses.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"d1077572-2689-47f7-abcf-6c7ddd54cc39","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"8b452733-14c1-4de4-bea7-5ba123469af7","path":"sprites/sprAllUltramodbosses/sprAllUltramodbosses.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"0677a1f6-47d4-4aaa-b7d4-3bdb79e748a3","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"f029b249-2b90-465f-bbd2-16d5ab52ba7c","path":"sprites/sprAllUltramodbosses/sprAllUltramodbosses.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"a23af0ea-5e04-4aca-b650-042c9cdeba9a","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"1ef925e8-bfcd-4807-9d5b-20515bc68a3e","path":"sprites/sprAllUltramodbosses/sprAllUltramodbosses.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"90094048-c5ed-44db-a69c-071b4767c7f8","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"dd3854bb-3df9-4c3a-b132-84a17b5076db","path":"sprites/sprAllUltramodbosses/sprAllUltramodbosses.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"fac6dd7b-82d6-49dc-b19e-117681f27291","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"4b9f98a0-58f6-453d-8284-ebe24054318c","path":"sprites/sprAllUltramodbosses/sprAllUltramodbosses.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"48c1669e-40d5-471e-92f5-a2b29bc9fe09","IsCreationKey":false,"Key":8.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"d282fe54-988f-4dae-9495-42c9cb7f595a","path":"sprites/sprAllUltramodbosses/sprAllUltramodbosses.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ffc0de69-99de-4da8-ad78-c987b9bc5c5a","IsCreationKey":false,"Key":9.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"0f2c76c9-b6ad-4a96-bc29-f46a59380f64","path":"sprites/sprAllUltramodbosses/sprAllUltramodbosses.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"e5af5a11-e28c-4226-afaa-740d73948385","IsCreationKey":false,"Key":10.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"69d4c6c5-b0a2-4074-9869-fa75dfcc896e","path":"sprites/sprAllUltramodbosses/sprAllUltramodbosses.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"20411393-1dd0-4310-8a7c-9b0ae8f7192b","IsCreationKey":false,"Key":11.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"84051300-8116-4bce-99e9-70e51382493a","path":"sprites/sprAllUltramodbosses/sprAllUltramodbosses.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"77f4e75e-e4f6-4bf9-a028-5d23afc6d50e","IsCreationKey":false,"Key":12.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"df98376d-9b87-412c-a38d-39b5cffff667","path":"sprites/sprAllUltramodbosses/sprAllUltramodbosses.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"46145d89-973c-4696-b230-95653f6a9f53","IsCreationKey":false,"Key":13.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"58250915-4f60-4f41-bcc4-9247f45ab257","path":"sprites/sprAllUltramodbosses/sprAllUltramodbosses.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"0f99a2d2-48ec-4b77-86ca-b46e1b3c56eb","IsCreationKey":false,"Key":14.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"92d58e0f-176d-4d07-be09-f11235017850","path":"sprites/sprAllUltramodbosses/sprAllUltramodbosses.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"352c7411-083a-4ad6-9c5b-4f3aa8fcc21d","IsCreationKey":false,"Key":15.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"28b33f80-8432-4608-a0d8-98a64f303034","path":"sprites/sprAllUltramodbosses/sprAllUltramodbosses.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"2aa4a90d-ea25-4cb1-b123-b6a0a30d57d1","IsCreationKey":false,"Key":16.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"f03641f1-6559-4bda-9cfd-396f66afd0ba","path":"sprites/sprAllUltramodbosses/sprAllUltramodbosses.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"63eb3471-f93f-4ec7-bc33-a4eb8611482a","IsCreationKey":false,"Key":17.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"56931027-5e0a-4d0f-944b-883e8bf7c495","path":"sprites/sprAllUltramodbosses/sprAllUltramodbosses.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"d135a3fb-c8d9-4bf1-a0fe-6de4cbf739a4","IsCreationKey":false,"Key":18.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"30341068-4e6e-46d6-8760-08b01be0c3ef","path":"sprites/sprAllUltramodbosses/sprAllUltramodbosses.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"1236e224-2c21-424e-8751-a9489e67851c","IsCreationKey":false,"Key":19.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"d4418539-7abc-40c5-813d-05147fcddf7d","path":"sprites/sprAllUltramodbosses/sprAllUltramodbosses.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"92a61067-21bd-47b2-bfac-8d1b5f4a8740","IsCreationKey":false,"Key":20.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"d81eb189-3ff7-4e71-bf09-b0cf27157733","path":"sprites/sprAllUltramodbosses/sprAllUltramodbosses.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ddcbe29a-9ed4-4c22-a53a-29ff34b1dfae","IsCreationKey":false,"Key":21.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"8bf87e13-c130-4bfd-8a5c-a7d43df199ee","path":"sprites/sprAllUltramodbosses/sprAllUltramodbosses.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"6484753a-3a61-4c5d-9d07-2774c7e124ba","IsCreationKey":false,"Key":22.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"8b90179b-0b3f-45b2-b578-f049b103d859","path":"sprites/sprAllUltramodbosses/sprAllUltramodbosses.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ab062d8b-239b-4d94-8cea-13ccb9500c58","IsCreationKey":false,"Key":23.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"a997ec94-e46e-4644-bc3b-a2bd8f574eae","path":"sprites/sprAllUltramodbosses/sprAllUltramodbosses.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"5adedea5-c14e-41bb-aa13-65d46e9b0ff0","IsCreationKey":false,"Key":24.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":248,
    "yorigin":235,
  },
  "swatchColours":null,
  "swfPrecision":0.5,
  "textureGroupId":{
    "name":"Default",
    "path":"texturegroups/Default",
  },
  "type":0,
  "VTile":false,
  "width":492,
}