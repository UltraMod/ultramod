{
  "$GMSprite":"",
  "%Name":"sprRogueEliteShielderDead",
  "bboxMode":0,
  "bbox_bottom":22,
  "bbox_left":0,
  "bbox_right":23,
  "bbox_top":0,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"065f4920-ef51-47a6-85c7-c55b83d1d1d7","name":"065f4920-ef51-47a6-85c7-c55b83d1d1d7","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"37c2d7e9-5673-4439-a5e9-e217e389ac0f","name":"37c2d7e9-5673-4439-a5e9-e217e389ac0f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"4c916216-439a-44d9-8e0f-9e5705bdac2b","name":"4c916216-439a-44d9-8e0f-9e5705bdac2b","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"518acdea-668c-4ea5-9785-a0f61ebfb5a2","name":"518acdea-668c-4ea5-9785-a0f61ebfb5a2","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"6117b01f-be28-43e8-814f-93c95084fd96","name":"6117b01f-be28-43e8-814f-93c95084fd96","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"8beedcd1-faf7-4c87-806a-0ae1f111c7b5","name":"8beedcd1-faf7-4c87-806a-0ae1f111c7b5","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":24,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"06b0c026-fd0e-466f-b2f2-1a2975b5fb25","blendMode":0,"displayName":"default","isLocked":false,"name":"06b0c026-fd0e-466f-b2f2-1a2975b5fb25","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprRogueEliteShielderDead",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"EliteShielder",
    "path":"folders/Sprites/Enemies/IDPD/Elite IDPD/EliteShielder.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":6.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"065f4920-ef51-47a6-85c7-c55b83d1d1d7","path":"sprites/sprRogueEliteShielderDead/sprRogueEliteShielderDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"a966ae04-f21d-4e82-a5bb-aa0dd6d7f741","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"37c2d7e9-5673-4439-a5e9-e217e389ac0f","path":"sprites/sprRogueEliteShielderDead/sprRogueEliteShielderDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"cb7866fa-4ec3-4b35-a8f6-36a1845bf648","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"4c916216-439a-44d9-8e0f-9e5705bdac2b","path":"sprites/sprRogueEliteShielderDead/sprRogueEliteShielderDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"3365e662-9f2e-408c-8d9c-0cfcc12aa0a3","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"518acdea-668c-4ea5-9785-a0f61ebfb5a2","path":"sprites/sprRogueEliteShielderDead/sprRogueEliteShielderDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"256c43a2-69bf-4f9b-b67b-5c7a908a1120","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"6117b01f-be28-43e8-814f-93c95084fd96","path":"sprites/sprRogueEliteShielderDead/sprRogueEliteShielderDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"28992a02-3a9e-4a0d-9c1a-d3556ec2a9c1","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"8beedcd1-faf7-4c87-806a-0ae1f111c7b5","path":"sprites/sprRogueEliteShielderDead/sprRogueEliteShielderDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"9134db33-e55c-474c-a7d6-6e464978b4f1","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":{
      "x":0.0,
      "y":0.0,
    },
    "volume":1.0,
    "xorigin":12,
    "yorigin":12,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"IDPD",
    "path":"texturegroups/IDPD",
  },
  "type":0,
  "VTile":false,
  "width":24,
}