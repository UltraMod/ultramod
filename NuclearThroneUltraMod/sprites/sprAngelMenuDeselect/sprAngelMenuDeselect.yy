{
  "$GMSprite":"",
  "%Name":"sprAngelMenuDeselect",
  "bboxMode":0,
  "bbox_bottom":47,
  "bbox_left":0,
  "bbox_right":30,
  "bbox_top":1,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"ecbc6531-2e76-4e96-938e-0d8f11f58a4c","name":"ecbc6531-2e76-4e96-938e-0d8f11f58a4c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"8ff7881b-824a-4ad9-b22c-4dfb247d243d","name":"8ff7881b-824a-4ad9-b22c-4dfb247d243d","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"861b0681-e2fe-493a-ba2e-eacec6db2fa1","name":"861b0681-e2fe-493a-ba2e-eacec6db2fa1","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"8544b115-8a25-4aca-b40d-1b018815d8f9","name":"8544b115-8a25-4aca-b40d-1b018815d8f9","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"4ec617c3-7379-4f78-b52f-7ed7831254fe","name":"4ec617c3-7379-4f78-b52f-7ed7831254fe","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"ea8247f6-87c8-4646-b067-786bcb58d4b9","name":"ea8247f6-87c8-4646-b067-786bcb58d4b9","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"b5f7bc85-fd06-4d85-8226-4eee7950ee3f","name":"b5f7bc85-fd06-4d85-8226-4eee7950ee3f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"69171170-b583-4910-b8fb-96ec7289c5d3","name":"69171170-b583-4910-b8fb-96ec7289c5d3","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"23a96890-e0f9-4928-8775-326712f23c1a","name":"23a96890-e0f9-4928-8775-326712f23c1a","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"dc814ad8-bde0-4338-9d1b-49abfdff7425","name":"dc814ad8-bde0-4338-9d1b-49abfdff7425","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"1dcb6f65-083b-4dd5-a109-80cc7672e04c","name":"1dcb6f65-083b-4dd5-a109-80cc7672e04c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"03b1a12c-d354-4c48-a6f9-0c54b649314f","name":"03b1a12c-d354-4c48-a6f9-0c54b649314f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"09bc1aa0-33d2-453e-b6df-e7fdbd68b62e","name":"09bc1aa0-33d2-453e-b6df-e7fdbd68b62e","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"e46c2346-2c4b-4ebe-b2b0-beb0c2172208","name":"e46c2346-2c4b-4ebe-b2b0-beb0c2172208","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"c51672bc-e068-4548-bd5e-4503db71d605","name":"c51672bc-e068-4548-bd5e-4503db71d605","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"96771b3c-723d-4292-bad5-4d1c702151d7","name":"96771b3c-723d-4292-bad5-4d1c702151d7","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"6bf3f6a9-52f3-42d7-9ec3-f84a044bddc3","name":"6bf3f6a9-52f3-42d7-9ec3-f84a044bddc3","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"6e9ab05c-f536-4231-a1b5-86c4188f334a","name":"6e9ab05c-f536-4231-a1b5-86c4188f334a","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":48,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"11b73af4-8b8f-4047-81d1-55c4880c5281","blendMode":0,"displayName":"default","isLocked":false,"name":"11b73af4-8b8f-4047-81d1-55c4880c5281","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprAngelMenuDeselect",
  "nineSlice":null,
  "origin":9,
  "parent":{
    "name":"MenuChar",
    "path":"folders/Sprites/Menu/MenuChar.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":18.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"ecbc6531-2e76-4e96-938e-0d8f11f58a4c","path":"sprites/sprAngelMenuDeselect/sprAngelMenuDeselect.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"6febee8a-286c-476b-bdd3-60bfb9a25f77","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"8ff7881b-824a-4ad9-b22c-4dfb247d243d","path":"sprites/sprAngelMenuDeselect/sprAngelMenuDeselect.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"728e064d-a5ff-446c-9149-94644c487d97","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"861b0681-e2fe-493a-ba2e-eacec6db2fa1","path":"sprites/sprAngelMenuDeselect/sprAngelMenuDeselect.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"745ef3e0-ff5f-4e66-afda-b962d1d0c1a4","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"8544b115-8a25-4aca-b40d-1b018815d8f9","path":"sprites/sprAngelMenuDeselect/sprAngelMenuDeselect.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"c87cf32a-0ac1-45d7-8858-7fe4357084fc","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"4ec617c3-7379-4f78-b52f-7ed7831254fe","path":"sprites/sprAngelMenuDeselect/sprAngelMenuDeselect.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ea63429f-b29e-4677-9c51-d8be586c810a","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"ea8247f6-87c8-4646-b067-786bcb58d4b9","path":"sprites/sprAngelMenuDeselect/sprAngelMenuDeselect.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"4edadb9f-d19f-469f-8829-0950fb6a95e5","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"b5f7bc85-fd06-4d85-8226-4eee7950ee3f","path":"sprites/sprAngelMenuDeselect/sprAngelMenuDeselect.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"253f6ef7-fe62-45ce-a0df-21eb87da9e28","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"69171170-b583-4910-b8fb-96ec7289c5d3","path":"sprites/sprAngelMenuDeselect/sprAngelMenuDeselect.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"6058a104-e1e4-471d-ba16-c97c91865bf3","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"23a96890-e0f9-4928-8775-326712f23c1a","path":"sprites/sprAngelMenuDeselect/sprAngelMenuDeselect.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"a8582e6d-a9ec-4223-a940-098d12e76b19","IsCreationKey":false,"Key":8.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"dc814ad8-bde0-4338-9d1b-49abfdff7425","path":"sprites/sprAngelMenuDeselect/sprAngelMenuDeselect.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"367720a6-6fd1-4b7c-9b34-b8959565d814","IsCreationKey":false,"Key":9.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"1dcb6f65-083b-4dd5-a109-80cc7672e04c","path":"sprites/sprAngelMenuDeselect/sprAngelMenuDeselect.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"804d906d-3eef-48f2-9c74-0e61cf0e6cf0","IsCreationKey":false,"Key":10.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"03b1a12c-d354-4c48-a6f9-0c54b649314f","path":"sprites/sprAngelMenuDeselect/sprAngelMenuDeselect.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ceba9c85-fa93-4c20-adc6-ba85e8d1396a","IsCreationKey":false,"Key":11.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"09bc1aa0-33d2-453e-b6df-e7fdbd68b62e","path":"sprites/sprAngelMenuDeselect/sprAngelMenuDeselect.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"7bb6cd25-e7f6-44ee-9328-ea30444b5095","IsCreationKey":false,"Key":12.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"e46c2346-2c4b-4ebe-b2b0-beb0c2172208","path":"sprites/sprAngelMenuDeselect/sprAngelMenuDeselect.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"bf4da278-1f94-4607-ab19-b0b53fd58075","IsCreationKey":false,"Key":13.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c51672bc-e068-4548-bd5e-4503db71d605","path":"sprites/sprAngelMenuDeselect/sprAngelMenuDeselect.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"caa77fe7-ce14-408f-a006-9f454e8c68a7","IsCreationKey":false,"Key":14.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"96771b3c-723d-4292-bad5-4d1c702151d7","path":"sprites/sprAngelMenuDeselect/sprAngelMenuDeselect.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f6d1bc82-e9f9-4b9f-a7ee-8a08fe29fad5","IsCreationKey":false,"Key":15.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"6bf3f6a9-52f3-42d7-9ec3-f84a044bddc3","path":"sprites/sprAngelMenuDeselect/sprAngelMenuDeselect.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"e120b547-88ec-48e0-a722-a767efe76197","IsCreationKey":false,"Key":16.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"6e9ab05c-f536-4231-a1b5-86c4188f334a","path":"sprites/sprAngelMenuDeselect/sprAngelMenuDeselect.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"bd4e9eba-8e49-4b48-9bcd-1c09162953e6","IsCreationKey":false,"Key":17.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":{
      "x":0.0,
      "y":0.0,
    },
    "volume":1.0,
    "xorigin":15,
    "yorigin":40,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"PlayerCharacter",
    "path":"texturegroups/PlayerCharacter",
  },
  "type":0,
  "VTile":false,
  "width":31,
}