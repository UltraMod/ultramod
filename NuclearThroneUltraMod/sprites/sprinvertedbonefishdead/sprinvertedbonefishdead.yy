{
  "$GMSprite":"",
  "%Name":"sprInvertedBoneFishDead",
  "bboxMode":0,
  "bbox_bottom":21,
  "bbox_left":0,
  "bbox_right":22,
  "bbox_top":0,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"847e764c-d0d0-4f18-b331-e6a09f21e023","name":"847e764c-d0d0-4f18-b331-e6a09f21e023","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"57b59da8-7d41-4a72-9438-10a80c81d4b1","name":"57b59da8-7d41-4a72-9438-10a80c81d4b1","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"81492b11-1565-4f32-baec-e3f7d6f0d216","name":"81492b11-1565-4f32-baec-e3f7d6f0d216","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"3a8c5729-4328-40b8-b9ee-a344e5947883","name":"3a8c5729-4328-40b8-b9ee-a344e5947883","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"a415af3a-d2d9-483d-9740-996be5ec55c4","name":"a415af3a-d2d9-483d-9740-996be5ec55c4","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"7f1a7463-2a60-4a76-ac9a-5fea080384f9","name":"7f1a7463-2a60-4a76-ac9a-5fea080384f9","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"b7120ec8-5e69-44e4-bcf5-40bb9fbd955d","name":"b7120ec8-5e69-44e4-bcf5-40bb9fbd955d","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"387e7e7f-f782-4e6c-b770-e167297fbc3f","name":"387e7e7f-f782-4e6c-b770-e167297fbc3f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"3df27c9c-78c1-48d5-be76-3ab9c13c7b84","name":"3df27c9c-78c1-48d5-be76-3ab9c13c7b84","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"087db570-6a71-416c-8e48-a2ec9003bb7c","name":"087db570-6a71-416c-8e48-a2ec9003bb7c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"f65f7e10-4793-4dbb-a0b2-e1aea83f201d","name":"f65f7e10-4793-4dbb-a0b2-e1aea83f201d","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":24,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"cf5c93a7-ce1a-49ff-bd9a-bd6a72c7b257","blendMode":0,"displayName":"default","isLocked":false,"name":"cf5c93a7-ce1a-49ff-bd9a-bd6a72c7b257","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprInvertedBoneFishDead",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"Oasis",
    "path":"folders/Sprites/Enemies/Oasis.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprInvertedBoneFishDead",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":11.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprInvertedBoneFishDead",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"847e764c-d0d0-4f18-b331-e6a09f21e023","path":"sprites/sprInvertedBoneFishDead/sprInvertedBoneFishDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"8931a2b8-d7d1-4cf7-84a7-8d2ad0c7199e","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"57b59da8-7d41-4a72-9438-10a80c81d4b1","path":"sprites/sprInvertedBoneFishDead/sprInvertedBoneFishDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"d25691d5-9366-45c1-92fb-657a172bd7df","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"81492b11-1565-4f32-baec-e3f7d6f0d216","path":"sprites/sprInvertedBoneFishDead/sprInvertedBoneFishDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"41dc3f3a-a340-472e-974a-9ef95c977ef1","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"3a8c5729-4328-40b8-b9ee-a344e5947883","path":"sprites/sprInvertedBoneFishDead/sprInvertedBoneFishDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"a6b37804-422b-4243-b792-e9074d946684","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"a415af3a-d2d9-483d-9740-996be5ec55c4","path":"sprites/sprInvertedBoneFishDead/sprInvertedBoneFishDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"0ebd3df1-bb2c-44c7-9b50-59c7ed221670","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"7f1a7463-2a60-4a76-ac9a-5fea080384f9","path":"sprites/sprInvertedBoneFishDead/sprInvertedBoneFishDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"591645d4-c2ab-4370-b59c-2d1a64565576","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"b7120ec8-5e69-44e4-bcf5-40bb9fbd955d","path":"sprites/sprInvertedBoneFishDead/sprInvertedBoneFishDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"1d5c6f07-a7b1-48ba-bd76-bff6acf479d8","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"387e7e7f-f782-4e6c-b770-e167297fbc3f","path":"sprites/sprInvertedBoneFishDead/sprInvertedBoneFishDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"6ca56bcf-4e3b-48f2-a418-d1eb5027cb8b","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"3df27c9c-78c1-48d5-be76-3ab9c13c7b84","path":"sprites/sprInvertedBoneFishDead/sprInvertedBoneFishDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"3a56dfe1-8a03-4e14-a20f-875fb2445883","IsCreationKey":false,"Key":8.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"087db570-6a71-416c-8e48-a2ec9003bb7c","path":"sprites/sprInvertedBoneFishDead/sprInvertedBoneFishDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"95ada5e6-27e9-4707-8595-408456bad36a","IsCreationKey":false,"Key":9.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"f65f7e10-4793-4dbb-a0b2-e1aea83f201d","path":"sprites/sprInvertedBoneFishDead/sprInvertedBoneFishDead.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"3078c348-da4a-4993-8d12-19e5585559a5","IsCreationKey":false,"Key":10.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":12,
    "yorigin":12,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Oasis",
    "path":"texturegroups/Oasis",
  },
  "type":0,
  "VTile":false,
  "width":24,
}