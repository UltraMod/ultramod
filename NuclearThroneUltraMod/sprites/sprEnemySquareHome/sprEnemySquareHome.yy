{
  "$GMSprite":"",
  "%Name":"sprEnemySquareHome",
  "bboxMode":0,
  "bbox_bottom":14,
  "bbox_left":0,
  "bbox_right":15,
  "bbox_top":0,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"6dae2556-5cd7-4b42-82bb-d9f2b2006f44","name":"6dae2556-5cd7-4b42-82bb-d9f2b2006f44","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"24adef6e-6738-4905-af41-b2f246561a39","name":"24adef6e-6738-4905-af41-b2f246561a39","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"ec9bb645-f2b2-4e87-b57a-2c213b80e2cb","name":"ec9bb645-f2b2-4e87-b57a-2c213b80e2cb","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"126595f1-0adc-494c-bdbb-057eb7a81b92","name":"126595f1-0adc-494c-bdbb-057eb7a81b92","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"c12443f6-ef9d-4c37-9d20-d2447ca34052","name":"c12443f6-ef9d-4c37-9d20-d2447ca34052","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"cdad4d39-3a7f-42fb-955b-85d6e68b40a9","name":"cdad4d39-3a7f-42fb-955b-85d6e68b40a9","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"3f1ba1ae-2660-4983-be11-c5139d167f0a","name":"3f1ba1ae-2660-4983-be11-c5139d167f0a","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"4cf975b0-10ea-4a20-a67d-b5357dd3ad6b","name":"4cf975b0-10ea-4a20-a67d-b5357dd3ad6b","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":15,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"8994143a-1293-4cda-9349-3a5dde57b5f0","blendMode":0,"displayName":"default","isLocked":false,"name":"8994143a-1293-4cda-9349-3a5dde57b5f0","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprEnemySquareHome",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"Projectiles",
    "path":"folders/Sprites/Projectiles.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprEnemySquareHome",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":8.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprEnemySquareHome",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"6dae2556-5cd7-4b42-82bb-d9f2b2006f44","path":"sprites/sprEnemySquareHome/sprEnemySquareHome.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"29318c8e-f79c-41e8-90f8-74232d50dedc","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"24adef6e-6738-4905-af41-b2f246561a39","path":"sprites/sprEnemySquareHome/sprEnemySquareHome.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"95126706-6b0d-4b93-b4d0-ec86bc058f82","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"ec9bb645-f2b2-4e87-b57a-2c213b80e2cb","path":"sprites/sprEnemySquareHome/sprEnemySquareHome.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"8d30b9ed-14bb-4c8b-8021-6ec25c0a005d","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"126595f1-0adc-494c-bdbb-057eb7a81b92","path":"sprites/sprEnemySquareHome/sprEnemySquareHome.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ef3ccafc-a973-46c1-8f16-ae2f245803f1","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c12443f6-ef9d-4c37-9d20-d2447ca34052","path":"sprites/sprEnemySquareHome/sprEnemySquareHome.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"0bb6689e-8eaa-433a-9059-6ac8abf5a7c1","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"cdad4d39-3a7f-42fb-955b-85d6e68b40a9","path":"sprites/sprEnemySquareHome/sprEnemySquareHome.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"807964c3-0fdf-4df5-9f00-3426efa164f8","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"3f1ba1ae-2660-4983-be11-c5139d167f0a","path":"sprites/sprEnemySquareHome/sprEnemySquareHome.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"aa490bd9-a333-4dda-9a97-24948b948e10","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"4cf975b0-10ea-4a20-a67d-b5357dd3ad6b","path":"sprites/sprEnemySquareHome/sprEnemySquareHome.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"590e7926-1152-4f39-bf62-ffda1cadd6ee","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":8,
    "yorigin":7,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Projectiles",
    "path":"texturegroups/Projectiles",
  },
  "type":0,
  "VTile":false,
  "width":16,
}