{
  "$GMSprite":"",
  "%Name":"sprFloor124",
  "bboxMode":0,
  "bbox_bottom":31,
  "bbox_left":0,
  "bbox_right":31,
  "bbox_top":0,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"67edd0a2-4aa3-49f1-b426-31fb7df425e7","name":"67edd0a2-4aa3-49f1-b426-31fb7df425e7","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"5597b331-3095-403d-9632-9d284bd1272c","name":"5597b331-3095-403d-9632-9d284bd1272c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"912202e5-4f66-4952-b8e2-602900d0b3d5","name":"912202e5-4f66-4952-b8e2-602900d0b3d5","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"9719fb04-02cd-4c8c-b2ad-380c72e9da18","name":"9719fb04-02cd-4c8c-b2ad-380c72e9da18","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"0da49420-911f-4fca-9cc4-3d5de4bd448e","name":"0da49420-911f-4fca-9cc4-3d5de4bd448e","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"e042ded3-19c2-49a0-ba07-c19d92696dc9","name":"e042ded3-19c2-49a0-ba07-c19d92696dc9","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"cc2a3318-3a6c-4fd7-a54c-70dc45ad13d5","name":"cc2a3318-3a6c-4fd7-a54c-70dc45ad13d5","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"7b1762bc-a162-497a-a67e-893d472db11b","name":"7b1762bc-a162-497a-a67e-893d472db11b","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":32,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"543785ad-78ec-4244-8ea6-13034ec61db7","blendMode":0,"displayName":"Layer 1","isLocked":false,"name":"543785ad-78ec-4244-8ea6-13034ec61db7","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
    {"$GMImageLayer":"","%Name":"207f0e61-fe85-4c3c-b005-902c5611bb96","blendMode":0,"displayName":"default","isLocked":false,"name":"207f0e61-fe85-4c3c-b005-902c5611bb96","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprFloor124",
  "nineSlice":null,
  "origin":0,
  "parent":{
    "name":"Mushroomland",
    "path":"folders/Sprites/Enviroment/Tiles/Mushroomland.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprFloor124",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":8.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprFloor124",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"67edd0a2-4aa3-49f1-b426-31fb7df425e7","path":"sprites/sprFloor124/sprFloor124.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"34531adb-4946-464e-bdbc-47ba5ac05ac9","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"5597b331-3095-403d-9632-9d284bd1272c","path":"sprites/sprFloor124/sprFloor124.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"81962d0b-14e5-4227-96dc-4f55bd97bb87","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"912202e5-4f66-4952-b8e2-602900d0b3d5","path":"sprites/sprFloor124/sprFloor124.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"9f4e49de-621a-4b96-949a-da64e7120f05","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"9719fb04-02cd-4c8c-b2ad-380c72e9da18","path":"sprites/sprFloor124/sprFloor124.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"6b637761-8404-4373-b723-0053e9a360d0","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"0da49420-911f-4fca-9cc4-3d5de4bd448e","path":"sprites/sprFloor124/sprFloor124.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"0a030e2b-3976-40fa-b455-3350f53f9cda","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"e042ded3-19c2-49a0-ba07-c19d92696dc9","path":"sprites/sprFloor124/sprFloor124.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"423e83c3-0a55-482e-abcc-92c2b8de9acb","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"cc2a3318-3a6c-4fd7-a54c-70dc45ad13d5","path":"sprites/sprFloor124/sprFloor124.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"81137e73-7122-49e3-90cf-722338edeed3","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"7b1762bc-a162-497a-a67e-893d472db11b","path":"sprites/sprFloor124/sprFloor124.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"fc4c4413-de87-48e9-b132-a2671ab82b14","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":0,
    "yorigin":0,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Jungle",
    "path":"texturegroups/Jungle",
  },
  "type":0,
  "VTile":false,
  "width":32,
}