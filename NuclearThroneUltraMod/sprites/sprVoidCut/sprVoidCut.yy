{
  "$GMSprite":"",
  "%Name":"sprVoidCut",
  "bboxMode":0,
  "bbox_bottom":10,
  "bbox_left":0,
  "bbox_right":36,
  "bbox_top":0,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"b348412e-03d5-4ab2-951b-6752f4022240","name":"b348412e-03d5-4ab2-951b-6752f4022240","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"f60a8cf0-37d4-4718-996e-234fe2d8e352","name":"f60a8cf0-37d4-4718-996e-234fe2d8e352","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"7a12f7c7-f62a-4f4f-8a9a-43cd6ca375d7","name":"7a12f7c7-f62a-4f4f-8a9a-43cd6ca375d7","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"f55c7d22-083b-4c7c-bd70-69a386079ca6","name":"f55c7d22-083b-4c7c-bd70-69a386079ca6","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"6cd9ab23-01ac-45d8-8ff8-f30d17efd433","name":"6cd9ab23-01ac-45d8-8ff8-f30d17efd433","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"6b765eb1-ae84-4534-884b-63debe18fea6","name":"6b765eb1-ae84-4534-884b-63debe18fea6","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"5ea59f66-ff61-468a-a054-31c0766f3991","name":"5ea59f66-ff61-468a-a054-31c0766f3991","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"7ea6a28a-db43-4d5e-aedc-d4fd8d9e2d26","name":"7ea6a28a-db43-4d5e-aedc-d4fd8d9e2d26","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"45142811-44a7-4a50-a4f3-4ceefce3a63c","name":"45142811-44a7-4a50-a4f3-4ceefce3a63c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":11,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"b4bef4fe-1ab4-4951-8a63-b7b6f176fd04","blendMode":0,"displayName":"Layer 1","isLocked":false,"name":"b4bef4fe-1ab4-4951-8a63-b7b6f176fd04","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
    {"$GMImageLayer":"","%Name":"a361cb51-8c93-45c7-b9ed-ad0364554bcb","blendMode":0,"displayName":"default","isLocked":false,"name":"a361cb51-8c93-45c7-b9ed-ad0364554bcb","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprVoidCut",
  "nineSlice":null,
  "origin":9,
  "parent":{
    "name":"Melee Atacks",
    "path":"folders/Sprites/Projectiles/Melee Atacks.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprVoidCut",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":9.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprVoidCut",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"b348412e-03d5-4ab2-951b-6752f4022240","path":"sprites/sprVoidCut/sprVoidCut.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"faa3ec8c-42ee-483d-ab38-3dcd9b8157cf","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"f60a8cf0-37d4-4718-996e-234fe2d8e352","path":"sprites/sprVoidCut/sprVoidCut.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"1a3469fe-1192-4b72-b9bc-f69e7ab21c3b","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"7a12f7c7-f62a-4f4f-8a9a-43cd6ca375d7","path":"sprites/sprVoidCut/sprVoidCut.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"b2a807c5-7ae9-4b00-b40f-17833c4384a9","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"f55c7d22-083b-4c7c-bd70-69a386079ca6","path":"sprites/sprVoidCut/sprVoidCut.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"1c31ebd1-2781-443f-ba3b-3950eaa55248","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"6cd9ab23-01ac-45d8-8ff8-f30d17efd433","path":"sprites/sprVoidCut/sprVoidCut.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f73a22b4-c64d-4b5a-bff0-34ec56d53b16","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"6b765eb1-ae84-4534-884b-63debe18fea6","path":"sprites/sprVoidCut/sprVoidCut.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"e840c40f-01f7-4f3b-bfd6-35afea042770","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"5ea59f66-ff61-468a-a054-31c0766f3991","path":"sprites/sprVoidCut/sprVoidCut.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"74f1129c-f990-45d0-83d7-8b3456294573","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"7ea6a28a-db43-4d5e-aedc-d4fd8d9e2d26","path":"sprites/sprVoidCut/sprVoidCut.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"904f8e48-7e0c-4000-96f3-de1070359c08","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"45142811-44a7-4a50-a4f3-4ceefce3a63c","path":"sprites/sprVoidCut/sprVoidCut.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f13d51ff-751c-430c-89a8-0190820ed68c","IsCreationKey":false,"Key":8.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":18,
    "yorigin":3,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Projectiles",
    "path":"texturegroups/Projectiles",
  },
  "type":0,
  "VTile":false,
  "width":37,
}