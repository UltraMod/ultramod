{
  "$GMSprite":"",
  "%Name":"sprIDPDTankDownDrive",
  "bboxMode":0,
  "bbox_bottom":89,
  "bbox_left":18,
  "bbox_right":110,
  "bbox_top":33,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"8224a613-da59-4e0c-9a22-c599a30be49c","name":"8224a613-da59-4e0c-9a22-c599a30be49c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"cdd8a13c-5911-41cc-bc6f-0e58ec83314f","name":"cdd8a13c-5911-41cc-bc6f-0e58ec83314f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"a657b976-1eac-44a6-bbac-260419bca00e","name":"a657b976-1eac-44a6-bbac-260419bca00e","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":128,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"9ea5347d-8ce4-4b6a-9923-16511f8c8dd2","blendMode":0,"displayName":"default","isLocked":false,"name":"9ea5347d-8ce4-4b6a-9923-16511f8c8dd2","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprIDPDTankDownDrive",
  "nineSlice":null,
  "origin":9,
  "parent":{
    "name":"Van",
    "path":"folders/Sprites/Enemies/IDPD/Elite IDPD/Van.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprIDPDTankDownDrive",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":3.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprIDPDTankDownDrive",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"8224a613-da59-4e0c-9a22-c599a30be49c","path":"sprites/sprIDPDTankDownDrive/sprIDPDTankDownDrive.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"49349864-76ad-4b4c-9182-350223f20794","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"cdd8a13c-5911-41cc-bc6f-0e58ec83314f","path":"sprites/sprIDPDTankDownDrive/sprIDPDTankDownDrive.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"b0615bf0-8529-4648-b320-4a3fc1925118","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"a657b976-1eac-44a6-bbac-260419bca00e","path":"sprites/sprIDPDTankDownDrive/sprIDPDTankDownDrive.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"20ee3365-daa6-4414-99ed-49af1d579e23","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":64,
    "yorigin":48,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"IDPD",
    "path":"texturegroups/IDPD",
  },
  "type":0,
  "VTile":false,
  "width":128,
}