{
  "$GMSprite":"",
  "%Name":"sprInvertedTechnoMancerFire1",
  "bboxMode":0,
  "bbox_bottom":91,
  "bbox_left":4,
  "bbox_right":113,
  "bbox_top":24,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"232e357b-b90a-4011-9368-cbb14b1360af","name":"232e357b-b90a-4011-9368-cbb14b1360af","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"430404be-d089-4081-a7ec-3c237d83e13c","name":"430404be-d089-4081-a7ec-3c237d83e13c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"49e82d1f-1291-4164-9686-73d27c618628","name":"49e82d1f-1291-4164-9686-73d27c618628","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"3e33e710-023f-498f-b301-3b088f8b7ff1","name":"3e33e710-023f-498f-b301-3b088f8b7ff1","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"c3640ce7-6253-499e-811c-60f6ae86bd1e","name":"c3640ce7-6253-499e-811c-60f6ae86bd1e","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"b7956765-3332-402d-abe3-596613eee41b","name":"b7956765-3332-402d-abe3-596613eee41b","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"a9b54e38-b2b1-441e-8690-4f8930b43c19","name":"a9b54e38-b2b1-441e-8690-4f8930b43c19","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"e7eb4df7-fd8a-4643-8e2a-169a3ec91148","name":"e7eb4df7-fd8a-4643-8e2a-169a3ec91148","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"9f5f8bf0-7a9e-4dda-92a5-2731e0e3e404","name":"9f5f8bf0-7a9e-4dda-92a5-2731e0e3e404","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"4f588548-cbc5-4cb2-8d8a-7f90abb73c62","name":"4f588548-cbc5-4cb2-8d8a-7f90abb73c62","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"ac0ae042-8faa-400d-bf32-7f79d8895ffc","name":"ac0ae042-8faa-400d-bf32-7f79d8895ffc","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"69798ce8-8bd4-4d84-8407-c67a741279f5","name":"69798ce8-8bd4-4d84-8407-c67a741279f5","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"550b5d38-d8d3-41aa-9503-b3ae983ebfba","name":"550b5d38-d8d3-41aa-9503-b3ae983ebfba","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"690d95d0-8f0c-43ab-aaca-9817d525c67d","name":"690d95d0-8f0c-43ab-aaca-9817d525c67d","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"7b023334-b100-4519-ad28-743c893e5cfe","name":"7b023334-b100-4519-ad28-743c893e5cfe","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"d158309b-20fa-4701-a395-c38e2b9350eb","name":"d158309b-20fa-4701-a395-c38e2b9350eb","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"3968df43-c687-4a4a-af9d-5f61dcdfbae9","name":"3968df43-c687-4a4a-af9d-5f61dcdfbae9","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"c018cb9c-612a-432a-a13b-bb825fac04e9","name":"c018cb9c-612a-432a-a13b-bb825fac04e9","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"62563bd3-9041-47b5-882f-e6ce26a30fad","name":"62563bd3-9041-47b5-882f-e6ce26a30fad","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"095786a4-6e79-4497-a742-c269ed138d91","name":"095786a4-6e79-4497-a742-c269ed138d91","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"b92a4d6d-8380-4ac1-86e2-81549eae715f","name":"b92a4d6d-8380-4ac1-86e2-81549eae715f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"40ef6b2e-4113-402f-b5cb-f33c4fc2027e","name":"40ef6b2e-4113-402f-b5cb-f33c4fc2027e","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"76d1d9e6-76ae-478a-8117-9979050b30aa","name":"76d1d9e6-76ae-478a-8117-9979050b30aa","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"65d37345-7f27-4b83-be59-e375cc6e8c46","name":"65d37345-7f27-4b83-be59-e375cc6e8c46","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"3443dc4d-f0ee-46d1-bf46-a30ae24ce6db","name":"3443dc4d-f0ee-46d1-bf46-a30ae24ce6db","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"4c1b3ed0-2be6-43d3-b243-12ab78946228","name":"4c1b3ed0-2be6-43d3-b243-12ab78946228","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"1887c7f3-e17c-4599-a0ad-9fa1f7554f65","name":"1887c7f3-e17c-4599-a0ad-9fa1f7554f65","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"392a2aee-28bb-4d0e-ae7f-b78006d481d8","name":"392a2aee-28bb-4d0e-ae7f-b78006d481d8","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":120,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"0fc4cd3f-1eff-440c-accc-14654becf5be","blendMode":0,"displayName":"default","isLocked":false,"name":"0fc4cd3f-1eff-440c-accc-14654becf5be","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprInvertedTechnoMancerFire1",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"Technomancer",
    "path":"folders/Sprites/Enemies/Boss/Technomancer.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprInvertedTechnoMancerFire1",
    "autoRecord":true,
    "backdropHeight":768,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1366,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":28.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprInvertedTechnoMancerFire1",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"232e357b-b90a-4011-9368-cbb14b1360af","path":"sprites/sprInvertedTechnoMancerFire1/sprInvertedTechnoMancerFire1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"66d620ec-d265-4559-a619-687d1c9866ba","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"430404be-d089-4081-a7ec-3c237d83e13c","path":"sprites/sprInvertedTechnoMancerFire1/sprInvertedTechnoMancerFire1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"2b29fee8-9483-4b5d-a4b6-46ec538025dc","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"49e82d1f-1291-4164-9686-73d27c618628","path":"sprites/sprInvertedTechnoMancerFire1/sprInvertedTechnoMancerFire1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"12e92282-4aaf-4161-8b4f-04ecc94df0bb","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"3e33e710-023f-498f-b301-3b088f8b7ff1","path":"sprites/sprInvertedTechnoMancerFire1/sprInvertedTechnoMancerFire1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"21d4d4b6-d290-4098-93a0-d0bb671cb645","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c3640ce7-6253-499e-811c-60f6ae86bd1e","path":"sprites/sprInvertedTechnoMancerFire1/sprInvertedTechnoMancerFire1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"bcbc115b-f8c9-43ac-a223-37e1592da90e","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"b7956765-3332-402d-abe3-596613eee41b","path":"sprites/sprInvertedTechnoMancerFire1/sprInvertedTechnoMancerFire1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"0cb699e0-1bf1-42d9-be79-35f0bbbb2a43","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"a9b54e38-b2b1-441e-8690-4f8930b43c19","path":"sprites/sprInvertedTechnoMancerFire1/sprInvertedTechnoMancerFire1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"7baff378-d84f-40b1-9fc0-2c2c883cfb24","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"e7eb4df7-fd8a-4643-8e2a-169a3ec91148","path":"sprites/sprInvertedTechnoMancerFire1/sprInvertedTechnoMancerFire1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ed1c97bf-652c-4a95-9c0a-294f2fcf72e8","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"9f5f8bf0-7a9e-4dda-92a5-2731e0e3e404","path":"sprites/sprInvertedTechnoMancerFire1/sprInvertedTechnoMancerFire1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"d402867b-e90d-42f9-bb61-58eff637148b","IsCreationKey":false,"Key":8.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"4f588548-cbc5-4cb2-8d8a-7f90abb73c62","path":"sprites/sprInvertedTechnoMancerFire1/sprInvertedTechnoMancerFire1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"dcb913d3-048e-4462-9c50-e1cc7cf11a96","IsCreationKey":false,"Key":9.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"ac0ae042-8faa-400d-bf32-7f79d8895ffc","path":"sprites/sprInvertedTechnoMancerFire1/sprInvertedTechnoMancerFire1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"76a96c8f-108f-45c5-8b1a-89d283f91c22","IsCreationKey":false,"Key":10.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"69798ce8-8bd4-4d84-8407-c67a741279f5","path":"sprites/sprInvertedTechnoMancerFire1/sprInvertedTechnoMancerFire1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"69962f21-2a0a-4eea-aa92-c890ad21d5f1","IsCreationKey":false,"Key":11.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"550b5d38-d8d3-41aa-9503-b3ae983ebfba","path":"sprites/sprInvertedTechnoMancerFire1/sprInvertedTechnoMancerFire1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"55921bc4-6905-4362-aeab-6d8990ddc1a2","IsCreationKey":false,"Key":12.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"690d95d0-8f0c-43ab-aaca-9817d525c67d","path":"sprites/sprInvertedTechnoMancerFire1/sprInvertedTechnoMancerFire1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"bc330cb7-493e-4a83-b796-b62ec3342ff5","IsCreationKey":false,"Key":13.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"7b023334-b100-4519-ad28-743c893e5cfe","path":"sprites/sprInvertedTechnoMancerFire1/sprInvertedTechnoMancerFire1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ca8fe0de-aa48-4940-9af3-638ef2c1289d","IsCreationKey":false,"Key":14.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"d158309b-20fa-4701-a395-c38e2b9350eb","path":"sprites/sprInvertedTechnoMancerFire1/sprInvertedTechnoMancerFire1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ab582ae9-bd12-492e-b004-5574370ee308","IsCreationKey":false,"Key":15.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"3968df43-c687-4a4a-af9d-5f61dcdfbae9","path":"sprites/sprInvertedTechnoMancerFire1/sprInvertedTechnoMancerFire1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"76332c15-76fc-4c62-b43f-f16bd13623d1","IsCreationKey":false,"Key":16.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c018cb9c-612a-432a-a13b-bb825fac04e9","path":"sprites/sprInvertedTechnoMancerFire1/sprInvertedTechnoMancerFire1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"6149c25d-002e-4a01-9b7a-1ad0506f1916","IsCreationKey":false,"Key":17.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"62563bd3-9041-47b5-882f-e6ce26a30fad","path":"sprites/sprInvertedTechnoMancerFire1/sprInvertedTechnoMancerFire1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"edfe295f-2e7d-4c10-b5ee-0eabeb0f1931","IsCreationKey":false,"Key":18.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"095786a4-6e79-4497-a742-c269ed138d91","path":"sprites/sprInvertedTechnoMancerFire1/sprInvertedTechnoMancerFire1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"4434965a-5bfd-42ea-aea0-b75dcfdf36ea","IsCreationKey":false,"Key":19.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"b92a4d6d-8380-4ac1-86e2-81549eae715f","path":"sprites/sprInvertedTechnoMancerFire1/sprInvertedTechnoMancerFire1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"1329bf97-ff54-46cf-9038-ff16c4556058","IsCreationKey":false,"Key":20.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"40ef6b2e-4113-402f-b5cb-f33c4fc2027e","path":"sprites/sprInvertedTechnoMancerFire1/sprInvertedTechnoMancerFire1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"86b58355-992b-47c9-8170-46a43c294a27","IsCreationKey":false,"Key":21.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"76d1d9e6-76ae-478a-8117-9979050b30aa","path":"sprites/sprInvertedTechnoMancerFire1/sprInvertedTechnoMancerFire1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"22371734-4620-45c5-9b59-f997e126cc00","IsCreationKey":false,"Key":22.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"65d37345-7f27-4b83-be59-e375cc6e8c46","path":"sprites/sprInvertedTechnoMancerFire1/sprInvertedTechnoMancerFire1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"6d2a254b-909f-4810-a559-26c80258c732","IsCreationKey":false,"Key":23.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"3443dc4d-f0ee-46d1-bf46-a30ae24ce6db","path":"sprites/sprInvertedTechnoMancerFire1/sprInvertedTechnoMancerFire1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"a35155f3-0888-4d4d-a1c3-503de0876606","IsCreationKey":false,"Key":24.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"4c1b3ed0-2be6-43d3-b243-12ab78946228","path":"sprites/sprInvertedTechnoMancerFire1/sprInvertedTechnoMancerFire1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"21b8bfde-609a-4c16-b145-ec7c80e91678","IsCreationKey":false,"Key":25.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"1887c7f3-e17c-4599-a0ad-9fa1f7554f65","path":"sprites/sprInvertedTechnoMancerFire1/sprInvertedTechnoMancerFire1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"bc076dca-21c1-4f43-9ad1-9d112c11cd4a","IsCreationKey":false,"Key":26.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"392a2aee-28bb-4d0e-ae7f-b78006d481d8","path":"sprites/sprInvertedTechnoMancerFire1/sprInvertedTechnoMancerFire1.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"b3b40537-56c8-4946-b5c9-1b021a010b1e","IsCreationKey":false,"Key":27.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":59,
    "yorigin":60,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Default",
    "path":"texturegroups/Default",
  },
  "type":0,
  "VTile":false,
  "width":118,
}