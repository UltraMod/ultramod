{
  "$GMSprite":"",
  "%Name":"sprPlasmaQuake",
  "bboxMode":1,
  "bbox_bottom":13,
  "bbox_left":0,
  "bbox_right":20,
  "bbox_top":0,
  "collisionKind":0,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"c0e7c179-9367-4c84-9ba2-91f1b6be89f5","name":"c0e7c179-9367-4c84-9ba2-91f1b6be89f5","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"74f40013-ff9a-4140-a18f-450d8c0390e2","name":"74f40013-ff9a-4140-a18f-450d8c0390e2","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"e8c3ee89-e001-4ed9-ae8d-5e4ea73a0a46","name":"e8c3ee89-e001-4ed9-ae8d-5e4ea73a0a46","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"d9d87b48-d034-432e-a0e5-501c4b65037a","name":"d9d87b48-d034-432e-a0e5-501c4b65037a","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"bd79065f-1195-47d4-9fa9-14542789bdfe","name":"bd79065f-1195-47d4-9fa9-14542789bdfe","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"b1efe9d3-ff47-44ee-aebf-4ce8bc2690fa","name":"b1efe9d3-ff47-44ee-aebf-4ce8bc2690fa","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"23bbfe77-0c1c-4ef6-b214-99d179d95c43","name":"23bbfe77-0c1c-4ef6-b214-99d179d95c43","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":14,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"6e82fdc4-bf7e-4f6e-9772-c3d4f4bf4419","blendMode":0,"displayName":"default","isLocked":false,"name":"6e82fdc4-bf7e-4f6e-9772-c3d4f4bf4419","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprPlasmaQuake",
  "nineSlice":null,
  "origin":9,
  "parent":{
    "name":"Custom",
    "path":"folders/Sprites/Weapons/Custom.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprPlasmaQuake",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":7.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprPlasmaQuake",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c0e7c179-9367-4c84-9ba2-91f1b6be89f5","path":"sprites/sprPlasmaQuake/sprPlasmaQuake.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"83707dfb-4a7a-4417-af62-7ca24f92e782","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"74f40013-ff9a-4140-a18f-450d8c0390e2","path":"sprites/sprPlasmaQuake/sprPlasmaQuake.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"d60f78de-b745-417c-937d-c24e503fbd30","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"e8c3ee89-e001-4ed9-ae8d-5e4ea73a0a46","path":"sprites/sprPlasmaQuake/sprPlasmaQuake.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"c01bf7cc-980f-47ab-a1ad-8a4402f4ee7c","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"d9d87b48-d034-432e-a0e5-501c4b65037a","path":"sprites/sprPlasmaQuake/sprPlasmaQuake.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"36e40526-8c48-4571-a2cd-745e3982793a","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"bd79065f-1195-47d4-9fa9-14542789bdfe","path":"sprites/sprPlasmaQuake/sprPlasmaQuake.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"18bbee51-0574-420e-8003-c19ab4ba2c15","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"b1efe9d3-ff47-44ee-aebf-4ce8bc2690fa","path":"sprites/sprPlasmaQuake/sprPlasmaQuake.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"3c24291d-98be-44c8-941e-b61edc83b525","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"23bbfe77-0c1c-4ef6-b214-99d179d95c43","path":"sprites/sprPlasmaQuake/sprPlasmaQuake.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"a770de0a-92fb-4ae8-b9a6-d3e02943ab14","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":5,
    "yorigin":5,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Weapons",
    "path":"texturegroups/Weapons",
  },
  "type":0,
  "VTile":false,
  "width":21,
}