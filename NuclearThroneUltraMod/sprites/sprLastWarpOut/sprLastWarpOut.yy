{
  "$GMSprite":"",
  "%Name":"sprLastWarpOut",
  "bboxMode":0,
  "bbox_bottom":60,
  "bbox_left":0,
  "bbox_right":66,
  "bbox_top":0,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"df421ca1-bdec-4e96-a741-073483c096b8","name":"df421ca1-bdec-4e96-a741-073483c096b8","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"878c92d7-f142-4505-b05b-11d59faad07d","name":"878c92d7-f142-4505-b05b-11d59faad07d","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"75aa331c-1b54-4e93-a8d3-c613743e5813","name":"75aa331c-1b54-4e93-a8d3-c613743e5813","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"c9bbf5f3-359b-43e6-9f95-6b545f25a4ae","name":"c9bbf5f3-359b-43e6-9f95-6b545f25a4ae","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"81af1e32-9e08-4897-b4d9-4b0b48625877","name":"81af1e32-9e08-4897-b4d9-4b0b48625877","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"b677071b-afc8-4d8e-ab95-4d8208259461","name":"b677071b-afc8-4d8e-ab95-4d8208259461","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"125e2f7a-69c9-4c6e-bcbf-0a7d045dbb38","name":"125e2f7a-69c9-4c6e-bcbf-0a7d045dbb38","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":72,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"698e2555-a9aa-4a01-8f2d-cb098c6af1f4","blendMode":0,"displayName":"default","isLocked":false,"name":"698e2555-a9aa-4a01-8f2d-cb098c6af1f4","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprLastWarpOut",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"Captain",
    "path":"folders/Sprites/Enemies/Boss/Captain.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprLastWarpOut",
    "autoRecord":true,
    "backdropHeight":768,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1366,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":7.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprLastWarpOut",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"df421ca1-bdec-4e96-a741-073483c096b8","path":"sprites/sprLastWarpOut/sprLastWarpOut.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f68e8b2a-f23c-4be7-863e-6f4d82bc8587","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"878c92d7-f142-4505-b05b-11d59faad07d","path":"sprites/sprLastWarpOut/sprLastWarpOut.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"edfe75f9-087a-4811-bf1f-e6ee7f008287","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"75aa331c-1b54-4e93-a8d3-c613743e5813","path":"sprites/sprLastWarpOut/sprLastWarpOut.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"e9188af6-287d-49ba-9482-acc30e3d6d0d","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c9bbf5f3-359b-43e6-9f95-6b545f25a4ae","path":"sprites/sprLastWarpOut/sprLastWarpOut.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"1d05f439-5a3d-419d-8c6e-a983a81a5bc1","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"81af1e32-9e08-4897-b4d9-4b0b48625877","path":"sprites/sprLastWarpOut/sprLastWarpOut.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"e5b77c6a-8852-49bb-a86d-3bcbf5bdc664","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"b677071b-afc8-4d8e-ab95-4d8208259461","path":"sprites/sprLastWarpOut/sprLastWarpOut.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"27b13102-440d-43cc-8aa0-f4e75ec8dd5d","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"125e2f7a-69c9-4c6e-bcbf-0a7d045dbb38","path":"sprites/sprLastWarpOut/sprLastWarpOut.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"a8ac4fe8-a567-4806-8b4a-b693a0fc5cd5","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":36,
    "yorigin":36,
  },
  "swatchColours":null,
  "swfPrecision":0.5,
  "textureGroupId":{
    "name":"Default",
    "path":"texturegroups/Default",
  },
  "type":0,
  "VTile":false,
  "width":72,
}