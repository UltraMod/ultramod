{
  "$GMSprite":"",
  "%Name":"sprInvertedCloudKniferDeadMidAir",
  "bboxMode":1,
  "bbox_bottom":63,
  "bbox_left":0,
  "bbox_right":63,
  "bbox_top":0,
  "collisionKind":0,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"5be4e0d5-4050-4a32-9dbb-361a93825906","name":"5be4e0d5-4050-4a32-9dbb-361a93825906","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"1c1c29bc-513a-4b82-914d-13ac8a2e7dc4","name":"1c1c29bc-513a-4b82-914d-13ac8a2e7dc4","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"3732e479-d550-4faa-9af0-11041eab054a","name":"3732e479-d550-4faa-9af0-11041eab054a","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"c7b46596-d2ef-400e-8c8d-02dfca75bc2d","name":"c7b46596-d2ef-400e-8c8d-02dfca75bc2d","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"639caa41-8518-45d4-aec4-4d181a488af2","name":"639caa41-8518-45d4-aec4-4d181a488af2","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"5599be67-8de1-4d11-ac70-b800f8f668d4","name":"5599be67-8de1-4d11-ac70-b800f8f668d4","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"1e099d3e-b5d1-45ff-9c8b-83f5891509f3","name":"1e099d3e-b5d1-45ff-9c8b-83f5891509f3","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"669dacf1-eb4e-4af6-b166-d3cda2e1f226","name":"669dacf1-eb4e-4af6-b166-d3cda2e1f226","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"38c91b96-b552-4935-b354-5e89d68487b8","name":"38c91b96-b552-4935-b354-5e89d68487b8","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"d64a9fcc-7a64-4868-b449-298024504ddb","name":"d64a9fcc-7a64-4868-b449-298024504ddb","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"4588b651-9cde-4e4c-ab46-7ff7772ab99c","name":"4588b651-9cde-4e4c-ab46-7ff7772ab99c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"07d855b9-cdcf-4889-b46d-dc5ef0ba3365","name":"07d855b9-cdcf-4889-b46d-dc5ef0ba3365","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":64,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"af7a3257-92a5-42cb-9b09-fc0e29be9704","blendMode":0,"displayName":"Layer 1","isLocked":false,"name":"af7a3257-92a5-42cb-9b09-fc0e29be9704","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprInvertedCloudKniferDeadMidAir",
  "nineSlice":null,
  "origin":9,
  "parent":{
    "name":"Cloudlands",
    "path":"folders/Sprites/Enemies/Cloudlands.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprInvertedCloudKniferDeadMidAir",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":12.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprInvertedCloudKniferDeadMidAir",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"5be4e0d5-4050-4a32-9dbb-361a93825906","path":"sprites/sprInvertedCloudKniferDeadMidAir/sprInvertedCloudKniferDeadMidAir.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ba74cdb9-8c1e-43d4-9d58-1b4a362eb8a8","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"1c1c29bc-513a-4b82-914d-13ac8a2e7dc4","path":"sprites/sprInvertedCloudKniferDeadMidAir/sprInvertedCloudKniferDeadMidAir.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"84c26a09-97d3-4c38-b3d8-60032997fe70","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"3732e479-d550-4faa-9af0-11041eab054a","path":"sprites/sprInvertedCloudKniferDeadMidAir/sprInvertedCloudKniferDeadMidAir.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"a174e4d4-0d37-451d-ae1e-2203d3bceea1","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c7b46596-d2ef-400e-8c8d-02dfca75bc2d","path":"sprites/sprInvertedCloudKniferDeadMidAir/sprInvertedCloudKniferDeadMidAir.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"4e5c97f9-e708-402a-8a6b-08b227fd12e1","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"639caa41-8518-45d4-aec4-4d181a488af2","path":"sprites/sprInvertedCloudKniferDeadMidAir/sprInvertedCloudKniferDeadMidAir.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"1883970b-0467-4034-8968-503d3fc9bd7d","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"5599be67-8de1-4d11-ac70-b800f8f668d4","path":"sprites/sprInvertedCloudKniferDeadMidAir/sprInvertedCloudKniferDeadMidAir.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"95199525-f36a-4bbe-ac58-ed651483df08","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"1e099d3e-b5d1-45ff-9c8b-83f5891509f3","path":"sprites/sprInvertedCloudKniferDeadMidAir/sprInvertedCloudKniferDeadMidAir.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"064165a9-b86f-4af6-95a1-6e7a835175a4","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"669dacf1-eb4e-4af6-b166-d3cda2e1f226","path":"sprites/sprInvertedCloudKniferDeadMidAir/sprInvertedCloudKniferDeadMidAir.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"2d559ab8-9b60-46b0-a4c3-d530919ca7ee","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"38c91b96-b552-4935-b354-5e89d68487b8","path":"sprites/sprInvertedCloudKniferDeadMidAir/sprInvertedCloudKniferDeadMidAir.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"5f832ca8-cc69-4748-9ebd-b8e000079353","IsCreationKey":false,"Key":8.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"d64a9fcc-7a64-4868-b449-298024504ddb","path":"sprites/sprInvertedCloudKniferDeadMidAir/sprInvertedCloudKniferDeadMidAir.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"98277e0e-3093-4ee2-9ca4-2de1d65d7e46","IsCreationKey":false,"Key":9.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"4588b651-9cde-4e4c-ab46-7ff7772ab99c","path":"sprites/sprInvertedCloudKniferDeadMidAir/sprInvertedCloudKniferDeadMidAir.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"28aac408-7e8a-4c94-9f4d-69d85fb13423","IsCreationKey":false,"Key":10.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"07d855b9-cdcf-4889-b46d-dc5ef0ba3365","path":"sprites/sprInvertedCloudKniferDeadMidAir/sprInvertedCloudKniferDeadMidAir.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"10194349-312c-4901-9881-a88534cd7d67","IsCreationKey":false,"Key":11.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":32,
    "yorigin":40,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Scrapyard",
    "path":"texturegroups/Scrapyard",
  },
  "type":0,
  "VTile":false,
  "width":64,
}