{
  "$GMSprite":"",
  "%Name":"sprHeavyFlameUpg",
  "bboxMode":0,
  "bbox_bottom":28,
  "bbox_left":1,
  "bbox_right":31,
  "bbox_top":1,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"97f1fa3f-02ec-484f-869e-aac45ffd0165","name":"97f1fa3f-02ec-484f-869e-aac45ffd0165","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"bec9b87a-bdb9-40b0-a042-4f6ed8029e19","name":"bec9b87a-bdb9-40b0-a042-4f6ed8029e19","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"99fdc24c-cfb2-499b-885c-825731a8ac79","name":"99fdc24c-cfb2-499b-885c-825731a8ac79","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"0face43d-b009-4659-ac04-202a47fdeae4","name":"0face43d-b009-4659-ac04-202a47fdeae4","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"93b27ff1-2eb9-431d-9c63-ab7bc4d3a981","name":"93b27ff1-2eb9-431d-9c63-ab7bc4d3a981","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"d0bc840d-923f-4055-9f47-f3e52276cf02","name":"d0bc840d-923f-4055-9f47-f3e52276cf02","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"1a8bb952-f688-44af-bce6-ce04c1c6240b","name":"1a8bb952-f688-44af-bce6-ce04c1c6240b","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"0fe0a828-7f24-4337-ab41-5c4904ae998f","name":"0fe0a828-7f24-4337-ab41-5c4904ae998f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"2cf96ec7-656e-4ef7-a2a1-bf26f95ae421","name":"2cf96ec7-656e-4ef7-a2a1-bf26f95ae421","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"36711d15-5310-4cad-9170-df2cca56a937","name":"36711d15-5310-4cad-9170-df2cca56a937","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"eadd9418-1194-40d3-bb51-61c0e68b9bac","name":"eadd9418-1194-40d3-bb51-61c0e68b9bac","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":32,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"05e784ec-f033-42b7-bb40-7f90dbbf5f04","blendMode":0,"displayName":"default","isLocked":false,"name":"05e784ec-f033-42b7-bb40-7f90dbbf5f04","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprHeavyFlameUpg",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"Projectiles",
    "path":"folders/Sprites/Projectiles.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprHeavyFlameUpg",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":11.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprHeavyFlameUpg",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"97f1fa3f-02ec-484f-869e-aac45ffd0165","path":"sprites/sprHeavyFlameUpg/sprHeavyFlameUpg.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"cef24adc-f1e7-4d4f-a00d-bc895e6f1094","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"bec9b87a-bdb9-40b0-a042-4f6ed8029e19","path":"sprites/sprHeavyFlameUpg/sprHeavyFlameUpg.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"5006cce7-692d-4233-904c-14f10be02ed6","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"99fdc24c-cfb2-499b-885c-825731a8ac79","path":"sprites/sprHeavyFlameUpg/sprHeavyFlameUpg.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"65d315d6-8134-4081-96e5-91fddd175053","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"0face43d-b009-4659-ac04-202a47fdeae4","path":"sprites/sprHeavyFlameUpg/sprHeavyFlameUpg.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"9eca5f38-648e-480d-8892-9f0e1487322b","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"93b27ff1-2eb9-431d-9c63-ab7bc4d3a981","path":"sprites/sprHeavyFlameUpg/sprHeavyFlameUpg.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"7e2d8109-cd2d-4040-b10f-1031181f1b12","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"d0bc840d-923f-4055-9f47-f3e52276cf02","path":"sprites/sprHeavyFlameUpg/sprHeavyFlameUpg.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"16bbbb38-d4a0-4f36-ba01-3dbc07543207","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"1a8bb952-f688-44af-bce6-ce04c1c6240b","path":"sprites/sprHeavyFlameUpg/sprHeavyFlameUpg.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"a6dccd6b-a932-4399-99c3-96ac53b652fa","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"0fe0a828-7f24-4337-ab41-5c4904ae998f","path":"sprites/sprHeavyFlameUpg/sprHeavyFlameUpg.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"abc2e3d9-1a49-44a0-afe3-68d6935c62ec","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"2cf96ec7-656e-4ef7-a2a1-bf26f95ae421","path":"sprites/sprHeavyFlameUpg/sprHeavyFlameUpg.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"afaa1822-4dc4-400a-a229-e7c152bf93a2","IsCreationKey":false,"Key":8.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"36711d15-5310-4cad-9170-df2cca56a937","path":"sprites/sprHeavyFlameUpg/sprHeavyFlameUpg.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"0f48270e-06a7-45ad-9750-c193ae9ea42b","IsCreationKey":false,"Key":9.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"eadd9418-1194-40d3-bb51-61c0e68b9bac","path":"sprites/sprHeavyFlameUpg/sprHeavyFlameUpg.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"7b172e36-a03b-4851-8d8d-e8b8dca172fa","IsCreationKey":false,"Key":10.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":16,
    "yorigin":16,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Projectiles",
    "path":"texturegroups/Projectiles",
  },
  "type":0,
  "VTile":false,
  "width":32,
}