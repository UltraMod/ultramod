{
  "$GMSprite":"",
  "%Name":"sprJungleAssassinHide",
  "bboxMode":0,
  "bbox_bottom":24,
  "bbox_left":5,
  "bbox_right":26,
  "bbox_top":7,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"3f437725-6198-4cff-b602-fe8143f5f449","name":"3f437725-6198-4cff-b602-fe8143f5f449","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"3403900f-cf5d-401a-8599-96f5dfadaf59","name":"3403900f-cf5d-401a-8599-96f5dfadaf59","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"989193e3-d82a-460c-b072-a7c81f581540","name":"989193e3-d82a-460c-b072-a7c81f581540","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"15920604-55ae-49ab-b56b-c59db3784633","name":"15920604-55ae-49ab-b56b-c59db3784633","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"1bdab43c-3577-4c5c-9c9d-4ae82b1325b3","name":"1bdab43c-3577-4c5c-9c9d-4ae82b1325b3","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"aed3fdd8-7c4b-4347-9486-fdec6ee4d7c3","name":"aed3fdd8-7c4b-4347-9486-fdec6ee4d7c3","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"b310e292-372f-4509-ac1a-4483fd7e6796","name":"b310e292-372f-4509-ac1a-4483fd7e6796","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"1da1313d-33f9-4bdc-8008-9ab5a538af1a","name":"1da1313d-33f9-4bdc-8008-9ab5a538af1a","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"2da8a8c2-6cbf-4896-a033-d2e39f03ecc9","name":"2da8a8c2-6cbf-4896-a033-d2e39f03ecc9","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"120879e6-1738-4da0-924c-2610a96a419a","name":"120879e6-1738-4da0-924c-2610a96a419a","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"4ab89dbc-ae1f-43ff-802d-e1a3b7036542","name":"4ab89dbc-ae1f-43ff-802d-e1a3b7036542","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"c178e8aa-3fbe-477a-994a-6f3e388818da","name":"c178e8aa-3fbe-477a-994a-6f3e388818da","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"28e13a7e-c185-4a91-97fe-9ca4b1566860","name":"28e13a7e-c185-4a91-97fe-9ca4b1566860","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"90c32013-9c52-46ca-8e12-93b5c933dc22","name":"90c32013-9c52-46ca-8e12-93b5c933dc22","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"fa64daa9-c4f7-43fb-b63e-52fe28525815","name":"fa64daa9-c4f7-43fb-b63e-52fe28525815","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"2f6486c9-c83c-48a6-b58c-b08a088133c5","name":"2f6486c9-c83c-48a6-b58c-b08a088133c5","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"ee61eb28-e48f-4241-8318-25db81255872","name":"ee61eb28-e48f-4241-8318-25db81255872","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"75fcbb28-8344-48e5-8fd9-f6dfd1c34a48","name":"75fcbb28-8344-48e5-8fd9-f6dfd1c34a48","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"292725c1-de0e-4b04-8bc9-61127f0af937","name":"292725c1-de0e-4b04-8bc9-61127f0af937","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"0a50bf9c-3268-4482-af68-53bc12cc1354","name":"0a50bf9c-3268-4482-af68-53bc12cc1354","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"21094236-acd5-4f49-87c6-8e76149591e9","name":"21094236-acd5-4f49-87c6-8e76149591e9","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"fcd09450-a991-4baf-8889-c59880050468","name":"fcd09450-a991-4baf-8889-c59880050468","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"f0f0ab7e-96c1-4113-9a3a-ed8901cbb3d9","name":"f0f0ab7e-96c1-4113-9a3a-ed8901cbb3d9","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"857a6ffb-17be-424b-babc-07ae15342d8a","name":"857a6ffb-17be-424b-babc-07ae15342d8a","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"969de047-4900-4ebd-a18b-b0459cf978da","name":"969de047-4900-4ebd-a18b-b0459cf978da","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"47e28c35-439e-4edc-a9ad-c991ad81501e","name":"47e28c35-439e-4edc-a9ad-c991ad81501e","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"ca2d2220-8d2e-4c75-853e-97a792a1b6bb","name":"ca2d2220-8d2e-4c75-853e-97a792a1b6bb","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"c3539c75-0ae4-4de2-9ce5-8f68580a9e2f","name":"c3539c75-0ae4-4de2-9ce5-8f68580a9e2f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"db52507c-0654-4ae3-b867-ea1c67d94d58","name":"db52507c-0654-4ae3-b867-ea1c67d94d58","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"f280a3f3-7225-4f0f-ad21-5fd95e960117","name":"f280a3f3-7225-4f0f-ad21-5fd95e960117","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"1087a041-7fea-4669-ba03-04c39356f249","name":"1087a041-7fea-4669-ba03-04c39356f249","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"0eda728f-d5d2-4292-8f46-f752bbb6e949","name":"0eda728f-d5d2-4292-8f46-f752bbb6e949","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"87291987-3865-4022-8791-09188165ff4f","name":"87291987-3865-4022-8791-09188165ff4f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"533a8cdd-1340-4e68-a433-4f8052caabbc","name":"533a8cdd-1340-4e68-a433-4f8052caabbc","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"cad8dcd8-c291-4106-802a-d5ecc2b2ddcd","name":"cad8dcd8-c291-4106-802a-d5ecc2b2ddcd","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"1a5af2e5-2696-4c64-b2e0-f53d772d84df","name":"1a5af2e5-2696-4c64-b2e0-f53d772d84df","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"5d78dbf2-b814-4352-8431-ebb8ac49e2dd","name":"5d78dbf2-b814-4352-8431-ebb8ac49e2dd","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"7bec9b02-d303-4a51-8e44-1a695cbef63d","name":"7bec9b02-d303-4a51-8e44-1a695cbef63d","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"02418f78-7abe-49ab-aef8-ee0997a9e69e","name":"02418f78-7abe-49ab-aef8-ee0997a9e69e","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"d2f7b692-5931-479e-9718-52928efbadd6","name":"d2f7b692-5931-479e-9718-52928efbadd6","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"ff557853-45a8-47e7-aea7-246c570059af","name":"ff557853-45a8-47e7-aea7-246c570059af","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":32,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"99191a18-e472-410f-a61e-7947eedae1a3","blendMode":0,"displayName":"default","isLocked":false,"name":"99191a18-e472-410f-a61e-7947eedae1a3","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprJungleAssassinHide",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"JungleEnemy",
    "path":"folders/Sprites/Enemies/JungleEnemy.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprJungleAssassinHide",
    "autoRecord":true,
    "backdropHeight":768,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1366,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":41.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprJungleAssassinHide",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"3f437725-6198-4cff-b602-fe8143f5f449","path":"sprites/sprJungleAssassinHide/sprJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"227fb6a8-59df-488e-8033-9af47dcb31b7","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"3403900f-cf5d-401a-8599-96f5dfadaf59","path":"sprites/sprJungleAssassinHide/sprJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"bd7db0a4-e553-4897-82df-aac6155dee4b","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"989193e3-d82a-460c-b072-a7c81f581540","path":"sprites/sprJungleAssassinHide/sprJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ffd8a368-40b3-4ae1-8c70-ec29d8765738","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"15920604-55ae-49ab-b56b-c59db3784633","path":"sprites/sprJungleAssassinHide/sprJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"a0d850f4-3421-44a7-b145-26fcd35aca2b","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"1bdab43c-3577-4c5c-9c9d-4ae82b1325b3","path":"sprites/sprJungleAssassinHide/sprJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ed6a8e55-243d-4f9d-91ab-057ba46c564b","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"aed3fdd8-7c4b-4347-9486-fdec6ee4d7c3","path":"sprites/sprJungleAssassinHide/sprJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"61b8185d-2aa1-4a57-97fc-34aaa78d4aa8","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"b310e292-372f-4509-ac1a-4483fd7e6796","path":"sprites/sprJungleAssassinHide/sprJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f65e9cb0-6106-4628-9b82-84e8d8efe000","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"1da1313d-33f9-4bdc-8008-9ab5a538af1a","path":"sprites/sprJungleAssassinHide/sprJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"00be77c6-de3e-42f4-862d-bcf12eeccb4e","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"2da8a8c2-6cbf-4896-a033-d2e39f03ecc9","path":"sprites/sprJungleAssassinHide/sprJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"a71bcb6f-fa7a-4082-99dd-328f0589aea7","IsCreationKey":false,"Key":8.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"120879e6-1738-4da0-924c-2610a96a419a","path":"sprites/sprJungleAssassinHide/sprJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"148f42e4-ee37-4df4-a302-dc1383e37876","IsCreationKey":false,"Key":9.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"4ab89dbc-ae1f-43ff-802d-e1a3b7036542","path":"sprites/sprJungleAssassinHide/sprJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"431164f1-205b-4af3-b216-d14258371d49","IsCreationKey":false,"Key":10.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c178e8aa-3fbe-477a-994a-6f3e388818da","path":"sprites/sprJungleAssassinHide/sprJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f14441ee-35a7-44b6-9fbb-c171510fc4ef","IsCreationKey":false,"Key":11.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"28e13a7e-c185-4a91-97fe-9ca4b1566860","path":"sprites/sprJungleAssassinHide/sprJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f17859a9-7066-40fe-bde4-cd9c19b5f49c","IsCreationKey":false,"Key":12.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"90c32013-9c52-46ca-8e12-93b5c933dc22","path":"sprites/sprJungleAssassinHide/sprJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"47a33198-c403-47bb-ac0a-1ac5b19e954d","IsCreationKey":false,"Key":13.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"fa64daa9-c4f7-43fb-b63e-52fe28525815","path":"sprites/sprJungleAssassinHide/sprJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"6b354103-36da-4276-9a59-69b7660678ba","IsCreationKey":false,"Key":14.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"2f6486c9-c83c-48a6-b58c-b08a088133c5","path":"sprites/sprJungleAssassinHide/sprJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"82dc8baa-1bf0-4b75-a366-b69c6f3a94ce","IsCreationKey":false,"Key":15.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"ee61eb28-e48f-4241-8318-25db81255872","path":"sprites/sprJungleAssassinHide/sprJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"95193f64-ac65-440c-b7f6-d4d41bd4148f","IsCreationKey":false,"Key":16.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"75fcbb28-8344-48e5-8fd9-f6dfd1c34a48","path":"sprites/sprJungleAssassinHide/sprJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"035c29e3-f94c-4bc0-91a8-8470b1386f1f","IsCreationKey":false,"Key":17.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"292725c1-de0e-4b04-8bc9-61127f0af937","path":"sprites/sprJungleAssassinHide/sprJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"253d98df-4873-4ac7-bc63-cee0ebfe2c40","IsCreationKey":false,"Key":18.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"0a50bf9c-3268-4482-af68-53bc12cc1354","path":"sprites/sprJungleAssassinHide/sprJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"2c4ed1f6-06af-4a98-9751-b793b3c7c250","IsCreationKey":false,"Key":19.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"21094236-acd5-4f49-87c6-8e76149591e9","path":"sprites/sprJungleAssassinHide/sprJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"b2b590dc-de42-4a3a-8db1-3577395a6d23","IsCreationKey":false,"Key":20.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"fcd09450-a991-4baf-8889-c59880050468","path":"sprites/sprJungleAssassinHide/sprJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ad6c4cde-1f1f-446a-a9f2-cbee55f1740b","IsCreationKey":false,"Key":21.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"f0f0ab7e-96c1-4113-9a3a-ed8901cbb3d9","path":"sprites/sprJungleAssassinHide/sprJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"08c55295-08df-4234-8d1b-abd3053bc15b","IsCreationKey":false,"Key":22.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"857a6ffb-17be-424b-babc-07ae15342d8a","path":"sprites/sprJungleAssassinHide/sprJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"90d99b69-f03c-4dd6-ad34-558cb3caafdb","IsCreationKey":false,"Key":23.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"969de047-4900-4ebd-a18b-b0459cf978da","path":"sprites/sprJungleAssassinHide/sprJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"d1c42cbc-4c22-4828-9311-63bfbdd23d3d","IsCreationKey":false,"Key":24.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"47e28c35-439e-4edc-a9ad-c991ad81501e","path":"sprites/sprJungleAssassinHide/sprJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"df68f17e-a70e-43a1-bc07-fb717e7fdda5","IsCreationKey":false,"Key":25.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"ca2d2220-8d2e-4c75-853e-97a792a1b6bb","path":"sprites/sprJungleAssassinHide/sprJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"9d3073a8-2aef-41d6-a596-3859ab701688","IsCreationKey":false,"Key":26.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c3539c75-0ae4-4de2-9ce5-8f68580a9e2f","path":"sprites/sprJungleAssassinHide/sprJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"121510a0-86a9-4567-8f7d-45053481def0","IsCreationKey":false,"Key":27.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"db52507c-0654-4ae3-b867-ea1c67d94d58","path":"sprites/sprJungleAssassinHide/sprJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"0cfd94d7-bf0d-4032-914a-96660beee889","IsCreationKey":false,"Key":28.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"f280a3f3-7225-4f0f-ad21-5fd95e960117","path":"sprites/sprJungleAssassinHide/sprJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"99b0f9d5-ae86-4278-90ce-2b3f0ce83778","IsCreationKey":false,"Key":29.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"1087a041-7fea-4669-ba03-04c39356f249","path":"sprites/sprJungleAssassinHide/sprJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"7a9591a8-61cb-44e2-b855-172a07bb9323","IsCreationKey":false,"Key":30.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"0eda728f-d5d2-4292-8f46-f752bbb6e949","path":"sprites/sprJungleAssassinHide/sprJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"94414c78-d2c0-43c9-b4c9-69d9642e3ed2","IsCreationKey":false,"Key":31.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"87291987-3865-4022-8791-09188165ff4f","path":"sprites/sprJungleAssassinHide/sprJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"3ed6307c-566e-47e6-8f64-d46f6aeef5f0","IsCreationKey":false,"Key":32.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"533a8cdd-1340-4e68-a433-4f8052caabbc","path":"sprites/sprJungleAssassinHide/sprJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"befd92b7-94c5-4e37-8d18-5d7c2c4ae3c1","IsCreationKey":false,"Key":33.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"cad8dcd8-c291-4106-802a-d5ecc2b2ddcd","path":"sprites/sprJungleAssassinHide/sprJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"e688e558-66fb-490e-a8bf-36013b08ab73","IsCreationKey":false,"Key":34.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"1a5af2e5-2696-4c64-b2e0-f53d772d84df","path":"sprites/sprJungleAssassinHide/sprJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"0050107e-f0b2-49c9-a905-3c230bcaaac3","IsCreationKey":false,"Key":35.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"5d78dbf2-b814-4352-8431-ebb8ac49e2dd","path":"sprites/sprJungleAssassinHide/sprJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"8b9856a4-f800-45a1-a26a-7682b7baf552","IsCreationKey":false,"Key":36.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"7bec9b02-d303-4a51-8e44-1a695cbef63d","path":"sprites/sprJungleAssassinHide/sprJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"04265d89-9d0f-4f3e-93c9-8d253dd428ff","IsCreationKey":false,"Key":37.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"02418f78-7abe-49ab-aef8-ee0997a9e69e","path":"sprites/sprJungleAssassinHide/sprJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"e941f70f-b928-4eea-aa79-dad589f656b2","IsCreationKey":false,"Key":38.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"d2f7b692-5931-479e-9718-52928efbadd6","path":"sprites/sprJungleAssassinHide/sprJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"9802d493-5bfc-4451-85c2-a387ccd7c608","IsCreationKey":false,"Key":39.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"ff557853-45a8-47e7-aea7-246c570059af","path":"sprites/sprJungleAssassinHide/sprJungleAssassinHide.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"2bc8a939-5f98-4c86-b478-ed7b5ae6779d","IsCreationKey":false,"Key":40.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":16,
    "yorigin":16,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Default",
    "path":"texturegroups/Default",
  },
  "type":0,
  "VTile":false,
  "width":32,
}