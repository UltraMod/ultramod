{
  "$GMSprite":"",
  "%Name":"sprInvertedSnowTankWalk",
  "bboxMode":1,
  "bbox_bottom":47,
  "bbox_left":0,
  "bbox_right":47,
  "bbox_top":0,
  "collisionKind":0,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"34068749-796c-4eac-9090-fd64396f15a7","name":"34068749-796c-4eac-9090-fd64396f15a7","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"c5549355-d66f-415c-b1aa-fb0ee77f3807","name":"c5549355-d66f-415c-b1aa-fb0ee77f3807","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"8783bfa3-96ad-49e5-913f-e109e093417d","name":"8783bfa3-96ad-49e5-913f-e109e093417d","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"c7198b38-a879-41e2-a2c9-7c4c3d2b55b0","name":"c7198b38-a879-41e2-a2c9-7c4c3d2b55b0","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"05b275e5-6fa2-43ac-b934-6e1aaeae36d7","name":"05b275e5-6fa2-43ac-b934-6e1aaeae36d7","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"effc6f42-2366-4068-aa8a-f7cc11c9b6b7","name":"effc6f42-2366-4068-aa8a-f7cc11c9b6b7","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"8b2cd4f9-aed4-4ecb-a9f8-033faa16aaa1","name":"8b2cd4f9-aed4-4ecb-a9f8-033faa16aaa1","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"bbb68a6e-04a5-4ce1-aa18-dd2549c32601","name":"bbb68a6e-04a5-4ce1-aa18-dd2549c32601","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":48,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"08ab38b7-c7a8-4692-b1cd-56f535be7d4e","blendMode":0,"displayName":"default","isLocked":false,"name":"08ab38b7-c7a8-4692-b1cd-56f535be7d4e","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprInvertedSnowTankWalk",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"FrozenCity",
    "path":"folders/Sprites/Enemies/FrozenCity.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":8.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"34068749-796c-4eac-9090-fd64396f15a7","path":"sprites/sprInvertedSnowTankWalk/sprInvertedSnowTankWalk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"fc507cfa-92cc-4cdf-bf8d-8f5476a2d527","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c5549355-d66f-415c-b1aa-fb0ee77f3807","path":"sprites/sprInvertedSnowTankWalk/sprInvertedSnowTankWalk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"fe4555ef-5c33-4168-acf9-17d1a425533b","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"8783bfa3-96ad-49e5-913f-e109e093417d","path":"sprites/sprInvertedSnowTankWalk/sprInvertedSnowTankWalk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"d91427c0-4833-4cfa-aa1b-06577c5d4308","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c7198b38-a879-41e2-a2c9-7c4c3d2b55b0","path":"sprites/sprInvertedSnowTankWalk/sprInvertedSnowTankWalk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"6cef113d-d20a-4f16-873c-9a294dd98091","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"05b275e5-6fa2-43ac-b934-6e1aaeae36d7","path":"sprites/sprInvertedSnowTankWalk/sprInvertedSnowTankWalk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f7b10c68-15ff-4b26-95e0-dc031f9775c5","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"effc6f42-2366-4068-aa8a-f7cc11c9b6b7","path":"sprites/sprInvertedSnowTankWalk/sprInvertedSnowTankWalk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"6be5e561-f4fc-4c50-a18e-f54865fc410d","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"8b2cd4f9-aed4-4ecb-a9f8-033faa16aaa1","path":"sprites/sprInvertedSnowTankWalk/sprInvertedSnowTankWalk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ac8e088f-e585-47fe-bacc-9fc134090292","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"bbb68a6e-04a5-4ce1-aa18-dd2549c32601","path":"sprites/sprInvertedSnowTankWalk/sprInvertedSnowTankWalk.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"1f4b7052-ac7b-47dc-a09f-f19320b1c0bf","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":{
      "x":0.0,
      "y":0.0,
    },
    "volume":1.0,
    "xorigin":24,
    "yorigin":24,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"InvertedFrozenCity",
    "path":"texturegroups/InvertedFrozenCity",
  },
  "type":0,
  "VTile":false,
  "width":48,
}