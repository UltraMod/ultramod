{
  "$GMSprite":"",
  "%Name":"sprSheepDashFXultrathrn",
  "bboxMode":1,
  "bbox_bottom":23,
  "bbox_left":0,
  "bbox_right":25,
  "bbox_top":0,
  "collisionKind":0,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"e7cc911c-24d4-4191-8739-8b63b2f31d8a","name":"e7cc911c-24d4-4191-8739-8b63b2f31d8a","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"1f9aa67f-19af-4869-9772-e16d4ca1d700","name":"1f9aa67f-19af-4869-9772-e16d4ca1d700","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"61f9946d-0da0-409f-b7cc-f664994eb1c1","name":"61f9946d-0da0-409f-b7cc-f664994eb1c1","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"c3f357da-aacb-4687-80c4-7e98a9ce4caa","name":"c3f357da-aacb-4687-80c4-7e98a9ce4caa","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"35e0b764-fdb3-46ef-b48b-ed0a7bab293f","name":"35e0b764-fdb3-46ef-b48b-ed0a7bab293f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"9e119f39-c852-4791-9c65-20c97b18c673","name":"9e119f39-c852-4791-9c65-20c97b18c673","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"9c6f1d1b-6522-4d46-91e7-3aec71a1cfad","name":"9c6f1d1b-6522-4d46-91e7-3aec71a1cfad","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"937f3cc7-8134-4ad7-96fc-70fd6e2902ee","name":"937f3cc7-8134-4ad7-96fc-70fd6e2902ee","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"35fbdf96-6462-4ff0-990b-c903758840dd","name":"35fbdf96-6462-4ff0-990b-c903758840dd","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"688f4a96-f973-4df2-8fe7-db7275b0afaa","name":"688f4a96-f973-4df2-8fe7-db7275b0afaa","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"5919702e-b296-4e2e-9d44-1dbbab6b6134","name":"5919702e-b296-4e2e-9d44-1dbbab6b6134","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"7f67328d-4df0-4874-9514-0e8b632a7662","name":"7f67328d-4df0-4874-9514-0e8b632a7662","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"165a4131-415d-472b-a401-33ee3b0b257a","name":"165a4131-415d-472b-a401-33ee3b0b257a","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"16591c84-82a5-4e0f-858e-f008fa850ce4","name":"16591c84-82a5-4e0f-858e-f008fa850ce4","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":24,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"3f8ca203-9621-4207-af13-e211a95717b2","blendMode":0,"displayName":"default","isLocked":false,"name":"3f8ca203-9621-4207-af13-e211a95717b2","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprSheepDashFXultrathrn",
  "nineSlice":null,
  "origin":9,
  "parent":{
    "name":"Sheep",
    "path":"folders/Sprites/Player/Custom/Sheep.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":14.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"e7cc911c-24d4-4191-8739-8b63b2f31d8a","path":"sprites/sprSheepDashFXultrathrn/sprSheepDashFXultrathrn.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"59c665cf-e434-41d4-95b7-424a392e135f","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"1f9aa67f-19af-4869-9772-e16d4ca1d700","path":"sprites/sprSheepDashFXultrathrn/sprSheepDashFXultrathrn.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"2d111103-fbf3-4cc8-8ef7-034f1331106d","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"61f9946d-0da0-409f-b7cc-f664994eb1c1","path":"sprites/sprSheepDashFXultrathrn/sprSheepDashFXultrathrn.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"3fa8a622-e886-4fbd-bc31-cc8c9da84f88","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c3f357da-aacb-4687-80c4-7e98a9ce4caa","path":"sprites/sprSheepDashFXultrathrn/sprSheepDashFXultrathrn.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"d9cc5a4e-d467-4475-a4bc-e7eceea487c3","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"35e0b764-fdb3-46ef-b48b-ed0a7bab293f","path":"sprites/sprSheepDashFXultrathrn/sprSheepDashFXultrathrn.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"78cceccc-c444-4979-889d-b2d1a84cad99","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"9e119f39-c852-4791-9c65-20c97b18c673","path":"sprites/sprSheepDashFXultrathrn/sprSheepDashFXultrathrn.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"6a03dbc0-aa9e-43a0-8c23-c7c0220f098c","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"9c6f1d1b-6522-4d46-91e7-3aec71a1cfad","path":"sprites/sprSheepDashFXultrathrn/sprSheepDashFXultrathrn.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"c41af4c1-5587-418e-b9f8-948b34deb785","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"937f3cc7-8134-4ad7-96fc-70fd6e2902ee","path":"sprites/sprSheepDashFXultrathrn/sprSheepDashFXultrathrn.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"64d91763-10cf-4a5a-86e0-8066f036a418","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"35fbdf96-6462-4ff0-990b-c903758840dd","path":"sprites/sprSheepDashFXultrathrn/sprSheepDashFXultrathrn.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"44a25a9c-c25e-4882-ae28-7eda762af576","IsCreationKey":false,"Key":8.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"688f4a96-f973-4df2-8fe7-db7275b0afaa","path":"sprites/sprSheepDashFXultrathrn/sprSheepDashFXultrathrn.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"3c07cda0-6d10-433c-9e0c-04524444110a","IsCreationKey":false,"Key":9.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"5919702e-b296-4e2e-9d44-1dbbab6b6134","path":"sprites/sprSheepDashFXultrathrn/sprSheepDashFXultrathrn.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f33e2f9e-afa5-4290-8f0a-1bb133d980e0","IsCreationKey":false,"Key":10.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"7f67328d-4df0-4874-9514-0e8b632a7662","path":"sprites/sprSheepDashFXultrathrn/sprSheepDashFXultrathrn.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"b9b3bed2-3e8b-4263-bc8a-2f764f11c56c","IsCreationKey":false,"Key":11.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"165a4131-415d-472b-a401-33ee3b0b257a","path":"sprites/sprSheepDashFXultrathrn/sprSheepDashFXultrathrn.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"13221f7c-0ae0-46ee-b8e5-22728652ed41","IsCreationKey":false,"Key":12.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"16591c84-82a5-4e0f-858e-f008fa850ce4","path":"sprites/sprSheepDashFXultrathrn/sprSheepDashFXultrathrn.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"31a81267-9ef0-442e-8304-2bdab26b6ca4","IsCreationKey":false,"Key":13.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":8,
    "yorigin":12,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"PlayerCharacter",
    "path":"texturegroups/PlayerCharacter",
  },
  "type":0,
  "VTile":false,
  "width":26,
}