{
  "$GMSprite":"",
  "%Name":"sprLastDeath",
  "bboxMode":0,
  "bbox_bottom":75,
  "bbox_left":7,
  "bbox_right":80,
  "bbox_top":4,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"6dccc92a-1acc-4029-a813-50db884f251c","name":"6dccc92a-1acc-4029-a813-50db884f251c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"c014acf8-0fc3-43f7-a68e-9742fa49eb61","name":"c014acf8-0fc3-43f7-a68e-9742fa49eb61","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"cb7ffc93-a5c3-40ca-971a-5f3d4ceee186","name":"cb7ffc93-a5c3-40ca-971a-5f3d4ceee186","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"8de59efa-e9fa-458b-83bc-46389a4b28db","name":"8de59efa-e9fa-458b-83bc-46389a4b28db","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"33dd70df-837c-498a-99aa-680136d42681","name":"33dd70df-837c-498a-99aa-680136d42681","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"265162bf-8343-4f9c-9d6a-48e782ffec7e","name":"265162bf-8343-4f9c-9d6a-48e782ffec7e","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"dfd3bd5c-56f6-49e7-8ea8-a15bf42e1850","name":"dfd3bd5c-56f6-49e7-8ea8-a15bf42e1850","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"a89d63b5-8881-4dc9-9460-ab3487494db8","name":"a89d63b5-8881-4dc9-9460-ab3487494db8","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"fdffe776-843c-4fcc-ad3b-fa15ffa5c1dc","name":"fdffe776-843c-4fcc-ad3b-fa15ffa5c1dc","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"7362b167-daaa-4b7f-8f6f-1e691e6f676e","name":"7362b167-daaa-4b7f-8f6f-1e691e6f676e","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"751ec4df-b324-4da5-b12f-06921de6ec03","name":"751ec4df-b324-4da5-b12f-06921de6ec03","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"5f5923fa-ed12-4219-9225-28bbe284e672","name":"5f5923fa-ed12-4219-9225-28bbe284e672","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"6aa6a635-5db9-4727-8dd0-03052b61f0b6","name":"6aa6a635-5db9-4727-8dd0-03052b61f0b6","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"b8b99093-b218-4617-84bc-5b7b73745a96","name":"b8b99093-b218-4617-84bc-5b7b73745a96","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"efa21c8e-3c5b-40a7-9a70-59dbe6cc0618","name":"efa21c8e-3c5b-40a7-9a70-59dbe6cc0618","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"ef76f177-9540-4c41-bb8e-fb4ef3f847be","name":"ef76f177-9540-4c41-bb8e-fb4ef3f847be","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"6ce7addb-a526-421f-a0ee-68dc95a5e2b1","name":"6ce7addb-a526-421f-a0ee-68dc95a5e2b1","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"41d63459-50ee-40ec-bf14-cbf47c563c3d","name":"41d63459-50ee-40ec-bf14-cbf47c563c3d","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"15de9e4a-03de-44cc-ae6a-70de94c31e61","name":"15de9e4a-03de-44cc-ae6a-70de94c31e61","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"1aa3b883-7223-448d-a9f6-5716645732f3","name":"1aa3b883-7223-448d-a9f6-5716645732f3","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"48f4d516-7407-4445-bdbf-29d5b3ce9c33","name":"48f4d516-7407-4445-bdbf-29d5b3ce9c33","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":96,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"5edecff8-184f-4194-8b2f-8e5ebdb88450","blendMode":0,"displayName":"default","isLocked":false,"name":"5edecff8-184f-4194-8b2f-8e5ebdb88450","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprLastDeath",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"Captain",
    "path":"folders/Sprites/Enemies/Boss/Captain.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprLastDeath",
    "autoRecord":true,
    "backdropHeight":768,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1366,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":21.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprLastDeath",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"6dccc92a-1acc-4029-a813-50db884f251c","path":"sprites/sprLastDeath/sprLastDeath.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ecd4f392-4a0f-409f-83ef-ab839fc225ae","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c014acf8-0fc3-43f7-a68e-9742fa49eb61","path":"sprites/sprLastDeath/sprLastDeath.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"204e4b29-60b5-4125-90d3-fc5c216e812f","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"cb7ffc93-a5c3-40ca-971a-5f3d4ceee186","path":"sprites/sprLastDeath/sprLastDeath.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"eeff0e0b-bcf6-4b27-be01-d3a21c39c418","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"8de59efa-e9fa-458b-83bc-46389a4b28db","path":"sprites/sprLastDeath/sprLastDeath.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"0fee4963-d8f6-4ce6-a91a-e9c9583b59b5","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"33dd70df-837c-498a-99aa-680136d42681","path":"sprites/sprLastDeath/sprLastDeath.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"a8a70b99-c92d-4f67-bca6-46f769c666ad","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"265162bf-8343-4f9c-9d6a-48e782ffec7e","path":"sprites/sprLastDeath/sprLastDeath.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"e79de9f5-0dcf-4f7f-b903-745ff8c0f5fd","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"dfd3bd5c-56f6-49e7-8ea8-a15bf42e1850","path":"sprites/sprLastDeath/sprLastDeath.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"0690f014-7533-4a73-96ea-0f11f1cc7a16","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"a89d63b5-8881-4dc9-9460-ab3487494db8","path":"sprites/sprLastDeath/sprLastDeath.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"bef725e5-ce70-4921-b19f-9135c9d34516","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"fdffe776-843c-4fcc-ad3b-fa15ffa5c1dc","path":"sprites/sprLastDeath/sprLastDeath.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"f4e5d35c-c677-45e5-be12-3431ae8b7c7d","IsCreationKey":false,"Key":8.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"7362b167-daaa-4b7f-8f6f-1e691e6f676e","path":"sprites/sprLastDeath/sprLastDeath.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"42acbf8d-7fbf-4225-9304-b78486231094","IsCreationKey":false,"Key":9.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"751ec4df-b324-4da5-b12f-06921de6ec03","path":"sprites/sprLastDeath/sprLastDeath.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"0b1d7777-90d7-44b5-9691-3c036f62b7b5","IsCreationKey":false,"Key":10.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"5f5923fa-ed12-4219-9225-28bbe284e672","path":"sprites/sprLastDeath/sprLastDeath.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"b90b8f5d-d5d9-4cc0-a82f-d90c47481dbc","IsCreationKey":false,"Key":11.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"6aa6a635-5db9-4727-8dd0-03052b61f0b6","path":"sprites/sprLastDeath/sprLastDeath.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"ecf2ef08-6693-4663-afe3-d4df50a0d7a1","IsCreationKey":false,"Key":12.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"b8b99093-b218-4617-84bc-5b7b73745a96","path":"sprites/sprLastDeath/sprLastDeath.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"a6bbce2f-b7dd-4215-a6c5-98d860463a94","IsCreationKey":false,"Key":13.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"efa21c8e-3c5b-40a7-9a70-59dbe6cc0618","path":"sprites/sprLastDeath/sprLastDeath.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"d0eca857-8cfe-4877-b9ad-b84c0b398eef","IsCreationKey":false,"Key":14.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"ef76f177-9540-4c41-bb8e-fb4ef3f847be","path":"sprites/sprLastDeath/sprLastDeath.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"7dd21310-cbc0-4c2f-a771-e602c7729fb2","IsCreationKey":false,"Key":15.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"6ce7addb-a526-421f-a0ee-68dc95a5e2b1","path":"sprites/sprLastDeath/sprLastDeath.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"d08e3c5a-50cc-46b2-a9b4-29dfbad04ed5","IsCreationKey":false,"Key":16.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"41d63459-50ee-40ec-bf14-cbf47c563c3d","path":"sprites/sprLastDeath/sprLastDeath.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"5475408a-1a61-49b6-af0b-03b246999758","IsCreationKey":false,"Key":17.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"15de9e4a-03de-44cc-ae6a-70de94c31e61","path":"sprites/sprLastDeath/sprLastDeath.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"0c3b45ab-21e6-4f9f-a84a-a61a1d6808a9","IsCreationKey":false,"Key":18.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"1aa3b883-7223-448d-a9f6-5716645732f3","path":"sprites/sprLastDeath/sprLastDeath.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"43aa679d-1e0e-48d7-a519-25effea1a84e","IsCreationKey":false,"Key":19.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"48f4d516-7407-4445-bdbf-29d5b3ce9c33","path":"sprites/sprLastDeath/sprLastDeath.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"d9247b55-3fa3-4c6a-b333-98fc6fc96b81","IsCreationKey":false,"Key":20.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":48,
    "yorigin":48,
  },
  "swatchColours":null,
  "swfPrecision":0.5,
  "textureGroupId":{
    "name":"Default",
    "path":"texturegroups/Default",
  },
  "type":0,
  "VTile":false,
  "width":96,
}