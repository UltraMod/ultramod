{
  "$GMSprite":"",
  "%Name":"sprFrogQueenIdle",
  "bboxMode":0,
  "bbox_bottom":43,
  "bbox_left":1,
  "bbox_right":45,
  "bbox_top":5,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"ea14ecfa-52ae-4799-8101-ae4ff707cdab","name":"ea14ecfa-52ae-4799-8101-ae4ff707cdab","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"e1dfc3c8-31d4-474f-a1a1-56c6dc45cf3b","name":"e1dfc3c8-31d4-474f-a1a1-56c6dc45cf3b","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"e3f63001-ca30-46bb-9f9b-8a4f6ad00c45","name":"e3f63001-ca30-46bb-9f9b-8a4f6ad00c45","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"8fa5fe24-65c3-44fc-9a13-8c6b384f7ea6","name":"8fa5fe24-65c3-44fc-9a13-8c6b384f7ea6","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"074a830c-8384-40b5-9fa6-7633fda28d9c","name":"074a830c-8384-40b5-9fa6-7633fda28d9c","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"7d4e87d2-da88-4925-a80f-8e2ac7414a88","name":"7d4e87d2-da88-4925-a80f-8e2ac7414a88","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"cbd1b323-831a-4cf7-ac20-d660fbe6fd2f","name":"cbd1b323-831a-4cf7-ac20-d660fbe6fd2f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"dc641b0e-7ecb-4d33-82a4-af7833035d67","name":"dc641b0e-7ecb-4d33-82a4-af7833035d67","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"3b747a02-8129-4ace-8d58-8043c28bda38","name":"3b747a02-8129-4ace-8d58-8043c28bda38","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"f2c3d0dd-beb4-4cf7-8726-3a578b7abe01","name":"f2c3d0dd-beb4-4cf7-8726-3a578b7abe01","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"173efbf5-6844-4d4e-9533-106b1b6f00f1","name":"173efbf5-6844-4d4e-9533-106b1b6f00f1","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"8cdfc5f0-e038-4c53-8aa8-ca8e591e53ac","name":"8cdfc5f0-e038-4c53-8aa8-ca8e591e53ac","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"c679930a-f72a-4b0a-ae04-a1ed0dae322e","name":"c679930a-f72a-4b0a-ae04-a1ed0dae322e","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"850140d8-0112-4120-a0de-070ea86f875d","name":"850140d8-0112-4120-a0de-070ea86f875d","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":48,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"1b0306c2-f7b0-411f-982c-e4b9f8f9ba49","blendMode":0,"displayName":"default","isLocked":false,"name":"1b0306c2-f7b0-411f-982c-e4b9f8f9ba49","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprFrogQueenIdle",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"BallMom",
    "path":"folders/Sprites/Enemies/Boss/BallMom.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprFrogQueenIdle",
    "autoRecord":true,
    "backdropHeight":1080,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1920,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":14.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprFrogQueenIdle",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"ea14ecfa-52ae-4799-8101-ae4ff707cdab","path":"sprites/sprFrogQueenIdle/sprFrogQueenIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"62e1babe-6fed-49f8-842d-b3f42e083023","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"e1dfc3c8-31d4-474f-a1a1-56c6dc45cf3b","path":"sprites/sprFrogQueenIdle/sprFrogQueenIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"4b22c987-87b2-4044-9fd1-9f7b573c30d7","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"e3f63001-ca30-46bb-9f9b-8a4f6ad00c45","path":"sprites/sprFrogQueenIdle/sprFrogQueenIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"9a195a08-2d66-4ee7-9ebe-8fb183ceb22d","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"8fa5fe24-65c3-44fc-9a13-8c6b384f7ea6","path":"sprites/sprFrogQueenIdle/sprFrogQueenIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"8dfdef55-1f14-4130-8c39-b6223be9dbd8","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"074a830c-8384-40b5-9fa6-7633fda28d9c","path":"sprites/sprFrogQueenIdle/sprFrogQueenIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"239a21ad-0116-4859-af77-709328b77bff","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"7d4e87d2-da88-4925-a80f-8e2ac7414a88","path":"sprites/sprFrogQueenIdle/sprFrogQueenIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"06c3f944-2c2d-47e6-805a-54713c6dd0e8","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"cbd1b323-831a-4cf7-ac20-d660fbe6fd2f","path":"sprites/sprFrogQueenIdle/sprFrogQueenIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"d5ba1e20-7d1f-4e3e-881f-2a57f3470966","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"dc641b0e-7ecb-4d33-82a4-af7833035d67","path":"sprites/sprFrogQueenIdle/sprFrogQueenIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"71298aac-c051-4817-9c4d-2e1b9e66385f","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"3b747a02-8129-4ace-8d58-8043c28bda38","path":"sprites/sprFrogQueenIdle/sprFrogQueenIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"a01e8f19-5d02-4cc0-8693-db1fda20f03c","IsCreationKey":false,"Key":8.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"f2c3d0dd-beb4-4cf7-8726-3a578b7abe01","path":"sprites/sprFrogQueenIdle/sprFrogQueenIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"e5a2ae13-5299-43ce-8a91-17c47d758a14","IsCreationKey":false,"Key":9.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"173efbf5-6844-4d4e-9533-106b1b6f00f1","path":"sprites/sprFrogQueenIdle/sprFrogQueenIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"3aa6ffc9-88bd-4c2c-8e66-df87f91bbf22","IsCreationKey":false,"Key":10.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"8cdfc5f0-e038-4c53-8aa8-ca8e591e53ac","path":"sprites/sprFrogQueenIdle/sprFrogQueenIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"fcf8d051-fc00-44fd-acb6-d4ffb36a9db1","IsCreationKey":false,"Key":11.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"c679930a-f72a-4b0a-ae04-a1ed0dae322e","path":"sprites/sprFrogQueenIdle/sprFrogQueenIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"57943104-a2a8-4f70-bc42-0265fee6f69a","IsCreationKey":false,"Key":12.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"850140d8-0112-4120-a0de-070ea86f875d","path":"sprites/sprFrogQueenIdle/sprFrogQueenIdle.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"fcc28a15-1051-4c76-89b8-5d8c4b350af3","IsCreationKey":false,"Key":13.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":24,
    "yorigin":24,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Sewers",
    "path":"texturegroups/Sewers",
  },
  "type":0,
  "VTile":false,
  "width":48,
}