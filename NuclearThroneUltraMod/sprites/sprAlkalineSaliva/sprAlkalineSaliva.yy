{
  "$GMSprite":"",
  "%Name":"sprAlkalineSaliva",
  "bboxMode":0,
  "bbox_bottom":20,
  "bbox_left":7,
  "bbox_right":18,
  "bbox_top":7,
  "collisionKind":1,
  "collisionTolerance":0,
  "DynamicTexturePage":false,
  "edgeFiltering":false,
  "For3D":false,
  "frames":[
    {"$GMSpriteFrame":"","%Name":"79fb85fa-9ea3-4f80-bee4-933938801e63","name":"79fb85fa-9ea3-4f80-bee4-933938801e63","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"03d33c56-e48c-47b0-a9fe-edd41f67c349","name":"03d33c56-e48c-47b0-a9fe-edd41f67c349","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"faecd24f-7da1-4708-8c11-51dfe7dbe717","name":"faecd24f-7da1-4708-8c11-51dfe7dbe717","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"040ac99b-b75a-470c-895e-400cb063cf1f","name":"040ac99b-b75a-470c-895e-400cb063cf1f","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"daac5ff0-ca16-445a-83c7-83d4e2a50f98","name":"daac5ff0-ca16-445a-83c7-83d4e2a50f98","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"fc561a43-73bc-43f2-a899-0a9e0b188387","name":"fc561a43-73bc-43f2-a899-0a9e0b188387","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"33019f23-a086-46de-8b15-02b48d6294b0","name":"33019f23-a086-46de-8b15-02b48d6294b0","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
    {"$GMSpriteFrame":"","%Name":"83060386-a0da-439f-9255-49f68bfdb2d9","name":"83060386-a0da-439f-9255-49f68bfdb2d9","resourceType":"GMSpriteFrame","resourceVersion":"2.0",},
  ],
  "gridX":0,
  "gridY":0,
  "height":24,
  "HTile":false,
  "layers":[
    {"$GMImageLayer":"","%Name":"3965d79b-0b7d-4e15-8e30-0966ed555342","blendMode":0,"displayName":"default","isLocked":false,"name":"3965d79b-0b7d-4e15-8e30-0966ed555342","opacity":100.0,"resourceType":"GMImageLayer","resourceVersion":"2.0","visible":true,},
  ],
  "name":"sprAlkalineSaliva",
  "nineSlice":null,
  "origin":4,
  "parent":{
    "name":"Custom",
    "path":"folders/Sprites/Player/Custom.yy",
  },
  "preMultiplyAlpha":false,
  "resourceType":"GMSprite",
  "resourceVersion":"2.0",
  "sequence":{
    "$GMSequence":"",
    "%Name":"sprAlkalineSaliva",
    "autoRecord":true,
    "backdropHeight":768,
    "backdropImageOpacity":0.5,
    "backdropImagePath":"",
    "backdropWidth":1366,
    "backdropXOffset":0.0,
    "backdropYOffset":0.0,
    "events":{
      "$KeyframeStore<MessageEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MessageEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "eventStubScript":null,
    "eventToFunction":{},
    "length":8.0,
    "lockOrigin":false,
    "moments":{
      "$KeyframeStore<MomentsEventKeyframe>":"",
      "Keyframes":[],
      "resourceType":"KeyframeStore<MomentsEventKeyframe>",
      "resourceVersion":"2.0",
    },
    "name":"sprAlkalineSaliva",
    "playback":1,
    "playbackSpeed":1.0,
    "playbackSpeedType":1,
    "resourceType":"GMSequence",
    "resourceVersion":"2.0",
    "showBackdrop":true,
    "showBackdropImage":false,
    "timeUnits":1,
    "tracks":[
      {"$GMSpriteFramesTrack":"","builtinName":0,"events":[],"inheritsTrackColour":true,"interpolation":1,"isCreationTrack":false,"keyframes":{"$KeyframeStore<SpriteFrameKeyframe>":"","Keyframes":[
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"79fb85fa-9ea3-4f80-bee4-933938801e63","path":"sprites/sprAlkalineSaliva/sprAlkalineSaliva.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"cf8c4075-5384-497e-84aa-f6a2bebb621e","IsCreationKey":false,"Key":0.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"03d33c56-e48c-47b0-a9fe-edd41f67c349","path":"sprites/sprAlkalineSaliva/sprAlkalineSaliva.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"521362d1-2c22-422a-9ce6-a0f8150f8ccd","IsCreationKey":false,"Key":1.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"faecd24f-7da1-4708-8c11-51dfe7dbe717","path":"sprites/sprAlkalineSaliva/sprAlkalineSaliva.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"fbad02d5-acd4-4bfb-a243-5b1defc13b9f","IsCreationKey":false,"Key":2.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"040ac99b-b75a-470c-895e-400cb063cf1f","path":"sprites/sprAlkalineSaliva/sprAlkalineSaliva.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"8975e0d8-f0f3-4d72-99c8-154f93ca4298","IsCreationKey":false,"Key":3.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"daac5ff0-ca16-445a-83c7-83d4e2a50f98","path":"sprites/sprAlkalineSaliva/sprAlkalineSaliva.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"245b30b0-5406-4a71-a3b2-592b49e0d014","IsCreationKey":false,"Key":4.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"fc561a43-73bc-43f2-a899-0a9e0b188387","path":"sprites/sprAlkalineSaliva/sprAlkalineSaliva.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"0c2974f2-66f3-4b41-a707-e120e03c51a3","IsCreationKey":false,"Key":5.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"33019f23-a086-46de-8b15-02b48d6294b0","path":"sprites/sprAlkalineSaliva/sprAlkalineSaliva.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"635520fd-3c84-4844-9c84-dbb30e4062f7","IsCreationKey":false,"Key":6.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
            {"$Keyframe<SpriteFrameKeyframe>":"","Channels":{
                "0":{"$SpriteFrameKeyframe":"","Id":{"name":"83060386-a0da-439f-9255-49f68bfdb2d9","path":"sprites/sprAlkalineSaliva/sprAlkalineSaliva.yy",},"resourceType":"SpriteFrameKeyframe","resourceVersion":"2.0",},
              },"Disabled":false,"id":"8e3a8969-0736-4fda-ad30-a37c2bbebfea","IsCreationKey":false,"Key":7.0,"Length":1.0,"resourceType":"Keyframe<SpriteFrameKeyframe>","resourceVersion":"2.0","Stretch":false,},
          ],"resourceType":"KeyframeStore<SpriteFrameKeyframe>","resourceVersion":"2.0",},"modifiers":[],"name":"frames","resourceType":"GMSpriteFramesTrack","resourceVersion":"2.0","spriteId":null,"trackColour":0,"tracks":[],"traits":0,},
    ],
    "visibleRange":null,
    "volume":1.0,
    "xorigin":12,
    "yorigin":12,
  },
  "swatchColours":null,
  "swfPrecision":2.525,
  "textureGroupId":{
    "name":"Default",
    "path":"texturegroups/Default",
  },
  "type":0,
  "VTile":false,
  "width":24,
}