///The Pit();
// /@description
///@param
function scrCreateThePit(){
	instance_create(x + 64,y,Floor)
	instance_create(x + 64,y + 32,Floor)
	instance_create(x + 32,y + 32,Floor)
	instance_create(x + 32,y + 64,Floor)
	instance_create(x,y + 64,Floor)
	//instance_create(x,y + 128,Floor)
		
	instance_create(x - 96,y,Floor)
	instance_create(x - 96,y + 32,Floor)
	instance_create(x - 64,y + 32,Floor)
	instance_create(x - 64,y + 64,Floor)
	instance_create(x - 32,y + 64,Floor)
		
	//instance_create(x + 64,y,Floor)
	instance_create(x + 64,y - 32,Floor)
	instance_create(x + 64,y - 64,Floor)
	instance_create(x + 32,y - 64,Floor)
	instance_create(x + 32,y - 96,Floor)
	instance_create(x,y - 96,Floor)
	//instance_create(x,y - 128,Floor)
		
	//instance_create(x - 64,y,Floor)
	instance_create(x - 96,y - 32,Floor)
	instance_create(x - 96,y - 64,Floor)
	instance_create(x - 64,y - 64,Floor)
	instance_create(x - 64,y - 96,Floor)
	instance_create(x - 32,y - 96,Floor)
	
	//Corridor
	instance_create(x + 96,y,Floor)
	instance_create(x + 96,y - 32,Floor)
	instance_create(x + 128,y,Floor)
	instance_create(x + 128,y - 32,Floor)
	with instance_create(x + 128,y - 16,Wall)
	{
		if place_meeting(x,y,hitme)
		{
			instance_destroy(id,false);	
		}
	}
	with instance_create(x + 128,y,Wall)
	{
		if place_meeting(x,y,hitme)
		{
			instance_destroy(id,false);	
		}
	}
	instance_create(x + 160,y,Floor)
	instance_create(x + 160,y - 32,Floor)
	instance_create(x + 192,y,Floor)
	instance_create(x + 192,y - 32,Floor)
	instance_create(x + 224,y,Floor)
	instance_create(x + 224,y - 32,Floor)
	instance_create(x + 256,y,Floor)
	instance_create(x + 256,y - 32,Floor)
	
	instance_create(x - 96,y,Floor)
	instance_create(x - 96,y - 32,Floor)
	instance_create(x - 128,y,Floor)
	instance_create(x - 128,y - 32,Floor)
	with instance_create(x - 128,y - 16,Wall)
	{
		if place_meeting(x,y,hitme)
		{
			instance_destroy(id,false);	
		}
	}
	with instance_create(x - 128,y,Wall)
	{
		if place_meeting(x,y,hitme)
		{
			instance_destroy(id,false);	
		}
	}
	instance_create(x - 160,y,Floor)
	instance_create(x - 160,y - 32,Floor)
	instance_create(x - 192,y,Floor)
	instance_create(x - 192,y - 32,Floor)
	instance_create(x - 224,y,Floor)
	instance_create(x - 224,y - 32,Floor)
	instance_create(x - 256,y,Floor)
	instance_create(x - 256,y - 32,Floor)
	
	instance_create(x - 32,y - 128,Floor)
	instance_create(x,y      - 128,Floor)
	instance_create(x - 32,y - 160,Floor)
	instance_create(x,y      - 160,Floor)
	with instance_create(x - 16,y - 160,Wall)
	{
		if place_meeting(x,y,hitme)
		{
			instance_destroy(id,false);	
		}
	}
	with instance_create(x,y - 160,Wall)
	{
		if place_meeting(x,y,hitme)
		{
			instance_destroy(id,false);	
		}
	}
	instance_create(x - 32,y - 192,Floor)
	instance_create(x,y      - 192,Floor)
	instance_create(x - 32,y - 224,Floor)
	instance_create(x,y      - 224,Floor)
	instance_create(x - 32,y - 256,Floor)
	instance_create(x,y      - 256,Floor)
	
	instance_create(x - 32,y + 96,Floor)
	instance_create(x,y      + 96,Floor)
	instance_create(x - 32,y + 128,Floor)
	instance_create(x,y      + 128,Floor)
	with instance_create(x - 16,y + 128,Wall)
	{
		if place_meeting(x,y,hitme)
		{
			instance_destroy(id,false);	
		}
	}
	with instance_create(x,y + 128,Wall)
	{
		if place_meeting(x,y,hitme)
		{
			instance_destroy(id,false);	
		}
	}
	instance_create(x - 32,y + 160,Floor)
	instance_create(x,y      + 160,Floor)
	instance_create(x - 32,y + 192,Floor)
	instance_create(x,y      + 192,Floor)
	instance_create(x - 32,y + 224,Floor)
	instance_create(x,y      + 224,Floor)
	instance_create(x - 32,y + 256,Floor)
	instance_create(x,y      + 256,Floor)
	
	//Explo floors
	instance_create(x + 48,y + 16,FloorExplo)
	instance_create(x - 64,y + 16,FloorExplo)
	instance_create(x + 48,y - 32,FloorExplo)
	instance_create(x - 64,y - 32,FloorExplo)
	
	instance_create(x + 16,y + 48,FloorExplo)
	instance_create(x - 32,y + 48,FloorExplo)
	instance_create(x + 16,y - 64,FloorExplo)
	instance_create(x - 32,y - 64,FloorExplo)
	
}