///scrLoseSkill();
// /@description need to run in player
///@param
function scrLoseSkill(skillIndex, disableInstead = false) {
	with GameRender
	{
		mutationDynamicHud = dynamicHudResetTime;
	}
	skill_got[skillIndex] = 0;
	snd_play_2d(sndLoseSkill,0,true);
	if skillIndex==13||skillIndex==14||skillIndex==15
	||skillIndex==16||skillIndex==17||skillIndex==21||skillIndex==42//wep specific
	||skillIndex==43
	{heavyheart--;}
	switch (skillIndex)
	{
		case 0: //HEAVY HEART
			heavyheart = 0;
		break;
		case 1: //RHINO SKIN
			if ultra_got[62] && altUltra//Living armour
			{
				maxarmour = max(0, maxarmour - 4)
				armour = min(armour, maxarmour);
			}
			else if race == 25
			{
				maxhealth -= 5;
				if maxhealth < 1
					maxhealth = 1;
				if my_health > maxhealth
				{
					my_health = max(maxhealth,my_health - 5);
					prevhealth = my_health;
				}
			}
			else
			{
				maxhealth -= 4;
				if maxhealth < 1
					maxhealth = 1;
				if my_health > maxhealth
				{
					my_health = max(maxhealth,my_health - 4);
					prevhealth = my_health;
				}
			}
		break;
		case 2: //EXTRA FEET
			if race == 25
				maxSpeed -=0.6;
			else
				maxSpeed -= 0.5;
		break;
		case 3: //PLUTONIUM HUNGER
			betterpluto = 32;
		break;
		case 4: //RABBIT PAW
			betterrabbitpaw = 0;
		break;
		case 5: //but BUTT
			if race == 1//Fish lose extra value of ammo drops
			{
				typ_ammo[1] = 40 typ_ammo[2] = 10 typ_ammo[3] = 9 typ_ammo[4] = 8 typ_ammo[5] = 13
			}
			else if race == 16
			{
				maxarmour --;
				freeArmourStrike --;
				if armour > maxarmour
					armour = maxarmour;
			}
			skeletonGambleBongas = 0;
		break;
		case 9: //SECOND STOMACH
			defaultOverhealAddition -= 1;
			defaultOverhealAddition = max(0,defaultOverhealAddition);
		break;
		case 10: //BACK MUSCLE
			if ultra_got[85]
			{
				if rogueammomax > 6
					rogueammomax = 6;
			}
			else if rogueammomax > 3
				rogueammomax = 3;
			if ultra_got[87] && altUltra//Imortal popo
				rogueammomax = 2;
			rogueammo = min(rogueammo,rogueammomax);	
					
			typ_amax[0] = 33 typ_amax[1] = 255 typ_amax[2] = 55 typ_amax[3] = 55 typ_amax[4] = 55 typ_amax[5] = 55
			ammo[1] = min(ammo[1],typ_amax[1]);
			ammo[2] = min(ammo[2],typ_amax[2]);
			ammo[3] = min(ammo[3],typ_amax[3]);
			ammo[4] = min(ammo[4],typ_amax[4]);
			ammo[5] = min(ammo[5],typ_amax[5]);
			if race == 25
				scrWeaponAdjustCost(1.11);
			else
				scrWeaponAdjustCost(1.07);
			//Cap the ammo
		break;
		case 11://SCARIER FACE
			excessDamageDeal = 0;
		break;
		case 13: //LONG ARMS
			bettermelee = 0;
		break;
		case 16: //RECYCLE GLAND
			betterrecyclegland = 0;
		break;
		case 17: //ENERGY BRAIN
			betterlaserbrain = 0;
		break;
		case 18: //LAST WISH
			if disableInstead
			{
				UberCont.refundLivesRegain = livesRegain;
				UberCont.refundLastWishPrevent = lastWishPrevent;
				UberCont.refundSkeletonLives = skeletonlives;
				UberCont.refundLastWish = true;
			}
			lastWishPrevent = false;
			skeletonlives -= 1;
			var tookLife = false;
			var al = array_length(livesRegain);
			for (var i = 0; i < al; i++) {
				if !tookLife && livesRegain[i] > 0
				{
					livesRegain[i] = 0;
					tookLife = true;
				}
			}
		break;
		case 19: //EAGLE EYES
			if race == 25
			{
				accuracy = standartAccuracy/0.25//0.3//in case you have build up rage use the standart
				standartAccuracy = accuracy//new standart
			}
			else
			{
				accuracy = standartAccuracy/0.28//0.38//in case you have build up rage use the standart
				standartAccuracy = accuracy//new standart
			}
		break;
		case 21: //BOLT MARROW
			betterboltmarrow = 0;
		break;
		case 25: //STRONG SPIRIT
			strongspirit = false;
			strongspiritused = false;
			snd_play(sndStrongSpiritLost);
		break;
		case 26: //HAMMER HEAD
			hammerheadtimer = 0;
			hammerheadcounter = 0;
			nearWall = false;
		break;
		case 27: //PATIENCE
			patience--;
		break;
		case 28: //RAGE
			if !disableInstead
				rage = 0;
			accuracy = standartAccuracy;
		break;
		case 31: //TOUGH SHELL
			if ultra_got[62] && altUltra//Living armour
			{
				maxarmour = max(0, maxarmour - 1)
				armour = min(armour, maxarmour);
			}
			else
			{
				maxhealth -= 1;
				if maxhealth < 1
					maxhealth = 1;
				if my_health > maxhealth
				{
					my_health = max(maxhealth,my_health - 1);
					prevhealth = my_health;
				}
			}
		break;
		case 33: //GLASS ARM CANNON
			
			if ultra_got[62] && altUltra
			{
				maxarmour += 2;
			}
			else if race != 25
				maxhealth += 2;
		break;
		case 38: //ENRICHED METABOLISM
			if !disableInstead
				rage = 0;
			metabolism = 0;
		break;
		case 39: //ALIENT INTESTINES
			alienIntestines = 0;
		break;
		case 41: //NERVES OF STEEL
			if ultra_got[62] && altUltra
			{
				maxarmour += 1;
			}
			else
			{
				if race == 25
				{
					maxhealth += 1;
				}
				else
				{
					maxhealth += 2;
				}
				//if gotMinimumArmour && !scrIsCrown(10) && maxarmour <= 1
				//{
					maxarmour -= 1;
					armour = min(maxarmour,armour);
					//gotMinimumArmour = false;
				//}
			}
		break;
		case 42: //TAIL END
			betterTail = 1.5;
		break;
		case 44: //SERENE DREAMS
			with CrescentMoon
			{
				instance_destroy();	
			}
		break;
	}
	level -= 1;
	totalSkills--;
	skillsChosen --;
	with GameRender {
		event_user(0);
	}
}