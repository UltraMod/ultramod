///scrAssignRunData();
// /@description
///@param
function scrAssignSaveGameData(gameSave){
	return {
		completion: gameSave.completion,
		datetime: gameSave.datetime
	};
}