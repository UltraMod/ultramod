///scrGenerateFloorMaker();
// /@description
///@param
function scrGenerateFloorMaker(limiter) {
	globalGoal = 100;
	if instance_exists(GenCont)
		globalGoal = GenCont.goal;
	if firstTry
	{
		direction = choose(0,90,180,270)
		if instance_exists(PitNavigation)
		{
			if PitNavigation.forceDirection
			{
				direction = choose(0,180)//Left or right for wide screening
				PitNavigation.forceDirection = false;
			}
		}
		styleb = choose(0,0,0,0,0,0,1)
		goal = 100//110
	
		if instance_exists(Player){
			if scrIsCrown(27)
			{
				//Crown of opposite onto crown of disco
				styleb = choose(1,1,1,1,1,1,0)
			}
			var s = clamp(Player.loops*5,0,30);
			goal += s;
			
			if ((Player.area = 3 || Player.area == 136) and Player.subarea = 3)
			{
				goal = 150+s//50
			}
			else if Player.area == 3 || Player.area == 136
				goal = 120+s;
			else if Player.area = 4
				goal = 120+s
			else if Player.area = 5
				goal = 160+s
			else if Player.area = 7 || Player.area == 108
			{
				if Player.subarea == 3
					goal = 30+s
				else
					goal = 130+s
			}
			else if Player.area = 100
			goal = 40+s
			else if Player.area = 101 || Player.area == 122
			goal = 130+s
			else if Player.area = 103
			goal = 140+s
			else if Player.area = 102
			goal = 50+s
			else if Player.area = 104
			goal = 10
			else if Player.area = 105
			goal = 150+s
			else if Player.area = 106
			goal = 130+s
			else if Player.area = 114 || Player.area == 123
			goal = 130+s
			else if Player.area = 117 || Player.area == 124
			goal = 140+s
			else if (Player.area = 6||Player.area=112) && Player.subarea=2//LABS BOSS
			goal = 1;
			else if (Player.area = 6||Player.area=112)
			goal = 110;
			else if Player.area == 9 && Player.subarea < 3
			goal = 130+s
			else if Player.area == 118 && Player.subarea < 3
			goal = 130+s
			else if Player.area == 10 || Player.area == 121
			{
				styleb = choose(0,0,0,0,0,0,1,1)//Slightly higher chance for B tiles
				goal = 110+s
			}
			else if Player.area == 9 && Player.subarea == 3
			{
				if scrIsGamemode(44)
					goal = 130;
				else
					goal = 350;
			}
			else if Player.area == 118 && Player.subarea == 3
			goal = 300;
			else if Player.area == 119 || Player.area == 120
			{
				if scrIsGamemode(44)
					goal = 140;
				else
					goal = 60;
				s = -10;
			}
			else if Player.area == 126 || Player.area == 127
				goal = 140+s
			else if Player.area == 128 || Player.area == 129
				goal = 140 + s;
			else if Player.area == 130 || Player.area == 131 || Player.area == 132 || Player.area == 133 || Player.area == 134//Factory
				goal = 150 + s;
			else if Player.area == 135//HQ
			{
				goal = 140 + s;
				if Player.subarea == 3
					goal = 1;
			}
			else if Player.area == 137//Void
				goal = 1;
			else if Player.area == 138
				goal = 17;
			else if Player.area == 140
				goal = 25;
			else if Player.area == 139//The pit
			{
				goal = 130;//190
				if Player.loops > 0
					goal -= 10;
			}
			if scrIsGamemode(6)//small levels
			{
				goal=45+s;
				if Player.area == 9 && Player.subarea == 3
					goal = 270;
				if Player.area == 118 && Player.subarea == 3
					goal = 240;
			}
			else if scrIsGamemode(20)//big levels
			{
				goal *= 2;
			}
		}
		if  !instance_exists(GenCont)
			globalGoal = goal;
	}
	//No more random
	firstTry = false;
	if scrIsGamemode(25) //Survival arena
	{
		goal = 2;
	}
	else if instance_exists(MenuGen)
		goal = 80//60
	else if instance_exists(InvertedBigMachine) || instance_exists(BigMachine)
		goal = 0
	endPieceSpawned = false;

	if (scrIsGamemode(25) && !instance_exists(Vlambeer))
	{
		limiter = scrMakeFloor(limiter);
		exit;
	}
	var fc = 0;
	with Floor{
		if canCount
			fc ++;
	}
	var maxLimit = 600;
	if instance_exists(MushroomBoss)
	{
		fc = 0;
	}
	while (fc <= globalGoal && myFloors < goal && limiter < maxLimit)//1000
	{
		fc = 0;
		with Floor{
			if canCount
				fc ++;
		}
		myFloors += 1;
		if UberCont.firstFloorMaker
		{
			//SetSeed();
			UberCont.firstFloorMaker = false;
			if instance_exists(Player) 
			{
				if (Player.area == 9 || Player.area == 118) && Player.subarea == 3
				{
					other.x = 10016;
					other.y = 10016;
					Player.x = other.x;
					Player.y = other.y;
					with WepPickup
					{
						x = Player.x;
						y = Player.y-200;
						speed = 0;
					}
					direction = 90;
				}
				if Player.ultra_got[66] && Player.altUltra && !instance_exists(MushroomBoss)
				{
					snd_play(sndWeaponChest);
					with instance_create(x,y,WepPickup)
					{
						scrWeapons()
						SetSeedWeapon();
						wep = scrDecideWep(0, 8)
						scrAddNewWeaponDrop(wep);
						SetSeed();
						name = wep_name[wep]
						ammo = 40
						type = wep_type[wep]
						curse = 0
						sprite_index = wep_sprt[wep]
					}
				}
			}
		}
		limiter = scrMakeFloor(limiter);
		/*
		with GenCont
		{
			alarm[0] = 3
			alarm[2] = 2
		}
		*/

		with MenuGen
			alarm[1] = 3;
	}
	if limiter >= maxLimit
	{
		alarm[0] = 1;
		with GenCont
			event_user(0);
		return limiter;
	}
	if !instance_exists(MushroomBoss) && !instance_exists(PitNavigation) && point_distance(x,y,10016,10016) > 48
	{
		if instance_exists(Player) && Player.area != 139{
				/*
			if (Player.area == 3 and Player.subarea == 3){//#safe spawns 4 big dog
				Player.x=x+16;
				Player.y=y+16;
			}*/
			instance_create(x,y,Floor)
			if (Player.area == 9 && Player.subarea == 3)
			{
				instance_create(x,y,Floor)
				instance_create(x+32,y,Floor)
				instance_create(x-32,y,Floor)
				instance_create(x+64,y,Floor)
				instance_create(x-64,y,Floor)
				instance_create(x+96,y,Floor)
				instance_create(x-96,y,Floor)
				instance_create(x+128,y,Floor)
				instance_create(x-128,y,Floor)
				instance_create(x+16,y+16,Carpet);
				if scrIsGamemode(44)
				{
					with Carpet
					{
						image_yscale = 0.5;	
					}
					instance_create(x,y,ChesireCat);
				}
				else
				{
					instance_create(x,y,NuclearThrone1);
					var yy = y + 404;
					var i = 0;
					repeat(5)
					{
						with instance_create(x - 96, yy, PalaceGuardianNest) {
							image_index = i;	
						}
						i += 2;
						yy += 132;
					}
					var yy = y + 404;
					var i = 0;
					repeat(5)
					{
						with instance_create(x + 128, yy, PalaceGuardianNest)
						{
							breakY -= 32;
							image_index = i;
						}
						i += 2;
						yy += 132;
					}
					if !scrIsGamemode(26) && !scrIsGamemode(27) && !scrIsGamemode(37) && !scrIsGamemode(50)
					{
						with instance_create(x - 160, y + 320, BigGenerator)
						{
							image_xscale = -1;
							right = -1;
						}
						instance_create(x + 192, y + 320, BigGenerator);
						with instance_create(x - 160, y + 480, BigGenerator)
						{
							image_xscale = -1;
							right = -1;
						}
						instance_create(x + 192, y + 480, BigGenerator);
					}

				}
				if GetPlayerLoops() > 1
					instance_create(x,y + 544, WantTank);
				if scrIsGamemode(6)
				{
					with Carpet
					{
						image_yscale = 0.5;	
					}
				}
			}
			else if (Player.area == 118 && Player.subarea == 3)
			{
				with instance_create(x+16,y+16,Carpet)
					sprite_index = sprInvertedCarpet;
				if scrIsGamemode(44)
				{
					with Carpet
					{
						image_yscale = 0.5;	
					}
					instance_create(x,y,InvertedChesireCat);
				}
				else
				{
					instance_create(x,y,InvertedNuclearThrone1);
				}
				if GetPlayerLoops() > 1
					instance_create(x,y + 544, WantTank);
				// instance_create(x,y + 256, WantTank); No tank here hmmmm
				if scrIsGamemode(6)
				{
					with Carpet
					{
						image_yscale = 0.5;	
					}
				}
			}
			else if Player.race=22
			{
				if Player.area!=104 && Player.area!=100 && !instance_exists(RogueAmmoChest)
					instance_create(x+16,y+16,RogueAmmoChest)
			}
			else if !instance_exists(RogueAmmoChest) && Player.area!=104 && Player.area != 137 && Player.race != 25 && !(Player.area == 9 && Player.subarea ==3)&& !(Player.area == 118 && Player.subarea ==3)//Not mutation smith
				instance_create(x+16,y+16,RadChest)
		}
	}
	instance_destroy();
	return limiter;
}