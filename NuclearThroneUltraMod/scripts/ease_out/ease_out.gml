///ease_out();
// /@description
///@param
function ease_out(t){
	return 1 - (t * t);	
}