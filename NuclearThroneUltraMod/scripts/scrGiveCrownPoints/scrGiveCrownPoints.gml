///scrGiveCrownPoints();
// /@description
///@param
function scrGiveCrownPoints(points = 1) {
	with Player
	{
		crownpoints += points
		currentCrowns = [];
		var al = array_length(crown);
		for (var i = 0; i < al; i++) {
			currentCrowns[i] = crown[i];
		}
	}
}