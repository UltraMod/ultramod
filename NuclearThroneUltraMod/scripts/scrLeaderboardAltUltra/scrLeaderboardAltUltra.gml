///scrLeaderboardAltUltra();
// /@description
///@param
function scrLeaderboardAltUltra(ultraMutation){
	switch (ultraMutation) {
		case 0:
		return sprCashFlowHUD;
		break;
		case 6:
		return sprCrystalCursedUltraHUD;
		break;
		case 9:
		return sprEyesStrangeStyleHUD;
		break;
		case 10:
		return sprVoidStyleHUD;
		break;
		case 13:
		return sprDeathStareHUD;
		break;
		case 18:
		return sprPlantPhotosynthesisHUD;
		break;
		case 19:
		return sprPlantKillKillKillHUD;
		break;
		case 20:
		return sprPlantSonicSpeedHUD;
		break;
		case 21:
		return sprYvVenuzianAirhornHUD;
		break;
		case 23:
		return sprYVBlasphemyHUD;
		break;
		case 24:
		return sprYvGodsDontDieHUD;
		break;
		case 27:
		return sprSteroidsPunchSwapHUD;
		break;
		case 29:
		return sprExclusiveTasteHUD
		break;
		case 30:
		return sprCursedTechonologyHUD
		break;
		case 35:
		return sprReverseFocusHUD;
		break;
		case 39:
		return sprBigRebelHUD;
		break;
		case 43:
		return sprSniperEyeHUD;
		break;
		case 47:
		return sprGreenTeamHUD;
		break;
		case 51:
		return sprHypnotizeHUD;
		break;
		case 55:
		return sprInconsistentIncompatabilityHUD;
		break;
		case 59:
		return sprPathOfDestructionHUD;
		break;
		case 61:
		return sprCaptainOfTheKrakenHUD;
		break;
		case 66:
		return sprQuickSwapperHUD;
		break;
		case 68:
		return sprEnginuityHUD;
		break;
		case 72:
		return sprMirrorHUD;
		break;
		case 76:
		return sprUltimateGambleIconHUD;
		break;
		case 77:
		return sprHoardingThiefHUD;
		break;
		case 87:
		return sprFreakRogueHUD
		break;
		case 92:
		return sprSpikedFrogHUD;
		break;
		case 96:
		return sprAirLordHUD;
		break;
		case 97:
		return sprBeeKeeperHUD;
		break;
		case 104:
		return sprGrumpyLectureHUD;
		break;
		case 106:
		return sprHothandsHUD;
		break;
		case 107:
		return sprExplosiveHandsHUD;
		break;
		default:
		return -1;
		break;
	}
}