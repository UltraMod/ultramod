///scrBleedDamage();
// /@description
///@param dmgTaken
function scrBleedDamage(dmgTaken){
	var newDmg = dmgTaken * 1.5;
	return newDmg;
}