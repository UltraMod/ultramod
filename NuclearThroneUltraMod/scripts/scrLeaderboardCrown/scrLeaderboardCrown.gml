///scrLeaderboardCrown();
// /@description
///@param
function scrLeaderboardCrown(takenCrown){
	switch (takenCrown)
	{
		case 1://None
			return mskPickupThroughWall;
		case 2://Life
			return sprCrown1Idle;
		break;
		case 3://Death
			return sprCrown2Idle;
		break;
		case 4://Haste
			return sprCrown3Idle;
		break;
		case 5://Guns
			return sprCrown4Idle;
		break;
		case 6://Hatred
			return sprCrown5Idle;
		break;
		case 7://Blood
			return sprCrown6Idle;
		break;
		case 8://Destiny
			return sprCrown7Idle;
		break;
		case 9://Love
			return sprCrown8Idle;
		break;
		case 10://Difficulty
			return sprCrown9Idle;
		break;
		case 11://Reincarnation
			return sprCrown10Idle;
		break;
		case 12://Inversion
			return sprCrown11Idle;
		break;
		case 13://Natural selection
			return sprCrown12Idle;
		break;
		case 14://Curses
			return sprCrown13Idle;
		break;
		case 15://Choice
			return sprCrown14Idle;
		break;
		case 16://Popo
			return sprCrown15Idle;
		break;
		case 17://indecision
			return sprCrown16Idle;
		break;
		case 18://Greed
			return sprCrown17Idle;
		break;
		case 19://Blindness
			return sprCrown18Idle;
		break;
		case 20://Protection
			return sprCrown19Idle;
		break;
		case 21://Risk
			return sprCrown20Idle;
		break;
		case 22://Luck
			return sprCrown21Idle;
		break;
		case 23://Speed
			return sprCrown22Idle;
		break;
		case 24://Sloth
			return sprCrown23Idle;
		break;
		case 25://Freedom
			return sprCrown24Idle;
		break;
		case 26://Energy
			return sprCrown25Idle;
		break;
		case 27://Disco
			return sprCrown26Idle;
		break;
		case 28://Apocalypse
			return sprCrown27Idle;
		break;
		case 29://Purity
			return sprCrown28Idle;
		break;
		case 30://Mercenary
			return sprCrown29Idle;
		break;
		case 31://Bounty
			return sprCrown30Idle;
		break;
		case 32://MISFORTUNE
			return sprCrown31Idle;
		break;
		case 33://ECHO
			return sprCrown32Idle
		break;
		case 34://TIME
			return sprCrown33Idle
		break;
		case 35://DICHOTOMY
			return sprCrown34Idle
		break;
		case 36://STABILITY
			return sprCrown35Idle
		break;
		case 37://FRIGHT
			return sprCrown36Idle
		break;
		case 38://ABUNDANCE
			return sprCrown37Idle
		break;
		case 39://DANGER
			return sprCrown38Idle
		break;
		case 40://Thousand cuts
			return sprCrown39Idle
		break;
		case 41://Mediocrity
			return sprCrown40Idle
		break;
		case 42://Scarcity
			return sprCrown41Idle
		break;
		case 43://Frog
			return sprCrownFrogIdle
		break;
	}
	return sprCrownFrogIdle;
}