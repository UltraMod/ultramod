///scrWeaponModInit();
// /@description
///@param
function scrWeaponModInit(){
	canBeMoved = true;
	canBeAngled = true;
	Mod1=0;
	Mod2=0;
	Mod3=0;
	Mod4=0;
	confDropChanceIndex = -1;
	itemDropChanceIndex = -1;
	weaponDropChanceIndex = -1;
	wepFire = 0;
	isVenomized = 0;
	hadSpeedApplied = false;
	poppop = false;
}