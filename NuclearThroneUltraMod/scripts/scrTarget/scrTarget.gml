function scrTarget() {
	target = noone;
	if instance_exists(Player)
	{
		if target == noone && !Player.justAsheep
		{
			if instance_exists(Decoy)//Chickens vanish
			{
				target = Decoy;
			}
			else {
				target = instance_nearest(x,y,Player);
				//Popo and enemies target each other?
				if target != noone
				{
					var distanceToPlayer = point_distance(x,y,target.x,target.y);
					if distanceToPlayer < 300 && instance_exists(Wall) && collision_line(x,y,target.x,target.y,Wall,false,false) < 0
					{
						if instance_exists(enemy) && instance_number(enemy) > 2
						{
							var nearest = instance_nearest_notme(x,y,enemy)
							if instance_exists(nearest) && nearest != noone && nearest.team != team && nearest.team != 0 && point_distance(x,y,nearest.x,nearest.y) < distanceToPlayer
								target = nearest;
						}
					}
				}
			}
		}
		if Player.race == 12 && target == Player.id {
			if instance_exists(YungCuzDupe){//Yung cuz's dupes
			    with enemy
			    {
					var n =instance_nearest(x,y,YungCuzDupe);
					if n != noone
				    if point_distance(x,y,Player.x,Player.y) < point_distance(x,y,n.x,n.y)
				    {
						target = instance_nearest(x,y,Player)
					}
				    else
					{
						target = n
					}
			    }
		    } 
		}
		else if Player.race == 10 && target == Player.id {
			if instance_exists(Ally){//Yung cuz's dupes
			    with enemy
			    {
					var n =instance_nearest(x,y,Ally);
					if n != noone
				    if point_distance(x,y,Player.x,Player.y) < point_distance(x,y,n.x,n.y)
				    {
						target = instance_nearest(x,y,Player)
					}
				    else
					{
						target = n
					}
			    }
		    } 
		}
		else if Player.ultra_got[3] && target == Player.id {
			if instance_exists(Partner){//Yung cuz's dupes
			    with enemy
			    {
					var n =instance_nearest(x,y,Partner);
					if n != noone
				    if point_distance(x,y,Player.x,Player.y) < point_distance(x,y,n.x,n.y)
				    {
						target = instance_nearest(x,y,Player)
					}
				    else
					{
						target = n;
					}
			    }
		    } 
		}
		if instance_exists(RobotTurret){//Robot's turret
			with enemy
			{
				var n =instance_nearest(x,y,RobotTurret);
				if n != noone
				if point_distance(x,y,Player.x,Player.y) < point_distance(x,y,n.x,n.y)
				{
					target = instance_nearest(x,y,Player)
				}
				else
				{
					target = n
				}
			}
		} 
		if target == Player.id && Player.justAsheep
		{
			target = noone;
		}
	}
		
	if target < 0 || !instance_exists(target)
		target = noone
}
