///scrIsAlsoShotgun();
// /@description
///@param
function scrIsAlsoShotgunType(wepIndex){
	return wepIndex == 258 || wepIndex == 680 || scrIsAllAmmoTypes(wepIndex)
	|| wepIndex == 834 || wepIndex == 835 || wepIndex == 836 || wepIndex == 837
	|| wepIndex == 842 || wepIndex == 844 || wepIndex == 845 || wepIndex == 852
	|| wepIndex == 853 || wepIndex == 868 || wepIndex == 62 || wepIndex == 64
	|| wepIndex == 65 || wepIndex == 74 || wepIndex == 154 || wepIndex == 163
	|| wepIndex == 248 || wepIndex == 284 || wepIndex == 285 || wepIndex == 309
	|| wepIndex == 309 || wepIndex == 449 || wepIndex == 450 || wepIndex == 490
	|| wepIndex == 491 || wepIndex == 575 || wepIndex == 664 || wepIndex == 724
	|| wepIndex == 826
}