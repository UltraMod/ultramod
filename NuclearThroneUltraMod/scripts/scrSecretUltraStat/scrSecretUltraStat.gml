function scrSecretUltraStat() {
	
	//0
	secret_ultra_name[0] = "CASH FLOW"
	secret_ultra_text[0] = "REPLACE BEAM WITH <y>CASH FLOW<y>#GET 100 <y>CASH<y> EVERY AREA#<y>CASH<y> MAXIMUM IS 500#KILLS GENERATE <y>CASH<y>##YOU CAN KEEP USING ACTIVE#EVEN WHILE IN <r>DEBT<r>#BUT ACTIVE WILL BE LOCKED UNTIL#OUT OF <r>DEBT<r> AT THE START OF AN AREA"
	secret_ultra_tips[0] = "C.R.E.A.M."
	secret_ultra_hint[0] = "GOLDEN";
	secret_ultra_unlk[0] = "HOLD A GOLDEN WEAPON";
	

	//FISH
	//4
	secret_ultra_name[1] = "FISH CAN GUN"
	secret_ultra_text[1] = "REDUCE <y>AMMO<y>/<g>RAD<g> COST BY 15%#THE MORE <y>AMMO<y> YOU HAVE#THE HIGHER YOUR <w>RELOAD SPEED<w>#DOES NOT HAVE AN EFFECT ON MELEE"
	secret_ultra_tips[1] = "loaded and goated"
	secret_ultra_hint[1] = "EYEPATCH";
	secret_ultra_unlk[1] = "USE C-SKIN";

	//CRYSTAL 
	//6
	secret_ultra_name[2] = "CURSED"
	secret_ultra_text[2] = "<w>TELEPORT<w> AFTER <p>SHIELDING<p>#<p>SHIELDING<p> FIRES <g>LASERS<g>#ALL <g>LASERS<g> BECOME <aq>BOUNCY<aq>##<aq>IMMUNE<aq> TO ENEMY CONTACT DAMAGE#WHILE <p>SHIELDED<p>"
	secret_ultra_tips[2] = "cursed crystal"
	secret_ultra_hint[2] = "CURSED";
	secret_ultra_unlk[2] = "HOLD A CURSED WEAPON";

	//EYES 
	//9
	secret_ultra_name[3] = "STRANGE STYLE"
	secret_ultra_text[3] = "TELEKINESIS PULLS#ENEMIES AND PROJECTILES#TOWARDS YOUR <w>CROSSHAIR<w>#<w>INFINITE<w> TELEKINESIS RANGE#STRONGER PROJECTILE PUSH"
	secret_ultra_tips[3] = "something strange"
	secret_ultra_hint[3] = "TRIPLE EYES";
	secret_ultra_unlk[3] = "HAVE THREE EYES MUTATIONS";

	//MELTING 
	//13
	secret_ultra_name[4] = "DEATH STARE"
	secret_ultra_text[4] = "LOOKING AT ENEMIES SLOWLY KILLS THEM#ENEMIES ARE <w>SCARED<w> OF YOU"
	secret_ultra_tips[4] = "the face of death"
	secret_ultra_hint[4] = "ONE";
	secret_ultra_unlk[4] = "HAVE 1 MAX HP";

	//PLANT
	//20
	secret_ultra_name[5] = "SONIC SPEED"
	secret_ultra_text[5] = "YOU CAN MOVE EXTREMELY <w>FAST<w>!#ACCELERATING OR DECELERATING QUICKLY#CREATES A <w>SONIC BOOM<w>"
	secret_ultra_tips[5] = "SONIC BOOM!"
	secret_ultra_hint[5] = "SPEED";
	secret_ultra_unlk[5] = "HAVE CROWN OF SPEED\nAND EXTRA FEET";

	//Y.V. YUNG VENUZ YV
	//23
	secret_ultra_name[6] = "BLASPHEMY"
	secret_ultra_text[6] = "CAN NO LONGER POP POP GUNS#45% HIGHER <w>MELEE<w> SWING RATE#MELEE IS FULLY <w>AUTOMATIC<w>#CAN POP POP <w>MELEE<w>"
	secret_ultra_tips[6] = "why would you forsake us gun god?"
	secret_ultra_hint[6] = "MELEE";
	secret_ultra_unlk[6] = "ALL WEAPONS YOU HOLD MUST BE MELEE";

    
	//STEROIDS 
	//27
	secret_ultra_name[7] = "PUNCHSWAP"
	secret_ultra_text[7] = "<w>SWAPPING<w> MAKES YOU SWING#WITH YOUR WEAPON#SWINGS <pi>DEFLECT<pi> PROJECTILES#AND DEAL DAMAGE"
	secret_ultra_tips[7] = "GET KNOCKED OUT"
	secret_ultra_hint[7] = "COPY WIELD";
	secret_ultra_unlk[7] = "HOLD TWO OF THE SAME WEAPON TYPE";


	//ROBOT 
	//29
	secret_ultra_name[8] = "EXCLUSIVE TASTE"
	secret_ultra_text[8] = "HOLDING ONLY ONE WEAPON WILL:#INCREASE DAMAGE DEALT TO ENEMIES BY 20%#RETURN 10% <y>AMMO<y>/<g>RAD<g> UPON FIRING#+15% <w>RELOAD SPEED<w> AND +4 MAX <r>HP<r>";
	secret_ultra_tips[8] = "gold tastes good"
	secret_ultra_hint[8] = "EAT THE RICH";
	secret_ultra_unlk[8] = "BE GOLDEN ROBOT\nBY EATING A GOLDEN WEAPON\nUSING A-SKIN";

    
	//CHICKEN 
	//33
	secret_ultra_name[9] = "PHOENIX"
	secret_ultra_text[9] = "CAN NO LONGER BECOME <w>HEADLESS<w> INSTEAD#WHILE YOU ARE ABOVE <g>LEVEL<g> 1#<aq>PREVENT<aq> <w>DEATH<w>#LOSE A LEVEL AND A RANDOM <g>MUTATION<g>##EVERYTIME YOUR DEATH IS PREVENTED#YOU PERMANENTLY BECOME <w>STRONGER<w>#AND YOU <g>LEVEL<g> UP SLOWER"
	secret_ultra_tips[9] = "rise and rise again"
	secret_ultra_hint[9] = "FIRE";
	secret_ultra_unlk[9] = "HOLD A FIRE WEAPON";
	//35
	secret_ultra_name[10] = "SUCOF"
	secret_ultra_text[10] = "EMIT <w>SESREVER<w> EVITCA"//ACTIVE REVERSES PROJECTILES | EMIT SESREVER EVITCA
	secret_ultra_tips[10] = "reverse time"
	secret_ultra_hint[10] = "OPPOSITES";
	secret_ultra_unlk[10] = "HAVE CROWN OF INVERSION";


	//REBEL
	//39
	secret_ultra_name[11] = "BIG REBEL"
	secret_ultra_text[11] = "+61 MAX <r>HP<r>!#YOU CAN'T <r>HEAL<r>#EVERY LOOP RESET HP TO FULL#ALLIES COST <y>AMMO<y>##RESISTANT TO <g>VENOM<g> DAMAGE"
	secret_ultra_tips[11] = "she is a maniac!"
	secret_ultra_hint[11] = "ULTRA / ANTI-HEAL";
	secret_ultra_unlk[11] = "HAVE NO HEALING MUTATIONS\nOR HOLD AN ULTRA WEAPON";


	//HUNTER 
	//43
	secret_ultra_name[12] = "SNIPER EYE"
	secret_ultra_text[12] = "REPLACE YOUR ACTIVE WITH A <w>SNIPER EYE<w>#THE EYE STUNS AND BREAKS <w>WALLS<w>#DAMAGE INCREASES EACH PIERCE#POWER SCALES SLOWLY#<w>FULLY CHARGED<w> EYE <w>DESTROYS<w> ENEMY PROJECTILES"
	secret_ultra_tips[12] = "proper eyesight"
	secret_ultra_hint[12] = "MIND'S EYE";
	secret_ultra_unlk[12] = "HAVE TWO OF THESE\nENERGY BRAIN, EAGLE EYES, OPEN MIND\nOR CROWN OF BLINDNESS";


	//YUNG CUZ 
	//47
	secret_ultra_name[13] = "GREEN TEAM"
	secret_ultra_text[13] = "SPAWNING DUPES COSTS <g>RADS<g>#INSTEAD OF MAX <r>HP<r>#DUPES DROP MORE <y>RES<y><g>OUR<g><r>CES<r>"
	secret_ultra_tips[13] = "pay up"
	secret_ultra_hint[13] = "GREEN RADIATION";
	secret_ultra_unlk[13] = "HAVE ABSORBING PORES\nAND PLUTONIUM HUNGER\nOR HOLD AN ULTRA WEAPON";

    
	//SHEEP 
	//50
	secret_ultra_name[14] = "CODENAME S.A.S. : SECRET AGENT SHEEP"
	secret_ultra_text[14] = "ENEMIES THAT DON'T KNOW YOU'RE THERE#TAKE 10X DAMAGE#TAKE ON THE APPEARANCE#OF ANY <w>CHARACTER<w>#ENABLING YOU TO USE THEIR <w>ACTIVE<w>#(NOT THEIR PASSIVE)#<pi>SECRET GATEWAYS<pi> APPEAR#IN THE <g>CROWN VAULT<g>"
	secret_ultra_tips[14] = "S.A.S."
	secret_ultra_hint[14] = "SECRET QUEEN";
	secret_ultra_unlk[14] = "HAVE A SECRET CROWN";
	//51
	secret_ultra_name[15] = "HYPNOTIZE"
	secret_ultra_text[15] = "ENEMIES REACT AND MOVE SLOWER##ACTIVE CHANGES INTO <pi>HYPNOSIS<pi>#<pi>HYPNOSIS<pi> RE-ACTIVATES YOUR <w>PASSIVE<w>#CAN BE USED 3 TIMES EACH AREA#THRONE BUTT INCREASES USES TO 5"
	secret_ultra_tips[15] = "no one expects a sheep to carry a gun"
	secret_ultra_hint[15] = "WEAK CHARGE";
	secret_ultra_unlk[15] = "DO NOT HAVE EXTRA FEET\nAND GAMMA GUTS";


	//PANDA 
	//55
	secret_ultra_name[16] = "INCONSISTENT INCOMPATABILITY"
	secret_ultra_text[16] = "YOUR <w>HITBOX<w> IS EVEN SMALLER##ALL WEAPONS ARE <w>FULLY AUTOMATIC<w>#YOU NO LONGER THROW WEAPONS#INSTEAD YOUR ACTIVE FIRES YOUR WEAPON#FROM THE ORIGIN OF YOUR <w>CROSSHAIR<w>";
	secret_ultra_tips[16] = "how does it do that"
	secret_ultra_hint[16] = "WEAK PAWS";
	secret_ultra_unlk[16] = "HAVE EITHER FLEXIBLE ELBOWS,\nIMPACT WRISTS OR LONG ARMS\nAND NO THRONEBUTT";


	//ATOM 
	//59
	secret_ultra_name[17] = "PATH OF DESTRUCTION"
	secret_ultra_text[17] = "EVERYWHERE YOU GO#YOU LEAVE BEHIND A TRAIL OF#DAMAGING <g>GOOP<g>##<w>PICKUPS<w> FADE FASTER#FADED PICKUPS <pi>REROLL<pi> INTO OTHER <w>PICKUPS<w>#PICKUPS CAN FADE/REROLL TWICE"
	secret_ultra_tips[17] = "breaking apart reality"
	secret_ultra_hint[17] = "KING OF RAPID DESTRUCTION";
	secret_ultra_unlk[17] = "DO NOT HAVE ENERGY BRAIN\nAND HAVE EITHER CROWN OF APOCALYPSE\nOR CROWN OF HASTE\nOR CROWN OF DEATH";


	//VIKING 
	//61
	secret_ultra_name[18] = "CAPTAIN OF THE KRAKEN"
	secret_ultra_text[18] = "<p>KRAKEN<p> WEAPONS DEAL MORE DAMAGE#<p>TENTACLES<p> ARE LONGER#<p>KRAKEN<p> WEAPONS COST 30% LESS <y>AMMO<y>##<p>TENTACLE<p> <gb>ARMOUR<gb> <w>STRIKE<w>"
	secret_ultra_tips[18] = "from the sea she came"
	secret_ultra_hint[18] = "OCEAN";
	secret_ultra_unlk[18] = "HOLD A KRAKEN WEAPON\nOR HAVE ALIEN INTESTINES\nOR HAVE CROWN OF DROWNING\nOR BE IN THE OASIS";
	//62
	secret_ultra_name[19] = "LIVING ARMOUR"
	secret_ultra_text[19] = "REPLACE ALL OF YOUR <r>HEALTH<r> WITH <gb>MAX ARMOUR<gb>##<r>HEALTH<r> DROPS BECOME <gb>ARMOUR<gb> DROPS#<gb>ARMOUR<gb> DROPS HAVE A LOW CHANCE TO APPEAR##(HEALING MUTATIONS DO NOT#REGENERATE ARMOUR)"
	secret_ultra_tips[19] = "iron woman"
	secret_ultra_hint[19] = "NO HIT POINTS";
	secret_ultra_unlk[19] = "DO NOT HAVE BLOODLUST,\nABSORBING PORES, ALKALINE SALIVA\nAND TOUGH SHELL";

    
	//WEAPON SMITH WEAPONSMITH
	//66
	secret_ultra_name[20] = "QUICK SWAPPER"
	secret_ultra_text[20] = "INCREASE WEAPON PICKUP RANGE##YOU DROP WEAPONS AT YOUR FEET##DROPPING A WEAPON#MAKES YOU <aq>IMMUNE<aq> FOR A SHORT DURATION#FIRST SHOT FIRED WHEN PICKING UP A WEAPON#COSTS 50% LESS AMMO##<p>PORTALS<p> SPIT OUT A NEW <pi>RANDOM<pi> WEAPON#UPON ENTERING AN AREA"
	secret_ultra_tips[20] = "slide swapping!"
	secret_ultra_hint[20] = "SINGLE ARMAMENT";
	secret_ultra_unlk[20] = "HOLD ONLY ONE WEAPON";
	//68
	secret_ultra_name[21] = "ENGINUITY"
	secret_ultra_text[21] = "YOUR ACTIVE BECOMES:#CHANGE YOUR WEAPON#INTO ONE OF THE SAME TIER#YOU CAN GO THROUGH#ALL AVAILABLE WEAPONS##WHENEVER YOU <w>CHANGE<w> OR <w>SWAP<w> WEAPONS#SPAWN <pi>MORPH<pi> ON YOUR <w>CROSSHAIR<w>#<p>THRONEBUTT<p> INCREASES MORPH AMOUNT"
	secret_ultra_tips[21] = "moldable weapons"
	secret_ultra_hint[21] = "REMORPH";
	secret_ultra_unlk[21] = "HOLD A MORPH WEAPON OR MOD";
	

	//ANGEL
	//72
	secret_ultra_name[22] = "MIRROR"
	secret_ultra_text[22] = "SLIGHTLY LONGER <pi>DEFLECT<pi>#ALSO <pi>DEFLECT<pi> ON YOUR CROSSHAIR"
	secret_ultra_tips[22] = "right back at ya"
	secret_ultra_hint[22] = "BROKEN";
	secret_ultra_unlk[22] = "REDUCE YOUR MAX HP\nOR HAVE CROWN OF MISFORTUNE";
		
    
	//SKELETON 
	//74
	secret_ultra_name[23] = "REMINISCE"
	secret_ultra_text[23] = "REPLACE YOUR ACTIVE WITH <r>BLOOD SPLATTER<r>#YOU SPAWN <r>BLOOD<r> TOWARDS NEARBY CORPSES##THRONEBUTT INCREASES AMOUNT OF <r>BLOOD<r>##<r>BLOOD<r> CAN DESTROY#ALL TYPES OF PROJECTILES#<r>BLOOD<r> CAN GO THROUGH <w>WALLS<w>"
	secret_ultra_tips[23] = "i used to have skin and warm blood"
	secret_ultra_hint[23] = "BLOOD GUN";
	secret_ultra_unlk[23] = "HOLD A BLOOD WEAPON";
	//76
	secret_ultra_name[24] = "THE ULTIMATE GAMBLE"
	secret_ultra_text[24] = "50% CHANCE TO <r>DIE<r> OR TO GET#TWO RANDOM ULTRA <g>MUTATIONS<g>#FROM ANOTHER CHARACTER#DOESN'T INCLUDE THINGS YOU CAN'T USE#(such as plant's snare related ultras)"
	secret_ultra_tips[24] = "that went well!"
	secret_ultra_hint[24] = "REBORN";
	secret_ultra_unlk[24] = "USED A LIFE";

	//BUSINESSHOG 
	//77
	secret_ultra_name[25] = "HOARDING THIEF"
	secret_ultra_text[25] = "GO TO YV'S CRIB#YOUR SHOP BECOMES A <w>WEAPON WHEEL<w>#SELECT A SLOT TO STORE#YOUR PRIMARY WEAPON#WEAPONS CAN BE TAKEN OUT AT ANY TIME#THRONE BUTT DOUBLES INVENTORY SIZE##AT THE START OF EACH LOOP#<r>OVERHEAL<r> BASED ON TOTAL#WEAPON TIER#IN YOUR INVENTORY"
	secret_ultra_tips[25] = "tax evasion"
	secret_ultra_hint[25] = "<3 GUNS";
	secret_ultra_unlk[25] = "HAVE HEAVY HEART#OR CROWN OF GUNS";
    
    
	//HORROR 
	// ULTRA E IS ULTRA 0


	//ROGUE 
	//87
	secret_ultra_name[26] = "IMMORTAL POLICE"
	secret_ultra_text[26] = "REPLACE PORTAL STRIKE WITH <g>REVIVE<g>#<g>REVIVE<g> USES <b>PORTAL STRIKE<b> AMMO#LOWER MAXIMUM <b>PORTAL STRIKE<b> AMMO BY 1#THRONE BUTT <g>REVIVES<g> YOU AT FULL <r>HP<r>#INSTEAD OF 1<r>HP<r>#PICKING UP PORTAL STRIKE AMMO#TRIGGERS BLAST ARMOUR"
	secret_ultra_tips[26] = "rogue freak"
	secret_ultra_hint[26] = "EMPTY LIFE";
	secret_ultra_unlk[26] = "HAVE AN EMPTY LIFE";
    
    
	//FROG
	//92
	secret_ultra_name[27] = "SPIKE BALL"
	secret_ultra_text[27] = "NORMAL <w>CONTROLS<w>#EMIT <w>SPLINTERS<w> INSTEAD OF <g>GAS<g>#ALL <w>SPLINTERS<w> ARE <g>GASEOUS<g>"
	secret_ultra_tips[27] = "porkupain"
	secret_ultra_hint[27] = "SPIKE WIELDER";
	secret_ultra_unlk[27] = "ALL WEAPONS YOU HOLD MUST BE BOLT-TYPE";

	//Elementor
	//93
	secret_ultra_name[28] = "AIR LORD"
	secret_ultra_text[28] = "YOUR ACTIVE CHANGES INTO#<w>WIND<w> PUSH#<w>WIND<w> PUSHES PROJECTILES AND ENEMIES#ENEMIES HITTING A <w>WALL<w> TAKE MORE DAMAGE#THRONE BUTT MAKES THE WIND STRONGER"
	secret_ultra_tips[28] = "blow"
	secret_ultra_hint[28] = "PATIENCE"
	secret_ultra_unlk[28] = "USE PATIENCE\nOR HAVE A REROLL READY";
    

	//DOCTOR
	//97
	secret_ultra_name[29] = "BEEKEEPER"
	secret_ultra_text[29] = "<g>RADIATION<g> SPAWNS <w>SWARM BOLTS<w>#ENEMIES HOLD 25% MORE <g>RADS<g>"
	secret_ultra_tips[29] = "retired to a farm"
	secret_ultra_hint[29] = "BOLT SPECIALIST";
	secret_ultra_unlk[29] = "HAVE NO WEAPON MUTATIONS\nEXCEPT FOR BOLT MARROW";


	//GOOD O'L HUMPHRY
	//104
	secret_ultra_name[30] = "GRUMPY LECTURE"
	secret_ultra_text[30] = "ACTIVE COSTS <w>UNEQUIPPED<w> <y>AMMO<y>#INSTEAD OF SKILL#ACTIVE <pi>DEFLECTS<pi> & DESTROYS#ALL ENEMY PROJECTILES"
	secret_ultra_tips[30] = "teach them a lesson!"
	secret_ultra_hint[30] = "DIRECTING TO ZERO";
	secret_ultra_unlk[30] = "HAVE 0 SKILL OR\nHOLD A DIRECTOR WEAPON";
		
	//HANDS
	//Secret B skin ultra
	//106
	secret_ultra_name[31] = "HOT HANDS!"
	secret_ultra_text[31] = "<r>FIERY<r> HANDS#MORE <r>FIRE<r> DAMAGE#HOMING <r>FIRE<r>#EVEN MORE <r>FIRE<r> DAMAGE#WHILE IN <w>INVERTED AREAS<w>#<w>INVERTED PORTALS<w>#(OVER) <r>HEAL<r> YOU FOR <w>3<w> <r>HP<r>"
	secret_ultra_tips[31] = "secret b-skin ultra mutation"
	secret_ultra_hint[31] = "HOT";
	secret_ultra_unlk[31] = "USE B-SKIN";
	//Secret C skin ultra
	//107
	secret_ultra_name[32] = "BOOM HANDS!"
	secret_ultra_text[32] = "<w>EXPLOSIVE<w> HANDS#HAND IS SLOWER#NORMAL HAND SPEED IN <w>INVERTED AREAS<w>#<w>INVERTED PORTALS<w>#(OVER) <r>HEAL<r> YOU FOR <w>3<w> <r>HP<r>"
	secret_ultra_tips[32] = "secret c-skin ultra mutation"
	secret_ultra_hint[32] = "POPO";
	secret_ultra_unlk[32] = "USE C-SKIN";
	
	//109
	secret_ultra_name[33] = "KILL KILL KILL"
	secret_ultra_text[33] = "YOUR ACTIVE CHANGES INTO#<w>KILL KILL KILL<w>:#TARGET ONE ENEMY#DEAL AN <r>ABSURD<r> AMOUNT OF <r>DAMAGE<r> TO IT#AND ANY OF ITS <w>TYPE<w>#IF YOU HAVE <p>THRONEBUTT<p> ALSO <r>HEAL<r> 2 <r>HP<r>#CAN BE DONE THRICE PER AREA"
	secret_ultra_tips[33] = "KILL KILL KILL"
	secret_ultra_hint[33] = "KILL KILL KILL BLOOD BLOOD BLOOD";
	secret_ultra_unlk[33] = "HAVE CROWN OF BLOOD/APOCALYPSE\nOR PLAY GORE GALORE GAMEMODE";
	
	secret_ultra_name[34] = "VOID STYLE"
	secret_ultra_text[34] = "YOUR ACTIVE CHANGES INTO <p>VOID<p> <w>PULL<w>#BURST PULL ALL <r>ENEMIES<r>#AND <r>ENEMY<r> <w>PROJECTILES<w> TOWARDS YOU#CREATE A <p>VOID<p> <w>CIRCLE<w> AROUND YOURSELF#THAT CONSUMES PROJECTILES#AND DEALS DAMAGE TO ENEMIES"
	secret_ultra_tips[34] = "see into the void"
	secret_ultra_hint[34] = "VOID KING GUTS";
	secret_ultra_unlk[34] = "HAVE GAMMA GUTS AND NO CROWN";
	
	secret_ultra_name[35] = "VENUZIAN AIRHORN"
	secret_ultra_text[35] = "NEAR <w>INFINITE RELOAD SPEED!<w>#ALL RELOAD YOU CHEAT#WILL NEED TO BE RELOADED NEXT AREA"
	secret_ultra_tips[35] = "infinite trigger finger"
	secret_ultra_hint[35] = "INNATE FIRE RATE";
	secret_ultra_unlk[35] = "DO NOT HAVE MORE THAN ONE\nRELOAD MUTATION";
	
	secret_ultra_name[36] = "GODS DON'T DIE"
	secret_ultra_text[36] = "WHEN BOTH YOUR GUNS ARE <w>RELOADED<w>#YOU ARE <aq>IMMUNE<aq> FOR 2 SECONDS"
	secret_ultra_tips[36] = "gun god gun god gun god"
	secret_ultra_hint[36] = "GODS DON'T DIE";
	secret_ultra_unlk[36] = "DO NOT HAVE ANY\nDEATH PREVENTION MUTATIONS";
	
	secret_ultra_name[37] = "CURSED TECHNOLOGY"
	secret_ultra_text[37] = "INSTALL <g>LASER<g> <w>DEFENSE<w> SYSTEM#<p>CURSED PICKUPS<p> ATTACK ENEMIES#EATING <p>CURSED WEAPONS<p> (OVER) <r>HEALS<r> <w>1<w><r>HP<r>##ONCE PER AREA:#WHEN TAKING <aq>LETHAL DAMAGE<aq>#<p>TELEPORT<p> AWAY FROM ENEMIES"
	secret_ultra_tips[37] = "eating guns from other dimensions"
	secret_ultra_hint[37] = "SPOILED FOOD";
	secret_ultra_unlk[37] = "BE CURSED ROBOT\nBY EATING A CURSED WEAPON\nUSING A-SKIN";
	
	var regalBut = "E";
		if instance_exists(UberCont)
			regalBut = scrAsciiChar(UberCont.opt_regal)
	secret_ultra_name[38] = "PHOTOSYNTHESIS"
	secret_ultra_text[38] = "PRESS <w>[" + regalBut + "]<w> TO MOVE#AWAY FROM <r>ENEMIES<r> AT <w>LIGHTSPEED<w>##YOU CAN ALWAYS <r>OVERHEAL<r> <w>+2<w>##WHENEVER YOU <r>HEAL<r>:#CAST THE POWER OF THE <y>SUN<y>#ON YOUR <w>CROSSHAIR<w>"
	secret_ultra_tips[38] = "BY THE POWER OF THE SUN!"
	secret_ultra_hint[38] = "DOUBLE HEALING";
	secret_ultra_unlk[38] = "HAVE ATLEAST TWO\nHEALING MUTATIONS";

	
	maxsecretultra = array_length(secret_ultra_name);
	//109
	secret_ultra_name[maxsecretultra + 1] = "TRASH"
	secret_ultra_text[maxsecretultra + 1] = "<pi>DIE<pi> WHENEVER YOU TAKE DAMAGE"
	secret_ultra_tips[maxsecretultra + 1] = "challenge run"
	secret_ultra_hint[maxsecretultra + 1] = "";
	secret_ultra_unlk[maxsecretultra + 1] = "WIELD NO WEAPON";
	

	/*
	secret_ultra_name[38] = "CRYSTALINE TORPEDO"
	secret_ultra_text[38] = "+2 MAX HP#ACTIVE COSTS 1 HP#WHILE SHIELDING#LAUNCH YOURSELF INTO A MASSIVE CRYSTAL TORPEDO#BREAK WALLS AND DEAL TONS OF DAMAGE#COMPLETLY <aq>IMMUNE<aq> WHILE SHIELDING"
	secret_ultra_tips[38] = "the boulder"
	secret_ultra_hint[38] = "CAVES";
	secret_ultra_unlk[38] = "REACH LEVEL ULTRA IN CRYSTAL CAVES";
	*/
	
	/*
	var dir = 0
	repeat(maxsecretultra)
	{secret_ultra_got[dir] = 0
	dir += 1}*/

}
