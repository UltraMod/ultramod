///scrWepModName();
// /@description
///@param wepmod
function scrWepModName(wm){
	switch (wm)
	{
		case 1:
		return "[TOXIC]"
		case 2:
		return "[EXPLOSIVE]"
		case 3:
		return "[FLAME]"
		case 4:
		return "[FROST]"
		case 5:
		return "[BLOOD]"
		case 6:
		return "[LIGHTNING]"
		case 7:
		return "[KRAKEN]"
		case 8:
		return "[SWARM]"
		case 9:
		return "[BOUNCER]"
		case 10:
		return "[SHOTGUN]"
		case 11:
		return "[PROJECTILE SPEED]"
		case 12:
		return "[RELOAD SPEED]"
		case 13:
		return "[HOMING]"
		case 14:
		return "[RADIATION]"
		case 15:
		return "[SPLINTER]"
		case 16:
		return "[MORPH]"
		case 17:
		return "[HP/AMMO DROP]"
		case 18:
		return "[WEAPON DROP]"
		default:
		return "UNKNOWN MOD?#IF YOU SEE THIS SOMETHING IS WRONG#PLS REPORT @ERDEPPOL"
	}
}