{
  "$GMSound":"",
  "%Name":"sndBlobTrail",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":3.990937,
  "name":"sndBlobTrail",
  "parent":{
    "name":"Explosive",
    "path":"folders/Sounds/Weapons/Explosive.yy",
  },
  "preload":true,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndBlobTrail.wav",
  "type":0,
  "volume":1.0,
}