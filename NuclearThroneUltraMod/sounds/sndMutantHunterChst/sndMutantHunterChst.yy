{
  "$GMSound":"",
  "%Name":"sndMutantHunterChst",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":1.427625,
  "name":"sndMutantHunterChst",
  "parent":{
    "name":"Hunter",
    "path":"folders/Sounds/Player/Hunter.yy",
  },
  "preload":false,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndMutantHunterChst.ogg",
  "type":0,
  "volume":1.0,
}