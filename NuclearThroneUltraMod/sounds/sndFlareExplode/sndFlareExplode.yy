{
  "$GMSound":"",
  "%Name":"sndFlareExplode",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":0.549342,
  "name":"sndFlareExplode",
  "parent":{
    "name":"Dragon",
    "path":"folders/Sounds/Weapons/Dragon.yy",
  },
  "preload":true,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndFlareExplode",
  "type":0,
  "volume":1.0,
}