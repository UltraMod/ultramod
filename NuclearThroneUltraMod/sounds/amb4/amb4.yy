{
  "$GMSound":"",
  "%Name":"amb4",
  "audioGroupId":{
    "name":"audiogroup_default",
    "path":"audiogroups/audiogroup_default",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":3,
  "conversionMode":0,
  "duration":32.0,
  "name":"amb4",
  "parent":{
    "name":"Music & amb",
    "path":"folders/Sounds/Music & amb.yy",
  },
  "preload":true,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"amb4.ogg",
  "type":1,
  "volume":1.0,
}