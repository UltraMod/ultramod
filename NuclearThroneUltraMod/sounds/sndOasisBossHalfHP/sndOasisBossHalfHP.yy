{
  "$GMSound":"",
  "%Name":"sndOasisBossHalfHP",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":1.24,
  "name":"sndOasisBossHalfHP",
  "parent":{
    "name":"OassisBoss",
    "path":"folders/Sounds/Enemies/Boss/OassisBoss.yy",
  },
  "preload":true,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndOasisBossHalfHP",
  "type":0,
  "volume":1.0,
}