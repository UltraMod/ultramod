{
  "$GMSound":"",
  "%Name":"sndTurtleShotgun",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":1.532517,
  "name":"sndTurtleShotgun",
  "parent":{
    "name":"Shell",
    "path":"folders/Sounds/Weapons/Shell.yy",
  },
  "preload":true,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndTurtleShotgun.ogg",
  "type":0,
  "volume":1.0,
}