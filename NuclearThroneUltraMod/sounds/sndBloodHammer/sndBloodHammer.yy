{
  "$GMSound":"",
  "%Name":"sndBloodHammer",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":1.162719,
  "name":"sndBloodHammer",
  "parent":{
    "name":"Melee",
    "path":"folders/Sounds/Weapons/Melee.yy",
  },
  "preload":true,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndBloodHammer",
  "type":0,
  "volume":1.0,
}