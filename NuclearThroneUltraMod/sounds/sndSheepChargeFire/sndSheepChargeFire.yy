{
  "$GMSound":"",
  "%Name":"sndSheepChargeFire",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":0.627868,
  "name":"sndSheepChargeFire",
  "parent":{
    "name":"Sheep",
    "path":"folders/Sounds/Player/Sheep.yy",
  },
  "preload":true,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndSheepChargeFire.ogg",
  "type":0,
  "volume":1.0,
}