{
  "$GMSound":"",
  "%Name":"ambUltra138",
  "audioGroupId":{
    "name":"audiogroup_default",
    "path":"audiogroups/audiogroup_default",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":3,
  "conversionMode":0,
  "duration":29.991043,
  "name":"ambUltra138",
  "parent":{
    "name":"Music & amb",
    "path":"folders/Sounds/Music & amb.yy",
  },
  "preload":true,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"ambUltra138.ogg",
  "type":0,
  "volume":1.0,
}