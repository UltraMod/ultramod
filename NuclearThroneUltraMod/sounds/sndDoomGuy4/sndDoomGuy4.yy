{
  "$GMSound":"",
  "%Name":"sndDoomGuy4",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":0.41059,
  "name":"sndDoomGuy4",
  "parent":{
    "name":"Explosive",
    "path":"folders/Sounds/Weapons/Explosive.yy",
  },
  "preload":true,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndDoomGuy4.ogg",
  "type":0,
  "volume":1.0,
}