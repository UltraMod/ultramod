{
  "$GMSound":"",
  "%Name":"sndWallBreakJungle",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":1.100625,
  "name":"sndWallBreakJungle",
  "parent":{
    "name":"Wallbreak",
    "path":"folders/Sounds/Enviroment/Wallbreak.yy",
  },
  "preload":false,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndWallBreakJungle.wav",
  "type":0,
  "volume":0.2,
}