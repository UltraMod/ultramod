{
  "$GMSound":"",
  "%Name":"sndEnergyPelletBounce",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":0.158662,
  "name":"sndEnergyPelletBounce",
  "parent":{
    "name":"HitWall",
    "path":"folders/Sounds/HitWall.yy",
  },
  "preload":true,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndEnergyPelletBounce.ogg",
  "type":0,
  "volume":1.0,
}