{
  "$GMSound":"",
  "%Name":"sndHandThrowGrab",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":0.266667,
  "name":"sndHandThrowGrab",
  "parent":{
    "name":"Hands",
    "path":"folders/Sounds/Player/Hands.yy",
  },
  "preload":false,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndHandThrowGrab.wav",
  "type":0,
  "volume":1.0,
}