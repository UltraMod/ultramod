{
  "$GMSound":"",
  "%Name":"sndMutant1Cnfm",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":0.395011,
  "name":"sndMutant1Cnfm",
  "parent":{
    "name":"Fish",
    "path":"folders/Sounds/Player/Fish.yy",
  },
  "preload":false,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndMutant1Cnfm",
  "type":0,
  "volume":1.0,
}