{
  "$GMSound":"",
  "%Name":"sndMutant1Thrn",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":1.925896,
  "name":"sndMutant1Thrn",
  "parent":{
    "name":"Fish",
    "path":"folders/Sounds/Player/Fish.yy",
  },
  "preload":false,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndMutant1Thrn",
  "type":0,
  "volume":1.0,
}