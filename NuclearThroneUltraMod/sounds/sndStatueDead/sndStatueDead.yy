{
  "$GMSound":"",
  "%Name":"sndStatueDead",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":4.202676,
  "name":"sndStatueDead",
  "parent":{
    "name":"Enviroment",
    "path":"folders/Sounds/Enviroment.yy",
  },
  "preload":true,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndStatueDead",
  "type":0,
  "volume":1.0,
}