{
  "$GMSound":"",
  "%Name":"sndFishRollUpg",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":0.302381,
  "name":"sndFishRollUpg",
  "parent":{
    "name":"Fish",
    "path":"folders/Sounds/Player/Fish.yy",
  },
  "preload":true,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndFishRollUpg",
  "type":0,
  "volume":1.0,
}