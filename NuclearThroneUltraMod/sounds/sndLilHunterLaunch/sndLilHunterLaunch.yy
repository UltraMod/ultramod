{
  "$GMSound":"",
  "%Name":"sndLilHunterLaunch",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":1.061338,
  "name":"sndLilHunterLaunch",
  "parent":{
    "name":"Regular",
    "path":"folders/Sounds/Enemies/Regular.yy",
  },
  "preload":true,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndLilHunterLaunch",
  "type":0,
  "volume":1.0,
}