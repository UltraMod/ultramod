{
  "$GMSound":"",
  "%Name":"sndCorpseExplo",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":0.44,
  "name":"sndCorpseExplo",
  "parent":{
    "name":"Player",
    "path":"folders/Sounds/Player.yy",
  },
  "preload":true,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndCorpseExplo",
  "type":0,
  "volume":1.0,
}