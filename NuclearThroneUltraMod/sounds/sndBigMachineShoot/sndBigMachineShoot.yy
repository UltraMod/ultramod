{
  "$GMSound":"",
  "%Name":"sndBigMachineShoot",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":1.544687,
  "name":"sndBigMachineShoot",
  "parent":{
    "name":"TheThrone",
    "path":"folders/Sounds/Palace/TheThrone.yy",
  },
  "preload":true,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndBigMachineShoot.wav",
  "type":0,
  "volume":1.0,
}