{
  "$GMSound":"",
  "%Name":"sndBloodCannon",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":3.536062,
  "name":"sndBloodCannon",
  "parent":{
    "name":"Explosive",
    "path":"folders/Sounds/Weapons/Explosive.yy",
  },
  "preload":true,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndBloodCannon",
  "type":0,
  "volume":1.0,
}