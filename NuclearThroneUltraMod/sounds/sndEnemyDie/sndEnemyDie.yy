{
  "$GMSound":"",
  "%Name":"sndEnemyDie",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":0.429342,
  "name":"sndEnemyDie",
  "parent":{
    "name":"Regular",
    "path":"folders/Sounds/Enemies/Regular.yy",
  },
  "preload":false,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndEnemyDie",
  "type":0,
  "volume":1.0,
}