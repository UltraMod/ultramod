{
  "$GMSound":"",
  "%Name":"sndHyperLightning",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":0.892062,
  "name":"sndHyperLightning",
  "parent":{
    "name":"Energy",
    "path":"folders/Sounds/Weapons/Energy.yy",
  },
  "preload":false,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndHyperLightning",
  "type":0,
  "volume":1.0,
}