{
  "$GMSound":"",
  "%Name":"sndNothing2Taunt",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":128,
  "compression":0,
  "conversionMode":0,
  "duration":4.866688,
  "name":"sndNothing2Taunt",
  "parent":{
    "name":"TheThrone",
    "path":"folders/Sounds/Palace/TheThrone.yy",
  },
  "preload":false,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndNothing2Taunt.wav",
  "type":0,
  "volume":1.0,
}