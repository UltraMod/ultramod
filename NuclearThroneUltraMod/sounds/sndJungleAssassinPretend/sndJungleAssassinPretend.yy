{
  "$GMSound":"",
  "%Name":"sndJungleAssassinPretend",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":128,
  "compression":0,
  "conversionMode":0,
  "duration":1.466687,
  "name":"sndJungleAssassinPretend",
  "parent":{
    "name":"Jungle",
    "path":"folders/Sounds/Enemies/Jungle.yy",
  },
  "preload":false,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndJungleAssassinPretend.wav",
  "type":0,
  "volume":1.0,
}