{
  "$GMSound":"",
  "%Name":"sndHammer",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":0.293333,
  "name":"sndHammer",
  "parent":{
    "name":"Melee",
    "path":"folders/Sounds/Weapons/Melee.yy",
  },
  "preload":true,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndHammer",
  "type":0,
  "volume":1.0,
}