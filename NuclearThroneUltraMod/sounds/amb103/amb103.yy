{
  "$GMSound":"",
  "%Name":"amb103",
  "audioGroupId":{
    "name":"audiogroup_default",
    "path":"audiogroups/audiogroup_default",
  },
  "bitDepth":1,
  "bitRate":128,
  "compression":3,
  "conversionMode":0,
  "duration":32.0,
  "name":"amb103",
  "parent":{
    "name":"Music & amb",
    "path":"folders/Sounds/Music & amb.yy",
  },
  "preload":false,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"amb103.ogg",
  "type":0,
  "volume":1.0,
}