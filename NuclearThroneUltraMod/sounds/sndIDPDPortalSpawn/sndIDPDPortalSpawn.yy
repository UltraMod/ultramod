{
  "$GMSound":"",
  "%Name":"sndIDPDPortalSpawn",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":5.448073,
  "name":"sndIDPDPortalSpawn",
  "parent":{
    "name":"IDPD sounds",
    "path":"folders/Sounds/IDPD sounds.yy",
  },
  "preload":false,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndIDPDPortalSpawn",
  "type":0,
  "volume":0.9,
}