{
  "$GMSound":"",
  "%Name":"sndMutant20Cnfm",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":0.516893,
  "name":"sndMutant20Cnfm",
  "parent":{
    "name":"BusinessHog",
    "path":"folders/Sounds/Player/BusinessHog.yy",
  },
  "preload":true,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndMutant20Cnfm",
  "type":0,
  "volume":1.0,
}