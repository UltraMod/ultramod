{
  "$GMSound":"",
  "%Name":"sndMutant3Dead",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":1.552018,
  "name":"sndMutant3Dead",
  "parent":{
    "name":"Player",
    "path":"folders/Sounds/Player.yy",
  },
  "preload":false,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndMutant3Dead",
  "type":0,
  "volume":1.0,
}