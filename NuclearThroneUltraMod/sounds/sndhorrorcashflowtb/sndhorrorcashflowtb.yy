{
  "$GMSound":"",
  "%Name":"sndHorrorCashFlowTB",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":0.184104,
  "name":"sndHorrorCashFlowTB",
  "parent":{
    "name":"Horror",
    "path":"folders/Sounds/Player/Horror.yy",
  },
  "preload":true,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndhorrorcashflowtb.ogg",
  "type":0,
  "volume":1.0,
}