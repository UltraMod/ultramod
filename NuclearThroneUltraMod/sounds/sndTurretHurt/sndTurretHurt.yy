{
  "$GMSound":"",
  "%Name":"sndTurretHurt",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":128,
  "compression":0,
  "conversionMode":0,
  "duration":0.501344,
  "name":"sndTurretHurt",
  "parent":{
    "name":"Labs",
    "path":"folders/Sounds/Labs.yy",
  },
  "preload":false,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndTurretHurt.wav",
  "type":0,
  "volume":1.0,
}