{
  "$GMSound":"",
  "%Name":"sndFreakPopoHurt",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":128,
  "compression":0,
  "conversionMode":0,
  "duration":0.493344,
  "name":"sndFreakPopoHurt",
  "parent":{
    "name":"Idpd",
    "path":"folders/Sounds/Enemies/Idpd.yy",
  },
  "preload":false,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndFreakPopoHurt.wav",
  "type":0,
  "volume":1.0,
}