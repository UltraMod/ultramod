{
  "$GMSound":"",
  "%Name":"sndGruntDeadM",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":0.671594,
  "name":"sndGruntDeadM",
  "parent":{
    "name":"Grunt",
    "path":"folders/Sounds/IDPD sounds/Grunt.yy",
  },
  "preload":false,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndGruntDeadM",
  "type":0,
  "volume":1.0,
}