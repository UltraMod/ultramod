{
  "$GMSound":"",
  "%Name":"sndLastNotifyDeath",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":128,
  "compression":0,
  "conversionMode":0,
  "duration":3.277375,
  "name":"sndLastNotifyDeath",
  "parent":{
    "name":"Captain",
    "path":"folders/Sounds/Enemies/Boss/Captain.yy",
  },
  "preload":false,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndLastNotifyDeath.wav",
  "type":0,
  "volume":1.0,
}