{
  "$GMSound":"",
  "%Name":"sndPlopMinigun",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":0.354512,
  "name":"sndPlopMinigun",
  "parent":{
    "name":"Weapons",
    "path":"folders/Sounds/Weapons.yy",
  },
  "preload":true,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndPlopMinigun.ogg",
  "type":0,
  "volume":1.0,
}