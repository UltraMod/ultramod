{
  "$GMSound":"",
  "%Name":"sndRogueCanister",
  "audioGroupId":{
    "name":"agsfx",
    "path":"audiogroups/agsfx",
  },
  "bitDepth":1,
  "bitRate":192,
  "compression":0,
  "conversionMode":0,
  "duration":0.568031,
  "name":"sndRogueCanister",
  "parent":{
    "name":"Rogue",
    "path":"folders/Sounds/Player/Rogue.yy",
  },
  "preload":true,
  "resourceType":"GMSound",
  "resourceVersion":"2.0",
  "sampleRate":44100,
  "soundFile":"sndRogueCanister",
  "type":0,
  "volume":1.0,
}