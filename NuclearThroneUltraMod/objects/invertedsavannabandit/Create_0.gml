event_inherited()
raddrop = 9;
maxhealth = 5;
EnemyHealthAdjustments();
spr_idle = sprInvertedSavannaBanditIdle
spr_walk = sprInvertedSavannaBanditWalk
spr_hurt = sprInvertedSavannaBanditHurt
spr_dead = sprInvertedSavannaBanditDead
spr_gun = sprInvertedSavannaBanditGun;

snd_hurt = sndBanditHit
snd_dead = sndBanditDie
loops ++;
ammo = 8;
maxammo = 8;