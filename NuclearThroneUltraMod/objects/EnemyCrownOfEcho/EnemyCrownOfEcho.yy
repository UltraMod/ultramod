{
  "$GMObject":"",
  "%Name":"EnemyCrownOfEcho",
  "eventList":[
    {"$GMEvent":"v1","%Name":"","collisionObjectId":null,"eventNum":0,"eventType":0,"isDnD":false,"name":"","resourceType":"GMEvent","resourceVersion":"2.0",},
  ],
  "managed":true,
  "name":"EnemyCrownOfEcho",
  "overriddenProperties":[],
  "parent":{
    "name":"CrownCourtyard",
    "path":"folders/Objects/Enemies/CrownCourtyard.yy",
  },
  "parentObjectId":{
    "name":"EnemyCrown",
    "path":"objects/EnemyCrown/EnemyCrown.yy",
  },
  "persistent":false,
  "physicsAngularDamping":0.1,
  "physicsDensity":0.5,
  "physicsFriction":0.2,
  "physicsGroup":0,
  "physicsKinematic":false,
  "physicsLinearDamping":0.1,
  "physicsObject":false,
  "physicsRestitution":0.1,
  "physicsSensor":false,
  "physicsShape":0,
  "physicsShapePoints":[],
  "physicsStartAwake":true,
  "properties":[],
  "resourceType":"GMObject",
  "resourceVersion":"2.0",
  "solid":false,
  "spriteId":{
    "name":"sprCrown32IdleEnemy",
    "path":"sprites/sprCrown32IdleEnemy/sprCrown32IdleEnemy.yy",
  },
  "spriteMaskId":{
    "name":"mskWalkingCrown",
    "path":"sprites/mskWalkingCrown/mskWalkingCrown.yy",
  },
  "visible":true,
}