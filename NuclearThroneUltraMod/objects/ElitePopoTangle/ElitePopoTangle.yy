{
  "$GMObject":"",
  "%Name":"ElitePopoTangle",
  "eventList":[
    {"$GMEvent":"v1","%Name":"","collisionObjectId":null,"eventNum":0,"eventType":0,"isDnD":false,"name":"","resourceType":"GMEvent","resourceVersion":"2.0",},
    {"$GMEvent":"v1","%Name":"","collisionObjectId":{"name":"hitme","path":"objects/hitme/hitme.yy",},"eventNum":0,"eventType":4,"isDnD":false,"name":"","resourceType":"GMEvent","resourceVersion":"2.0",},
  ],
  "managed":true,
  "name":"ElitePopoTangle",
  "overriddenProperties":[],
  "parent":{
    "name":"Ability",
    "path":"folders/Objects/Player/Ability.yy",
  },
  "parentObjectId":{
    "name":"PopoTangle",
    "path":"objects/PopoTangle/PopoTangle.yy",
  },
  "persistent":false,
  "physicsAngularDamping":0.1,
  "physicsDensity":0.5,
  "physicsFriction":0.2,
  "physicsGroup":0,
  "physicsKinematic":false,
  "physicsLinearDamping":0.1,
  "physicsObject":false,
  "physicsRestitution":0.1,
  "physicsSensor":false,
  "physicsShape":0,
  "physicsShapePoints":[],
  "physicsStartAwake":true,
  "properties":[],
  "resourceType":"GMObject",
  "resourceVersion":"2.0",
  "solid":false,
  "spriteId":{
    "name":"sprSlowField",
    "path":"sprites/sprSlowField/sprSlowField.yy",
  },
  "spriteMaskId":null,
  "visible":true,
}