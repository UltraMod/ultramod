/// @description CanDoDamage

// Inherit the parent event
event_inherited();

alarm[0] = 60;
dealtDamage = false;
dmg = 1;
slowdown = 0.15;
image_xscale = 1.5;
image_yscale = 1.5;