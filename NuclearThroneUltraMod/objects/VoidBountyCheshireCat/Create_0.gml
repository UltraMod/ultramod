/// @description Change bountyNumber

// Inherit the parent event
event_inherited();
name = "COMPLETE [CHESHIRE CAT] BOSS BOUNTY";
bountyNumber = secretChallengeBosses.cheshireCat;
item_index = bountyNumber;