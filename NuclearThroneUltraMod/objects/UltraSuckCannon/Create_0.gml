/// @description Init

// Inherit the parent event
event_inherited();
image_xscale = 1.25;
image_yscale = 1.25;
dmg += 2;
cantSuck = -10;
suckStrength = 3;
loopSnd = sndUltraSuckCannonLoop;
sndRelease = sndUltraSuckCannonRelease;
canSuckEnem = 2;