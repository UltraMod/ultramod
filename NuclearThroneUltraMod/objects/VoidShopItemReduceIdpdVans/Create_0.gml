/// @description Give a sprite

// Inherit the parent event
event_inherited();

spr_item = sprVoidItemIdpdVan;
name = "REDUCE IDPD VANS THIS RUN";
if UberCont.extraVan == -1
	cost = 16;
else
	cost = 14;