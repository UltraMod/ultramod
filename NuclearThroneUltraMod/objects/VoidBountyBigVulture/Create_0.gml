/// @description Change bountyNumber

// Inherit the parent event
event_inherited();
name = "COMPLETE [BIG VULTURE] BOSS BOUNTY";
bountyNumber = secretChallengeBosses.bigVulture;
item_index = bountyNumber;