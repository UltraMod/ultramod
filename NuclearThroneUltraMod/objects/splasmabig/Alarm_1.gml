/// @description Spawn tiny spiraling splasma balls
alarm[1] = 12;
if BackCont.shake < 2
	BackCont.shake = 2
with instance_create(x,y,SplasmaBall)
{
	direction = other.direction;
	speed = 1
	scrCopyWeaponMod(other);
	angleDir = 20;
}
with instance_create(x,y,SplasmaBall)
{
	direction = other.direction+180;
	speed = 1
	scrCopyWeaponMod(other);
	angleDir = 20;
}
snd_play(sndPlasmaMinigunUpg,0.2,true);