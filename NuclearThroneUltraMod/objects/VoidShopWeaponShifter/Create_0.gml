/// @description Give a sprite

// Inherit the parent event
event_inherited();
spawnLocationY = y - 32;
spr_item = sprVoidItemWeaponShifter;
name = "REROLL WEAPON TO ANOTHER OF THE SAME TIER";
cost = 1;
originalWep = 0;
wep = 0;
chooseTimer = 0;
scrRaces();
scrStartingWeapons();