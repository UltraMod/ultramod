event_inherited();
friction = 0.1//0.6
typ = 2 //0 = normal, 1 = deflectable, 2 = destructable
candmg=true;
dmg = 30;
boltStick = BoltStick;
hitEntities = [];
trailScale = 1;
if UberCont.ultramodSwap
	event_user(0);
xprev = x;
yprev = y;
hitEntities = [];
boltMarrow = false;
if instance_exists(Player) && Player.skill_got[21]
	boltMarrow = true;