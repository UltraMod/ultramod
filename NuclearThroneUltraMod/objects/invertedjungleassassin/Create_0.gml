event_inherited()
raddrop = 12;
maxhealth = 7//12
EnemyHealthAdjustments()
spr_idle = sprInvertedJungleAssassinIdle
spr_walk = sprInvertedJungleAssassinWalk
spr_hurt = sprInvertedJungleAssassinHurt
spr_dead = sprInvertedJungleAssassinDead
maxSpeed = 4.5;
tellTime = 10;
actTime = 1;
range = 80;
isInverted = true;
if loops > 0
	tellTime = 8;