ammo -= 1

alarm[0] = time


if instance_exists(creator)
{
x = creator.x
y = creator.y
//FIRING
snd_play_fire(snd)
repeat(2)
	with instance_create(x,y,Shell)
		motion_add(point_direction(x,y,UberCont.mouse__x,UberCont.mouse__y)+other.aimOffset+180+random(50)-25,2+random(2))
		
var boost = 0;
if instance_exists(Player)
	boost = Player.skill_got[13];
with instance_create(x,y,SpinSlashBullet)
{
	if other.ultramodded
		alarm[11] = 0;
	motion_add(point_direction(x,y,UberCont.mouse__x,UberCont.mouse__y)+other.aimOffset+(random(4)-2*other.accuracy),16 + other.ammo + boost)
	scrCopyWeaponMod(other);

	image_angle = direction
	team = other.team
}

BackCont.viewx2 += lengthdir_x(9,point_direction(x,y,UberCont.mouse__x,UberCont.mouse__y)+180)*UberCont.opt_shake
BackCont.viewy2 += lengthdir_y(9,point_direction(x,y,UberCont.mouse__x,UberCont.mouse__y)+180)*UberCont.opt_shake
BackCont.shake += 5
creator.wkick = 4.5}


if ammo <= 0
instance_destroy()

