event_inherited()
if object_index != SuperSeekerBall
{
	snd_play(sndSeekerShotgun,0.1,true)
	Sleep(10)
	instance_create(x+lengthdir_x(12,direction),y+lengthdir_y(12,direction),WallBreakWallOnly);
	var am = 6;
	var ang = fireRotation;
	var angStep = 360/am;
	repeat(am)
	{
		with instance_create(x,y,SeekerBolt)
		{motion_add(ang,7)
		image_angle = direction
		team = other.team}
		ang += angStep;
	}
}