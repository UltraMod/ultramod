/// @description Init
event_inherited();
typ = 3;
canBeMoved = false;
target = -1;
alarm[0] = 80;//lifetime
alarm[1] = 11;
alarm[3] = 1;
doesNotMove = false;
fallAngle = 270 + random_range(-20,20);
team = 1;