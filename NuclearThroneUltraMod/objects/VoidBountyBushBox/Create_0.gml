/// @description Change bountyNumber

// Inherit the parent event
event_inherited();
name = "COMPLETE [BUSH BOX] BOSS BOUNTY";
bountyNumber = secretChallengeBosses.bushBox;
item_index = bountyNumber;