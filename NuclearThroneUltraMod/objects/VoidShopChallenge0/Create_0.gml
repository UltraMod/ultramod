/// @description Give a sprite

// Inherit the parent event
event_inherited();

spr_item = sprVoidChallengeIcon;
challenge = "25% LOWER RELOAD SPEED"
reward = "DOUBLE YOUR PORTAL ESSENCE";
cost = 1;