/// @description Speed up and spin

// Inherit the parent event
event_inherited();
grow -= 0.01;
acc += 1;
maxSpeed += 6;
alarm[1] = 2;
angleDir = 0;
sdelay = 10;
alarm[2] = 30;