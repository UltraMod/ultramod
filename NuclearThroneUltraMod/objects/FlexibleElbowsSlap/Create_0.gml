/// @description Init
target = noone;
owner = noone;
offset = choose(20,-20);
time = 0;
followOwner = true;
tx = x;
ty = y;
snd_play(sndChickenThrow);
queue = 0;
hook = 0;
scrInitDrops(1);
team = 2;