/// @description Toggle Stealth on
if !place_meeting(x,y,Tangle)
{
	targetAlpha = stealthAlpha;
	stalking = true;
}