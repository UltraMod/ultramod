/// @description Init

// Inherit the parent event
event_inherited();
radius = 100;
dmg = 6;
alarm[0] = 1;
hits = [];
allHits = 0;
index = 0;
time = 2;
rotation = 20;
image_angle = random(360);
middleAngle = random(360);
outerAngle = random(360);
image_xscale = 1.5;
image_yscale = 1.5;