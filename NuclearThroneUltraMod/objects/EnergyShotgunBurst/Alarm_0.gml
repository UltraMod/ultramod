ammo -= 1

alarm[0] = time

if instance_exists(creator)
{
	x = creator.x
	y = creator.y
	//FIRING
	snd_play(sndEnergyShotgunShot,0.1);
	var aimDirection = point_direction(x,y,UberCont.mouse__x,UberCont.mouse__y);
	var ang = aimDirection - ((accuracy * creator.accuracy)*0.5);
	var angstep = (accuracy * creator.accuracy) / amountOfProjectiles;
	repeat(amountOfProjectiles)
	{
		with instance_create(x,y,Bullet2Energy)
		{
			motion_add(ang,other.projectileSpeed + (other.ammo * 2))
			image_angle = direction
			team = other.team
			scrCopyWeaponMod(other);
		}
		ang += angstep;
	}
	BackCont.viewx2 += lengthdir_x(14,aimDirection+180)*UberCont.opt_shake
	BackCont.viewy2 += lengthdir_y(14,aimDirection+180)*UberCont.opt_shake
	BackCont.shake += 9
	with creator
	{
		if object_index != Player || !skill_got[2]
		{
			motion_add(aimDirection+180,3.4)
		}	
		wkick = 8
	}
}


if ammo <= 0
instance_destroy()


