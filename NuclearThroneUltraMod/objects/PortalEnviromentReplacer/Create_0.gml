/// @description Radius
radius = 10;
opacity = 1;
area = 10;
prevArea = 1;
showNewArea = true;
maxRadius = 200;
alarm[0] = 1;