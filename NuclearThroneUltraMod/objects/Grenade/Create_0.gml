event_inherited();
image_speed = 0;
friction = 0.1
alarm[1] = 6
alarm[0] = 60
alarm[6] = 10;
blinkTime = 5;
alarm[3] = alarm[0] - 16;
isGrenade = true;
offx = random(2)-1
offy = random(2)-1
dmg = 15;
typ = 1 
sticky = 0;
alarm[11] = 1;
canExplodeBlade = false;
gotHit = false;
knockback = 10;
stickTarget = noone;
if instance_exists(ThroneIISpiral)
	alarm[9] = 5;