/// @description Init
maxhealth = 7;
spr_idle = sprInvertedMushroomProp1
spr_hurt = sprInvertedMushroomProp1Hurt
spr_dead = sprInvertedMushroomProp1Dead
mySize = 1
// Inherit the parent event
event_inherited();

snd_hurt = sndHitPlant
shadowSprite = shd16;
