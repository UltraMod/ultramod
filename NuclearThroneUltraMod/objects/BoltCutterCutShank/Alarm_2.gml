/// @description Cutsprite reset
with owner
{
	wkick -= 5;
	wep_sprt[876] = sprBoltCutter;
}
BackCont.shake += 2;