/// @description Follow target
if target != noone && instance_exists(target) && image_index < 8
{
	x = target.x;
	y = target.y;
}
