{
  "$GMObject":"",
  "%Name":"InvertedTechnomancer",
  "eventList":[
    {"$GMEvent":"v1","%Name":"","collisionObjectId":null,"eventNum":0,"eventType":0,"isDnD":false,"name":"","resourceType":"GMEvent","resourceVersion":"2.0",},
  ],
  "managed":true,
  "name":"InvertedTechnomancer",
  "overriddenProperties":[],
  "parent":{
    "name":"Labs",
    "path":"folders/Objects/Enemies/Boss/Labs.yy",
  },
  "parentObjectId":{
    "name":"Technomancer",
    "path":"objects/Technomancer/Technomancer.yy",
  },
  "persistent":false,
  "physicsAngularDamping":0.1,
  "physicsDensity":0.5,
  "physicsFriction":0.2,
  "physicsGroup":1,
  "physicsKinematic":false,
  "physicsLinearDamping":0.1,
  "physicsObject":false,
  "physicsRestitution":0.1,
  "physicsSensor":false,
  "physicsShape":1,
  "physicsShapePoints":[],
  "physicsStartAwake":true,
  "properties":[],
  "resourceType":"GMObject",
  "resourceVersion":"2.0",
  "solid":false,
  "spriteId":{
    "name":"sprInvertedTechnoMancer",
    "path":"sprites/sprInvertedTechnoMancer/sprInvertedTechnoMancer.yy",
  },
  "spriteMaskId":null,
  "visible":true,
}