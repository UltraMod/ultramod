/// @description Camera shenanigans
alarm[2] = 1;
BackCont.shake += 2;
BackCont.viewx2 += lengthdir_x(5,direction + 180)*UberCont.opt_shake;
BackCont.viewy2 += lengthdir_y(5,direction + 180)*UberCont.opt_shake;