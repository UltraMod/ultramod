event_inherited()
maxhealth = 4
raddrop = 8
EnemyHealthAdjustments();
spr_idle = sprInvertedThiefIdle
spr_walk = sprInvertedThiefWalk
spr_hurt = sprInvertedThiefHurt
spr_dead = sprInvertedThiefDead
runawaySpeed = 4;
proj = EnemyBullet1Square
actTime = 7;
acc = 2;
maxSpeed = 5;
maxRunSpeed = 7;
isInverted = false;
stealWeaponOdds = 2;
stealAmount = 3;
if loops > 0
	actTime = 4;
canDodge = false;