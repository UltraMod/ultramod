/// @description Give a sprite

// Inherit the parent event
event_inherited();
spr_item = sprVoidChallengeIcon;
item_index = 1;
challenge = "TAKE ONE EXTRA DAMAGE\nFROM EVERYTHING"
reward = "GAIN ONE LIFE";
cost = 3;