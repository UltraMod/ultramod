/// @description 1 sec to destroy

// Inherit the parent event
event_inherited();
dmg = 5;
ammo = 2;
time = 3;
angStep = 360/ammo;
alarm[0] = 40
blinkTime = 4;
alarm[3] = alarm[0] - 12;