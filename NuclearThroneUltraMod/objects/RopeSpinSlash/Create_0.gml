/// @description Init

// Inherit the parent event
event_inherited();
wallPierce *= 0.5;
dmg = 14;
shanked = false;
image_speed -= 0.1;
alarm[2] = 4;
canAlwaysDeflect = true;