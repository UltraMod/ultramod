/// @description Give a sprite

// Inherit the parent event
event_inherited();

spr_item = sprVoidChallengeIcon;
item_index = 4;
challenge = "DISABLE ALL REGULAR MUTATIONS"
reward = "INCREASE MAX LEVEL";
cost = 0;