/// @description zigzag offset

// Inherit the parent event
event_inherited();
team = 1;
offset = 2.5;
angle = 90;
time = 0;
tdir = 0.06;
alarm[3] += 60;
alarm[4] = 1 + irandom(10);
alarm[6] = alarm[4] + 5;
alarm[5] = 1;