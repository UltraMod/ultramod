/// @description Init

// Inherit the parent event
event_inherited();
alarm[0] = 0;
meleedamage = 40
raddrop = 40
if instance_exists(SurvivalWave)
	raddrop = 30;
maxhealth = 190//65
EnemyHealthAdjustments();
spr_idle = sprGoldCrystalIdle
spr_walk = sprGoldCrystalIdle
spr_hurt = sprGoldCrystalHurt
spr_dead = sprGoldCrystalDead
spr_fire = sprGoldCrystalFire

snd_hurt = sndGoldCrystalHit

tellTime -= 7;
maxAmmo = 6;