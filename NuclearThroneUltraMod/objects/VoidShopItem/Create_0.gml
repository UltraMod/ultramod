/// @description Init
depth = 0;
image_speed = 0;
name = "SHOP ITEM";
challenge = "CHALLENGE";
reward = "REWARD";
isChallenge = false;
cost = 7;
image_speed = 0.4;
spr_item = sprVoidItemGoldChest;
item_index = 0;
wave = 0;
onTheTable = false;
spawnLocationX = x;
spawnLocationY = y + 32;
explain = 0;
alarm[6] = 2;