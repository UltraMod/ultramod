/// @description Limited time buy, disappear
mask_index = mskPickupThroughWall;
sprite_index = sprVoidShopItemDisappear;
image_speed = 0.4;