/// @description xxx

// Inherit the parent event
event_inherited();
range += 100;
alarm[1] = 30;
raddrop = 16
maxhealth = 30
EnemyHealthAdjustments();
spr_idle = sprGoldNecromancerIdle
spr_walk = sprGoldNecromancerWalk
spr_hurt = sprGoldNecromancerHurt
spr_dead = sprGoldNecromancerDead

snd_hurt = sndGoldNecromancerHit

actTime -= 6;

maxSpeed = 4;
acc = 1.1;
reviveArea = GoldReviveArea;
alarm[0] = 0;