/// @description Can we create void?
scrCreateVoidArea(false);
with Player
{
	if !skill_got[maxskill + 1]
	{
		getVision = true;	
	}
}
instance_destroy();