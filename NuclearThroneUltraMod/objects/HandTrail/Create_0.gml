/// @description Timer
event_inherited();
alarm[0] = 0;
image_xscale = 0.15;
image_yscale = image_xscale;
growRate = 0.2;
shrinkRate = 0.05;
alpha = 1;