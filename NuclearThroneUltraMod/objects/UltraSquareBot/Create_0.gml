/// @description Ultra

// Inherit the parent event
event_inherited();
raddrop += 20;
maxhealth = 40;
maxSpeed += 0.5;
acc += 0.2;
actTime -= 40;
projectileSpeed += 2;
spr_idle = sprUltraSquareBotIdle;
spr_walk = sprUltraSquareBotWalk;
spr_hurt = sprUltraSquareBotHurt;
spr_dead = sprUltraSquareBotDead;
spr_fire = sprUltraSquareBotFire;