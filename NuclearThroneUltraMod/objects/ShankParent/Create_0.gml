event_inherited();
image_speed = 0.4

typ = 0 //0 = nothing, 1 = deflectable, 2 = destructable, 3 = deflectable

image_xscale += 0.05;
image_yscale += 0.05;
walled = 1;
dmg = 6;
