raddrop = 4
maxhealth = 18
mySize = 1

event_inherited()
meleedamage = 1
canFly = true;
venomous = true;
spr_idle = sprCloudFlyer
spr_walk = sprCloudFlyer
spr_hurt = sprCloudFlyerHurt
spr_dead = sprCloudFlyerDead
spr_fire = sprCloudFlyerFire;
snd_hurt = sndCloudFlyerHurt
snd_dead = sndCloudFlyerDie
isInverted = false;
//behavior
walk = 0
alarm[1] = 30+random(90);
gunangle = 0;
wkick = 0
actTime = 18;
loops = GetPlayerLoops();
if loops < 1
	actTime = 22;
minRange = 64;
maxRange = 320;
startingPointDistance = 200;
attackRange = 240;
acc = 0.8;
maxSpeed = 3.2;