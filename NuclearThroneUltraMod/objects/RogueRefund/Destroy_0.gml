/// @description No gain in blastarmour power!
with Player
{
	if blastArmourPower > 3
		blastArmourPower -= 1;	
}