event_inherited();
friction = 0.35;
dmg = 5;
boltStick = sprSplinterStick;
trailSize = 1.25;
trailShrinkRate = 0.3;