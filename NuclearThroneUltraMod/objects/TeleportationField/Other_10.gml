/// @description Top draw
if sprite_index == sprAtomTeleportationFieldCore
{
	draw_sprite_ext(sprAtomTeleportationFieldMiddleRing,0,x,y,image_xscale,image_yscale,middleAngle,c_white,1);
	draw_sprite_ext(sprAtomTeleportationFieldOuterRing,image_index,x,y,image_xscale,image_yscale,outerAngle,c_white,1);
}