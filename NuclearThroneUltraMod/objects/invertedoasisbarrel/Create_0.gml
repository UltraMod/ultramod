/// @description Change sprite

// Inherit the parent event
event_inherited();
maxhealth = 9
my_health = maxhealth;
spr_idle = sprInvertedOasisBarrel
spr_hurt = sprInvertedOasisBarrelHurt
spr_dead = sprInvertedOasisBarrelDead