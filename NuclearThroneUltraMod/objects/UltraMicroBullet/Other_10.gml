/// @description ultramod
var um = GetPlayerUltramod()
if um == ultramods.bulletShotgun
{
	with instance_create(x,y,Bullet5)
	{
		dmg = 11;
		scrCopyWeaponMod(other);
		direction = other.direction;
		image_angle = direction;
		speed = 17;
		team = other.team;
		alarm[11] = 0;
	}
	canDamage = false;
	instance_destroy(id,false);
} else if um == ultramods.boltBullet
{
	instance_destroy(id,false);
	snd_play_fire(sndSplinterGun)
	with instance_create(x,y,UltraSplinter)
	{
		dmg = 11;
		scrCopyWeaponMod(other);
		direction = other.direction;
		image_angle = direction;
		speed = 22;
		team = other.team;
		alarm[11] = 0;
	}
	canDamage = false;
}
else if um == ultramods.laserBullet
{
	with Player
	{
		if Player.skill_got[17] = 1
			snd_play_fire(sndLaserUpg)
		else
			snd_play_fire(sndLaser)	
	}
	instance_destroy(id,false);
	canDamage = false;
	with instance_create(x,y,Laser)
	{
		defaultPierce += 32
		image_yscale += 0.15;
		scrCopyWeaponMod(other);
		isog = false;
		image_angle = other.direction;
		team = other.team
		event_perform(ev_alarm,0);
	}
}
else if um == ultramods.bulletPlasma
{
	instance_destroy(id,false);
	canDamage = false;
	with Player
	{
		if skill_got[17] = 1
			snd_play_fire(sndPlasmaMinigunUpg)
		else
			snd_play_fire(sndPlasmaMinigun)	
	}
	with instance_create(x,y,PlasmaBall)
	{
		acc += 6;
		maxSpeed += 8;
		scrCopyWeaponMod(other);
		ptime = 6;
		direction = other.direction;
		originalDirection = direction;
		image_angle = direction;
		speed = other.speed;
		team = other.team;
		alarm[11] = 0;
	}
}