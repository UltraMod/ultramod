if other.team != team and other.my_health > 0//the thing I hit must not be myself
{
	with other//enemy
	{
		if sprite_index != spr_hurt
		{
			snd_play(snd_hurt, hurt_pitch_variation,true)
			instance_create(other.x,other.y,FishBoost)
			DealDamage(other.dmg);
			other.dmg = max(2,other.dmg - 1);
			if (instance_exists(Player) && Player.skill_got[43]) && team != 0 && !other.hasStunned
			{
				with Tentacle
				{
					hasStunned = true;	
				}
				scrMoodSwingStun(6);
				if Player.ultra_got[97] && !Player.altUltra {
					scrMoodSwingIcicle(2, id);
					scrMoodSwingFlameSpread(24);
				}
			}
			else if team != 0 {
				var pullD = other.direction+180;
				motion_add(pullD,2);
			}
			sprite_index = spr_hurt
			image_index = 0
			if other.alarm[1] < 1
			{
				snd_play(sndMeatExplo,0,true)
				with instance_create(x,y,MeatExplosion) {
					sprite_index = sprTentacleMeatExplosion;
					dmg -= 1;
				}
				with instance_create(x,y,FishBoost)
				{
					motion_add(random(360),3);
				}
				other.bloodDelay += 1;
				other.alarm[1] = other.bloodDelay;
			}
		}
		else
		{
			scrIframeSkipper(0.065);
		}
	}
}
