raddrop = 12
maxhealth = 23
mySize = 1
event_inherited()
team = 7;
meleedamage = 0
spr_idle = sprPitGhostSpawner;
spr_walk = sprPitGhostSpawnerWalk;
spr_hurt = sprPitGhostSpawnerHurt;
spr_dead = sprPitGhostSpawnerDead;
spr_fire = sprPitGhostSpawnerFire;

snd_hurt = sndGhostHurt
snd_dead = sndGhostDie

//behavior
walk = 0
gunangle = random(360)
alarm[1] = 30+random(60)
wkick = 0
actTime = 12;

acc = 1;
maxSpeed = 4.5;
materializeRange = 164;
if GetPlayerLoops() > 0
	materializeRange = 132;