/// @description dmg

// Inherit the parent event
event_inherited();
dmg = 4;
team = 1;
typ = 4;
image_speed = 0.4;
image_angle = 90*irandom(3)

angleDir = 1
owner = -1;
ownerAngle = 0;
distance = 0;
targetDistance = 200;
time = 0;
alarm[0] = 1;
ownerAngleRotationSpeed = 4;
rotationDirection = 1;
startDistance = 0;
