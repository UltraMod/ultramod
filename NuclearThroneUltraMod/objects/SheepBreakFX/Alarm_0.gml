/// @description Copy angle one frame in
if instance_exists(SheepStorm)
{
	image_angle = SheepStorm.image_angle;
	direction = image_angle;
}