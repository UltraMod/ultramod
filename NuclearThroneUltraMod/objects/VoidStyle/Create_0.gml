/// @description Init
event_inherited();
depth = -12;
owner = noone;
dmg = 4;
alarm[1] = 1;
BackCont.shake += 5;
image_speed = 0.5;
endEarly = 3;
existTime = 0;
totalTime = 8 / image_speed;