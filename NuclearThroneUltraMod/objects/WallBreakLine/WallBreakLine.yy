{
  "$GMObject":"",
  "%Name":"WallBreakLine",
  "eventList":[],
  "managed":true,
  "name":"WallBreakLine",
  "overriddenProperties":[],
  "parent":{
    "name":"Projectiles",
    "path":"folders/Objects/Projectiles.yy",
  },
  "parentObjectId":{
    "name":"WallBreak",
    "path":"objects/WallBreak/WallBreak.yy",
  },
  "persistent":false,
  "physicsAngularDamping":0.1,
  "physicsDensity":0.5,
  "physicsFriction":0.2,
  "physicsGroup":0,
  "physicsKinematic":false,
  "physicsLinearDamping":0.1,
  "physicsObject":false,
  "physicsRestitution":0.1,
  "physicsSensor":false,
  "physicsShape":0,
  "physicsShapePoints":[],
  "physicsStartAwake":true,
  "properties":[],
  "resourceType":"GMObject",
  "resourceVersion":"2.0",
  "solid":false,
  "spriteId":{
    "name":"mskWallBreakLine",
    "path":"sprites/mskWallBreakLine/mskWallBreakLine.yy",
  },
  "spriteMaskId":{
    "name":"mskWallBreakLine",
    "path":"sprites/mskWallBreakLine/mskWallBreakLine.yy",
  },
  "visible":false,
}