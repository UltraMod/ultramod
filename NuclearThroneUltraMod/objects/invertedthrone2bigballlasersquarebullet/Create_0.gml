/// @description Init
/// @description Init

// Inherit the parent event
event_inherited();
typ = 3;
dmg = 10;
projectileToSpawn = InvertedExploGuardianSquareBullet;
projectileToSpawnSprite = sprInvertedGuardianSquareBulletSpawn;
hitSprite = sprInvertedGuardianBulletHit;
//pSpeed = 6 + clamp((loops-1)*0.5,0,2);