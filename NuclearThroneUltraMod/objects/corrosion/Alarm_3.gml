/// @description Fade out
alpha -= 0.05;
image_xscale -= 0.15;
image_yscale = image_xscale;
alarm[3] = 1;