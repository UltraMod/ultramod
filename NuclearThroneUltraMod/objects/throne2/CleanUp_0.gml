/// @description Stop looping sounds
if audio_is_playing(sndNothingBeamLoop)
	audio_stop_sound(sndNothingBeamLoop);