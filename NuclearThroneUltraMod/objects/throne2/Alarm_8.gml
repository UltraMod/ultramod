/// @description Stop Beam
audio_stop_sound(sndNothingBeamLoop)
with Throne2Beam
	event_user(0);

alarm[5] = 70 - min(30,loops*2);