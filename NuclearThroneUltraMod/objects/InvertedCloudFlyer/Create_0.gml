/// @description Init

// Inherit the parent event
event_inherited();

raddrop = 6
maxhealth = 10
EnemyHealthAdjustments();
mySize = 1
actTime -= 4;
isInverted = true;
spr_idle = sprInvertedCloudFlyer
spr_walk = sprInvertedCloudFlyer
spr_hurt = sprInvertedCloudFlyerHurt
spr_dead = sprInvertedCloudFlyerDead
spr_fire = sprInvertedCloudFlyerFire;
