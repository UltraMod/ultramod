/// @description One way projectile

// Inherit the parent event
event_inherited();
dmg += 1;
acc += 2;
maxSpeed += 4;
alarm[11] = 0;