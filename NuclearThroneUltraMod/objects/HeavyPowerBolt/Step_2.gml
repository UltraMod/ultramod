/// @description Stationary for a bit

// Inherit the parent event
event_inherited();

if alarm[4] > 0
{
	x = xstart;
	y = ystart;
	scrForcePosition60fps();
}