/// @description Init

// Inherit the parent event
event_inherited();
raddrop += 2;
maxhealth = 35;
maxSpeed -= 0.5;
actTime -= 50;
projectileSpeed = 3;
EnemyHealthAdjustments();
spr_idle = sprGoldenOctaBotIdle;
spr_walk = sprGoldenOctaBotWalk;
spr_hurt = sprGoldenOctaBotHurt;
spr_dead = sprGoldenOctaBotDead;
spr_fire = sprGoldenOctaBotFire;