{
  "$GMObject":"",
  "%Name":"AnimDestroyBloom",
  "eventList":[],
  "managed":true,
  "name":"AnimDestroyBloom",
  "overriddenProperties":[],
  "parent":{
    "name":"FX",
    "path":"folders/Objects/FX.yy",
  },
  "parentObjectId":{
    "name":"AnimDestroy",
    "path":"objects/AnimDestroy/AnimDestroy.yy",
  },
  "persistent":false,
  "physicsAngularDamping":0.1,
  "physicsDensity":0.5,
  "physicsFriction":0.2,
  "physicsGroup":1,
  "physicsKinematic":false,
  "physicsLinearDamping":0.1,
  "physicsObject":false,
  "physicsRestitution":0.1,
  "physicsSensor":false,
  "physicsShape":1,
  "physicsShapePoints":[],
  "physicsStartAwake":true,
  "properties":[],
  "resourceType":"GMObject",
  "resourceVersion":"2.0",
  "solid":false,
  "spriteId":null,
  "spriteMaskId":null,
  "visible":true,
}