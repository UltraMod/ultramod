/// @description May hit again?
hits --
if hits > 0
{
	hitEntities = [];
	dmg *= 0.5;
}
speed *= 0.999;