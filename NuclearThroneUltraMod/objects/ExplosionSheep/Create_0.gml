/// @description Make it Ultra

// Inherit the parent event
event_inherited();
maxhealth = 20;
my_health = 20;
dmg = 10;
scrWeaponModInit();
spr_idle = sprExplosiveSheepIdle;
spr_idle_b = sprExplosiveSheepIdle;
spr_walk = sprExplosiveSheepWalk;
spr_hurt = sprExplosiveSheepHurt;
spr_dead = sprExplosiveSheepDead;
maxSpeedNormal = 4;
maxSpeed = 9;
alarm[3] = 1;
alarm[2] = 2;
alarm[4] = 18;
alarm[1] = 19;
originalDirection = 0;
image_speed = 0.6;
actTime = 10;
willExplode = 30;