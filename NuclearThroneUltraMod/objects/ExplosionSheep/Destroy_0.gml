/// @description Explodes

// Inherit the parent event
event_inherited();

instance_create(x,y,Explosion);
snd_play(sndExplosion);