/// @description Now killable
killable = true;
spr_hurt = spr_dead;
sprite_index = spr_idle;
my_health = maxhealth;
mask_index = mskLastDeathWait;