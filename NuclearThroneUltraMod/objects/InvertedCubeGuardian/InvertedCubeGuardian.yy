{
  "$GMObject":"",
  "%Name":"InvertedCubeGuardian",
  "eventList":[
    {"$GMEvent":"v1","%Name":"","collisionObjectId":null,"eventNum":0,"eventType":0,"isDnD":false,"name":"","resourceType":"GMEvent","resourceVersion":"2.0",},
    {"$GMEvent":"v1","%Name":"","collisionObjectId":null,"eventNum":0,"eventType":1,"isDnD":false,"name":"","resourceType":"GMEvent","resourceVersion":"2.0",},
    {"$GMEvent":"v1","%Name":"","collisionObjectId":null,"eventNum":1,"eventType":2,"isDnD":false,"name":"","resourceType":"GMEvent","resourceVersion":"2.0",},
  ],
  "managed":true,
  "name":"InvertedCubeGuardian",
  "overriddenProperties":[],
  "parent":{
    "name":"Palace",
    "path":"folders/Objects/Enemies/Palace.yy",
  },
  "parentObjectId":{
    "name":"CubeGuardian",
    "path":"objects/CubeGuardian/CubeGuardian.yy",
  },
  "persistent":false,
  "physicsAngularDamping":0.1,
  "physicsDensity":0.5,
  "physicsFriction":0.2,
  "physicsGroup":1,
  "physicsKinematic":false,
  "physicsLinearDamping":0.1,
  "physicsObject":false,
  "physicsRestitution":0.1,
  "physicsSensor":false,
  "physicsShape":1,
  "physicsShapePoints":[],
  "physicsStartAwake":true,
  "properties":[],
  "resourceType":"GMObject",
  "resourceVersion":"2.0",
  "solid":false,
  "spriteId":{
    "name":"sprInvertedCubeGuardianWalk",
    "path":"sprites/sprInvertedCubeGuardianWalk/sprInvertedCubeGuardianWalk.yy",
  },
  "spriteMaskId":null,
  "visible":true,
}