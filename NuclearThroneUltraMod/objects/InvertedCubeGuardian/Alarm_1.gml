/// @description Awake
if (instance_exists(Player) && !Player.justAsheep) sleeping = false;;

// Inherit the parent event
event_inherited();

