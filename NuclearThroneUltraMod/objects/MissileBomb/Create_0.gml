/// @description 1 sec to destroy

// Inherit the parent event
event_inherited();
ammo = 8;
time = 3;
angStep = 360/ammo;
blinkTime = 4;
alarm[0] = 30
alarm[3] = alarm[0] - 12;