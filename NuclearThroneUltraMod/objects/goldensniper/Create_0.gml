/// @description Init

// Inherit the parent event
event_inherited();
alarm[0] = 0;
tellTime -= 1;
actTime -= 2;
raddrop = 12
maxhealth = 23
my_health = maxhealth;
EnemyHealthAdjustments();
spr_idle = sprGoldSniperIdle
spr_walk = sprGoldSniperWalk
spr_hurt = sprGoldSniperHurt
spr_dead = sprGoldSniperDead