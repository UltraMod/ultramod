/// @description Init
event_inherited();
actTime += 5;
raddrop = 12
maxhealth = 32
EnemyHealthAdjustments();
maxSpeed += 0.5;
spr_idle = sprInvertedCloudShooter
homingSpeed += 1.75;
homingTimeAdjustment += 45;
spr_idle = sprInvertedCloudShooter
spr_walk = sprInvertedCloudShooter
spr_hurt = sprInvertedCloudShooterHurt
spr_dead = sprInvertedCloudShooterDead