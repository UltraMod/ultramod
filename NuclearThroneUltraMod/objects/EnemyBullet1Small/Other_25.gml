/// @description Deflected sprite change

// Inherit the parent event
event_inherited();
if team == 2
{
	sprite_index = sprEnemyBullet1SmallDeflected;
}
else
{
	sprite_index = sprEnemyBullet1Small;	
}