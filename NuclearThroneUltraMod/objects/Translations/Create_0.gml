/// @description Loredumper
loreDump = [
	[
		//FISH
		[
			//FIRST TIME
			"So you're a fish?","Green man",
		],
		[
			//LOOP 1
			"We in the void simply look for a way to return*You can help us with your portal essence*Collect portal essence by going through portals*Any portal, except for the green ones",
			"I will be watching you Fish",
			"Shouldn't you be in water?",
			"Have you heard of the one called Frog?"
		],
		[
			//LOOP 2
			"You use your past world's experience well*A natural leader of the group*Though your mutant group is very chaotic",
			"The irony of it all*Your last day before retirement*And then the bombs hit",
			"Should you guide all these mutants to power?",
			"Every world you leave behind, dies"
		],
		[
			//LOOP 3
			"Your growth in power has become a threat*We shall inevitably face off against each other"
		],
		[
			//SPECIAL
			"Your partner has returned, but at what cost?"
		]
	],
	[
		//CRYSTAL
		[
			//FIRST TIME
			"Moving Crystals?","Purple woman",
		],
		[
			//LOOP 1
			"We in the void simply look for a way to return*You can help us with your portal essence*Collect portal essence by going through portals*Any portal, except for the green ones",
			"I will watch how you handle this Crystal",
		],
		[
			//LOOP 2
			"Such a big family*It's inevitable that you join them",
			"Every world you leave behind, dies"
		],
		[
			//LOOP 3
			"Your growth in power has become a threat*We shall inevitably face off against each other"
		],
	],
	[
		//EYES
		[
			//FIRST TIME
			"You can almost see as much as me","You have good perspective",
		],
		[
			//LOOP 1
			"We in the void simply look for a way to return*You can help us with your portal essence*Collect portal essence by going through portals*Any portal, except for the green ones",
			"I will be watching you Eyes, fellow visionary",
			"Hi eyes, its me eye",
		],
		[
			//LOOP 2
			"With no mouth I am the only one that can see what you want to say*Such lonelyness",
			"Every world you leave behind, dies"
		],
		[
			//LOOP 3
			"Your growth in power has become a threat*We shall inevitably face off against each other",
			"You are not lonely at all*Think you can face me with the friends you made?"
		],
	],
	[
		//MELTING
		[
			//FIRST TIME
			"You are in pain","Melting away",
		],
		[
			//LOOP 1
			"We in the void simply look for a way to return*You can help us with your portal essence*Collect portal essence by going through portals*Any portal, except for the green ones",
			"I will be watching your pain Melting",
		],
		[
			//LOOP 2
			"Your pain will never stop*You do not have long, why do you fight?",
			"When your skin fully melts away what is left of you*Will you remain the same?",
			"Every world you leave behind, dies"
		],
		[
			//LOOP 3
			"Your growth in power has become a threat*We shall inevitably face off against each other",
		],
		[
			//SPECIAL
			"You have become a void seer!*See how everything ends*We are insignificant in these worlds"
		]
	],
	[
		//PLANT
		[
			//FIRST TIME
			"I see potential in you",
		],
		[
			//LOOP 1
			"We in the void simply look for a way to return*You can help us with your portal essence*Collect portal essence by going through portals*Any portal, except for the green ones",
			"I will be watching where your bloodlust takes you Plant",
		],
		[
			//LOOP 2
			"Such bloodlust you have",
			"A walking vegetable*Irradiated into being",
			"Eat all flesh",
			"Every world you leave behind, dies"
		],
		[
			//LOOP 3
			"Your growth in power has become a threat*We shall inevitably face off against each other",
		],
		[
			//SPECIAL
			"KILL KILL KILL*KILL! KILL! KILL!"
		]
	],
	[
		//YV
		[
			//FIRST TIME
			"I am so sorry Gun God I did not recognize you",
		],
		[
			//LOOP 1
			"We in the void simply look for a way to return*You can help us with your portal essence*Collect portal essence by going through portals*Any portal, except for the green ones",
			"Mind if I peek in once in a while Gun God?",
			"I apologize once again Gun God*For seeing you as a threat"
		],
		[
			//LOOP 2
		],
		[
			//LOOP 3
			"Your growth in power has become a threat*We shall inevitably face off against each other",
		],
	],
	[
		//STEROIDS
		[
			//FIRST TIME
			"How are you able to understand me so quickly?",
		],
		[
			//LOOP 1
			"We in the void simply look for a way to return*You can help us with your portal essence*Collect portal essence by going through portals*Any portal, except for the green ones",
			"Are you aware of the void?*How far did all your research take you?",
			"Looks like you cant procreate",
		],
		[
			//LOOP 2
			"You see a way out through the void?",
			"You've pumped yourself full of steroids because you knew the apocalypse was coming*You were the only one that knew*But the other mutant Humphry was also prepared.",
			"Every world you leave behind, dies",
		],
		[
			//LOOP 3
			"Your growth in power has become a threat*We shall inevitably face off against each other",
		],
	],
	[
		//ROBOT
		[
			//FIRST TIME
			"I shall speak in binary",
		],
		[
			//LOOP 1
			"We in the void simply look for a way to return*You can help us with your portal essence*Collect portal essence by going through portals*Any portal, except for the green ones",
			"I will be watching what you devour next Robot",
			"I might have mistaken you for a vacuum cleaner"
		],
		[
			//LOOP 2
			"The factory that made you must be destroyed*However there are many versions*And an infinite amount of parallel copies*An impossible task*An infinite drive for a ruthless machine*",
			"Every world you leave behind, dies",
		],
		[
			//LOOP 3
			"Your growth in power has become a threat*We shall inevitably face off against each other",
		],
	],
	[
		//CHICKEN
		[
			//FIRST TIME
			"Are you ready?"
		],
		[
			//LOOP 1
			"We in the void simply look for a way to return*You can help us with your portal essence*Collect portal essence by going through portals*Any portal, except for the green ones",
			"I will be watching you Chicken",
			"Keep up the focus",
		],
		[
			//LOOP 2
			"This is not like the movies*but you enjoy the thrill of the fight",
			"Every world you leave behind, dies",
		],
		[
			//LOOP 3
			"Your growth in power has become a threat*We shall inevitably face off against each other",
		],
		[
			//SPECIAL
			"Where did your face go?"
		]
	],
	[
		//REBEL
		[
			//FIRST TIME
			"you are accumulating an army?*what makes them follow you?"
		],
		[
			//LOOP 1
			"We in the void simply look for a way to return*You can help us with your portal essence*Collect portal essence by going through portals*Any portal, except for the green ones",
			"I will be watching where you take your army Rebel",
			"I sense you are somwhat attuned\nwith interdimensional travel*Similar to the panda",
		],
		[
			//LOOP 2
			"Every world you leave behind, dies",
		],
		[
			//LOOP 3
			"Your growth in power has become a threat*We shall inevitably face off against each other",
			"Your atunement with the void and interdimensional travel*Has allowed you to gather allies from several indistinguishable worlds",
		],
		[
			//SPECIAL
			"I see a path where you do not kill your brother"
		]
	],
	[
		//HUNTER
		[
			//FIRST TIME
			"I will speak in binary for you",
		],
		[
			//LOOP 1
			"We in the void simply look for a way to return*You can help us with your portal essence*Collect portal essence by going through portals*Any portal, except for the green ones",
			"I will be watching where your hunt takes you Hunter",
			"What are you hunting?",
		],
		[
			//LOOP 2
			"On the hunt for the next boss*The greatest foes there are*Take them down one by one*And become*The strongers hunter there is",
			"Every world you leave behind, dies",
		],
		[
			//LOOP 3
			"Your growth in power has become a threat*We shall inevitably face off against each other",
		],
	],
	[
		//YC
		[
			//FIRST TIME
			"venuzians are always welcome here"
		],
		[
			//LOOP 1
			"We in the void simply look for a way to return*You can help us with your portal essence*Collect portal essence by going through portals*Any portal, except for the green ones",
			"I want to watch your battles Yung Cuz",
			"I apologize once again for seeing you as a threat*I did not realise you were family of the Gun God",
		],
		[
			//LOOP 2
			"Family of a God*But not quite a God himself",
			"Living in the shadow of a greater God",
			"Trying to prove yourself",
			"Every world you leave behind, dies",
		],
		[
			//LOOP 3
			"Your growth in power has become a threat*We shall inevitably face off against each other",
		],
	],
	[
		//SHEEP
		[
			//FIRST TIME
			"You're not just a sheep are you?",
		],
		[
			//LOOP 1
			"We in the void simply look for a way to return*You can help us with your portal essence*Collect portal essence by going through portals*Any portal, except for the green ones",
			"I will be watching you Sheep and discover who you truly are",
			"You are by far my favourite mutant*So mysterious what are you?*Are you even from the mutant world?",
		],
		[
			//LOOP 2
			"Every world you leave behind, dies",
		],
		[
			//LOOP 3
			"Your growth in power has become a threat*We shall inevitably face off against each other",
		],
	],
	[
		//PANDA
		[
			//FIRST TIME
			"Funny creature",
		],
		[
			//LOOP 1
			"We in the void simply look for a way to return*You can help us with your portal essence*Collect portal essence by going through portals*Any portal, except for the green ones",
			"I will be watching you Panda",
			"Such a cute panda*Eventhough your radiation levels would instantly kill several lesser creatures",
			"You seem to have more affinity for portal travel/nthan the others that pass through here*Except for rebel, she is on the same level of affinity",
		],
		[
			//LOOP 2
			"Every world you leave behind, dies",
			"From the jungle nearly extinct*Yet here you stand one of few"
		],
		[
			//LOOP 3
			"Your growth in power has become a threat*We shall inevitably face off against each other",
		],
	],
	[
		//ATOM
		[
			//FIRST TIME
			"you are very difficult to read"
		],
		[
			//LOOP 1
			"We in the void simply look for a way to return*You can help us with your portal essence*Collect portal essence by going through portals*Any portal, except for the green ones",
			"I will be watching you Atom, you can teleport all you want*I will still keep track",
			"A bein of pure atomic power*How did you come in existence?",
		],
		[
			//LOOP 2
			"Every world you leave behind, dies",
			"Spontanious existence*I can see you slipping through the void at all times",
			"One part always in multiple dimensions"
		],
		[
			//LOOP 3
			"Your growth in power has become a threat*We shall inevitably face off against each other",
		],
	],
	[
		//VIKING
		[
			//FIRST TIME
			"nice armour"
		],
		[
			//LOOP 1
			"We in the void simply look for a way to return*You can help us with your portal essence*Collect portal essence by going through portals*Any portal, except for the green ones",
			"I will watch where you will go Viking*You and your serpents",
			"Snake wrangler or possesed by the snakes?*Possibly something else something more mutualistic?",
		],
		[
			//LOOP 2
			"Every world you leave behind, dies",
			"Bit by a radioactive serpent*You are one of the luckier ones",
			"No angel and devil on your side*You live in a true symbiotic relationship*For they saved you*And you saved them"
		],
		[
			//LOOP 3
			"Your growth in power has become a threat*We shall inevitably face off against each other",
		],
	],
	[
		//WEAPONSMITH
		[
			//FIRST TIME
			"nice craftsmanship"
		],
		[
			//LOOP 1
			"We in the void simply look for a way to return*You can help us with your portal essence*Collect portal essence by going through portals*Any portal, except for the green ones",
			"I will watch the path you forge Weaponsmith",
		],
		[
			//LOOP 2
			"Every world you leave behind, dies",
			"The wastelands are a playfield of weaponry*You feel right at home",
		],
		[
			//LOOP 3
			"Your growth in power has become a threat*We shall inevitably face off against each other",
		],
	],
	[
		//ANGEL
		[
			//FIRST TIME
			"I can also fly you see"
		],
		[
			//LOOP 1
			"We in the void simply look for a way to return*You can help us with your portal essence*Collect portal essence by going through portals*Any portal, except for the green ones",
			"I will be watching you Angel",
			"You are one of the ancient guardians?",
		],
		[
			//LOOP 2
			"Every world you leave behind, dies",
			"Left your duties for power",
			"Guardian of the crown",
		],
		[
			//LOOP 3
			"Your growth in power has become a threat*We shall inevitably face off against each other",
		],
	],
	[
		//SKELETON
		[
			//FIRST TIME
			"Looks like you've been through some things"
		],
		[
			//LOOP 1
			"We in the void simply look for a way to return*You can help us with your portal essence*Collect portal essence by going through portals*Any portal, except for the green ones",
			"I will be watching you Skeleton",
			"You seem familiar like another mutant I've seen here before",
			"Do you remember who you were?"
		],
		[
			//LOOP 2
			"Every world you leave behind, dies",
			"You are not quite like the melting mutant*Instead you are something new*An event so significant it created you as a unique individual",
		],
		[
			//LOOP 3
			"Your growth in power has become a threat*We shall inevitably face off against each other",
		],
	],
	[
		//BUSINESS HOG
		[
			//FIRST TIME
			"scum"
		],
		[
			//LOOP 1
			"We in the void simply look for a way to return*You can help us with your portal essence*Collect portal essence by going through portals*Any portal, except for the green ones",
			"I will be watching you, Business Hog",
			"You seem untrustworthy",
		],
		[
			//LOOP 2
			"Every world you leave behind, dies",
			"A stealing thief looking for power and profit*",
			"When I try to look into your past and future*I see various different conflicting images*You are misleading me",
		],
		[
			//LOOP 3
			"Your growth in power has become a threat*We shall inevitably face off against each other",
			"You have no fear*Stealing from all gods and deities you can find*Am I your next target?",
		],
	],
	[
		//HORROR
		[
			//FIRST TIME
			"a being of pure radiation*you will definitely be able to help us!"
		],
		[
			//LOOP 1
			"We in the void simply look for a way to return*You can help us with your portal essence*Collect portal essence by going through portals*Any portal, except for the green ones",
			"I will be watching you Horror",
		],
		[
			//LOOP 2
			"Every world you leave behind, dies",
			"Consumed by radiation*Power fuels more power",
		],
		[
			//LOOP 3
			"Your growth in power has become a threat*We shall inevitably face off against each other",
		],
	],
	[
		//ROGUE
		[
			//FIRST TIME
			"What are you running from?"
		],
		[
			//LOOP 1
			"We in the void simply look for a way to return*You can help us with your portal essence*Collect portal essence by going through portals*Any portal, except for the green ones",
			"I will be watching you Rogue",
			"those IDPD will eventually catch you"
		],
		[
			//LOOP 2
			"Every world you leave behind, dies",
			"Consumed by radiation*Power fuels more power",
		],
		[
			//LOOP 3
			"Your growth in power has become a threat*We shall inevitably face off against each other",
		],
		[
			//SPECIAL
			"You can't keep running*You'll have to face the captain one day",
			"With your father dead now*Why did you not take his place?*Has the mutant way has gotten to you*The lust for power and the thrill of the fight"
		],
	],
	[
		//FROG
		[
			//FIRST TIME
			"Your shiny gold skin blinds me"
		],
		[
			//LOOP 1
			"We in the void simply look for a way to return*You can help us with your portal essence*Collect portal essence by going through portals*Any portal, except for the green ones",
			"I will be watching you shine Frog!",
			"Even I can smell you",
			"Your voice amuzes me",
			"Your shiny gold skin blinds me"
		],
		[
			//LOOP 2
			"Every world you leave behind, dies",
			"Born from the mother*There are many of you, but you are different*You shine gold and sing*Its positivity is almost contagious",
			"You are having a great time*Gaining power is fun"
		],
		[
			//LOOP 3
			"Your growth in power has become a threat*We shall inevitably face off against each other",
		],
		[
			//SPECIAL
			"You have slain your own mother*Though it is not really your mother*There are an infinite amount of them in parellel worlds*There is a path you can take where you do not kill her",
			"You have gained the power to stand still*This is unheard of for your species"
		],
	],
	[
		//ELEMENTOR
		[
			//FIRST TIME
			"how does one control the elements?"
		],
		[
			//LOOP 1
			"We in the void simply look for a way to return*You can help us with your portal essence*Collect portal essence by going through portals*Any portal, except for the green ones",
			"I will be watching you Elementor",
			"How does one control the elements?",
		],
		[
			//LOOP 2
			"Every world you leave behind, dies",
			"Mixed through various elements*The right amount of radiation added*This mushroom received a consciousness",
			"Not toxic like the other mushrooms*But achieved balance in mind and in elements",
		],
		[
			//LOOP 3
			"Your growth in power has become a threat*We shall inevitably face off against each other",
		],
	],
	[
		//DOCTOR
		[
			//FIRST TIME
			"are you truly a doctor?"
		],
		[
			//LOOP 1
			"We in the void simply look for a way to return*You can help us with your portal essence*Collect portal essence by going through portals*Any portal, except for the green ones",
			"I will be watching you Doctor",
			"You hurt yourself for power?",
		],
		[
			//LOOP 2
			"Every world you leave behind, dies",
			"You are no doctor*Doctor's are called to heal",
		],
		[
			//LOOP 3
			"Your growth in power has become a threat*We shall inevitably face off against each other",
			"Your experiments resulted in the mutated ravens*Splicing their DNA into your own has made you what you are*It has however allowed you to survive*You continue to tap life for power"
		],
	],
	[
		//GOOD O'L HUMPHRY
		[
			//FIRST TIME
			"An old looking creature"
		],
		[
			//LOOP 1
			"We in the void simply look for a way to return*You can help us with your portal essence*Collect portal essence by going through portals*Any portal, except for the green ones",
			"I will be watching you Good O'l Humphry, such a strange name*Very curious to see what lies behind it",
			"Unlike the others that pass through here you would not rid yourself of your name*I wonder why",
		],
		[
			//LOOP 2
			"Every world you leave behind, dies",
			"Ran away and hiding for centuries*Now regret finally kicked in and you decided to fight",
			"You knew the apocalypse was coming*Similar to steroids but he knew for a fact*But for you, your preperations were pure hysteria.",
		],
		[
			//LOOP 3
			"Your growth in power has become a threat*We shall inevitably face off against each other",
			"Your experiments resulted in the mutated ravens*Splicing their DNA into your own has made you what you are*It has however allowed you to survive*You continue to tap life for power"
		],
	],
	[
		//HANDS
		[
			//FIRST TIME
			"You seem familiar",
		],
		[
			//LOOP 1
			"We in the void simply look for a way to return*You can help us with your portal essence*Collect portal essence by going through portals*Any portal, except for the green ones",
			"I will be watching you Hands",
			"Ghosts haunt you",
			"Hi hands, its me eye"
		],
		[
			//LOOP 2
			"Every world you leave behind, dies",
			"From the closest plane of existence*A mere piece of me, of it",
			"One part always in another world*A past, a future that does not exist",
		],
		[
			//LOOP 3
			"Your growth in power has become a threat*We shall inevitably face off against each other",
			"Not what you used to be*But slowly reclaiming your power*A shattered deity*The inverted proto mutant",
			"Not what you used to be*But slowly reclaiming your power*A shattered deity*The inverted proto mutant",
		],
		[
			//SPECIAL
			"Now you have defeated protomind from this world*Do you feel whole again?",
		]
	],
]