raddrop = 40
maxhealth = 225;//175

mySize = 3
if GetPlayerLoops() > 0{
	maxhealth = 300;
}
sucking = false;
scrBossHealthBuff();

event_inherited();
meleedamage = 3

healAmount = 10;

spr_idle = sprOasisBossIdle
spr_walk = sprOasisBossWalk
spr_hurt = sprOasisBossHurt
spr_dead = sprOasisBossDead
spr_fire = sprOasisBossFire
spr_startfire = sprOasisBossFireStart
spr_endfire = sprOasisBossFireEnd

snd_dead = sndOasisBossDead;
snd_hurt = sndOasisBossHurt;
snd_fire = sndOasisBossFire;
snd_melee = sndOasisBossMelee;
snd_tackle = sndOasisBossTackle;

walk=0;
//behavior
alarm[1] = 30+random(50)

if instance_exists(Player)
motion_add(point_direction(Player.x,Player.y,x,y),3)

intro=1;
suckstrength=0;
suckAcc = 0.091;
maxSpeed=2
originalMaxspeed = maxSpeed;
persistent_direction = 0;
actTime = 9;
fishFireRate = 6;
fishTime = 5;
suckTime = 75;
chargeSpeed = 5;
chargeDis = 70;
chargeTell = 23;
snd_play(sndOasisBossIntro);    
inverted = false;
suckRange = 100;
cam=true
loops = GetPlayerLoops();
maxShoot = 10;
currentShoot = 0;
if loops > 0
{
	actTime = 7;
	maxShoot = 99999;
	suckRange += 20;
	maxSpeed += 0.5;
	fishTime = 10;
	fishFireRate = 4;
	chargeSpeed += 1;
	chargeDis += 10;
	chargeTell -= 4;
}
if loops > 1
{
	fishFireRate = 3;
	chargeSpeed += 1;
	chargeDis += 10;
	chargeTell -= 6;
}
if loops > 4
{
	fishFireRate = 2;	
}
with BigFish
{
	if id!=other.id
		other.cam=false;
}

if !(instance_exists(Player) && (Player.area == 2 || Player.area == 110))
	with instance_create(x,y,DramaCamera)
	{
		name = "BIG FISH";
		if random(80) < 1
			name = choose("O'L BITEY","TRASH DWELLER","FISH'S UNCLE?","SEWER FISH");
	}

friction = 0.8;
scrAddDrops(1);

if instance_exists(Player) && Player.skill_got[29] {
	alarm[1] += 60;
	scrGiveSnooze();
}
instance_create(x,y,BigWallBreak);