/// @description Init

// Inherit the parent event
event_inherited();
amountOfProjectiles = 5;
projectileSpeed = 15;
boost = 0;
accuracy = 40;