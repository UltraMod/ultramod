/// @description Reset load speed
with owner
{
	wep_load[other.wep] = wep_load_base[other.wep];	
}