maxhealth = 50
mySize = 2

spr_idle = sprSkeletonSkull
spr_hurt = sprSkeletonSkullHurt
spr_dead = sprSkeletonSkullDead


event_inherited()
shadowSprite = shd16;
canMoveOver = true;
snd_hurt = sndHitRock

revived = false;
alarm[1] = 10;