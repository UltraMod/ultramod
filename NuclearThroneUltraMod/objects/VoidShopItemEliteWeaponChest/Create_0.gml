/// @description Give a sprite

// Inherit the parent event
event_inherited();
spr_item = sprVoidItemEliteWeaponChest;
name = "ELITE WEAPON CHEST";
cost = 7;
spawnLocationY = y - 16;
wep = [];
scrWeapons();
event_user(2);