{
  "$GMObject":"",
  "%Name":"InvertedGoldWorm",
  "eventList":[
    {"$GMEvent":"v1","%Name":"","collisionObjectId":null,"eventNum":0,"eventType":0,"isDnD":false,"name":"","resourceType":"GMEvent","resourceVersion":"2.0",},
  ],
  "managed":true,
  "name":"InvertedGoldWorm",
  "overriddenProperties":[],
  "parent":{
    "name":"Vulcano",
    "path":"folders/Objects/Enemies/Vulcano.yy",
  },
  "parentObjectId":{
    "name":"GoldWorm",
    "path":"objects/GoldWorm/GoldWorm.yy",
  },
  "persistent":false,
  "physicsAngularDamping":0.1,
  "physicsDensity":0.5,
  "physicsFriction":0.2,
  "physicsGroup":1,
  "physicsKinematic":false,
  "physicsLinearDamping":0.1,
  "physicsObject":false,
  "physicsRestitution":0.1,
  "physicsSensor":false,
  "physicsShape":1,
  "physicsShapePoints":[],
  "physicsStartAwake":true,
  "properties":[],
  "resourceType":"GMObject",
  "resourceVersion":"2.0",
  "solid":false,
  "spriteId":{
    "name":"sprInvertedGoldWormIdle",
    "path":"sprites/sprInvertedGoldWormIdle/sprInvertedGoldWormIdle.yy",
  },
  "spriteMaskId":{
    "name":"mskFireWorm",
    "path":"sprites/mskFireWorm/mskFireWorm.yy",
  },
  "visible":true,
}