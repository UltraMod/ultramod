/// @description zigzagoffset

// Inherit the parent event
event_inherited();
onlyHitPlayerTeam = false;
type = 3;
dmg = 5;
image_speed = 0.4;
alarm[3] = 240;
spawnDelay = 20;
offset = 1;
angle = 90;
time = 1;
tdir = 0.1;
alarm[4] = 1;
alarm[5] = 0;
desY = y;
desX = x;
desTime = 0;
aimDirection = 0;
destAcc = 0.012;
my_health = 10;//For the bullet spawner
pSpeed = 3;