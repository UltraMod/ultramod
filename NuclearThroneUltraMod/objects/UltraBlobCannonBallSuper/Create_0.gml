/// @description More Damage

// Inherit the parent event
event_inherited();
inkSprite = sprUltraInkBlob;
dmg += 22;
dmgAdd += 24;
blobSpeed += 5;
image_xscale += 0.25;
image_yscale = image_xscale;