/// @description Init
maxhealth = 12
mySize = 2;
// Inherit the parent event
event_inherited();

snd_hurt = sndHitMetal;
spr_idle = sprPopoFreakVenomizer
spr_hurt = sprPopoFreakVenomizerHurt
spr_dead = sprPopoFreakVenomizerDead

myPartner = noone;
alarm[0] = 0;
alarm[1] = 5;
alarm[2] = 2;
alarm[3] = 15;
lineScale = 1;
isHost = true;
speed = 0;
scrInitDrops(1);