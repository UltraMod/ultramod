/// @description Change bountyNumber

// Inherit the parent event
event_inherited();
name = "COMPLETE [BIG ASSASSIN] BOSS BOUNTY";
bountyNumber = secretChallengeBosses.bigAssassin;
item_index = bountyNumber;