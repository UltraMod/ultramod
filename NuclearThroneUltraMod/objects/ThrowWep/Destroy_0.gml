with Player
{
    if wep=0&&other.dontteleport==false
    {
	    wep=other.wep;
	    curse = other.curse
	    wepmod1=other.wepmod1;
	    wepmod2=other.wepmod2;
	    wepmod3=other.wepmod3;
	    wepmod4=other.wepmod4;
		isPermanent = other.isPermanent;
		visitedPortals = other.visitedPortals;
		hasBeenEaten = other.hasBeenEaten;
		if (ultra_got[54] == 1 && wep_type[wep] != 0)
		{
			//dont start empty handed
			if ( ammo[wep_type[wep]] < typ_ammo[wep_type[wep]]*2 )//if ammo below three times an ammo drop
			{ammo[wep_type[wep]]=typ_ammo[wep_type[wep]]*2;}
		}
		
	    //hold it properly now
	    scrWeaponHold();
		if other.reload > 0
			scrFlexibleElbowReload(wep);
	    exit;
    }
    else if bwep == 0 &&other.dontteleport==false
    {
	    bwep=other.wep;
	    bcurse = other.curse
	    bwepmod1=other.wepmod1;
	    bwepmod2=other.wepmod2;
	    bwepmod3=other.wepmod3;
	    bwepmod4=other.wepmod4;
		isPermanent = other.isPermanent;
		visitedPortals = other.visitedPortals;
		hasBeenEaten = other.hasBeenEaten;
		if (ultra_got[54] == 1 && wep_type[bwep] != 0)
		{
			//dont start empty handed
			if ( ammo[wep_type[bwep]] < typ_ammo[wep_type[bwep]]*2 )//if ammo below three times an ammo drop
			{ammo[wep_type[bwep]]=typ_ammo[wep_type[bwep]]*2;}
		}
		if other.reload > 0
			scrFlexibleElbowReload(bwep);
	    exit;
    }
}
if !collision_point(x,y,Floor,false,false)
{
	var n = instance_nearest(x,y,Floor);
	
	if n != noone
	{
		var o = 16;
		if n.object_index == FloorExplo
		{
			o = 8;	
		}
		x = n.x + o;
		y = n.y + o;
	}
}
if canHeavyHeart
{
	canHeavyHeart = false;
		with scrDropHeavyHeart()
		{
			alarm[0] = 1;	 
		}
}
with instance_create(x,y,WepPickupForOneWepOnly)
{
	ammo = other.ammo;
	oneweponly=false;
	image_angle=other.image_angle;
	scrWeapons()
	wep=other.wep
	name = wep_name[wep]
	type = wep_type[wep]
	curse = other.curse
	wepmod1=other.wepmod1;
	wepmod2=other.wepmod2;
	wepmod3=other.wepmod3;
	wepmod4=other.wepmod4;
	isPermanent = other.isPermanent;
	visitedPortals = other.visitedPortals;
	hasBeenEaten = other.hasBeenEaten;
	persistent = other.persistent;
	visible = other.visible;
	if persistent
		wasThrown = true;
	sprite_index = wep_sprt[wep]
}