if instance_exists(Player) && Player.my_health < Player.maxhealth
	scrDrop(80,0,false,0,3)//Only drops health
morphMe = 6;
event_inherited()
with instance_create(x,y,GhostCorpse)
{
	snd_play(sndGhostFlyDie,0.1,false,true,3,false,false,0.65,false,id);
	mySize = other.mySize;
	sprite_index = other.spr_hurt;
	my_health = other.my_health;
	spr_dead = other.spr_dead;
	corpseBoost = other.corpseBoost;
	right = other.right;
	holdSpeed = other.speed;
	holdDirection = other.direction;
	speed = other.speed;
	direction = other.direction;
	image_angle = direction;
}
repeat(3)
{
	with instance_create(x,y,GhostEffect)
	{
		motion_add(random(360),1 + random(2));
	}
}