/// @description ultramods
var um = GetPlayerUltramod();
if um == ultramods.bulletShotgun
{
	with instance_create(x,y,Bullet6)
	{
		scrCopyWeaponMod(other);
		direction = other.direction;
		image_angle = direction;
		speed = other.speed+14;
		team = other.team;
		alarm[11] = 0;
	}
	instance_destroy(id,false);
} 
else if um == ultramods.bulletPlasma
{
	instance_destroy(id,false);
	with Player
	{
		if skill_got[17] = 1
			snd_play_fire(sndPlasmaUpg)
		else
			snd_play_fire(sndPlasma)	
	}
	with instance_create(x,y,BouncerPlasmaBall)
	{
		dmg = other.dmg - 1;
		nomscale -= 0.2;
		scrCopyWeaponMod(other);
		direction = other.direction;
		image_angle = direction;
		speed = other.speed;
		team = other.team;
		alarm[11] = 0;
	}
} else if um == ultramods.laserBullet
{
	with Player
	{
		if Player.skill_got[17] = 1
			snd_play_fire(sndLaserUpg)
		else
			snd_play_fire(sndLaser)	
	}
	instance_destroy(id,false);
	with instance_create(x,y,Laser)
	{
		laserhit += 1;
		sprite_index=sprBouncingLaser;
		image_yscale -= 0.25
		alarm[2] = max(1,alarm[2] - 1);
		defaultPierce -= 16;
		scrCopyWeaponMod(other);
		isog = false;
		image_angle = other.direction;
		team = other.team
		event_perform(ev_alarm,0);
	}
} else if um == ultramods.boltBullet
{
	UberCont.ultramodSwap = false;
	instance_destroy(id,false);
	snd_play_fire(sndSplinterGun)
	with instance_create(x,y,BouncerBolt)
	{
		dmg = other.dmg
		image_speed = 0.6;
		scrCopyWeaponMod(other);
		direction = other.direction;
		image_angle = direction;
		speed = other.speed+14;
		team = other.team;
		alarm[11] = 0;
	}
	UberCont.ultramodSwap = true;
}
