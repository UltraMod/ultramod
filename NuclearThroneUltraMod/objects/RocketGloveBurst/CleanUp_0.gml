/// @description If you can temporarily move
if disablePlayerMove
{
	with Player
	{
		canMove = false;	
	}
}