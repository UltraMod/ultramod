{
  "$GMObject":"",
  "%Name":"TinyTemporarySnare",
  "eventList":[],
  "managed":true,
  "name":"TinyTemporarySnare",
  "overriddenProperties":[],
  "parent":{
    "name":"Ability",
    "path":"folders/Objects/Player/Ability.yy",
  },
  "parentObjectId":{
    "name":"TemporarySnare",
    "path":"objects/TemporarySnare/TemporarySnare.yy",
  },
  "persistent":false,
  "physicsAngularDamping":0.1,
  "physicsDensity":0.5,
  "physicsFriction":0.2,
  "physicsGroup":0,
  "physicsKinematic":false,
  "physicsLinearDamping":0.1,
  "physicsObject":false,
  "physicsRestitution":0.1,
  "physicsSensor":false,
  "physicsShape":0,
  "physicsShapePoints":[],
  "physicsStartAwake":true,
  "properties":[],
  "resourceType":"GMObject",
  "resourceVersion":"2.0",
  "solid":false,
  "spriteId":{
    "name":"sprTinyTangle",
    "path":"sprites/sprTinyTangle/sprTinyTangle.yy",
  },
  "spriteMaskId":{
    "name":"sprTinyTangle",
    "path":"sprites/sprTinyTangle/sprTinyTangle.yy",
  },
  "visible":true,
}