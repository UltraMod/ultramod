if other.team != team and other.my_health > 0 && image_yscale > 0.15 && alarm[3] < 1
{
	with other
	{
		if sprite_index != spr_hurt
		{
			if object_index == Player
			{
				if (alarm[3] > 0)//When immune dont deal damage and dont trigger blast armour
				{
					exit;
				}
				hitBy = sprEnemyLaserRepresent;
			}
			other.alarm[3] = 5;
			DealDamage(other.dmg)
			sprite_index = spr_hurt
			image_index = 0
			motion_add(other.image_angle,4)
			snd_play(snd_hurt, hurt_pitch_variation,true);
			instance_create(x,y,Smoke)
		}
	}
}

