/// @description Give a sprite

// Inherit the parent event
event_inherited();

spr_item = sprVoidChallengeIcon;
item_index = 5;
challenge = "DIE IN ONE HIT"
reward = "INCREASE MAX LEVEL";
cost = 0;