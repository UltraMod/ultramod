/// @description Give a sprite

// Inherit the parent event
event_inherited();

spr_item = sprVoidItemGoldChest;
name = "GOLDEN WEAPON CHEST";
cost = 3;
spawnLocationX -= 16;
spawnLocationY += 8;