event_inherited();
depth -= 3;
maxSpeed = 1.9;
alarm[10] = 0;
maxhealth = 200;
EnemyHealthAdjustments();
spr_idle = sprCrown2IdleEnemy
spr_walk = sprCrown2WalkEnemy
spr_hurt = sprCrown2HurtEnemy
spr_dead = sprCrown2DeadEnemy
fuse = 60;
alarm[2] = fuse*2;
alarm[3] = 40;