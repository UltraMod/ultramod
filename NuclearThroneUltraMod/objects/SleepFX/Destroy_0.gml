/// @description Stop snoozing
if owner != noone
	with owner {
		isSnooze = false;
	}