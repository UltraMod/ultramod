/// @description Additional hit

// Inherit the parent event
event_inherited();
dmg = 14;
hits += 1;
shouldBleed = false;
bleedAngle = 0;