/// @description Init

// Inherit the parent event
event_inherited();

dmg = 15;
owner = noone;
boltStick = RetractorFireAble;
alarm[0] = 2;
stickToWallTime = 60;
stickToWallTimeRandom = 0;