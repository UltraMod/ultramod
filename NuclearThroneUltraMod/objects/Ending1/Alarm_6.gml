/// @description Rogue walks off screen
alarm[7] = 110;
with Messenger
{
	targetY += 256;
	walk = true;
}