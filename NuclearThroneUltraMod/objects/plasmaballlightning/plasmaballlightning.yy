{
  "$GMObject":"",
  "%Name":"PlasmaBallLightning",
  "eventList":[
    {"$GMEvent":"v1","%Name":"","collisionObjectId":null,"eventNum":0,"eventType":1,"isDnD":false,"name":"","resourceType":"GMEvent","resourceVersion":"2.0",},
  ],
  "managed":true,
  "name":"PlasmaBallLightning",
  "overriddenProperties":[],
  "parent":{
    "name":"Projectiles",
    "path":"folders/Objects/Projectiles.yy",
  },
  "parentObjectId":{
    "name":"PlasmaBall",
    "path":"objects/PlasmaBall/PlasmaBall.yy",
  },
  "persistent":false,
  "physicsAngularDamping":0.1,
  "physicsDensity":0.5,
  "physicsFriction":0.2,
  "physicsGroup":0,
  "physicsKinematic":false,
  "physicsLinearDamping":0.1,
  "physicsObject":false,
  "physicsRestitution":0.1,
  "physicsSensor":false,
  "physicsShape":0,
  "physicsShapePoints":[],
  "physicsStartAwake":true,
  "properties":[],
  "resourceType":"GMObject",
  "resourceVersion":"2.0",
  "solid":false,
  "spriteId":{
    "name":"sprLightningPlasmaBall",
    "path":"sprites/sprLightningPlasmaBall/sprLightningPlasmaBall.yy",
  },
  "spriteMaskId":{
    "name":"mskPlasmaBall",
    "path":"sprites/mskPlasmaBall/mskPlasmaBall.yy",
  },
  "visible":true,
}