/// @description Init
depth = 12;
if instance_exists(ThroneIISpiral)
{
	depth = ThroneIISpiral.depth - 1;
}
image_alpha = 0.2;
image_speed = 0.4;
friction = 0.1;
right = 1;
alarm[0] = 30;
alarm[1] = 200;