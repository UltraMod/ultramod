/// @description Can I start?
if instance_exists(GenCont)
	alarm[0] = 1;
else
	image_speed = 0.4;