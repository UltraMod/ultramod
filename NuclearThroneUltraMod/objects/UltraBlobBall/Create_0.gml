/// @description More damage

// Inherit the parent event
event_inherited();
blobSpeed += 1.5;
dmg += 5;
dmgAdd += 3;
inkSprite = sprUltraInkBlobSplat;