/// @description Change some things

// Inherit the parent event
event_inherited();
actTime -= 1;
raddrop = 150
maxhealth = 420;
scrBossHealthBuff();
EnemyHealthAdjustments();
inverted = true;
spr_idle = sprInvertedFrogQueenIdle
spr_walk = sprInvertedFrogQueenWalk
spr_hurt = sprInvertedFrogQueenHurt
spr_dead = sprInvertedFrogQueenDead
spr_fire = sprInvertedFrogQueenFire;
spr_dying = sprInvertedFrogQueenDying;
