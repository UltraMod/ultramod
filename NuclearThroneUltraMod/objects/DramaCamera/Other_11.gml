/// @description Get sound
with BanditBoss
{
	other.bossIntroSound = sndBigMushroomBossIntro
}
with BigMachine
{
	other.bossIntroSound = sndBigMachineActivate
}
with BanditBoss
{
	other.bossIntroSound = sndBigBanditIntro
}
with InvertedBanditBoss
{
	other.bossIntroSound = sndBigBanditIntro
}
with BigVulture
{
	other.bossIntroSound = sndBigVultureCharge
}
with LilHunter
{
	other.bossIntroSound = sndLilHunterAppear
}
with InvertedLilHunter
{
	other.bossIntroSound = sndLilHunterAppear
}
with ScrapBoss
{
	other.bossIntroSound = sndBigDogIntro
}
with InvertedScrapBoss
{
	other.bossIntroSound = sndBigDogIntro
}
with HotDrake
{
	other.bossIntroSound = sndDragonIntro
}
with InvertedHotDrake
{
	other.bossIntroSound = sndDragonIntro
}

with AssassinBoss
{
	other.bossIntroSound = sndAssassinPretend
}

with InvertedAssassinBoss
{
	other.bossIntroSound = sndAssassinPretend
}
with BallMom
{
	other.bossIntroSound = sndBallMamaAppear;
}
with HyperCrystal
{
	other.bossIntroSound = sndHyperCrystalAppear;
}
with Technomancer
{
	other.bossIntroSound = sndTechnomancerActivate;
}
with CloudBoss
{
	other.bossIntroSound = sndVoidCreepEnd;
}