/// @description Make it Ultra

// Inherit the parent event
event_inherited();
maxhealth = 20;
my_health = 20;
dmg = 10;
scrWeaponModInit();
spr_idle = sprRamIdle;
spr_idle_b = sprRamIdle;
spr_walk = sprRamWalk;
spr_hurt = sprRamHurt;
spr_dead = sprRamDead;
maxSpeed = 12;
alarm[3] = 1;
alarm[2] = 2;
alarm[4] = 20;
alarm[1] = 30;
originalDirection = 0;
image_speed = 0.6;