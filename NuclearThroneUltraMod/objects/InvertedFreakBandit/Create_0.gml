raddrop = 9
maxhealth = 35
meleedamage = 0
mySize = 2

event_inherited()

spr_idle = sprInvertedFreakBanditIdle
spr_walk = sprInvertedFreakBanditWalk
spr_hurt = sprInvertedFreakBanditHurt
spr_dead = sprInvertedFreakBanditDead
//behavior
maxDetectRange = 310;
smackDetectionRange = 140;
smackRange = 11;
smackSpeed = 8;
actTime = 10;
pSpeed = 8.6;
acc = 1.6;
maxSpeed = 4.75;
shootTell = 1;
smackTell = 10;