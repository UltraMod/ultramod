/// @description Deflected sprite change

// Inherit the parent event
event_inherited();
if team == 2
{
	sprite_index = sprEnemyBullet1Deflected;
}
else
{
	sprite_index = sprEnemyBullet1;	
}