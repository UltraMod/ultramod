/// @description Give a sprite

// Inherit the parent event
event_inherited();

spr_item = sprVoidItemUnlockSecondaryStarting;
name = "UNLOCK SECONDARY STARTING WEAPON";
cost = 10;