/// @description End snooze
instance_create(x,y,SnoozeEnd);
with owner
{
	isSnooze = false;	
}