/// @description Spawn inverted projectiles instead

// Inherit the parent event
event_inherited();
projectileToSpawn = InvertedGuardianBulletSpawn;
hitSprite = sprInvertedGuardianBulletHit
stopSpeed = 0.3;