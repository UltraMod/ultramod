/// @description stay with player
event_inherited();
if instance_exists(Player)
{
	x = Player.x;
	y = Player.y;
}