raddrop = 8
maxhealth = 25
mySize = 2

event_inherited()
meleedamage = 2

spr_idle = sprJockIdle
spr_walk = sprJockWalk
spr_hurt = sprJockHurt
spr_dead = sprJockDead
spr_fire = sprJockFire

snd_hurt = sndJockHurt
snd_dead = sndJockDie

//behavior
walk = 0
gunangle = random(360)
alarm[1] = 30+random(90)
wkick = 0
ammo = 5

