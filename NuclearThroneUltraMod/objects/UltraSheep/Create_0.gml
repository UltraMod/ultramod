/// @description Make it Ultra

// Inherit the parent event
event_inherited();
raddrop += 2;
spr_idle = sprUltraSheepIdle;
spr_idle_b = sprUltraSheepIdleB;
spr_walk = sprUltraSheepWalk;
spr_hurt = sprUltraSheepHurt;
spr_dead = sprUltraSheepDead;
dmg = 1;