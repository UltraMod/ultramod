event_inherited();
friction = 0.1;
typ = 2 //0 = normal, 1 = deflectable, 2 = destructable
dmg = 50;
if UberCont.ultramodSwap
	event_user(0);
xprev = x;
yprev = y;
hitEntities = [];
knockback = 10;