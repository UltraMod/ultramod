/// @description Retarget
if target != noone && instance_exists(target)
{
	gunangle = point_direction(x,y,target.x,target.y);	
}