/// @description Dash attack finish!
forceAnimation = true;
forceAnimationSprite = spr_chrg_end;
forceAnimationIndex = 0;
forceAnimationEnd = sprite_get_number(forceAnimationSprite);