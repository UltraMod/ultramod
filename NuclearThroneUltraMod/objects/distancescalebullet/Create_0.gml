/// @description More damage as it exists longer

// Inherit the parent event
event_inherited();

typ = 1;
RecycleGlandType();
friction = 0;
dmg = 4;
imagexscale = 0.9;
imageyscale = 0.9;
alarm[2] = 4;
alarm[11] = 1;