raddrop = 60
maxhealth = 680
mySize = 4
scrBossHealthBuff();

event_inherited()
meleedamage = 3
canFly = true;
alarm[10] = 0;
spr_idle = sprChesireCatIdle
spr_walk = sprChesireCatIdle
spr_hurt = sprChesireCatHurt
spr_dead = sprChesireCatDead
spr_fire = sprChesireCatLaser
spr_fire_tell = sprChesireCatPrepLaser;


snd_dead = sndBigBanditMeleeHit;
snd_hurt = sndBigDogHit;
snd_melee = sndRhinoFreakMelee;
walk=0;
//behavior
alarm[2] = 120+random(60);
if instance_exists(Player)
{
	if Player.skill_got[29]	//Insomnia
	{
		alarm[2] += 30;
	}
}
visible=false;
//if instance_exists(Player)
//motion_add(point_direction(Player.x,Player.y,x,y),1)

mask_index=mskPickupThroughWall;

drama=false;

instance_create(x,y,ChesireCatTail);

ammo=7;

friction = 0.8;
myWazer = -1;
wazerDuration = 102;
wazerRotation = 0;
wazerDirection = 0;
wazerOffset = 120;
wazerAccelerate = 1;
wazerSpeed = 0;
wazerMaxSpeed = 10;
tellTime = 16;
actTime = 13;


loops = GetPlayerLoops();
startLoop = 1;
ca = 6 + min(6+(loops-startLoop)*2,14);
cang = random(360);
caspd = min(5,3+((loops-startLoop)*0.5));
if loops > 0
{
	actTime -= 2;
	wazerDuration -= 4;
}
if loops > 1
{
	actTime = 6;
	wazerDuration -= 4;
}
if loops >= startLoop
{
	alarm[6] = 160;
	if instance_exists(Player)
	{
		if Player.skill_got[29]	//Insomnia
		{
			alarm[6] += 100;
		}
	}
}
gunangle = 0;
scrAddDrops(2);
maxSpeed = 6;
firstTime = true;
forceAnimation = 0;
forceAnimationIndex = 0;
forceAnimationDuration = 0;