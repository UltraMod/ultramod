/// @description Reduce damage over time
alarm[1] = 2;
dmg = max(dmg - 0.5, 0.5);