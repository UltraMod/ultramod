/// @description Flame trail

// Inherit the parent event
event_inherited();

alarm[2] = choose(1,2);
trailColour = make_color_rgb(255,159,76);
