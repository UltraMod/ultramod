/// @description Change bountyNumber

// Inherit the parent event
event_inherited();
name = "COMPLETE [HYPER CRYSTAL] BOSS BOUNTY";
bountyNumber = secretChallengeBosses.hyperCrystal;
item_index = bountyNumber;