/// @description Scale up
image_xscale += 0.0025;
image_yscale += 0.0025;
rotation *= 0.999;
alarm[4] = 1;