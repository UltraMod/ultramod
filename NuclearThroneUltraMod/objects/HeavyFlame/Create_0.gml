/// @description More Damage
event_inherited();
dmg += 3;
alarm[4] = 1;
friction += 0.05;
rotation *= 1.25;
image_index = choose(0,1);