/// @description Give a sprite

// Inherit the parent event
event_inherited();

spr_item = sprVoidItemSaveSlotUp;
name = "INCREASE SAVED RUN SLOTS";
cost = 2 * UberCont.total_run_slots;