/// @description Init
image_speed = 0;
depth = - 100;
alarm[0] = 230 + random(90);
alarm[2] = 5;
image_xscale = choose(1,-1);
whatToSpawn = [];
spawnOffsetX = 0;
spawnOffsetY = 0;
floorA = sprFloor6;
floorB = sprFloor6B;
floorE = sprFloor6Explo;
spawnDelayed = 0;