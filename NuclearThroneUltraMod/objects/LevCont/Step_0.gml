__view_set( e__VW.XView, 0, 0 )
__view_set( e__VW.YView, 0, 0 )
with GameRender
{
	gamemodeDynamicHud = dynamicHudResetTime;
	mutationDynamicHud = dynamicHudResetTime;
}
if scrIsGamemode(28) || instance_exists(AllMutationsLeft)//ALL MUTATION CHOICES
{
	var spd = scrollSpeed;
	if mouse_wheel_down() {
		if scroll <= scrollWidth
		{
			with SkillIcon {
				x -= spd;
			}
			with UltraIcon {
				x -= spd;
			}
			scroll += spd;
		}
	}
	else if mouse_wheel_up(){
		if scroll > 0 
		{
			with SkillIcon {
				x += spd;
			}
			with UltraIcon {
				x += spd;
			}
			scroll -= spd;
		}
	}
}
if KeyCont.key_west[0] == 1
{
	snd_play_2d(sndHover);
	selectedIndex -= 1;
	var hasSelectedSkill = false;
	if instance_exists(UltraIcon) || instance_exists(SkillIcon)
	do {
		if instance_exists(UltraIcon)
		{
			if selectedIndex < 0
			{
				selectedIndex = instance_number(UltraIcon) + instance_number(SkillIcon) - 1;
			}
			with UltraIcon
			{
				if skillIndex == other.selectedIndex
				{
					selected = true;
					hasSelectedSkill = true;
				}
				else
					selected = false;
			}
		}
		else if instance_exists(SkillIcon)
		{
			if selectedIndex < 0
				selectedIndex = instance_number(SkillIcon) - 1;
		}
		with SkillIcon
		{
			if skillIndex == other.selectedIndex
			{
				hasSelectedSkill = true;
				selected = true;
				if x < camera_get_view_x(view_camera[0]) + 32 ||
				x > camera_get_view_x(view_camera[0]) + camera_get_view_width(view_camera[0]) - 32
				{
					with other
					{
						var disto = other.x - (camera_get_view_x(view_camera[0]) + camera_get_view_width(view_camera[0]) * 0.5)
						scroll += disto;
						with SkillIcon {
							x -= disto;
						}
						with UltraIcon {
							x -= disto;
						}
					}
				}
			}
			else
				selected = false;
		}
	} until (hasSelectedSkill)
	
}
else if KeyCont.key_east[0] == 1
{
	snd_play_2d(sndHover);
	selectedIndex += 1;
	var hasSelectedSkill = false;
	if instance_exists(UltraIcon) || instance_exists(SkillIcon)
	do {
		if instance_exists(UltraIcon)
		{
			if selectedIndex > instance_number(UltraIcon) + instance_number(SkillIcon)- 1
				selectedIndex = 0;
			with UltraIcon
			{
				if skillIndex == other.selectedIndex
				{
					selected = true;
					hasSelectedSkill = true;
				}
				else
					selected = false;
			}
		}
		else if instance_exists(SkillIcon)
		{
			if selectedIndex > instance_number(SkillIcon) - 1
				selectedIndex = 0;
		}
		with SkillIcon
		{
			if skillIndex == other.selectedIndex
			{
				hasSelectedSkill = true;
				selected = true;
				if x < camera_get_view_x(view_camera[0]) + 32 ||
				x > camera_get_view_x(view_camera[0]) + camera_get_view_width(view_camera[0]) - 32
				{
					with other
					{
						var disto = other.x - (camera_get_view_x(view_camera[0]) + camera_get_view_width(view_camera[0]) * 0.5)
						scroll += disto;
						with SkillIcon {
							x -= disto;
						}
						with UltraIcon {
							x -= disto;
						}
					}
				}
			}
			else
				selected = false;
		}
	}
	until (hasSelectedSkill)
}