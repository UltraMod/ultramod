event_inherited()
raddrop = 12
maxhealth = 7
EnemyHealthAdjustments();
spr_idle = sprite_index
spr_hurt = sprInvertedJungleAssassinHurt
spr_dead = sprInvertedJungleAssassinDead
spr_walk = sprite_index
snd_hurt = sndJungleAssassinHurt
snd_dead = sndJungleAssassinDead
wakeTime = 6;
wakeObject = InvertedJungleAssassin;
wakeSound = sndJungleAssassinWake;