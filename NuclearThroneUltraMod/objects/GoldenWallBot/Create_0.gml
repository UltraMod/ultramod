/// @description Init

// Inherit the parent event
event_inherited();
raddrop += 2;
maxhealth = 35;
maxSpeed -= 0.5;
actTime -= 50;
projectileSpeed = 3;
EnemyHealthAdjustments();
spr_idle = sprGoldenWallBotIdle;
spr_walk = sprGoldenWallBotWalk;
spr_hurt = sprGoldenWallBotHurt;
spr_dead = sprGoldenWallBotDead;
spr_fire = sprGoldenWallBotFire;
