if other.team != team and other.my_health > 0
{
	instance_destroy()
	with other
	{
		DealDamage(other.dmg,false,false)
		sprite_index = spr_hurt
		image_index = 0
		motion_add(other.direction,9);
		snd_play(snd_hurt, hurt_pitch_variation,true)
	}
	with instance_create(x,y,BulletHit)
	{
		if other.team == 2
			sprite_index = sprPopoSlugHitRogue;
		else
			sprite_index = sprPopoSlugHit
	}
	if other.object_index == Player
	{
		with other
		{
			hitBy = sprite_index;	
		}
	}
}