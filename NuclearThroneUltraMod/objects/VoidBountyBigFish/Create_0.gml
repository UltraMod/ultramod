/// @description Change bountyNumber

// Inherit the parent event
event_inherited();
name = "COMPLETE [BIG FISH] BOSS BOUNTY";
bountyNumber = secretChallengeBosses.bigFish;
item_index = bountyNumber;