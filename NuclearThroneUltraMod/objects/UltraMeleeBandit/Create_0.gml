event_inherited();
raddrop = 18
maxhealth = 14
EnemyHealthAdjustments();
spr_idle = sprUltraMeleeIdle
spr_walk = sprUltraMeleeWalk
spr_hurt = sprUltraMeleeHurt
spr_dead = sprUltraMeleeDead;
dodgeRange = 70;
dodgeAcc = 3;
tellTime += 1;
maxSpeed += 0.5;
dodgeCooldown += 2;
originalDodgeCooldown = dodgeCooldown;
dodgeTime = 5;