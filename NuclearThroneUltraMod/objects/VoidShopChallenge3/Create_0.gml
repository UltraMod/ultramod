/// @description Give a sprite

// Inherit the parent event
event_inherited();

spr_item = sprVoidChallengeIcon;
item_index = 3;
challenge = "CARRY ONLY THE RUSTY REVOLVER"
reward = "INCREASE MAX LEVEL";
cost = 0;