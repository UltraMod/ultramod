/// @description Disconnect
network_destroy(serverSocket);
if audio_is_playing(amb0c)
	audio_stop_sound(amb0c);
if UberCont.opt_sideart != sprite_get_number(sprSideArt) + 1
	scrDisableBigScreen();