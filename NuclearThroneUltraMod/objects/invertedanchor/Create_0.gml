/// @description Change sprite

// Inherit the parent event
event_inherited();

spr_idle = sprInvertedAnchor
spr_hurt = sprInvertedAnchorHurt
spr_dead = sprInvertedAnchorDead