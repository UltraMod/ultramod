/// @description 1 sec to destroy

// Inherit the parent event
event_inherited();
dmg = 10;
alarm[0] = 20
blinkTime = 4;
alarm[3] = 0;
ammo = 10;
angStep = 360/ammo;
angOffset = 180 + (angStep*0.5);